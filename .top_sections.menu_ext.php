<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;

$aMenuLinks = array();
$obCache = new CPHPCache();
if ($obCache->InitCache(86400, serialize(1), '/menu/top_sections')) {
    $aMenuLinks = $obCache->GetVars();
}
elseif ($obCache->StartDataCache()) {
    Bitrix\Main\Loader::includeModule('iblock');

    $aMenuLinks[] = array(
        'Для кондитеров',
        '/catalog/katalog_konditera/',
        array(),
        array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1)
    );

    $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), array('IBLOCK_ID' => IBLOCK_CATALOG, 'SECTION_ID' => SECTION_CONFECTIONER, 'GLOBAL_ACTIVE' => 'Y', 'ACTIVE' => 'Y', 'UF_TOP' => true), false, array('ID', 'CODE', 'NAME', 'DEPTH_LEVEL', 'LEFT_MARGIN', 'RIGHT_MARGIN', 'SECTION_PAGE_URL'));
    $rsSections->SetUrlTemplates();
    while ($section = $rsSections->GetNext()) {
        $aMenuLinks[] = array(
            $section['NAME'],
            $section['SECTION_PAGE_URL'],
            array(),
            array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2)
        );
    }

    $aMenuLinks[] = array(
        'Для мыловаров',
        '/catalog/katalog_mylovara/',
        array(),
        array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1)
    );

    $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), array('IBLOCK_ID' => IBLOCK_CATALOG, 'SECTION_ID' => SECTION_SOAPBOILER, 'GLOBAL_ACTIVE' => 'Y', 'ACTIVE' => 'Y', 'UF_TOP' => true), false, array('ID', 'CODE', 'NAME', 'DEPTH_LEVEL', 'LEFT_MARGIN', 'RIGHT_MARGIN', 'SECTION_PAGE_URL'));
    $rsSections->SetUrlTemplates();
    while ($section = $rsSections->GetNext()) {
        $aMenuLinks[] = array(
            $section['NAME'],
            $section['SECTION_PAGE_URL'],
            array(),
            array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2)
        );
    }

    $obCache->EndDataCache($aMenuLinks);
}