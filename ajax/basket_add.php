<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?

    $productId = $_POST["id"];
    if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog") ) {
     $quantity = 0;
     $dbBasket = CSaleBasket::GetList(
        array(
           "NAME" => "ASC",
           "ID" => "ASC"
        ),
        array(
          "PRODUCT_ID" => $productId,
          "FUSER_ID" => CSaleBasket::GetBasketUserID(),
          "LID" => SITE_ID,
          "ORDER_ID" => "NULL"
        ),
        array("ID","QUANTITY")
     );
     if($arItems = $dbBasket->Fetch()){
         $quantity = 1;
     } else if($_POST["incr"]){
         $quantity = 2;
     } else if($_POST["adding"]){
         $quantity = 1;
     }
        if (IntVal($productId)>0) {
            Add2BasketByProductID(
             $productId,
             $quantity,
             array()
          );
        }

     $dbBasketItems = CSaleBasket::GetList(
        array(
           "NAME" => "ASC",
           "ID" => "ASC"
        ),
        array(
          "PRODUCT_ID" => $productId,
          "FUSER_ID" => CSaleBasket::GetBasketUserID(),
          "LID" => SITE_ID,
          "ORDER_ID" => "NULL"
        ),
        array("ID","QUANTITY")
     );

     if($arItems = $dbBasketItems->Fetch()){
        $quantity = $arItems["QUANTITY"];
      }
    }
   echo $quantity;


?>
