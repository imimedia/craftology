<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?  
     if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")){
    $productId = $_POST["id"];
     $basket_id = 0;
     $delete_id = 0;
     $dbBasketItems = CSaleBasket::GetList(
                array(
                   "NAME" => "ASC",
                   "ID" => "ASC"
                ),
                array(
                  "PRODUCT_ID" => $productId,
                  "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                  "LID" => SITE_ID,
                  "ORDER_ID" => "NULL"
                ),
                array("ID","QUANTITY")
             );
      if($arItems = $dbBasketItems->Fetch()){
      // Изменим количество товара в записи $arItems корзины на -1 штуки
        if($arItems["QUANTITY"] > 1){
            $arFields = array(
               "QUANTITY" => $arItems["QUANTITY"] - 1,
            );
            $quantity = $arItems["QUANTITY"] - 1;
            $basket_id = CSaleBasket::Update($arItems["ID"], $arFields);
        } else {
            $delete_id = CSaleBasket::Delete($arItems["ID"]); 
        }
      }
      if($basket_id){
          echo $quantity;
      } else if($delete_id) {
          echo 0;
      }; 
     }
?>
