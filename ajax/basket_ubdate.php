<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?  
     if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")){
     $productId = $_POST["id"];
     $quantity = $_POST["quantity"];
     $basket_id = 0;
     $delete_id = 0;
     $dbBasketItems = CSaleBasket::GetList(
        array(
           "NAME" => "ASC",
           "ID" => "ASC"
        ),
        array(
          "PRODUCT_ID" => $productId,
          "FUSER_ID" => CSaleBasket::GetBasketUserID(),
          "LID" => SITE_ID,
          "ORDER_ID" => "NULL"
        ),
        array("ID","QUANTITY")
     );
      if($arItems = $dbBasketItems->Fetch()){
        if($quantity <= 0){
            $delete_id = CSaleBasket::Delete($arItems["ID"]); 
        } else {
            $arFields = array(
               "QUANTITY" => $quantity,
            );
            $basket_id = CSaleBasket::Update($arItems["ID"], $arFields);
        }
      // ������� ���������� ������ � ������ $arItems ������� �� -1 �����

      } else {
        $basket_id = Add2BasketByProductID(
             $productId,
             $quantity,
             array()
          );
      }
     $basketItems = CSaleBasket::GetList(
        array(
           "NAME" => "ASC",
           "ID" => "ASC"
        ),
        array(
          "PRODUCT_ID" => $productId,
          "FUSER_ID" => CSaleBasket::GetBasketUserID(),
          "LID" => SITE_ID,
          "ORDER_ID" => "NULL"
        ),
        array("ID","QUANTITY")
     );
      if($arItems = $basketItems->Fetch()){
        $quantity = $arItems["QUANTITY"];
      }
      if($basket_id){
          echo $quantity;
      } else if($delete_id) {
          echo 0;
      }; 
     }
?>
