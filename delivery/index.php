<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");
?>

    <script>
        (function($) {
            $(function() {

                $('ul.tabs__caption').each(function(i) {
                    var storage = '0';
//                    var storage = localStorage.getItem('tab' + i);

                    if (storage) {
                        $(this).find('li').removeClass('active').eq(storage).addClass('active')
                            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq(storage).addClass('active');
                    }
                });

                $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
                    $(this)
                        .addClass('active').siblings().removeClass('active')
                        .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
                    var ulIndex = $('ul.tabs__caption').index($(this).parents('ul.tabs__caption'));
                    localStorage.removeItem('tab' + ulIndex);
                    localStorage.setItem('tab' + ulIndex, $(this).index());
                });

            });
        })(jQuery);
    </script>

    <section class="b-section text-black">
        <div class="container">
            <div class="section-header text-center">
                <h1>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include",
                        "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/d_title.php"
                        ), false);
                    ?>
                </h1>
                <p class="section-legend">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include",
                        "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/d_desc.php"
                        ), false);
                    ?>
                </p>
            </div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.line",
                "d_step",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "CACHE_GROUPS" => "N",
                    "CACHE_TIME" => "86400",
                    "CACHE_TYPE" => "A",
                    "DETAIL_URL" => "/news/#SECTION_CODE#/#ELEMENT_CODE#/",
                    "FIELD_CODE" => array(
                        0 => "NAME",
                        1 => "PREVIEW_TEXT",
                        2 => "DETAIL_TEXT",
                        3 => "DETAIL_PICTURE",
                        4 => "PREVIEW_PICTURE",
                        5 => "",
                    ),
                    "IBLOCKS" => array(
                        0 => "16",
                    ),
                    "IBLOCK_TYPE" => "content",
                    "NEWS_COUNT" => "5",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "TITLE" => "Новости магазина",
                    "COMPONENT_TEMPLATE" => "d_step",
                    "HTML_CONTAINER" => "TOP",
                    "COMPOSITE_FRAME_MODE" => "A",
                    "COMPOSITE_FRAME_TYPE" => "AUTO"
                ),
                false
            );?>

        </div>
    </section>

    <section class="b-section text-black delivery-options">
        <div class="container">
            <div class="section-header text-center">
                <h2>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include",
                        "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/dos_title.php"
                        ), false);
                    ?>
                </h2>
                <p class="section-legend">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include",
                        "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/dos_desc.php"
                        ), false);
                    ?>
                </p>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:news.line", "var_d", Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                "CACHE_GROUPS" => "N",	// Учитывать права доступа
                "CACHE_TIME" => "86400",	// Время кеширования (сек.)
                "CACHE_TYPE" => "A",	// Тип кеширования
                "DETAIL_URL" => "/news/#SECTION_CODE#/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
                "FIELD_CODE" => array(	// Поля
                    0 => "NAME",
                    1 => "PREVIEW_TEXT",
                    2 => "PREVIEW_PICTURE",
                    3 => "DETAIL_TEXT",
                    4 => "DETAIL_PICTURE",
                    5 => "",
                ),
                "IBLOCKS" => array(	// Код информационного блока
                    0 => "13",
                ),
                "IBLOCK_TYPE" => "content",	// Тип информационного блока
                "NEWS_COUNT" => "5",	// Количество новостей на странице
                "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                "TITLE" => "Новости магазина",	// Заголовок
                "COMPONENT_TEMPLATE" => "d_step",
                "HTML_CONTAINER" => "TOP",	// HTML контейнер
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO"
            ),
                false
            );?>


        </div>

    </section>

    <section class="b-section bg-gray text-black delivery-options">
        <div class="container">
            <div class="section-header text-center">
                <h2>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include",
                        "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/p_title.php"
                        ), false);
                    ?>
                </h2>
                <p class="section-legend">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include",
                        "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/p_desc.php"
                        ), false);
                    ?>
                </p>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:news.line", "pauy", Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                "CACHE_GROUPS" => "N",	// Учитывать права доступа
                "CACHE_TIME" => "86400",	// Время кеширования (сек.)
                "CACHE_TYPE" => "A",	// Тип кеширования
                "DETAIL_URL" => "/news/#SECTION_CODE#/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
                "FIELD_CODE" => array(	// Поля
                    0 => "NAME",
                    1 => "PREVIEW_TEXT",
                    2 => "PREVIEW_PICTURE",
                    3 => "DETAIL_TEXT",
                    4 => "DETAIL_PICTURE",
                    5 => "",
                ),
                "IBLOCKS" => array(	// Код информационного блока
                    0 => "14",
                ),
                "IBLOCK_TYPE" => "content",	// Тип информационного блока
                "NEWS_COUNT" => "5",	// Количество новостей на странице
                "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                "TITLE" => "Новости магазина",	// Заголовок
                "COMPONENT_TEMPLATE" => "var_d",
                "HTML_CONTAINER" => "TOP",	// HTML контейнер
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO"
            ),
                false
            );?>

            <hr>

            <div class="row">
                <div class="col-md-1">
                    <div class="icon-wallet__wrap">
                        <img src="/images/icon-wallet.png" alt="">
                    </div>
                </div>
                <div class="col-md-11">
                    <h5 class="text-red"><?$APPLICATION->IncludeComponent("bitrix:main.include",
                            "", array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/ss_title.php"
                            ), false);
                        ?></h5>
                    <div class="row">
                        <div class="col-md-6">
                            <p><?$APPLICATION->IncludeComponent("bitrix:main.include",
                                    "", array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => SITE_DIR."include/ss_desc.php"
                                    ), false);
                                ?></p>
                        </div>
                        <div class="col-md-6">
                            <p><?$APPLICATION->IncludeComponent("bitrix:main.include",
                                    "", array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => SITE_DIR."include/ss_desc1.php"
                                    ), false);
                                ?></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

    <section class="b-section">
        <div class="container">
            <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"all_moskow", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "86400",
		"CACHE_TYPE" => "A",
		"DETAIL_URL" => "/news/#SECTION_CODE#/#ELEMENT_CODE#/",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "PREVIEW_PICTURE",
			3 => "DETAIL_TEXT",
			4 => "DETAIL_PICTURE",
			5 => "",
		),
		"IBLOCKS" => array(
			0 => "15",
		),
		"IBLOCK_TYPE" => "content",
		"NEWS_COUNT" => "5",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"TITLE" => "Новости магазина",
		"COMPONENT_TEMPLATE" => "all_moskow",
		"HTML_CONTAINER" => "TOP",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"IBLOCK_ID" => "15",
		"FILTER_NAME" => "",
		"PROPERTY_CODE" => array(
			0 => "TYPE_DELIV",
			1 => "DELIVERY",
			2 => "HOW",
			3 => "RED",
			4 => "",
		),
		"CHECK_DATES" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_FILTER" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"STRICT_SECTION_CHECK" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>


        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>