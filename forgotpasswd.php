<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Форма восстановления пароля");
?>
<div class="dialog-content">
	<div class="dialog_auth">
		
		<p>Нажмите на кнопку "Забыли пароль" для восстановления пароля. После заполнения формы на указанную почту придет новый пароль.</p>
		<div class="open-other-dialogs">
		    <div class="justify-items">
		        <span class="justify-item"></span>
		        <a href="" class="open-forgot justify-item btn js-dialog" data-dialog="forgotpasswd">Забыли пароль</a>
		        
		        <span class="justify-item"></span>
		    </div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>