<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if (!empty($_REQUEST['action'])) {
    $APPLICATION->IncludeComponent(
        "wsg:catalog.actions",
        "",
        Array(
            "ACTION_VARIABLE" => "action",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "IBLOCK_ID" => "3",
            "IBLOCK_TYPE" => "1c_catalog",
            "PARTIAL_PRODUCT_PROPERTIES" => "Y",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "USE_PRODUCT_QUANTITY" => "Y"
        )
    );
}