<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$result = array();

Bitrix\Main\Loader::includeModule('iblock');
Bitrix\Main\Loader::includeModule('sale');
Bitrix\Main\Loader::includeModule('catalog');
Bitrix\Main\Loader::includeModule('highloadblock');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$cmps = isset($_REQUEST['cmps']) && is_array($_REQUEST['cmps']) ? $_REQUEST['cmps'] : null;

$cmp = 'favorite';
if (!$cmps || in_array($cmp, $cmps)) {
    if ($USER->IsAuthorized()) {
        $user = CUser::GetList($by, $order, array('ID' => $USER->GetID()), array('SELECT' => array('UF_FAVORITE_PRODUCTS')))->Fetch();
        $result[$cmp] = $user['UF_FAVORITE_PRODUCTS'];
    } else {
        $result[$cmp] = $APPLICATION->get_cookie('FAVORITE_PRODUCTS') ? array_filter(array_map(function ($v) {
            return intval($v) > 0 ? intval($v) : null;
        }, explode(',', $APPLICATION->get_cookie('FAVORITE_PRODUCTS')))) : array();
    }
}

$cmp = 'cart';
if (!$cmps || in_array('cart', $cmps)) {
    $result[$cmp] = array();
    $result[$cmp]['items'] = array();
    $result[$cmp]['total_price'] = 0;
    $result[$cmp]['total_price_format'] = 0;


    $fUserID = intval(CSaleBasket::GetBasketUserID(True));
    $basketFilter = ($fUserID > 0) ? array("FUSER_ID" => $fUserID, "LID" => SITE_ID, "ORDER_ID" => "NULL", "DELAY"=>"N") : null;
    if ($basketFilter) {
        $cnt = 0;
        $rsBasket = CSaleBasket::GetList(
            array(),
            $basketFilter,
            false,
            false,
            array(
                "QUANTITY", "PRICE", "CURRENCY", "DISCOUNT_PRICE", "WEIGHT", "VAT_RATE",
                "ID", "SET_PARENT_ID", "PRODUCT_ID", "CATALOG_XML_ID", "PRODUCT_XML_ID",
                "PRODUCT_PROVIDER_CLASS", "TYPE", "BASE_PRICE"
            )
        );
        while ($arItem = $rsBasket->Fetch()) {
            if (CSaleBasketHelper::isSetItem($arItem))
                continue;

            if (!isset($arItem['BASE_PRICE']) || (float)$arItem['BASE_PRICE'] <= 0)
                $arItem['BASE_PRICE'] = $arItem['PRICE'] + $arItem['DISCOUNT_PRICE'];

            $arItem['PRICE_FORMAT'] = CCurrencyLang::CurrencyFormat($arItem['PRICE'], $arItem['CURRENCY']);

            $product = CIBlockElement::GetList(array(), array('ID' => $arItem['PRODUCT_ID']), false, false, array('ID', 'NAME', 'DETAIL_PICTURE', 'DETAIL_PAGE_URL'))->GetNext();
            $arItem['PRODUCT'] = array(
                'ID' => $product['ID'],
                'NAME' => $product['NAME'],
                'DETAIL_PAGE_URL' => $product['DETAIL_PAGE_URL'],
                'IMAGE_SM' => array('src' => '')
            );
            if ($product['DETAIL_PICTURE']) {
                $arItem['PRODUCT']['IMAGE_SM'] = CFile::ResizeImageGet($product['DETAIL_PICTURE'], array("width" => 60, "height" => 60), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            }

            $cnt += $arItem['QUANTITY'];

            $result[$cmp]['items'][] = $arItem;
        }

        if ($result[$cmp]['items']) {
            $totalPrice = 0;
            $totalWeight = 0;

            foreach ($result[$cmp]['items'] as $arItem) {
                $totalPrice += $arItem["PRICE"] * $arItem["QUANTITY"];
                $totalWeight += $arItem["WEIGHT"] * $arItem["QUANTITY"];
            }

            $arOrder = array(
                'SITE_ID' => SITE_ID,
                'ORDER_PRICE' => $totalPrice,
                'ORDER_WEIGHT' => $totalWeight,
                'BASKET_ITEMS' => $arBasketItems
            );

            if (is_object($GLOBALS["USER"])) {
                $arOrder['USER_ID'] = $GLOBALS["USER"]->GetID();
                $arErrors = array();
                CSaleDiscount::DoProcessOrder($arOrder, array(), $arErrors);
            }

            $result[$cmp]['quantity'] = $cnt;
            $result[$cmp]['total_price'] = $arOrder['ORDER_PRICE'];
            $result[$cmp]['total_price_format'] = CCurrencyLang::CurrencyFormat($arOrder['ORDER_PRICE'], 'RUB');
        }
    }
}

$cmp = 'rating';
$dataRatingIds = isset($_REQUEST['rating']['id']) && is_array($_REQUEST['rating']['id']) ? $_REQUEST['rating']['id'] : [];
$dataRatingIds = array_filter(array_map(function($v) { return intval($v) > 0 ? intval($v) : null; }, $dataRatingIds));
if (!empty($dataRatingIds) && (!$cmps || in_array($cmp, $cmps))) {
    $result[$cmp] = array();
    $db = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG, 'SECTION_GLOBAL_ACTIVE' => 'Y', 'ACTIVE' => 'Y', '=ID' => $dataRatingIds), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_rating'));
    while ($element = $db->GetNext()) {
        $result[$cmp]['rating'][$element['ID']] = round($element['PROPERTY_RATING_VALUE']);
    }
    $result[$cmp]['user_rating'] = isset($_SESSION['CATALOG_RATING']) && is_array($_SESSION['CATALOG_RATING']) ? $_SESSION['CATALOG_RATING'] : array();
}

$cmp = 'reviews';
$dataReviewsProductId = isset($_REQUEST[$cmp]['productId']) && intval($_REQUEST[$cmp]['productId']) > 0 ? intval($_REQUEST[$cmp]['productId']) : 0;
if ($dataReviewsProductId) {
    $element = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG, 'SECTION_GLOBAL_ACTIVE' => 'Y', 'ACTIVE' => 'Y', 'ID' => $dataReviewsProductId), false, false, array('ID'))->Fetch();
    if ($element) {
        $hlblock = HL\HighloadBlockTable::getById(1)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $class = $entity->getDataClass();

        $name = $USER->IsAuthorized() ? $USER->GetFullName() : '';

        $db = $class::getList(array(
            'select' => array('*'),
            'filter' => array('UF_PRODUCT_ID' => $dataReviewsProductId),
            'order' => array('ID' => 'DESC')
        ));
        while ($row = $db->fetch()) {
            $result[$cmp][$dataReviewsProductId][] = array(
                'name' => $name ?: $row['UF_NAME'],
                'date' => FormatDate('j F Y', MakeTimeStamp($row['UF_DATE'], CSite::GetDateFormat())),
                'text' => $row['UF_TEXT']
            );
        }
    }
}

$cmp = 'product_subscribe';
$dataProductId = isset($_REQUEST[$cmp]['productId']) && is_array($_REQUEST[$cmp]['productId']) ? $_REQUEST[$cmp]['productId'] : array();
$dataProductId = array_filter(array_map(function($v) { return intval($v) > 0 ? intval($v) : null; }, $dataProductId));
if ($dataProductId) {
    $hlblock = HL\HighloadBlockTable::getById(HLBLOCK_PRODUCT_SUBSCRIBE)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $class = $entity->getDataClass();

    $result[$cmp] = array();
    $db = $class::getList(array(
        'select' => array('*'),
        'filter' => array('UF_PRODUCT_ID' => $dataProductId, 'UF_USER_ID' => $USER->GetID()),
        'order' => array('ID' => 'DESC')
    ));
    while ($row = $db->fetch()) {
        $result[$cmp][] = $row['UF_PRODUCT_ID'];
    }
}

echo json_encode($result);