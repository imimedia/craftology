<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$result = array();

Bitrix\Main\Loader::includeModule('iblock');

$action = isset($_REQUEST['action']) && trim($_REQUEST['action']) ? trim($_REQUEST['action']) : '';
$id = isset($_REQUEST['id']) && intval($_REQUEST['id']) ? intval($_REQUEST['id']) : '';


if ($id) {
    if ($USER->IsAuthorized()) {
        if ($action == 'add') {
            $element = CIBlockElement::GetByID($id)->Fetch();

            if ($element) {
                $user = CUser::GetList($by, $order, array('ID' => $USER->GetID()), array('SELECT' => array('UF_FAVORITE_PRODUCTS')))->Fetch();
                if (!is_array($user['UF_FAVORITE_PRODUCTS']))
                    $user['UF_FAVORITE_PRODUCTS'] = array();

                if (!in_array($id, $user['UF_FAVORITE_PRODUCTS'])) {
                    $userObj = new CUser;
                    if ($userObj->Update($USER->GetID(), array('UF_FAVORITE_PRODUCTS' => array_merge($user['UF_FAVORITE_PRODUCTS'], (Array)$id))))
                        $result['SUCCESS'] = 'OK';
                    else
                        $result['SUCCESS'] = 'ERROR';
                }
            } else {
                $result = array('SUCCESS' => 'ERROR', 'MESSAGE' => 'Товар не найден');
            }
        } else if ($action == 'remove') {
            $user = CUser::GetList($by, $order, array('ID' => $USER->GetID()), array('SELECT' => array('UF_FAVORITE_PRODUCTS')))->Fetch();
            if (!is_array($user['UF_FAVORITE_PRODUCTS']))
                $user['UF_FAVORITE_PRODUCTS'] = array();

            if (in_array($id, $user['UF_FAVORITE_PRODUCTS'])) {
                unset($user['UF_FAVORITE_PRODUCTS'][array_search($id, $user['UF_FAVORITE_PRODUCTS'])]);
                $userObj = new CUser;
                if ($userObj->Update($USER->GetID(), array('UF_FAVORITE_PRODUCTS' => $user['UF_FAVORITE_PRODUCTS'])))
                    $result['SUCCESS'] = 'OK';
                else
                    $result['SUCCESS'] = 'ERROR';
            }
        }
    }
    else {
        if ($action == 'add') {
           $_SESSION["FAV"][]=$id;
            $result['SUCCESS'] = 'OK';
        }
        else if ($action == 'remove') {
            unset($_SESSION["FAV"][array_search($id, $_SESSION["FAV"])]);
            $result['SUCCESS'] = 'OK';
        }
        $APPLICATION->set_cookie('FAVORITE_PRODUCTS',implode(",",$_SESSION["FAV"]));
    }
}

echo json_encode($result);