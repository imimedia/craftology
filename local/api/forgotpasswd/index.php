<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->IncludeComponent(
    "customized:system.auth.forgotpasswd",
    "dialog",
    Array(
        "IS_AJAX" => isset($_REQUEST["ajax"]) && $_REQUEST["ajax"] === "y" ? "Y" : "N",
        "FORM_ID" => 'forgotpasswd_form',
        "SENT_FORM_ID" => isset($_REQUEST["FORM_ID"]) ? $_REQUEST["FORM_ID"] : ''
    )
);