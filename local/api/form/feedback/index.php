<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "feedback",
    Array(
        "COMPONENT_TEMPLATE" => "feedback",
        "WEB_FORM_ID" => "1",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "Y",
        "SEF_MODE" => "N",
        "VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "86400",
        "LIST_URL" => "?ajax=y",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "?ajax=y",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "SUCCESS_MESSAGE" => "В ближайшее время мы с вами свяжемся.",

        "THEME" => isset($_REQUEST['theme']) ? trim(urldecode($_REQUEST['theme'])) : ''
    )
);
?>