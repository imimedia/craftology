<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

Bitrix\Main\Loader::includeModule('iblock');
Bitrix\Main\Loader::includeModule('catalog');
Bitrix\Main\Loader::includeModule('highloadblock');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$hlblock = HL\HighloadBlockTable::getById(HLBLOCK_PRODUCT_SUBSCRIBE)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$class = $entity->getDataClass();

$result = array();

$action = isset($_REQUEST['action']) && trim($_REQUEST['action']) ? trim($_REQUEST['action']) : 0;

if ($action == 'add') {
    $productId = isset($_REQUEST['product_id']) && intval($_REQUEST['product_id']) > 0 ? intval($_REQUEST['product_id']) : 0;
    $email = !$USER->IsAuthorized() && isset($_REQUEST['email']) && trim($_REQUEST['email']) && filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL) ? trim($_REQUEST['email']) : '';
    $element = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG, 'SECTION_GLOBAL_ACTIVE' => 'Y', 'ACTIVE' => 'Y', 'ID' => $productId), false, false, array('ID'))->Fetch();

    if ($element && ($USER->IsAuthorized() || $email)) {
        $params = array(
            'select' => array('ID'),
            'filter' => array('UF_PRODUCT_ID' => $productId, 'UF_ACTIVE' => true)
        );
        if ($USER->IsAuthorized()) {
            $params['filter']['UF_USER_ID'] = $USER->GetID();
        }
        else {
            $params['filter']['UF_EMAIL'] = $email;
        }
        $db = $class::getList($params);
        if (!$db->fetch()) {
            $price = CCatalogProduct::GetOptimalPrice($element['ID']);

            $res = $class::add(array(
                'UF_PRODUCT_ID' => $productId,
                'UF_USER_ID' => $USER->GetID(),
                'UF_EMAIL' => $email,
                'UF_DATE' => date($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), time()),
                'UF_ACTIVE' => true,
                'UF_CATALOG_GROUP_' . CATALOG_PRICE => $price['PRICE']['PRICE']
            ));
            if ($res->isSuccess()) {
                $result['SUCCESS'] = 'OK';
            } else {
                $result['SUCCESS'] = 'ERROR';
                $result['MESSAGE'] = 'Простите произошла ошибка. Попробуйте позже.';
            }
        }
    }
}
else if ($action == 'delete') {
    $productId = isset($_REQUEST['product_id']) && intval($_REQUEST['product_id']) > 0 ? intval($_REQUEST['product_id']) : 0;
    if ($productId) {
        $fields = array(
            'select' => array('ID'),
            'filter' => array('UF_PRODUCT_ID' => $productId, 'UF_ACTIVE' => true)
        );
        if ($USER->IsAuthorized()) {
            $fields['filter']['UF_USER_ID'] = $USER->GetID();
        }
        else {
            $fields['filter']['UF_EMAIL'] = $email;
        }
        $db = $class::getList($fields);
        while ($row = $db->fetch()) {
            $res = $class::delete($row['ID']);
            if ($res->isSuccess()) {
                $result['SUCCESS'] = 'OK';
            } else {
                $result['SUCCESS'] = 'ERROR';
                $result['MESSAGE'] = 'Простите произошла ошибка. Попробуйте позже.';
            }
        }
    }
}

echo json_encode($result);