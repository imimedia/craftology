<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

Bitrix\Main\Loader::includeModule('iblock');

$result = array();

$action = isset($_REQUEST['action']) && trim($_REQUEST['action']) ? trim($_REQUEST['action']) : 0;
$id = isset($_REQUEST['id']) && intval($_REQUEST['id']) > 0 ? intval($_REQUEST['id']) : 0;
$vote = isset($_REQUEST['vote']) && intval($_REQUEST['vote']) > 0 ? intval($_REQUEST['vote']) : 0;

if ($id && (!array_key_exists('CATALOG_RATING', $_SESSION) || !in_array($id, $_SESSION['CATALOG_RATING']))) {
    if ($action == 'set' && $vote > 0 && $vote < 6) {
        $element = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG, 'ID' => $id, 'SECTION_GLOBAL_ACTIVE' => 'Y', 'ACTIVE' => 'Y'), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_vote_count', 'PROPERTY_vote_sum', 'PROPERTY_rating'))->GetNext();
        if ($element) {
            $voteCount = intval($element['PROPERTY_vote_count_VALUE']) + 1;
            $voteSum = intval($element['PROPERTY_vote_sum_VALUE']) + $vote;
            $rating = $voteSum / $voteCount;
            CIBlockElement::SetPropertyValuesEx($id, IBLOCK_CATALOG, array(
                'vote_count' => $voteCount,
                'vote_sum' => $voteSum,
                'rating' => $rating
            ));

            $result['rating'] = $rating;

            $_SESSION['CATALOG_RATING'][] = $id;
        }
    }
}

echo json_encode($result);