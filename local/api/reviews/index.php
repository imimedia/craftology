<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

Bitrix\Main\Loader::includeModule('iblock');
Bitrix\Main\Loader::includeModule('highloadblock');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$result = array();

$action = isset($_REQUEST['action']) && trim($_REQUEST['action']) ? trim($_REQUEST['action']) : 0;

if ($action == 'add' && isset($_REQUEST['review']) && is_array($_REQUEST['review'])) {
    $productId = isset($_REQUEST['review']['product_id']) && intval($_REQUEST['review']['product_id']) > 0 ? intval($_REQUEST['review']['product_id']) : 0;
    $name = !$USER->IsAuthorized() && isset($_REQUEST['review']['name']) && trim($_REQUEST['review']['name']) ? trim(htmlspecialcharsEx($_REQUEST['review']['name'])) : '';
    $text = isset($_REQUEST['review']['text']) && trim($_REQUEST['review']['text']) ? trim(htmlspecialcharsEx($_REQUEST['review']['text'])) : '';
    $element = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG, 'SECTION_GLOBAL_ACTIVE' => 'Y', 'ACTIVE' => 'Y', 'ID' => $datareviewProductId), false, false, array('ID'))->Fetch();

    if ($element && ($USER->IsAuthorized() || $name) && $text) {
        $hlblock = HL\HighloadBlockTable::getById(1)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $class = $entity->getDataClass();

        $res = $class::add(array(
            'UF_PRODUCT_ID' => $productId,
            'UF_DATE' => date($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), time()),
            'UF_TEXT' => $text,
            'UF_NAME' => $name,
            'UF_USER_ID' => $USER->GetID()
        ));
        if ($res->isSuccess()) {
            $result['SUCCESS'] = 'OK';
        }
        else {
            $result['SUCCESS'] = 'ERROR';
            $result['MESSAGE'] = 'Простите произошла ошибка. Попробуйте позже.';
        }
    }
}

echo json_encode($result);