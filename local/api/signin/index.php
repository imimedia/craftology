<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->IncludeComponent(
    "customized:system.auth.form",
    "dialog",
    Array(
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => "",
        "REGISTER_URL" => "",
        "SHOW_ERRORS" => "Y",
        "IS_AJAX" => isset($_REQUEST["ajax"]) && $_REQUEST["ajax"] === "y" ? "Y" : "N",
        "FORM_ID" => 'auth_form',
        "SENT_FORM_ID" => isset($_REQUEST["FORM_ID"]) ? $_REQUEST["FORM_ID"] : ''
    )
);