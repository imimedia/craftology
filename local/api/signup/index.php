<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->IncludeComponent("customized:main.register", "dialog", Array(
    "AUTH" => "Y",	// Автоматически авторизовать пользователей
    "REQUIRED_FIELDS" => array(	// Поля, обязательные для заполнения
        0 => "EMAIL",
        1 => "NAME",
        2 => "PERSONAL_PHONE"
    ),
    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
    "SHOW_FIELDS" => array(	// Поля, которые показывать в форме
        0 => "EMAIL",
        1 => "NAME",
        2 => "PERSONAL_PHONE"
    ),
    "SUCCESS_PAGE" => "",	// Страница окончания регистрации
    "USER_PROPERTY" => "",	// Показывать доп. свойства
    "USER_PROPERTY_NAME" => "",	// Название блока пользовательских свойств
    "USE_BACKURL" => "N",	// Отправлять пользователя по обратной ссылке, если она есть
    "IS_AJAX" => isset($_REQUEST["ajax"]) && $_REQUEST["ajax"] === "y" ? "Y" : "N",
    "FORM_ID" => 'register_form',
    "SENT_FORM_ID" => isset($_REQUEST["FORM_ID"]) ? $_REQUEST["FORM_ID"] : ''
),
    false
);