<?php
use \Bitrix\Main\Localization\Loc as Loc;
use \Bitrix\Main\Loader as Loader;
use \Bitrix\Main\SystemException as SystemException;
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CBitrixComponent::includeComponentClass("bitrix:catalog.viewed.products");

Loc::loadMessages(__FILE__);

class CSaleRecommendedProductsComponent extends CCatalogViewedProductsComponent
{
	/**
	 * @override
	 */
	public function onIncludeComponentLang()
	{
		parent::onIncludeComponentLang();
		$this->includeComponentLang(basename(__FILE__));
	}
	/**
	 * @param $params
	 * @override
	 * @return array
	 */
	public function onPrepareComponentParams($params)
	{
		if(Loader::includeModule("catalog"))
		{
			$catalogIterator = CCatalog::getList(array("IBLOCK_ID" => "ASC"));
			while($row = $catalogIterator->fetch())
			{
				$params['SHOW_PRODUCTS_' . $row['IBLOCK_ID']] = true;
			}
		}

		$params = parent::onPrepareComponentParams($params);

		if(!isset($params["CACHE_TIME"]))
			$params["CACHE_TIME"] = 86400;

		$params["DETAIL_URL"] = trim($params["DETAIL_URL"]);

		$params["MIN_BUYES"] = IntVal($params["MIN_BUYES"]);
		if($params["MIN_BUYES"] <= 0)
			$params["MIN_BUYES"] = 2;

		if (isset($params['ID'])) {
			if (!is_array($params['ID']))
				$params['ID'] = (Array)$params['ID'];
			$params['ID'] = array_filter(array_map(function($v) { return intval($v) > 0 ? intval($v) : null; }, $params['ID']));
		}
		else
			$params['ID'] = array();

		if(isset($params['CODE']))
			$params['CODE'] = trim($params['CODE']);
		else
			$params['CODE'] = '';

		if(isset($params['IBLOCK_ID']))
			$params['IBLOCK_ID'] = (int)$params['IBLOCK_ID'];
		else
			$params['IBLOCK_ID'] = -1;

		$IDs = array();
		foreach ($params['ID'] as $ID) {
			$IDs = array_merge($IDs, CIBlockFindTools::getElementID(
				$ID,
				$params["CODE"],
				false,
				false,
				array(
					"IBLOCK_ID" => $params["IBLOCK_ID"],
					"IBLOCK_LID" => SITE_ID,
					"IBLOCK_ACTIVE" => "Y",
					"ACTIVE_DATE" => "Y",
					//"ACTIVE" => "Y",
					"CHECK_PERMISSIONS" => "Y",
				)
			));
		}

		if(!$params["ID"])
		{
			$this->errors[] = Loc::getMessage("SRP_PRODUCT_ID_REQUIRED");
		}

		return $params;
	}


	/**
	 * @override
	 * @return bool
	 */
	protected function extractDataFromCache()
	{
		if($this->arParams['CACHE_TYPE'] == 'N')
			return false;

		global $USER;
		return !($this->StartResultCache(false, $USER->GetGroups()));
	}

	protected function putDataToCache()
	{
		$this->endResultCache();
	}

	protected function abortDataCache()
	{
		$this->AbortResultCache();
	}

	/**
	 * @override
	 * @param $productIds
	 */
	protected function resortItemsByIds($productIds)
	{
		parent::resortItemsByIds($productIds);

		$newItems = array();
		foreach ($this->items as $item)
		{
			if(!isset($newItems[$item['ID']]))
			{
				$newItems[$item['ID']] = $item;
				unset($newItems[$item['ID']]['IS_CONVERTED']);
			}
		}
		$this->items = $newItems;
	}

	/**
	 * @override
	 * @return integer[]
	 */
	protected function getProductIds()
	{
		$productIds = array();
		$productIterator = $this->GetProductList(
			$this->arParams["ID"],
			$this->arParams["MIN_BUYES"],
			$this->arParams["PAGE_ELEMENT_COUNT"],
			true
		);

		if($productIterator)
		{
			global $CACHE_MANAGER;
			$CACHE_MANAGER->RegisterTag("sale_product_buy");
			while($product = $productIterator->fetch())
			{
				$productIds[] = $product['PARENT_PRODUCT_ID'];
			}
		}
		return $productIds;
	}

	function GetProductList($IDs, $minCNT, $limit, $getParentOnly = false)
	{
		global $DB;

		if (!$IDs)
			return false;
		$limit = (int)$limit;
		if ($limit < 0)
			$limit = 0;
		$minCNT = (int)$minCNT;
		if ($minCNT < 0)
			$minCNT = 0;

		$getParentOnly = ($getParentOnly === true);

		$elementInclude = array();
		$elementExclude = array();

		foreach ($IDs as $ID) {
			$elementInclude[] = $ID;

			if (Loader::includeModule('catalog')) {
				$intIBlockID = (int)CIBlockElement::GetIBlockByID($ID);
				if ($intIBlockID == 0)
					return false;

				$skuInfo = CCatalogSKU::GetInfoByProductIBlock($intIBlockID);
				if (!empty($skuInfo)) {
					$itemsIterator = CIBlockElement::GetList(
						array(),
						array('IBLOCK_ID' => $skuInfo['IBLOCK_ID'], 'PROPERTY_' . $skuInfo['SKU_PROPERTY_ID'] => $ID),
						false,
						false,
						array('ID', 'IBLOCK_ID', 'PROPERTY_' . $skuInfo['SKU_PROPERTY_ID'])
					);
					while ($item = $itemsIterator->Fetch()) {
						$item['ID'] = (int)$item['ID'];
						$elementInclude[] = $item['ID'];
						$elementExclude[] = $item['ID'];
					}
				}
			}
		}

		if ($getParentOnly)
		{
			$strSql = "select PARENT_PRODUCT_ID from b_sale_product2product where PRODUCT_ID IN (".implode(',', $elementInclude).")";
			if (!empty($elementExclude))
				$strSql .= " and PARENT_PRODUCT_ID not in (".implode(',', $elementExclude).")";
			if ($minCNT > 0)
				$strSql .= " and CNT >= ".$minCNT;
			$strSql .= ' group by PARENT_PRODUCT_ID';
			if ($limit > 0)
				$strSql .= " limit ".$limit;
		}
		else
		{
			$strSql = "select * from b_sale_product2product where PRODUCT_ID in (".implode(',', $elementInclude).")";
			if (!empty($elementExclude))
				$strSql .= " and PARENT_PRODUCT_ID not in (".implode(',', $elementExclude).")";
			if ($minCNT > 0)
				$strSql .= " and CNT >= ".$minCNT;
			$strSql .= " order by CNT desc, PRODUCT_ID asc";
			if ($limit > 0)
				$strSql .= " limit ".$limit;
		}
		return $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
	}


	/**
	 * @override
	 * @throws Exception
	 */
	protected function checkModules()
	{
		parent::checkModules();
		if(!$this->isSale)
			throw new SystemException(Loc::getMessage("CVP_SALE_MODULE_NOT_INSTALLED"));
	}

	/**
	 * @override
	 */
	protected function formatResult()
	{
		parent::formatResult();
		if(empty($this->arResult['ITEMS']))
			$this->arResult = array();
		else
			$this->arResult['ID'] = is_array($this->items) ? array_keys($this->items) : array();
	}


}
?>