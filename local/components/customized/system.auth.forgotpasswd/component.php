<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arResult['JSON'] = array();

$arParamsToDelete = array(
	"login",
	"logout",
	"register",
	"forgot_password",
	"change_password",
	"confirm_registration",
	"confirm_code",
	"confirm_user_id",
);
$arResult["AUTH_URL"] = defined("AUTH_404") ? POST_FORM_ACTION_URI : $APPLICATION->GetCurPageParam("forgot_password=yes",$arParamsToDelete);
$arResult["AUTH_AUTH_URL"] = $APPLICATION->GetCurPageParam("login=yes",$arParamsToDelete);

if (isset($_REQUEST['forgotpasswd']) && $_REQUEST['forgotpasswd']) {
	if (isset($_REQUEST['EMAIL']) && filter_var(trim($_REQUEST['EMAIL']), FILTER_VALIDATE_EMAIL)) {
		$email = trim($_REQUEST['EMAIL']);
		$u = CUser::GetList(($by="id"), ($order="desc"), array('EMAIL' => $email), array('SELECT'=>array('ID', 'EMAIL', 'NAME', 'LAST_NAME')))->Fetch();
		if ($u) {
			$pass = randString();

			$nu = new CUser;
			if ($nu->Update($u['ID'], array('PASSWORD'=>$pass))) {
				$arResult['JSON'] = array('success' => true, 'message' => 'На e-mail <strong>'.$email.'</strong> выслано письмо с новым паролем.');
				CEvent::SendImmediate('FORGOT_PASSWD', 's1', array('EMAIL'=>$u['EMAIL'], 'PASSWORD'=>$pass, 'NAME'=>$u['NAME'].' '.$u['LAST_NAME']), 'N');
			}
			else {
				$arResult['JSON'] = array('success' => false, 'error' => $nu->LAST_ERROR);
			}
		}
		else {
			$arResult['JSON'] = array('success' => false, 'error' => 'Такого Email не существует.');
		}
	}
	else {
		$arResult['JSON'] = array('success' => false, 'error' => 'Неправильно набран Email.');
	}
}

$this->IncludeComponentTemplate();