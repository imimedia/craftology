<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
		"NAME" => 'Действия с товарами',
		"DESCRIPTION" => 'Действия с товарами',
		"ICON" => "",
		"CACHE_PATH" => "Y",
		"SORT" => 10,
		"PATH" => array(
				"ID" => "wsg",
				"NAME" => "White Sage",
				"CHILD" => array(
						"ID" => "wsg_catalog",
						"NAME" => 'Каталог',
						"SORT" => 10,
				)
		),
);
?>