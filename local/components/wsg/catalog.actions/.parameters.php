<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
Loader::includeMOdule('iblock');
Loader::includeMOdule('catalog');

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock=array();
$rsIBlock = CIBlock::GetList(array("sort" => "asc"), array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch()) {
    $arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arComponentParameters = array(
    "GROUPS" => array(
        "BASKET" => array(
            "NAME" => GetMessage("SBP_BASKET"),
        ),
    ),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CVP_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CVP_IBLOCK"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
        ),
        "ACTION_VARIABLE" => array(
            "PARENT" => "BASKET",
            "NAME" => GetMessage("SBP_ACTION_VARIABLE"),
            "TYPE" => "STRING",
            "DEFAULT" => "action",
        ),
        "PRODUCT_ID_VARIABLE" => array(
            "PARENT" => "BASKET",
            "NAME" => GetMessage("SBP_PRODUCT_ID_VARIABLE"),
            "TYPE" => "STRING",
            "DEFAULT" => "id",
        ),
        "USE_PRODUCT_QUANTITY" => array(
            "PARENT" => "BASKET",
            "NAME" => GetMessage("SBP_USE_PRODUCT_QUANTITY"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "REFRESH" => "Y",
        ),
        "PRODUCT_QUANTITY_VARIABLE" => array(
            "PARENT" => "BASKET",
            "NAME" => GetMessage("SBP_PRODUCT_QUANTITY_VARIABLE"),
            "TYPE" => "STRING",
            "DEFAULT" => "quantity",
        ),
        "PRODUCT_PROPS_VARIABLE" => array(
            "PARENT" => "BASKET",
            "NAME" => GetMessage("SBP_PRODUCT_PROPS_VARIABLE"),
            "TYPE" => "STRING",
            "DEFAULT" => "prop",
            "HIDDEN" => (isset($arCurrentValues['ADD_PROPERTIES_TO_BASKET']) && $arCurrentValues['ADD_PROPERTIES_TO_BASKET'] == 'N' ? 'Y' : 'N')
        ),
        "ADD_PROPERTIES_TO_BASKET" => array(
            "PARENT" => "BASKET",
            "NAME" => GetMessage("SBP_ADD_PROPERTIES_TO_BASKET"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
            "REFRESH" => "Y"
        ),
        "PRODUCT_PROPS_VARIABLE" => array(
            "PARENT" => "BASKET",
            "NAME" => GetMessage("SBP_PRODUCT_PROPS_VARIABLE"),
            "TYPE" => "STRING",
            "DEFAULT" => "prop",
            "HIDDEN" => (isset($arCurrentValues['ADD_PROPERTIES_TO_BASKET']) && $arCurrentValues['ADD_PROPERTIES_TO_BASKET'] == 'N' ? 'Y' : 'N')
        ),

        "PARTIAL_PRODUCT_PROPERTIES" => array(
            "PARENT" => "BASKET",
            "NAME" => GetMessage("SBP_PARTIAL_PRODUCT_PROPERTIES"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "HIDDEN" => (isset($arCurrentValues['ADD_PROPERTIES_TO_BASKET']) && $arCurrentValues['ADD_PROPERTIES_TO_BASKET'] == 'N' ? 'Y' : 'N')
        ),
    )
);

$iblockMap = array();
$iblockIterator = CIBlock::GetList(array("SORT" => "ASC"), array("ACTIVE" => "Y"));
while ($iblock = $iblockIterator->fetch()) {
    $iblockMap[$iblock['ID']] = $iblock;
}

$catalogs = array();
$productsCatalogs = array();
$skuCatalogs = array();
$catalogIterator = CCatalog::GetList(
    array("IBLOCK_ID" => "ASC"),
    array("@IBLOCK_ID" => array_keys($iblockMap)),
    false,
    false,
    array('IBLOCK_ID', 'PRODUCT_IBLOCK_ID', 'SKU_PROPERTY_ID')
);
while($catalog = $catalogIterator->fetch()) {
    $isOffersCatalog = (int)$catalog['PRODUCT_IBLOCK_ID'] > 0;
    if($isOffersCatalog)
    {
        $skuCatalogs[$catalog['PRODUCT_IBLOCK_ID']] = $catalog;
        if (!isset($productsCatalogs[$catalog['PRODUCT_IBLOCK_ID']]))
            $productsCatalogs[$catalog['PRODUCT_IBLOCK_ID']] = $catalog;
    }
    else {
        $productsCatalogs[$catalog['IBLOCK_ID']] = $catalog;
    }
}

foreach($productsCatalogs as $catalog) {
    $catalog['VISIBLE'] = isset($arCurrentValues['SHOW_PRODUCTS_' . $catalog['IBLOCK_ID']]) &&	$arCurrentValues['SHOW_PRODUCTS_' . $catalog['IBLOCK_ID']] == "Y";
    $catalogs[] = $catalog;

    if (isset($skuCatalogs[$catalog['IBLOCK_ID']])) {
        $skuCatalogs[$catalog['IBLOCK_ID']]['VISIBLE'] = $catalog['VISIBLE'];
        $catalogs[] = $skuCatalogs[$catalog['IBLOCK_ID']];
    }
}

$defaultListValues = array("-" => getMessage("SBP_UNDEFINED"));
foreach ($catalogs as $catalog) {
    $iblock = $iblockMap[$catalog['IBLOCK_ID']];
    if ((int)$catalog['SKU_PROPERTY_ID'] > 0) // sku
        $groupName = sprintf(getMessage("SBP_GROUP_OFFERS_CATALOG_PARAMS"), $iblock['NAME']);
    else
        $groupName = sprintf(getMessage("SBP_GROUP_PRODUCT_CATALOG_PARAMS"), $iblock['NAME']);

    // 3. Cart properties
    $arComponentParameters["PARAMETERS"]['CART_PROPERTIES_' . $iblock['ID']] = array(
        "PARENT" => $groupId,
        "NAME" => GetMessage("SBP_PROPERTY_ADD_TO_BASKET"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $treeProperties,
        "ADDITIONAL_VALUES" => "Y",
        "HIDDEN" => ((isset($arCurrentValues['ADD_PROPERTIES_TO_BASKET']) && $arCurrentValues['ADD_PROPERTIES_TO_BASKET'] == 'N') ||
        !$catalog['VISIBLE'] ? 'Y' : 'N')
    );
}