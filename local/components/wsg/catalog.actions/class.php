<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
    Bitrix\Iblock,
    Bitrix\Catalog,
    Bitrix\Main\Localization\Loc,
    \Bitrix\Main\SystemException;

CBitrixComponent::includeComponentClass("bitrix:catalog.viewed.products");

class WSGCatalogActionsComponent extends CCatalogViewedProductsComponent {

    const ACTION_BUY = 'BUY';
    const ACTION_ADD_TO_BASKET = 'ADD2BASKET';
    const ACTION_SUBSCRIBE = 'SUBSCRIBE_PRODUCT';
    const ACTION_ADD_TO_COMPARE_LIST = 'ADD_TO_COMPARE_LIST';

    protected function doActionsList()
    {
        $action = isset($_REQUEST[$this->arParams['ACTION_VARIABLE']]) ? trim($_REQUEST[$this->arParams['ACTION_VARIABLE']]) : null;
        if (!$action)
            return;
        $productId = isset($_REQUEST[$this->arParams['PRODUCT_ID_VARIABLE']])  ? (Int) $_REQUEST[$this->arParams['PRODUCT_ID_VARIABLE']] : 0;
        if ($productId < 0)
            throw new Main\SystemException(Loc::getMessage('CVP_ACTION_PRODUCT_ID_REQUIRED'));

        if ($action == self::ACTION_BUY) {
            parent::addProductToBasket($productId, parent::getProductQuantityFromRequest(), parent::getProductPropertiesFromRequest());
        }
        else if ($action == self::ACTION_ADD_TO_BASKET) {
            parent::addProductToBasket($productId, parent::getProductQuantityFromRequest(), parent::getProductPropertiesFromRequest());
        }
        else if ($action == self::ACTION_SUBSCRIBE) {
            parent::addProductToBasket($productId, parent::getProductQuantityFromRequest(), parent::getProductPropertiesFromRequest());
        }
    }

    function onPrepareComponentParams($params) {
        $params = parent::onPrepareComponentParams($params);
        return $params;
    }

    function toJson($data) {
        return true ? json_encode($data) : CUtil::PhpToJSObject($data);
    }

    public function executeComponent() {
        global $APPLICATION;

        try {
            parent::checkModules();
            $this->processRequest();

            $APPLICATION->restartBuffer();
            echo $this->toJson(array('STATUS' => 'OK', 'MESSAGE' => ''));
            die();
        }
        catch (Main\SystemException $e) {
            $APPLICATION->restartBuffer();
            echo $this->toJson(array('STATUS' => 'ERROR', 'MESSAGE' => $e->getMessage()));
            die();
        }
    }
}