<?
$MESS["CVP_TYPE"] = "Information block type";
$MESS["CVP_IBLOCK"] = "Information block";
$MESS["SBP_BASKET"] = "Items in shopping cart";
$MESS["SBP_ACTION_VARIABLE"] = "Variable containing the action";
$MESS["SBP_PRODUCT_ID_VARIABLE"] = "Variable containing the purchasable product ID";
$MESS["SBP_USE_PRODUCT_QUANTITY"] = "Show product quantity input field";
$MESS["SBP_PRODUCT_QUANTITY_VARIABLE"] = "Variable containing the product quantity";
$MESS["SBP_PRODUCT_PROPS_VARIABLE"] = "Variable containing the product properties";
$MESS["SBP_ADD_PROPERTIES_TO_BASKET"] = "Pass item and SKU properties to shipping cart";
$MESS["SBP_PARTIAL_PRODUCT_PROPERTIES"] = "Allow partially empty properties";
$MESS["SBP_UNDEFINED"] = "Not specified";
$MESS["SBP_GROUP_OFFERS_CATALOG_PARAMS"] = "Display parameters for the \"%s\" SKU";
$MESS["SBP_GROUP_PRODUCT_CATALOG_PARAMS"] = "Display parameters for items of the \"%s\" catalog";
$MESS["SBP_PROPERTY_ADD_TO_BASKET"] = "Properties for adding to cart";