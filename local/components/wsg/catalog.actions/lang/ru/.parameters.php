<?
$MESS["CVP_TYPE"] = "Тип инфоблока";
$MESS["CVP_IBLOCK"] = "Инфоблок";
$MESS["SBP_BASKET"] = "Добавление в корзину";
$MESS["SBP_ACTION_VARIABLE"] = "Название переменной, в которой передается действие";
$MESS["SBP_PRODUCT_ID_VARIABLE"] = "Название переменной, в которой передается код товара для покупки";
$MESS["SBP_USE_PRODUCT_QUANTITY"] = "Разрешить указание количества товара";
$MESS["SBP_PRODUCT_QUANTITY_VARIABLE"] = "Название переменной, в которой передается количество товара";
$MESS["SBP_PRODUCT_PROPS_VARIABLE"] = "Название переменной, в которой передаются характеристики товара";
$MESS["SBP_ADD_PROPERTIES_TO_BASKET"] = "Добавлять в корзину свойства товаров и предложений";
$MESS["SBP_PARTIAL_PRODUCT_PROPERTIES"] = "Разрешить частично заполненные свойства";
$MESS["SBP_UNDEFINED"] = "(не выбрано)";
$MESS["SBP_GROUP_OFFERS_CATALOG_PARAMS"] = "Параметры вывода торговых предложений \"%s\"";
$MESS["SBP_GROUP_PRODUCT_CATALOG_PARAMS"] = "Параметры вывода товаров из каталога \"%s\"";
$MESS["SBP_PROPERTY_ADD_TO_BASKET"] = "Свойства для добавления в корзину";