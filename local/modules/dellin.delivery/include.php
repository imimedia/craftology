<?php

$module_id = 'dellin.delivery';

CModule::AddAutoloadClasses(
    $module_id,
    array(
        "DellinAPI" => "classes/general/DellinAPI.php",
        "DellinDelivery" => "classes/general/DellinDelivery.php"
    )
);
