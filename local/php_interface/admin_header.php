<?php 
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

if(CModule::IncludeModule("sale") && CModule::IncludeModule("iblock")) {
    $orderID = $_REQUEST['ID'];
    $arOrder = CSaleOrder::GetByID($orderID);
    //echo '<pre>', print_r($arOrder), '</pre>' ;
    //-- получаем телефоны и адрес
    $order_props = CSaleOrderPropsValue::GetOrderProps($orderID);
    $phone="";
    $index = "";
    $country_name = "";
    $city_name = "";
    $address = "";
    $email = "";
    $number_order=$arOrder['ACCOUNT_NUMBER'];

    while ($arProps = $order_props->Fetch())
    {
        //    echo '<pre>', print_r($arProps), '</pre>' ;
        if ($arProps["CODE"] == "PHONE")
        {
            $phone = htmlspecialchars($arProps["VALUE"]);
        }
        if ($arProps["CODE"] == "EMAIL")
        {
            $email = htmlspecialchars($arProps["VALUE"]);
        }
        if ($arProps["CODE"] == "LOCATION")
        {
            $arLocs = CSaleLocation::GetByID($arProps["VALUE"]);
            $country_name =  $arLocs["COUNTRY_NAME_ORIG"];
            $city_name = $arLocs["CITY_NAME_ORIG"];
        }

        if ($arProps["CODE"] == "ZIP")
        {
            $index = $arProps["VALUE"];
        }

        if ($arProps["CODE"] == "ADDRESS")
        {
            $address = $arProps["VALUE"];
        }
        if ($arProps["CODE"] == "FIO")
        {
            $fio = $arProps["VALUE"];
        }
    }





}


?>

<script>

/*
    var Dialog = new BX.CDialog({
        title: "Отправить кнопку оплаты",
//            head: 'Текст до формы',
        content: '<form method="POST" style="overflow:hidden;" action="button.php" id="mailform"><textarea name="comment" id="comment" style="height: 300px; width: 600px;"></textarea><br><br><input id="sostav" type="checkbox" checked><label for="sostav">Добавить кнопку онлайн оплаты</label></form>',
        icon: 'head-block',
        resizable: true,
        draggable: true,
        height: '400',
        width: '600',
        buttons: ['<input type="submit" class="btnSubmit" value="Отправить" />', BX.CDialog.btnCancel, BX.CDialog.btnClose]
    });
    */

    var Dialog = new BX.CDialog({
        title: "Отправить письмо",
//            head: 'Текст до формы',
        content: '<form method="POST" style="overflow:hidden;" action="button.php" id="mailform"><textarea name="comment" id="comment" style="height: 300px; width: 600px;"> Неоплаченные заказы бронируются на 3 рабочих дня.\n\n  Заказ будет отправлен в течении 1-2 рабочих дней после поступления оплаты.\n Номер для отслеживания посылки будет выслан на Вашу электронную почту.\n\n 1. Перевод на карту Сбербанка \n\n Номер карты: 4817 7600 9767 4380\nФИО: ИП Волков Артем Анатольевич \n\n 2. Оплатить банковской картой на нашем сайте: https://craftology.ru/forma-oplaty-zakaza/ \n\n 3. Яндекс-кошелек: 410011026885710 </textarea><br><br><input id="sostav" value="on" type="checkbox" checked><label for="sostav">Добавить кнопку онлайн оплаты</label></form>',
        icon: 'head-block',
        resizable: true,
        draggable: true,
        height: '400',
        width: '600',
        buttons: ['<input type="submit" class="btnSubmit" value="Отправить" />', BX.CDialog.btnCancel, BX.CDialog.btnClose]
    });

    function mail_ms(id) {
        Dialog.Show();
        var text=' - Сумма Вашего заказа: '+ $('#order_info_price_basket_discount').html() +'\n\n - Стоимость доставки: '+ $('#order_info_delivery_price').html() +'\n\n - Способ доставки: '+ $('#SHIPMENT_SECTION_SHORT_1').find('td.adm-detail-content-cell-l:eq(1)').find('span').html()+'\n\n\nСумма к оплате '+ $('#order_info_buyer_price').html() +'\n\n\nСрок резерва товаров 3 рабочих дня.\n\nЗаказ будет отправлен в течении 1-2 рабочих дней после поступления оплаты.\n\nОтправка из города Пермь.\n\nНомер для отслеживания посылки будет выслан на Вашу электронную почту.\n\n\nЧтобы оплатить заказ Нажмите на кнопку (обработкой платежа занимается  Яндекс.Касса )'

        //var text='- Сумма Вашего заказа: '+ $('#order_info_price_basket_discount').html() +'\n\n - Стоимость доставки: '+ $('#order_info_delivery_price').html() +'\n\n - Способ доставки: '+ $('#SHIPMENT_SECTION_SHORT_1').find('td.adm-detail-content-cell-l:eq(1)').find('span').html()+'\n\n\n Сумма к оплате '+ $('#order_info_buyer_price').html() +'\n\n\nНеоплаченные заказы бронируются на 3 рабочих дня.\n\n Обязательно сообщайте о произведенной оплате на электронную почту.\n\n Заказ будет отправлен в течении 1-2 рабочих дней после поступления оплаты.\n\n Срок доставки:\n Отправка из города Пермь.\n\n Номер для отслеживания посылки будет выслан на Вашу электронную почту.\n\n 1. Перевод на карту Сбербанка \n\n Номер карты: 4276 4900 2230 2287\nФИО: ИП Волков Артем Анатольевич \n Срок действия карты до 02/20 \n\n 2. Оплатить банковской картой на нашем сайте: https://craftology.ru/forma-oplaty-zakaza/ \n\n 3. Оплатить c помощью Яндекс-касса: http://dev.craftology.ru/order/payment/?ORDER_ID=<?=$_REQUEST["ID"]?>  '
//        alert(text);
        $('#comment').val(text);
        $('.btnSubmit').on('click', function () {
            var comment = $('#comment').val();
            if($('#sostav').is(":checked")) {var sostav = 'on';} else {var sostav = 'off';}
//            alert(mailform);
            $.ajax({
                type: 'GET',
                url: '/bitrix/admin/button.php',
                data: { id: id, comment: comment, sostav: sostav },
                success: function(data) {
//                    $('#mail_ms').html(data);
                    Dialog.Close();
                    location='<?=$_SERVER["REQUEST_URI"]?>';

                },
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        })

    }
    
    function mail_ms_old(id) {
        Dialog.Show();
        var text='- Сумма Вашего заказа: '+ $('#order_info_price_basket_discount').html() +'\n\n - Стоимость доставки: '+ $('#order_info_delivery_price').html() +'\n\n - Способ доставки: '+ $('#SHIPMENT_SECTION_SHORT_1').find('td.adm-detail-content-cell-l:eq(1)').find('span').html()+'\n\n\n Сумма к оплате '+ $('#order_info_buyer_price').html() +'\n\n\nНеоплаченные заказы бронируются на 3 рабочих дня.\n\n Обязательно сообщайте о произведенной оплате на электронную почту.\n\n Заказ будет отправлен в течении 1-2 рабочих дней после поступления оплаты.\n\n Срок доставки:\n Отправка из города Пермь.\n\n Номер для отслеживания посылки будет выслан на Вашу электронную почту.\n\n 1. Перевод на карту Сбербанка \n\n Номер карты: 4817 7600 9767 4380\nФИО: ИП Волков Артем Анатольевич \n\n 2. Оплатить банковской картой на нашем сайте: https://craftology.ru/forma-oplaty-zakaza/ \n\n 3. Яндекс-кошелек: 410011026885710  '
//        alert(text);
        $('#comment').val(text);
        $('.btnSubmit').on('click', function () {
            var comment = $('#comment').val();
            if($('#sostav').is(":checked")) {var sostav = 'on';} else {var sostav = 'off';}
//            alert(mailform);
            $.ajax({
                type: 'GET',
                url: '/bitrix/admin/button.php',
                data: { id: id, comment: comment, sostav: sostav },
                success: function(data) {
//                    $('#mail_ms').html(data);
                    Dialog.Close();
                    location='<?=$_SERVER["REQUEST_URI"]?>';

                },
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        })

    }

    function mail_call(id) {
        Dialog.Show();
        var text='Здравствуйте, <?=$fio?>! \n\nПо телефону <?=$phone?>, указанному в Вашем заказе №<?=$arOrder[ACCOUNT_NUMBER]?>, вы оказались недоступны. \n\nПросим уточнить номер, или напишите, пожалуйста, удобное для звонка время.';
//        alert(text);
        $('#comment').val(text);
        $('.btnSubmit').on('click', function () {
            var comment = $('#comment').val();
            if($('#sostav').is(":checked")) {var sostav = 'on';} else {var sostav = 'off';}
//            alert(mailform);
            $.ajax({
                type: 'GET',
                url: '/bitrix/admin/button1.php',
                data: { id: id, comment: comment, email: "<?=$email?>", fio: "<?=$fio?>" },
                success: function(data) {
//                    $('#mail_ms').html(data);
                    Dialog.Close();
                    location='<?=$_SERVER["REQUEST_URI"]?>';

                },
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        })

    }

    
</script>

<div id="mail_call">
</div>
<div id="mail_ms">
</div>