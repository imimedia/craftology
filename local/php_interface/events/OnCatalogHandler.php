<?

class OnCatalogHandler {

    function OnPriceUpdate($id, $arFields) {
        global $USER;
        if (!$class = self::getProductSubscribeClass())
            return;

        $db = CIBlockElement::GetByID($arFields['PRODUCT_ID']);
        $db->SetUrlTemplates();
        $element = $db->GetNext();

        $rows = $class::getList(array(
            'select' => array('*'),
            'filter' => array('UF_PRODUCT_ID' => $arFields['PRODUCT_ID'], '>UF_CATALOG_GROUP_'.$arFields['CATALOG_GROUP_ID'] => $arFields['PRICE'], 'UF_ACTIVE' => true),
            'order' => array('ID' => 'DESC')
        ));

        foreach ($rows as $row) {
            $user = $USER->IsAuthorized()
                ? CUser::GetList($by, $order, array('ID' => $row['UF_USER_ID']), array('SELECT' => array('EMAIL')))->Fetch()
                : ($row['EMAIL'] ? array('EMAIL' => $row['EMAIL']) : array());

            if ($user) {
                $emailFields = array(
                    'EMAIL' => $user['EMAIL'],
                    'SALE_EMAIL' => COption::GetOptionString('sale', 'order_email'),
                    'OLD_PRICE' => $row['UF_CATALOG_GROUP_' . $arFields['CATALOG_GROUP_ID']],
                    'NEW_PRICE' => CCurrencyLang::CurrencyFormat($arFields['PRICE'], $arFields['CURRENCY']),
                    'PRODUCT_NAME' => $element['NAME'],
                    'PRODUCT_URL' => $element['DETAIL_PAGE_URL']
                );

                $event = new CEvent;
                $event->Send('PRODUCT_SUBSCRIBE_PRICE_REDUCTION', SITE_ID, $emailFields);

                $class::update($row['ID'], array('UF_ACTIVE' => false));
            }
            else {
                $class::delete($row['ID']);
            }
        }
    }

    static $productSubscribeClass = false;
    function getProductSubscribeClass() {
        if (self::$productSubscribeClass === false) {
            Bitrix\Main\Loader::includeModule('catalog');

            $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(HLBLOCK_PRODUCT_SUBSCRIBE)->fetch();
            $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            self::$productSubscribeClass = $entity->getDataClass();
        }
        return self::$productSubscribeClass;
    }

}