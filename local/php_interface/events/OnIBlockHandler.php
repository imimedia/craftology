<?

class OnIBlockHandler {

    private static $oldElementsFields = [];

    function catalogBeforeElementAdd(&$arFields) {
        if ($arFields['IBLOCK_ID'] != IBLOCK_CATALOG)
            return;

        if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'import') {
            $delFields = array(
                'IBLOCK_SECTION'
            );
            foreach ($delFields as $field) {
                if (array_key_exists($field, $arFields)) {
                    unset($arFields[$field]);
                }
            }
        }
    }

    function catalogBeforeElementUpdate(&$arFields) {
        global $DB;

        if ($arFields['IBLOCK_ID'] != IBLOCK_CATALOG)
            return;

        if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'import') {
            $id = intval($arFields['ID']);
            self::$oldElementsFields[$id] = $DB->Query("SELECT ID, ACTIVE FROM b_iblock_element WHERE ID={$id}", true)->Fetch();

            $propsId = array(
                'CML2_ARTICLE', 21
            );

            $props = array();
            if (array_key_exists('PROPERTY_VALUES', $arFields)) {
                foreach ($arFields['PROPERTY_VALUES'] as $key => $prop) {
                    if (in_array($key, $propsId)) {
                        $props[$key] = $prop;
                    }
                }
            }

            $delFields = array(
                'NAME',
                'CODE',
                'PREVIEW_TEXT_TYPE',
                'PREVIEW_TEXT',
                'DETAIL_TEXT_TYPE',
                'DETAIL_TEXT',
                'PREVIEW_PICTURE',
                'DETAIL_TEXT',
                'DETAIL_PICTURE',
                'IBLOCK_SECTION',
                'TAGS',

                'PROPERTY_VALUES'
            );

            foreach ($delFields as $field) {
                if (array_key_exists($field, $arFields)) {
                    unset($arFields[$field]);
                }
            }

            if ($props) {
                CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, $props);
            }
        }
    }

    function catalogAfterElementUpdate(&$arFields) {
        global $DB;
        if ($arFields['IBLOCK_ID'] != IBLOCK_CATALOG)
            return;

        $id = intval($arFields['ID']);
        if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'import') {
            if ($arFields['RESULT']) {
                if (self::$oldElementsFields[$id]) {
                    $oldFields = self::$oldElementsFields[$id];
                    if ($arFields['ACTIVE'] != $oldFields['ACTIVE']) {
                        $DB->Query("
                        UPDATE b_iblock_element 
                        SET ACTIVE = '" . $oldFields['ACTIVE'] . "' 
                        WHERE ID = {$id}"
                            , true);
                    }
                }
            }
        }
    }

    function catalogBeforeSectionAdd(&$arFields) {
        if ($arFields['IBLOCK_ID'] != IBLOCK_CATALOG)
            return;

        if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'import') {
            if (!intval($arFields['IBLOCK_SECTION_ID']))
                $arFields['IBLOCK_SECTION_ID'] = CATALOG_IMPORT_SECTION;
        }
    }

    function catalogBeforeSectionUpdate(&$arFields) {
        if ($arFields['IBLOCK_ID'] != IBLOCK_CATALOG)
            return;

        if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'import') {
            if (!intval($arFields['IBLOCK_SECTION_ID']))
                $arFields['IBLOCK_SECTION_ID'] = CATALOG_IMPORT_SECTION;
        }
    }
}