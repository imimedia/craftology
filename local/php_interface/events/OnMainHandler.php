<?

class OnMainHandler {

    function error404() {
        if (defined('ERROR_404') && ERROR_404 == 'Y') {
            $template = "main";
            global $APPLICATION;
            $APPLICATION->RestartBuffer();
            include $_SERVER["DOCUMENT_ROOT"]."/local/templates/".$template."/header.php";
            include $_SERVER["DOCUMENT_ROOT"]."/404.php";
            include $_SERVER["DOCUMENT_ROOT"]."/local/templates/".$template."/footer.php";
        }
    }

    function setFilters() {
        global $USER;

        global $filterNew;
        $filterNew = array(
            //'PROPERTY_NEW_VALUE' => 'Да'
            '>DATE_CREATE'=> ConvertTimeStamp(time() - (30 * 24 * 60 * 60), "FULL")
        );
        
        // Кондитер
        global $filterNewKonditer;
        $filterNewKonditer = array(
        	'SECTION_ID' => 127,
	        array(
		        'LOGIC' => 'OR',
		        array('>DATE_CREATE'=> ConvertTimeStamp(time() - (30 * 24 * 60 * 60), "FULL")),
		        array('PROPERTY_NEW_VALUE' => 'Да'),
		    )
	    );
	    global $filterHitKonditer;
        $filterHitKonditer = array(
        	'SECTION_ID' => 127,
	        'PROPERTY_HIT_VALUE' => 'Да'
	    );
	    global $filterSuperPriceKonditer;
        $filterSuperPriceKonditer = array(
        	'SECTION_ID' => 127,
	        'PROPERTY_SUPER_PRICE_VALUE' => 'Да'
	    );
	    global $filterPRINTKonditer;
        $filterPRINTKonditer = array(
        	'SECTION_ID' => 127,
	        'PROPERTY_PRINT_VALUE' => 'Да'
	    );
	    global $filterNEW_YEARKonditer;
        $filterNEW_YEARKonditer = array(
        	'SECTION_ID' => 127,
	        'PROPERTY_NEW_YEAR_VALUE' => 'Да'
	    );
	    global $filterVALENTINKonditer;
        $filterVALENTINKonditer = array(
        	'SECTION_ID' => 127,
	        'PROPERTY_VALENTIN_VALUE' => 'Да'
	    );
	    global $filterFEVRALKonditer;
        $filterFEVRALKonditer = array(
        	'SECTION_ID' => 127,
	        'PROPERTY_FEVRAL_VALUE' => 'Да'
	    );
	    global $filterMARTKonditer;
        $filterMARTKonditer = array(
        	'SECTION_ID' => 127,
	        'PROPERTY_MART_VALUE' => 'Да'
	    );
	    global $filterPASCHAKonditer;
        $filterPASCHAKonditer = array(
        	'SECTION_ID' => 127,
	        'PROPERTY_PASCHA_VALUE' => 'Да'
	    );
	    global $filterFIRSR_SENTEMBERKonditer;
        $filterFIRSR_SENTEMBERKonditer = array(
        	'SECTION_ID' => 127,
	        'PROPERTY_FIRSR_SENTEMBER_VALUE' => 'Да'
	    );
        global $filterFIRSR_SVADBAKonditer;
        $filterFIRSR_SVADBAKonditer = array(
            'SECTION_ID' => 127,
            'PROPERTY_SVADBA_VALUE' => 'Да'
        );
	    
	    // Мыловар
	    global $filterNewMylovara;
        $filterNewMylovara = array(
        	'SECTION_ID' => 85,
	        array(
		        'LOGIC' => 'OR',
		        array('>DATE_CREATE'=> ConvertTimeStamp(time() - (30 * 24 * 60 * 60), "FULL")),
		        array('PROPERTY_NEW_VALUE' => 'Да'),
		    )
	    );
	    global $filterHitMylovara;
        $filterHitMylovara = array(
        	'SECTION_ID' => 85,
	        'PROPERTY_HIT_VALUE' => 'Да'
	    );
	    global $filterSuperPriceMylovara;
        $filterSuperPriceMylovara = array(
        	'SECTION_ID' => 85,
	        'PROPERTY_SUPER_PRICE_VALUE' => 'Да'
	    );
	    global $filterPRINTMylovara;
        $filterPRINTMylovara = array(
        	'SECTION_ID' => 85,
	        'PROPERTY_PRINT_VALUE' => 'Да'
	    );
	    global $filterNEW_YEARMylovara;
        $filterNEW_YEARMylovara = array(
        	'SECTION_ID' => 85,
	        'PROPERTY_NEW_YEAR_VALUE' => 'Да'
	    );
	    global $filterVALENTINMylovara;
        $filterVALENTINMylovara = array(
        	'SECTION_ID' => 85,
	        'PROPERTY_VALENTIN_VALUE' => 'Да'
	    );
	    global $filterFEVRALMylovara;
        $filterFEVRALMylovara = array(
        	'SECTION_ID' => 85,
	        'PROPERTY_FEVRAL_VALUE' => 'Да'
	    );
	    global $filterMARTMylovara;
        $filterMARTMylovara = array(
        	'SECTION_ID' => 85,
	        'PROPERTY_MART_VALUE' => 'Да'
	    );
	    global $filterPASCHAMylovara;
        $filterPASCHAMylovara = array(
        	'SECTION_ID' => 85,
	        'PROPERTY_PASCHA_VALUE' => 'Да'
	    );
	    global $filterFIRSR_SENTEMBERMylovara;
        $filterFIRSR_SENTEMBERMylovara = array(
        	'SECTION_ID' => 85,
	        'PROPERTY_FIRSR_SENTEMBER_VALUE' => 'Да'
	    );
		
		// Другой поиск
        global $filterSearch;
        $filterSearch = array(
            "PARAMS" => array(
                "SEARCH_PAGE" => 'Y'
            )
        );
    }

}