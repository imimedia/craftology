<?php
use Bitrix\Main;

Main\EventManager::getInstance()->addEventHandler(
    'main',
    'OnBeforeEventAdd',
    array('OnSaleOrderTrackingNumber', 'GetSMSSaleOrderTrackingNumber'),
    false,
    '10'
);

class OnSaleOrderTrackingNumber{

    public static function GetSMSSaleOrderTrackingNumber($event_name, $site, &$params) {
        if($event_name === 'SALE_ORDER_TRACKING_NUMBER'){
            try{
                $order = \Bitrix\Sale\Order::load($params['ORDER_REAL_ID']);
                $deliveryIds = $order->getDeliverySystemId();
                $d1 = '';
                $d2 = '';
                if (count($deliveryIds)>0) {
                    $deliv1 = \Bitrix\Sale\Delivery\Services\Manager::getById($deliveryIds[0]);
                    $d1 = $deliv1['NAME'];

                    if (!empty($deliv1['PARENT_ID'])) {
                        $deliv2 = \Bitrix\Sale\Delivery\Services\Manager::getById($deliv1['PARENT_ID']);
                        $d2 = $deliv2['NAME'];
                    }
                }

                $params['DELIVERY_NAME'] = $d2.' ('.$d1.')';
            } catch (Exception $e) {
                file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/dev/SMS4b_exceptions.txt",  PHP_EOL . 'Error:' . PHP_EOL . $e->getMessage() . PHP_EOL . $e->getCode() . PHP_EOL . $e->getFile() . PHP_EOL);
                return false;
            }
        }

    }
}

