<?

class OnSearchHandler {

    static private $prepareBeforeIndex = false;
    static private $updateIBlockElements = array();
    static private $notUpdateIBlockElements = array();

    function BeforeIndex($arFields) {
        if ($arFields['MODULE_ID'] == 'iblock' && $arFields['PARAM2'] == IBLOCK_CATALOG) {
            self::prepareBeforeIndex();

            $db = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG, 'ID' => $arFields['ITEM_ID']), false, false, array('ID', 'IBLOCK_ID', 'DETAIL_TEXT', 'PROPERTY_CML2_ARTICLE', 'CATALOG_AVAILABLE'));
            if ($element = $db->GetNext()) {
                $arFields['PARAMS']['SEARCH_PAGE'] = $element['CATALOG_AVAILABLE'] == 'Y' & $element['CATALOG_QUANTITY_TRACE'] == 'Y' ? 'Y' : 'N';
                /*if (trim($element['DETAIL_TEXT'])) {
                    $arFields['TITLE'] .= "\n" . trim(strip_tags($element['DETAIL_TEXT']));
                }*/
                if (trim($element['PROPERTY_CML2_ARTICLE_VALUE'])) {
                    $arFields['TITLE'] .= "\n" . trim($element['PROPERTY_CML2_ARTICLE_VALUE']);
                }
                if (!in_array($arFields['ITEM_ID'], self::$notUpdateIBlockElements))
                    self::$updateIBlockElements[] = $arFields['ITEM_ID'];
            }
            $arFields['TITLE'] = trim($arFields['TITLE']);
            $arFields['BODY'] = $arFields['TITLE'];
        }
        return $arFields;
    }

    function updateIBlockElements() {
        if (self::$updateIBlockElements) {
            self::prepareBeforeIndex();

            foreach (self::$updateIBlockElements as $id) {
                unset(self::$updateIBlockElements[array_search($id, self::$updateIBlockElements)]);
                self::$notUpdateIBlockElements[] = $id;
                CIBlockElement::UpdateSearch($id, true);
            }
        }
    }

    private function prepareBeforeIndex() {
        if (self::$prepareBeforeIndex === false) {
            Bitrix\Main\Loader::includeModule('iblock');
            self::$prepareBeforeIndex = true;
        }
    }

}