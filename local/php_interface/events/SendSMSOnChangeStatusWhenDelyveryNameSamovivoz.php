<?php
use Bitrix\Main;

Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleBeforeStatusOrderChange',
    array('SendSMSOnChangeStatusWhenDelyveryNameSamovivoz', 'sendSMSonStatusChange')
);


/**
 * Класс отправляет смс по измениню статуса заказа на "Собран", а доставки "Самовывоз Пермь" или "Самовывоз Москва"
 * $arAllowedShipping = array(array(0=>184, 1=>184),array(0=>202, 1=>202));
 *
 */
final class SendSMSOnChangeStatusWhenDelyveryNameSamovivoz
{
    private static function checkIncludeModule()
    {
        if (!CModule::IncludeModule('rarus.sms4b')) {
            throw new ExceptionSendSMSModuleNotInstalled('SMS module not installed');
        }
    }

    /**
     * @param Csms4b $obSms4b
     * @param $userPhone
     * @return false|string
     * @throws Exception
     */
    private static function validateUserPhone(Csms4b $obSms4b, $userPhone)
    {
        $userPhoneFormat = $obSms4b->is_phone($userPhone);

        if ($userPhoneFormat === false) {
            throw new \Exception('Error phone format' . $userPhone);
        }
        return $userPhoneFormat;
    }


    /**
     * @param Main\Event $obEvent
     * @param Csms4b $obSms4b
     * @param array $arFields
     * @return array
     * @throws Main\ObjectNotFoundException
     */
    private static function getFieldsSms(Main\Event $obEvent, Csms4b $obSms4b, array $arFields)
    {
        /**
         * @var \Bitrix\Sale\Order $order
         */
        $order = $obEvent->getParameter('ENTITY');

        $arFields['DELIVERY_ID'] = $order->getDeliverySystemId();

        if (!($order instanceof Bitrix\Sale\Order)) {
            throw new ExceptionSendSMSNotHaveObject('It is not order object');
        }

        /**
         * @var \Bitrix\Sale\Order $order
         */
        $arFields['ACCOUNT_NUMBER'] = $order->getField("ACCOUNT_NUMBER");
        $arFields['ORDER_ID'] = $arFields['ACCOUNT_NUMBER'];
        $arFields['PHONE_TO'] = $obSms4b->is_phone($obSms4b->GetPhoneOrder($arFields['ORDER_ID'], $order->getSiteId()));

        return $arFields;
    }


    /**
     * @param Csms4b $obSms4b
     * @param array $arFields
     * @param $templName
     * @return array
     */
    private static function getTextMessage(Csms4b $obSms4b, array $arFields, $templName)
    {
        $textMessage = $obSms4b->GetEventTemplate($templName, 's1');


        foreach ($arFields as $k => $value) {
            $textMessage['MESSAGE'] = str_replace('#' . $k . '#', $value, $textMessage['MESSAGE']);
        }

        $textMessage['MESSAGE'] = str_replace('#PHONE_TO#', $arFields['PHONE_TO'], $textMessage['MESSAGE']);

        if ($obSms4b->use_translit === 'Y') {
            $textMessage['MESSAGE'] = $obSms4b->Translit($textMessage['MESSAGE']);
        }

        return $textMessage;
    }


    /**
     * @param Main\Event $obEvent
     * @return bool
     * @throws ExceptionSendSMSModuleNotInstalled
     */
    public static function sendSMSonStatusChange(Main\Event $obEvent)
    {
        try {
            $OrderStatus = array('SO');

            $arAllowedShipping = array(
                array(0=>184, 1=>184),
                array(0=>202, 1=>202)
            );

            $arFields['NEW_STATUS_ID'] = $obEvent->getParameter('VALUE');

            if(in_array($arFields['NEW_STATUS_ID'], $OrderStatus)){

                self::checkIncludeModule();

                /* @var Csms4b $SMS4B */
                global $SMS4B;

                $arFields = self::getFieldsSms($obEvent, $SMS4B, $arFields);

                if(!in_array($arFields['DELIVERY_ID'], $arAllowedShipping)){
                    return false;
                }

                $arFields['PHONE_TO'] = self::validateUserPhone($SMS4B, $arFields['PHONE_TO']);

                $userTextMessage = self::getTextMessage($SMS4B, $arFields, 'SMS4B_SALE_STATUS_CHANGED_'.$arFields['NEW_STATUS_ID']);

                $arSendSms[$arFields['PHONE_TO']] = $userTextMessage['MESSAGE'];

                if (empty($arSendSms)) {
                    throw new ExceptionSendSMSBadLogic();
                }
                $SMS4B->SendSmsSaveGroup($arSendSms, null, null, null, null, null, $arFields['ORDER_ID'], 'SALE_STATUS_CHANGED', 0);

                return true;

            }

        } catch (\Exception $e) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/dev/SmSEventsError.txt", PHP_EOL .'Text Error:'. PHP_EOL.$e->getMessage(). PHP_EOL.$e->getCode(). PHP_EOL.$e->getFile(). PHP_EOL);
            return false;
        }
    }
}
