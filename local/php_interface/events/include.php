<?

//
// OnMainHandler
//

include_once('OnMainHandler.php');
AddEventHandler('main', 'OnEpilog', array('OnMainHandler', 'error404'));
AddEventHandler('main', 'OnBeforeProlog', array('OnMainHandler', 'setFilters'));

//
// OnCatalogHandler
//

include_once('OnCatalogHandler.php');
AddEventHandler('catalog', 'OnPriceUpdate', array('OnCatalogHandler', 'OnPriceUpdate'));

//
// OnSearchHandler
//

include_once('OnSearchHandler.php');
AddEventHandler('search', 'BeforeIndex', array('OnSearchHandler', 'BeforeIndex'));
AddEventHandler('catalog', 'OnProductUpdate', array('OnSearchHandler', 'updateIBlockElements'));

//
// OnIBlockHandler
//

include_once('OnIBlockHandler.php');
AddEventHandler('iblock', 'OnBeforeIBlockElementAdd', array('OnIBlockHandler', 'catalogBeforeElementAdd'));
AddEventHandler('iblock', 'OnBeforeIBlockElementUpdate', array('OnIBlockHandler', 'catalogBeforeElementUpdate'));
AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array('OnIBlockHandler', 'catalogAfterElementUpdate'));
AddEventHandler('iblock', 'OnBeforeIBlockSectionAdd', array('OnIBlockHandler', 'catalogBeforeSectionAdd'));
AddEventHandler('iblock', 'OnBeforeIBlockSectionUpdate', array('OnIBlockHandler', 'catalogBeforeSectionUpdate'));


//
// addEventHandler
//
use Bitrix\Main\Diag\Debug; 
use Bitrix\Main;
Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderBeforeSaved',
    'BeforeSavedOrder'
);

function BeforeSavedOrder(Main\Event $event)
{
	$order = $event->getParameter("ENTITY");
	$rsUser = CUser::GetByID($order->getUserId());
	$arUser = $rsUser->Fetch();	
	$propertyCollection = $order->getPropertyCollection();
	$phonePropValue = $propertyCollection->getPhone();
	//$NamePropValue = $propertyCollection->getItemByOrderPropertyId(9);	
 	//Debug::dumpToFile($propertyCollection);
 	//Debug::dumpToFile($arUser);
	
	//if ($arUser['PERSONAL_PHONE'] == '' or $arUser['NAME'] == '' or $arUser['LAST_NAME'] == '' or $arUser['SECOND_NAME'] == '')  {
		
		if ($arUser['PERSONAL_PHONE'] == '') {
			$fields = Array( 
			"PERSONAL_PHONE" => $phonePropValue->getValue(), 
			); 
			$user = new CUser;
			$user->Update($arUser['ID'], $fields);
		}
		
		if ($arUser['LOGIN']<>$arUser['EMAIL']) {
			$fields = Array( 
			"LOGIN" => $arUser['EMAIL'], 
			); 
			$user = new CUser;
			$user->Update($arUser['ID'], $fields);
		}
		
	$deliveryIds = $order->getDeliverySystemId();
	$d1 = '';
	$d2 = '';
	
	$typePropValue = $propertyCollection->getItemByOrderPropertyId(16);		
	
	foreach ($deliveryIds as &$value) {
	    if ($value !== null) {
			$deliv1 = \Bitrix\Sale\Delivery\Services\Manager::getById($value);
			$d1 = $deliv1['NAME'];
			$d2 = $deliv1['NAME'];
		
			if (!empty($deliv1['PARENT_ID'])) {
				$deliv2 = \Bitrix\Sale\Delivery\Services\Manager::getById($deliv1['PARENT_ID']);
				$d1 = $deliv2['NAME'];
			}
			
			//Debug::dumpToFile($d1);
			
			if ( stripos($d1, "Почта") !== false ) {
				$typePropValue->setValue("Почта");	
			}
			elseif ( stripos($d1, "Наложенный") !== false ) {
				$typePropValue->setValue("Почта");	
			}
			elseif (stripos($d1, "СДЭК") !== false ) {
				$typePropValue->setValue("СДЭК");
			}
			elseif (stripos($d1, "Пермь") !== false ) {
				$typePropValue->setValue("Доставка Пермь");
			}
			elseif (stripos($d1, "Москва") !== false ) {
				$typePropValue->setValue("Доставка Москва");
			}
			elseif (stripos($d2, "Самовывоз Пермь") !== false ) {
				$typePropValue->setValue("Самовывоз Пермь");
			}
			elseif (stripos($d2, "Самовывоз Москва") !== false ) {
				$typePropValue->setValue("Самовывоз Москва");
			}
			elseif (stripos($d1, "Согласовать") !== false ) {
				$typePropValue->setValue("Не выбрано");
			}
			else {
				$typePropValue->setValue("ТК");
			}
			
			break;
		}
	}
	
/*
		$firstname = $propertyCollection->getItemByOrderPropertyId(8)->getValue();
		$name = $propertyCollection->getItemByOrderPropertyId(9)->getValue();
		$lastname = $propertyCollection->getItemByOrderPropertyId(10)->getValue();
		
		$propFIO = "".$firstname ." ". $name ." ". $lastname;
		
		$somePropValue = $propertyCollection->getItemByOrderPropertyId(11);
		$somePropValue->setValue($propFIO);
*/
		//$somePropValue->save();
		
		//$order->setField('FIO', $propFIO); 
		//CSaleOrderPropsValue::Update(8, array("FIO"=>"ADDRESS"));
	//}
		
}

/*
Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnBeforeSaleOrderFinalAction',
    'BeforeOrderSetField'
);



function BeforeOrderSetField(Main\Event $event)
{
	$order = $event->getParameter("ENTITY");
	$propertyCollection = $order->getPropertyCollection();
	
	$city=UserEx::getInstance()->getField('PERSONAL_CITY');
	
	if (!$city == '') {
		$somePropValue = $propertyCollection->getItemByOrderPropertyId(1);
		$somePropValue->setValue($city);
	}
	
	//Debug::dumpToFile($city);

}
*/
//use Bitrix\Main\Diag\Debug;

AddEventHandler("sale", "OnSaleComponentOrderProperties", "OnSaleComponentOrderPropertiesHandler"); 

function OnSaleComponentOrderPropertiesHandler(&$arUserResult, $request, &$arParams, &$arResult) 
{ 
	if($arUserResult['ORDER_PROP'][1] == '') {
		
		$ip = $_SESSION['SESS_IP'];
		$lang = "ru";
		
		$city = \Bitrix\Sale\Location\GeoIp::getLocationCode($ip, $lang);
		$cityID = \Bitrix\Sale\Location\GeoIp::getLocationId($ip, $lang);
		if (isset($city) && !empty($city))
	    {
			$arUserResult['ORDER_PROP'][1] = $city;
			
			$arLocs = CSaleLocation::GetLocationZIP($cityID); 
			$arLocs = $arLocs->Fetch(); 	
			$arUserResult['ORDER_PROP'][2] = $arLocs['ZIP'];
		}
		
/*
		$city=UserEx::getInstance()->getField('PERSONAL_CITY');
		if (isset($city) && !empty($city))
	    {
		    $zip=UserEx::getInstance()->getField('PERSONAL_ZIP');
			$rsLocation = CSaleLocation::GetList(
		        array(
		                "SORT" => "ASC",
		                "COUNTRY_NAME_LANG" => "ASC",
		                "CITY_NAME_LANG" => "ASC"
		            ),
		        array("CITY_NAME" => $city),
		        false,
		        false,
		        array()
		    );
		
		
		    if ($arLocation=$rsLocation->Fetch()){
			  	$def_value = $arLocation["ID"];
			  	//$def_index = $arLocation["ID"];
			  	}
		    else {
		        $def_value=0;
		        //$def_index = 0;
		    }
		    
			//Debug::dumpToFile($arLocation); 
			$arUserResult['ORDER_PROP'][1] = $def_value; 
			$arUserResult['ORDER_PROP'][2] = $zip; 
			
		}
		else {
			$ip = $_SESSION['SESS_IP'];
			$lang = "ru";
			// Возвращает код местоположения
			$arUserResult['ORDER_PROP'][1] = \Bitrix\Sale\Location\GeoIp::getLocationCode($ip, $lang);
			//Debug::dumpToFile($ip);
			// Возвращает индекс
			$arUserResult['ORDER_PROP'][2] = \Bitrix\Sale\Location\GeoIp::getZipCode($ip, $lang);
		}
*/
	}
}


?>