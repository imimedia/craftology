<?

define('IBLOCK_CATALOG', 3);
define('CATALOG_IMPORT_SECTION', 275);//448|275
define('CATALOG_PRICE', 1);
define('SECTION_CONFECTIONER', 127);
define('SECTION_SOAPBOILER', 85);
define('HLBLOCK_PRODUCT_SUBSCRIBE', 2);

include_once('lib/GetClientIp.php');
include_once('lib/UserEx.php');
include_once('events/include.php');
include_once('events/OnSaleOrderTrackingNumber.php');
include_once('events/SendSMSOnChangeStatusWhenDelyveryNameSamovivoz.php');


CModule::IncludeModule("sale");
global $arBasket;
$dbBasketItems = CSaleBasket::GetList(
      array(
              "NAME" => "ASC",
              "ID" => "ASC"
          ),
      array(
              "FUSER_ID" => CSaleBasket::GetBasketUserID(),
              "LID" => SITE_ID,
              "ORDER_ID" => "NULL",
          ),
      false,
      false,
      array("ID", "CALLBACK_FUNC", "MODULE",
            "PRODUCT_ID", "QUANTITY", "DELAY",
            "CAN_BUY", "PRICE", "WEIGHT")
  );
  while ($arBasketItems=$dbBasketItems->Fetch()){
     $arBasket["QUANTITY_BASKET"][$arBasketItems["PRODUCT_ID"]]=$arBasketItems["QUANTITY"];
     $arBasket["ITEM"][$arBasketItems["PRODUCT_ID"]]=$arBasketItems;
  }

function Reindex_Search() {         
	CModule::IncludeModule("search");
	AddMessage2Log('Reindex_Search');
	$NS = false;     
	$NS = CSearch::ReIndexAll(false, 60, $NS);     
	while(is_array($NS))         
	$NS = CSearch::ReIndexAll(false, 60, $NS);          
	//mail("pactx@yandex.ru", "Reindex", "Pereindeksirovano: ".$NS);          
	return "Reindex_Search();"; 
}

function pre($o) {                                            
    $b = debug_backtrace();
    $d = $_SERVER['DOCUMENT_ROOT'];
    $b[0]["file"] = str_replace($d,'',str_replace(str_replace('/','\\',$d),'',$b[0]['file']));
    echo '<div style="font-size:9pt;color:#000;background:white;border:1px dashed #A0C3FF;">'
        .'<div style="padding:5px;background:#A0C3FF;">'.$b[0]["file"].' ['.$b[0]["line"].']</div>'
        .'<pre style="padding:10px;">'; print_r($o); echo '</pre></div>';
}
function arshow($array, $adminCheck = false){
        global $USER;
        $USER = new Cuser;
        if ($adminCheck) {
            if (!$USER->IsAdmin()) {
                return false;
            }
        }
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }
class App
{
    static function getVar($var, $def) {
        if (isset($_REQUEST[$var]) && trim($_REQUEST[$var]))
            return trim($_REQUEST[$var]);
        return $def;
    }

    static function getNumEnding($num, $endings) {
        $num = intval($num);
        $num = $num % 100;
        if ($num >= 11 && $num <= 19) {
            $endings = $endings[2];
        }
        else {
            switch ($num % 10) {
                case (1): $endings = $endings[0]; break;
                case (2):
                case (3):
                case (4): $endings = $endings[1]; break;
                default: $endings = $endings[2];
            }
        }
        return $endings;
    }

    static function socservOnclickToArray($str) {
        $start = strpos($str, "'");
        $end = strrpos($str, "'");
        $url = substr($str, $start + 1, $end - $start - 1);
        $size = array_map('intval', explode(',', substr($str, strrpos(substr($str, 0, strrpos($str, ',')), ',')+1, -1)));
        return array(
            'url' => $url,
            'width' => $size[0],
            'height' => $size[1]
        );
    }

    static function getFacebookShareLink() {
        global $APPLICATION;
        return 'https://www.facebook.com/dialog/share?'
        .'app_id=524804231063953'
        .'&display=popup'
        .'&href='.urlencode('http://'.$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri())
        .'&redirect_uri='.urlencode('http://'.$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri());
    }

}


use Bitrix\Sale;
// Добавим в оповещение о заказе доп. поля
AddEventHandler("sale", "OnOrderNewSendEmail", "bxModifySaleMails");

function bxModifySaleMails($orderID, &$eventName, &$arFields)
{
	CModule::IncludeModule('sale');
    $ID=$orderID;

    $dbBasketItems = CSaleBasket::GetList(
        array(
            "NAME" => "ASC",
            "ID" => "ASC"
        ),
        array(
            "ORDER_ID" => $ID
        ),
        false,
        false,
        array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT", "PRODUCT_PROVIDER_CLASS", "NAME", 'SET_PARENT_ID')
    );
    while ($arItems = $dbBasketItems->Fetch())
    {
        if($arItems[SET_PARENT_ID]>0){

            $arUpdate[$arItems[ID]] = array(
                "NAME" => $arItems[NAME],
                "PARENT_ID" => $arItems[SET_PARENT_ID],
                "ID" => $arItems[ID],
                "PRODUCT_ID" => $arItems[PRODUCT_ID],
                "PRICE" => $arItems[PRICE],
                "QUANTITY" => $arItems[QUANTITY],
            );
        }
        $arBasketItems[$arItems[ID]] = $arItems;
    }
//echo '<pre>', print_r($arUpdate), '</pre>' ;
//echo '<pre>', print_r($arBasketItems), '</pre>' ;

    if(!empty($arUpdate)){
        // Изменим цену товара в записи $ID корзины
        foreach ($arUpdate as $update) {
            $arFields = array(
                "PRICE" => $arBasketItems[$update[PARENT_ID]][PRICE] * $arBasketItems[$update[PARENT_ID]][QUANTITY]/$update[QUANTITY],
            );
//            echo '<pre>', print_r($arFields), '</pre>' ;
            CSaleBasket::Update($update[ID], $arFields);
        }
    }


	
  $arOrder = CSaleOrder::GetByID($orderID);
  
  //-- получаем телефоны и адрес
  $order_props = CSaleOrderPropsValue::GetOrderProps($orderID);
  $phone="";
  $index = ""; 
  $country_name = "";
  $city_name = "";  
  $address = "";
  
  while ($arProps = $order_props->Fetch())
  {
    if ($arProps["CODE"] == "PHONE")
    {
       $phone = htmlspecialchars($arProps["VALUE"]);
    }
    if ($arProps["CODE"] == "LOCATION")
    {
        $arLocs = CSaleLocation::GetByID($arProps["VALUE"]);
        $country_name =  $arLocs["COUNTRY_NAME_ORIG"];
        $city_name = $arLocs["CITY_NAME_ORIG"];
    }

    if ($arProps["CODE"] == "ZIP")
    {
      $index = $arProps["VALUE"];   
    }

    if ($arProps["CODE"] == "ADDRESS")
    {
      $address = $arProps["VALUE"];
    }
  }

  $full_address = $index.", ".$country_name."-".$city_name.", ".$address;

  //-- получаем название платежной системы   
  $arPaySystem = CSalePaySystem::GetByID($arOrder["PAY_SYSTEM_ID"]);
  $pay_system_name = "";
  if ($arPaySystem)
  {
    $pay_system_name = $arPaySystem["NAME"];
  }

  //-- добавляем новые поля в массив результатов
  $arFields["ORDER_DESCRIPTION"] = $arOrder["USER_DESCRIPTION"]; 
  $arFields["PHONE"] =  $phone;
 
  $arFields["PAY_SYSTEM_NAME"] =  $pay_system_name;
  $arFields["FULL_ADDRESS"] = $full_address;   
  
  $order = Sale\Order::load($orderID);
	$deliveryIds = $order->getDeliverySystemId();
	$d1 = '';
	$d2 = '';
	if (count($deliveryIds)>0) {
		$deliv1 = \Bitrix\Sale\Delivery\Services\Manager::getById($deliveryIds[0]);
		$d1 = $deliv1['NAME'];
	
		if (!empty($deliv1['PARENT_ID'])) {
			$deliv2 = \Bitrix\Sale\Delivery\Services\Manager::getById($deliv1['PARENT_ID']);
			$d2 = $deliv2['NAME'];
		}
	}
	$arFields["DELIVERY_PRICE"] = "". number_format($order->getDeliveryPrice(), 2, '.', '') . " руб.";
	
 $arFields["DELIVERY_NAME"] = $d2.' '.$d1;
  
  if(CModule::IncludeModule("sale") && CModule::IncludeModule("iblock"))
  {
	 //СОСТАВ ЗАКАЗА РАЗБИРАЕМ SALE_ORDER НА ЗАПЧАСТИ
	      $strOrderList = "";
	      $dbBasketItems = CSaleBasket::GetList(
	                 array("NAME" => "ASC"),
	                 array("ORDER_ID" => $orderID),
	                 false,
	                 false,
	                 array("PRODUCT_ID", "ID", "NAME", "QUANTITY", "PRICE", "CURRENCY")
	               );
	 $allsumm = 0; 
	 while ($arProps = $dbBasketItems->Fetch())
	  {
		$article_find = CIBlockElement::GetProperty('3', $arProps["PRODUCT_ID"], array(), Array("CODE"=>"CML2_ARTICLE"));
		if($article_value = $article_find->Fetch()) { 
			$product_article = $article_value["VALUE"];
			}
		else {
			$product_article = 'No'; 
			} 
	  	//ПЕРЕМНОЖАЕМ КОЛИЧЕСТВО НА ЦЕНУ
      	$summ = number_format($arProps['QUANTITY'] * $arProps['PRICE'], 2, '.', '');
      	$allsumm = $allsumm + ($arProps['QUANTITY'] * $arProps['PRICE']);
	  	//СОБИРАЕМ В СТРОКУ ТАБЛИЦЫ   
	  	$strCustomOrderList .= "<tr style='border-bottom: 1pt solid black;'><td width='60' style='width:60px !important;'>".$product_article."</td><td width='250' style='width:250px !important;'>".$arProps['NAME']."</td><td width='50' style='width:50px !important;'>".$arProps['QUANTITY']." шт."."</td><td width='80' style='width:80px !important;'>".number_format($arProps['PRICE'], 2, '.', '')."</td><td>".$summ."</td><tr>";
	  }
	  
	  $arFields["PRODUCT_PRICE"] = "". number_format($allsumm, 2, '.', ''). " руб.";
	  //ОБЪЯВЛЯЕМ ПЕРЕМЕННУЮ ДЛЯ ПИСЬМА
	  $arFields["ORDER_TABLE_ITEMS"] = $strCustomOrderList;
  }
}

use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader; 

function getMsStatus()
{	
    //Loader::includeModule("gwi.msstatusimport"); 
    CModule::IncludeModule('gwi.msstatusimport');
    $objStatus = new GWIMSStatusImport();
	//Debug::dumpToFile("test");	
    return "getMsStatus();";


}

AddEventHandler('main', 'OnAdminContextMenuShow', 'OrderDetailAdminContextMenuShow');
function OrderDetailAdminContextMenuShow(&$items){
    if ($_SERVER['REQUEST_METHOD']=='GET' && $GLOBALS['APPLICATION']->GetCurPage()=='/bitrix/admin/sale_order_edit.php' && $_REQUEST['ID']>0)
    {
        $items[] = array(
            "TEXT"=>"Отправить кнопку оплаты",
            "LINK"=>"javascript:mail_ms(".$_REQUEST['ID'].")",
            "TITLE"=>"Отправить письмо",
            "ICON"=>"adm-btn",
        );
        $items[] = array(
            "TEXT"=>"Отправить письмо",
            "LINK"=>"javascript:mail_ms_old(".$_REQUEST['ID'].")",
            "TITLE"=>"Отправить письмо",
            "ICON"=>"adm-btn",
        );
    }
    if ($_SERVER['REQUEST_METHOD']=='GET' && $GLOBALS['APPLICATION']->GetCurPage()=='/bitrix/admin/sale_order_view.php' && $_REQUEST['ID']>0)
    {
        $items[] = array(
            "TEXT"=>"Отправить кнопку оплаты",
            "LINK"=>"javascript:mail_ms(".$_REQUEST['ID'].")",
            "TITLE"=>"Отправить письмо",
            "ICON"=>"adm-btn",
        );

        $items[] = array(
            "TEXT"=>"Отправить письмо",
            "LINK"=>"javascript:mail_ms_old(".$_REQUEST['ID'].")",
            "TITLE"=>"Отправить письмо",
            "ICON"=>"adm-btn",
        );
    }
}

AddEventHandler('main', 'OnBeforeEventSend', "ChangeMail");
function ChangeMail(&$arFields, $arTemplate)
{  
	    if ($arTemplate['EVENT_NAME'] == 'SALE_ORDER_TRACKING_NUMBER') {
	        CModule::IncludeModule('sale');
			$order = \Bitrix\Sale\Order::load($arFields['ORDER_REAL_ID']);
			$deliveryIds = $order->getDeliverySystemId();
			$d1 = '';
			$d2 = '';
			if (count($deliveryIds)>0) {
				$deliv1 = \Bitrix\Sale\Delivery\Services\Manager::getById($deliveryIds[0]);
				$d1 = $deliv1['NAME'];
			
				if (!empty($deliv1['PARENT_ID'])) {
					$deliv2 = \Bitrix\Sale\Delivery\Services\Manager::getById($deliv1['PARENT_ID']);
					$d2 = $deliv2['NAME'];
				}
			}
	        
	        //$mess = $arTemplate["MESSAGE"];
			//$arTemplate["MESSAGE"] = str_replace('#DELIVERY_NAME#', $d2.' '.$d1, $mess);
			
			//$arTemplate['DELIVERY_NAME'] = $d2.' '.$d1;
			$arFields['DELIVERY_NAME'] = $d2.' ('.$d1.')';
			
			//$arTemplate['DELIVERY_NAME'] = $d2.' '.$d1;
			//Debug::dumpToFile($arFields);
			//Debug::dumpToFile($arTemplate);
			
		}	
   
}