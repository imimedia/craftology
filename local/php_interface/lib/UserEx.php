<?

Bitrix\Main\Loader::includeModule('highloadblock');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class UserEx {

    private $id = 0;
    private $fields;
    private $_USER;

    function __construct($id) {
        global $USER;
        $this->_USER = $USER;

        $this->id = intval($id ?: $USER->GetID());

        $this->reload();
    }

    function reload() {
        if ($this->id) {
            $by = 'id';
            $order = 'desc';
            $this->fields = CUser::GetList($by, $order, array('ID' => $this->id), array('SELECT' => array('*', 'UF_*')))->Fetch();

            if (!$this->fields)
                $this->id = 0;
        }
    }

    function getId() {
        return $this->id;
    }

    function getFields() {
        return $this->fields;
    }

    function getField($field, $def = null) {
        return array_key_exists($field, $this->fields) ? $this->fields[$field] : $def;
    }

    //
    // Semi-static methods
    //

    private static function coalesce($v,$ch)
    {
        return  ($v)?$v.$ch:"";
    }

    function GetAddress()
    {
        return self::coalesce(GetCountryByID($this->fields['PERSONAL_COUNTRY'],"ru"),", ").self::coalesce($this->fields['PERSONAL_STATE'],", ").
                self::coalesce($this->fields['PERSONAL_CITY'],", ").self::coalesce($this->fields['PERSONAL_ZIP'],", ").
                $this->fields['PERSONAL_STREET'];
    }

    function getDisplayName($name = null, $secondName = null, $lastName = null) {
        if ($name === null) {
            $name = $this->fields['NAME'];
            $secondName = $this->fields['SECOND_NAME'];
            $lastName = $this->fields['LAST_NAME'];
        }
        return $lastName.' '.$name.($secondName ? ' '.substr($secondName, 0, 1).'.' : '');
    }

    public static function SetBuyerValues($ID)
    {
        if (CModule::IncludeModule("sale")) {

            global $USER;

            $arUser=CUser::GetByID($USER->GetID())->Fetch();

            $db_vals = CSaleOrderPropsValue::GetList(
                        array("SORT" => "ASC"),
                        array(
                                "ORDER_ID" => $ID,
                            )
                    );
            $out=array();
            while ($arVals = $db_vals->Fetch())
            {
                if ($arVals["CODE"]=="CITY") {
                    $arLoc=CSaleLocation::GetByID($arVals["VALUE"]);
                    if ($arLoc) {
                        if (!$arUser["PERSONAL_CITY"])
                            $out["PERSONAL_CITY"]=$arLoc["CITY_NAME"];
                        if (!$arUser["PERSONAL_COUNTRY"])
                            $out["PERSONAL_COUNTRY"]=$arLoc["COUNTRY_ID"];
                    }
                } else
                    if ($arVals["CODE"]=="ZIP" && !$arUser["PERSONAL_ZIP"])
                        $out["PERSONAL_ZIP"]=$arVals["VALUE"];
            }
            if (count($out)>0)
            {
                $user = new CUser;
                $user->Update($USER->GetID(), $out);
            }
        }
    }

    //
    // Instance
    //

    private static $instance = array();

    public static function getInstance($id = 0) {
        if (empty(self::$instance[$id]))
            self::$instance[$id] = new self($id);
        return self::$instance[$id];
    }
}