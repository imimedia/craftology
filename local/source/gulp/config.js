var development = 'build/',
    production  = '../../';

var path = {
    build: {
        html: development,
        js: 'js/',
        jquery: 'js/jquery/',
        html5shiv: 'js/html5shiv/',
        css: 'css/',
        cssSprite: 'src/styles/partials/',
        img: 'images/',
        imgSprite: 'images/',
        fonts: 'fonts/'
    },
    src: {
        html: 'src/*.html',
        jsInternal: 'src/js/internal.js',
        jsExternal: 'src/js/external.js',
        jquery: 'bower_components/jquery/dist/*.*',
        html5shiv: 'bower_components/html5shiv/dist/*.*',
        styleInternal: 'src/styles/internal.scss',
        styleExternal: 'src/styles/external.scss',
        styleInclude: 'src/styles/',
        img: ['src/images/**/*.*', '!src/images/sprite/**/*.*', '!src/images/icons/**/*.*'],
        imgSprite: 'src/images/sprite/**/*.*',
        imgSpriteSvg: 'src/images/sprite_svg/**/*.*',
        imgSpriteSvg2Png: 'src/images/sprite/',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        jsInternal: ['src/js/**/*.js', '!src/js/external.js'],
        jsExternal: 'src/js/external.js',
        styleInternal: ['src/styles/**/*.scss', '!src/styles/external.scss'],
        styleExternal: 'src/styles/external.scss',
        img: 'src/images/**/*.*',
        imgSprite: 'src/images/sprite/**/*.*',
        fonts: 'src/fonts/**/*.*'
    }
};

module.exports = {
    browsersync: {
        server: {
            baseDir: './' + development
        },
        tunnel: false,
        open: false,
        host: 'localhost',
        port: 3000,
        logPrefix: "Frontend_Blank"
    },
    html: {
        src: path.src.html,
        dest: path.build.html
    },
    js: {
        srcInternal: path.src.jsInternal,
        srcExternal: path.src.jsExternal,
        srcJquery: path.src.jquery,
        srcHtml5shiv: path.src.html5shiv,
        development: {
            dest: development + path.build.js,
            destJquery: development + path.build.jquery,
            destHtml5shiv: development + path.build.html5shiv
        },
        production: {
            dest: production + path.build.js,
            destJquery: production + path.build.jquery,
            destHtml5shiv: production + path.build.html5shiv
        }
    },
    sass: {
        srcInternal: path.src.styleInternal,
        srcExternal: path.src.styleExternal,
        options: {
            includePaths: [path.src.styleInclude],
            sourceMap: true,
            errLogToConsole: true
        },
        development: {
            dest: development + path.build.css
        },
        production: {
            dest: production + path.build.css
        }
    },
    autoprefixer: {
        browsers: [
            'last 10 versions',
            'IE 8',
            'IE 9',
            '> 3%'
        ]
    },
    images: {
        src: path.src.img,
        development: {
            dest: development + path.build.img
        },
        production: {
            dest: production + path.build.img
        }
    },
    imagemin: {
        // use добавляется в таске
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        interlaced: true
    },
    // https://habrahabr.ru/post/227945/
    sprites: {
        src: path.src.imgSprite,
        srcSvg: path.src.imgSpriteSvg,
        srcSvg2Png: path.src.imgSpriteSvg2Png,
        dest: {
            css: path.build.cssSprite,
            image: production + path.build.imgSprite
        },
        options: {
            imgName: 'sprite.png',
            cssName: '_sprite.scss',
            imgPath: '../images/sprite.png',
            cssFormat: 'scss',
            algorithm: 'top-down',
            padding: 10,
            engine: 'pngsmith',
            imgOpts: {
                format: 'png'
            },
            cssVarMap: function(sprite) {
                sprite.name = 's-' + sprite.name
            }
        }
    },
    fonts: {
        src: path.src.fonts,
        development: {
            dest: development + path.build.fonts
        },
        production: {
            dest: production + path.build.fonts
        }
    },
    watch: {
        html: path.watch.html,
        jsInternal: path.watch.jsInternal,
        jsExternal: path.watch.jsExternal,
        sassInternal: path.watch.styleInternal,
        sassExternal: path.watch.styleExternal,
        images: path.watch.img,
        sprites: path.watch.imgSprite,
        fonts: path.watch.fonts
    },
    clean: {
        development: {
            dest: './' + development
        },
        production: {
            dest: './' + production
        }
    },
    /**
     * Wrap gulp streams into fail-safe function for better error reporting
     * Usage:
     * gulp.task('js', config.wrapPipe(function(success, error) {
     *   return gulp.src('js/*.js')
     *      .pipe(js().on('error', error))
     *      .pipe(gulp.dest('app/css'));
     * }));
     */
    wrapPipe: function(taskFn) {
        return function(done) {
            var onSuccess = function() {
                done();
            };
            var onError = function(err) {
                done(err);
            }
            var outStream = taskFn(onSuccess, onError);
            if(outStream && typeof outStream.on === 'function') {
                outStream.on('end', onSuccess);
            }
        }
    }
};