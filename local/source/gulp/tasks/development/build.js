var gulp = require('gulp');

gulp.task('build', [
    'html',
    'js',
    'sass',
    //'sprites',
    'images',
    'fonts'
]);