var gulp        = require('gulp'),
    browsersync = require('browser-sync'),
    sourcemaps  = require('gulp-sourcemaps'),
    prefixer    = require('gulp-autoprefixer'),
    sass        = require('gulp-sass'),
    config      = require('../../config'),
    cssBase64   = require('gulp-css-base64'),
    cleanCSS    = require('gulp-clean-css');

/*
 * Компиляция SASS в CSS
 * Создание sourcemaps
 * Минификация
 */

gulp.task('sass', [
    'sass-internal',
    'sass-external'
]);

gulp.task('sass-internal', config.wrapPipe(function(success, error) {
    browsersync.notify('Compiling sass');

    return gulp.src(config.sass.srcInternal)
        //.pipe(sourcemaps.init())
        .pipe(sass(config.sass.options).on('error', error))
        .pipe(prefixer(config.autoprefixer))
        //.pipe(sourcemaps.write())
        .pipe(cssBase64({
            baseDir: "../../src/images/icons",
            extensionsAllowed: ['.png']
        }))
        //.pipe(cleanCSS())
        .pipe(gulp.dest(config.sass.development.dest))
}));

gulp.task('sass-external', config.wrapPipe(function(success, error) {
    browsersync.notify('Compiling sass');

    return gulp.src(config.sass.srcExternal)
        //.pipe(sourcemaps.init())
        .pipe(sass(config.sass.options).on('error', error))
        .pipe(cleanCSS())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(config.sass.development.dest))
}));