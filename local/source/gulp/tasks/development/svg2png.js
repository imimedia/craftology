var gulp        = require('gulp'),
    browsersync = require('browser-sync'),
    svg2png     = require('gulp-svg2png'),
    config      = require('../../config');

gulp.task('svg2png', function() {
    browsersync.notify('Compiling sprites');

    gulp.src(config.sprites.srcSvg)
        .pipe(svg2png())
        .pipe(gulp.dest(config.sprites.srcSvg2Png));
});