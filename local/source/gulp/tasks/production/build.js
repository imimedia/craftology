var gulp = require('gulp');

gulp.task('build:production', [
    'js:production',
    'sass:production',
    'images:production',
    'fonts:production'
]);