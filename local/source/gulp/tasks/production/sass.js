var gulp        = require('gulp'),
    browsersync = require('browser-sync'),
    sourcemaps  = require('gulp-sourcemaps'),
    prefixer    = require('gulp-autoprefixer'),
    cleanCSS    = require('gulp-clean-css'),
    sass        = require('gulp-sass'),
    cssBase64   = require('gulp-css-base64'),
    config      = require('../../config');

/*
 * Компиляция SASS в CSS
 * Создание sourcemaps
 * Минификация
 */

gulp.task('sass:production', [
    'sass-internal:production',
    'sass-external:production'
]);

gulp.task('sass-internal:production', config.wrapPipe(function(success, error) {
    browsersync.notify('Compiling sass');

    return gulp.src(config.sass.srcInternal)
        //.pipe(sourcemaps.init())
        .pipe(sass(config.sass.options).on('error', error))
        .pipe(prefixer(config.autoprefixer))
        .pipe(cleanCSS())
        .pipe(cssBase64({
            baseDir: "../../src/images/icons",
            extensionsAllowed: ['.png', '.svg']
        }))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(config.sass.production.dest))
}));

gulp.task('sass-external:production', config.wrapPipe(function(success, error) {
    browsersync.notify('Compiling sass');

    return gulp.src(config.sass.srcExternal)
        //.pipe(sourcemaps.init())
        .pipe(sass(config.sass.options).on('error', error))
        .pipe(cleanCSS())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(config.sass.production.dest))
}));