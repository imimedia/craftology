function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
var exhApplication = (function($, _window){
    var exhApplication = {},
    	markersList = [],
    	pointsList = [],
        currId = '',
        $mapViewBtn,
        $mapSchemeBtn,
    	$schemeWrapper,
    	$pointsList = $('.points-list'),
    	$infoPlank = $('.info-exh-wrapper'),
    	$infoPlankClose = $('.exh-info-close'),
        markerSmall = '/img/icon_marker.png',
        markerBig = '/img/icon_marker_active.png',
        scrollableList = $('.points-list-wrapper'),
        $mapWrapper,
        hashAreaId = '',
        modeMap = '',
        $overlay = '',
        initialZoom = 7,
        map,
        bounds;

    var bindEvents = function() {
		/* close plank click */
		$infoPlankClose.on('click', function(e) {
            e.preventDefault();
            currId = '';
			// закрыть планку
			exhApplication.closeInfoPlank();
			// сделать неактивными все маркеры
            $.each(markersList, function(idx, marker){
                marker.setIcon(markerSmall);
            });
            exhApplication.resetBounds();
            // сделать неактивными пункты меню
            $('.points-list-item').removeClass('active');

            // скрыть переключатель вида
            exhApplication.disableSwitchView();

			// сделать схему площадки неактивной
			$schemeWrapper.removeClass('active');

			// сделать оверлей схемы неактивным
			$('.overlay-scheme').removeClass('active');
			// скрыть попап дома
			$('.js-popup-on-scheme').css('display', 'none');
			// скрыть активную картинку дома
			$('scheme-item-image').remove();
        });

		/* on point list click */
        $pointsList.on('click', '.point-info', function(e) {
            e.preventDefault();

            var $listItem = $(this).parents('.points-list-item');
            currId = $listItem.attr('id');

            /* Scroll to clicked item */
            scrollableList.mCustomScrollbar("scrollTo", "#" + currId);

            $('.points-list-item').removeClass('active');
            $listItem.addClass('active');

            /* Select marker */
            $.each(markersList, function(idx, elt){
                if (elt.pointId == currId) {
                    google.maps.event.trigger(elt, 'click');
                }
            });

            if (modeMap === 'desktop') {
                exhApplication.activeInfoPlank(currId);
            } else if (modeMap === 'mobile') {
                exhApplication.closeInfoPlank();
            }

            map.setZoom(15);

        });

    },

    bindMoreMobile = function() {
        var $moreMobileBtn = $('.mobile-point-more');
        if ($moreMobileBtn.length) {
            $moreMobileBtn.on('click', function (e) {
                e.preventDefault();

                var $this = $(this);
                exhApplication.activeInfoPlank(currId);

            })
        }
    },

    // переключение вида площадки
    bindSwitchView = function(type) {
        $mapViewBtn = $mapWrapper.find('.js-sv-map'),
        $mapSchemeBtn = $mapWrapper.find('.js-sv-scheme');

        // on map switch click 
        $mapViewBtn.on('click', function() {
            console.log($mapViewBtn);
            if(!$(this).hasClass('active')) {
                $mapSchemeBtn.removeClass('active');
                $('.sv-scheme').removeClass('active');
                $schemeWrapper.removeClass('active');
                $(this).addClass('active');
            }
        });

        if(type == 'extended') {
            // on scheme switch click 
            $mapSchemeBtn.on('click', function() {
                if(!$(this).hasClass('active')) {
                    $mapViewBtn.removeClass('active');
                    // сделать активным переключатель на схему
                    $(this).closest('.sv-scheme').addClass('active');
                    // сделать неактивными все виды схем
                    $('.js-sv-scheme').removeClass('active');
                    // сделать активной нажатую ссылку
                    $(this).addClass('active');

                }

                if($(this).hasClass('js-view-small')) {
                    exhApplication.constructScheme(pointsList[currId]['scheme']);
                }
                if($(this).hasClass('js-view-big')) {
                    exhApplication.constructScheme(pointsList[currId]['sub_scheme']);
                }

                bindSchemePopupEvents();
            });
        }

        if(type == 'default') {
            // on scheme switch click 
            $mapSchemeBtn.on('click', function() {
                if(!$(this).hasClass('active')) {
                    $mapViewBtn.removeClass('active');
                    $(this).addClass('active');
                }

                exhApplication.constructScheme(pointsList[currId]['scheme']);
                bindSchemePopupEvents();
            });
        }

    },

    bindPrint = function() {
        $('.print').on('click', function(e) {
            e.preventDefault();
            setTimeout(function(){window.print();}, 300);
        });
    },

    /* клик по маркеру */
    bindMarkerEvents = function(marker, content){
        google.maps.event.addListener(marker, 'click', function(){
            currId = this.pointId;
            switchIcons(markersList, this);
            map.setZoom(15);
            scrollableList.mCustomScrollbar("scrollTo", "#" + this.pointId);
            $('.points-list-item').removeClass('active');
            $('#' + this.pointId).addClass('active');

            if (modeMap === 'desktop') {
                exhApplication.activeInfoPlank(this.pointId);
            } else if(modeMap === 'mobile') {

            }


            var point = pointsList[this.pointId];

            // скрыть переключатель вида
            //if($mapWrapper.find('.switch-view').length) {
            //    exhApplication.disableSwitchView();
            //}

            if (modeMap === 'desktop') {
                if('sub_scheme' in point) {
                    exhApplication.constructSwitchView('extended');
                } else {
                    exhApplication.constructSwitchView('default');
                }
            }

			// сделать оверлей схемы неактивным
			$('.overlay-scheme').removeClass('active');
			// скрыть попап дома
			$('.js-popup-on-scheme').css('display', 'none');
			// скрыть активную картинку дома
			$('scheme-item-image').remove();

        });
    },

    /* клик по дому на схеме */
    bindSchemePopupEvents = function() {
        $('.js-scheme-item').on('click', function(e) {
        	e.preventDefault();
    		var idPopup = $(this).attr('href'),
    			$overlay = $('.overlay-scheme'),
    			schemeItem = '';

    		if(idPopup.length) {
    			showSchemeItemImage($(this));
    			$overlay.addClass('active');
    			showSchemePopup(idPopup);
    		}

    	});

		$('.close-popup-scheme').on('click', function() {
			$(this).closest('.js-popup-on-scheme').css('display', 'none');
			$('.overlay-scheme').removeClass('active');
			$('.scheme-item-image').remove();
		});
    },

    /* показать активный дом на схеме */
    showSchemeItemImage = function (schemeItem) {
    	var schemeItemPos = schemeItem.position(),
    		xSchemeItemPos = schemeItemPos.left,
    		ySchemeItemPos = schemeItemPos.top,
    		wSchemeItemPos = schemeItem.width(),
    		hSchemeItemPos = schemeItem.height(),
    		schemeHtml = $('#scheme-item-image').html(),
    		imageBG = schemeItem.data('scheme-item-image'),
    		schemeContent = $('.scheme-content');

			var $scheme = $(schemeHtml),
				$schemeItem = $($scheme[0]);

			$schemeItem.css('left', xSchemeItemPos);
			$schemeItem.css('top', ySchemeItemPos);
			$schemeItem.css('width', wSchemeItemPos);
			$schemeItem.css('height', hSchemeItemPos);
			$schemeItem.css('background', "url('" + imageBG + "') no-repeat");

			schemeContent.append($schemeItem);
    },

    showSchemePopup = function(idPopup) {
    	var $popup = $(idPopup),
    		coord;
    	if($popup.length) {
            console.log('Popup is present. Id: ' + idPopup + ', length: ' + $popup.length);
    		coord = getCoordinatesPopup(idPopup);

    		$popup.css('display', 'block');
    		$popup.css('opacity', '1');
    		$popup.css('visibility', 'visible');
    		$popup.css('left', coord['x'] + 'px')
    		$popup.css('top', coord['y'] + 'px')
    	}
    },

    getCoordinatesPopup = function(idPopup) {
    	var $popup = $(idPopup),
    		wPopup = $('.product-container').width(),
    		trigger = $('a[href=' + idPopup + ']'),
    		triggerPosition = trigger.position(),
    		xTrigger = triggerPosition.left,
    		wTrigger = trigger.width(),
    		container = $('.scheme-wrapper'),
    		wContainer = container.width(),
    		coord = [],
    		margin = 15;

    		if(wContainer - (xTrigger + wTrigger) > wPopup) {
    			coord['x'] = xTrigger + wTrigger + margin;
    		} else {
    			coord['x'] = xTrigger - wPopup - margin;
    		}

    		coord['y'] = 70;

    		return coord;
    },

    switchIcons = function(markersList, currentMarker){
        $.each(markersList, function(idx, marker){
            marker.setIcon(markerSmall);
        });
        currentMarker.setIcon(markerBig);
        map.panTo(currentMarker.getPosition());
    };

    exhApplication.init = function ($map, mode) {

        // режим карты - мобильная или десктопная (desktop, mobile)
        modeMap = mode;

        if (modeMap === 'desktop') {
            $overlay = $('.exhibition-wrapper > .overlay');
            $schemeWrapper = $('.exhibition-wrapper').find('.scheme-wrapper');
            $mapWrapper = $('.exhibition-wrapper').find('.map-wrapper');
        } else if (modeMap === 'mobile') {
            $overlay = $('.exhibition-wrapper-mobile > .overlay');
            $schemeWrapper = $('.exhibition-wrapper-mobile').find('.scheme-wrapper');
            $mapWrapper = $('.exhibition-wrapper-mobile').find('.map-wrapper');
        }

        var lat = $map.attr('data-lat'),
        	long = $map.attr('data-long'),
        	style = [
					    {
					        "featureType": "administrative",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "poi",
					        "stylers": [
					            {
					                "visibility": "simplified"
					            }
					        ]
					    },
					    {
					        "featureType": "road",
					        "stylers": [
					            {
					                "visibility": "simplified"
					            }
					        ]
					    },
					    {
					        "featureType": "water",
					        "stylers": [
					            {
					                "visibility": "simplified"
					            }
					        ]
					    },
					    {
					        "featureType": "transit",
					        "stylers": [
					            {
					                "visibility": "simplified"
					            }
					        ]
					    },
					    {
					        "featureType": "landscape",
					        "stylers": [
					            {
					                "visibility": "simplified"
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "stylers": [
					            {
					                "visibility": "on"
					            }
					        ]
					    },
					    {
					        "featureType": "road.local",
					        "stylers": [
					            {
					                "visibility": "on"
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "visibility": "on"
					            }
					        ]
					    },
					    {
					        "featureType": "road.arterial",
					        "stylers": [
					            {
					                "visibility": "on"
					            }
					        ]
					    },
					    {
					        "featureType": "water",
					        "stylers": [
					            {
					                "color": "#5f94ff"
					            },
					            {
					                "lightness": 26
					            },
					            {
					                "gamma": 5.86
					            }
					        ]
					    },
					    {},
					    {
					        "featureType": "road.highway",
					        "stylers": [
					            {
					                "weight": 0.6
					            },
					            {
					                "saturation": -85
					            },
					            {
					                "lightness": 61
					            }
					        ]
					    },
					    {
					        "featureType": "road"
					    },
					    {},
					    {
					        "featureType": "landscape",
					        "stylers": [
					            {
					                "hue": "#0066ff"
					            },
					            {
					                "saturation": 74
					            },
					            {
					                "lightness": 100
					            }
					        ]
					    }
					],
			mapOptions = {
                center: new google.maps.LatLng(lat, long),
                zoom: initialZoom,
                disableDefaultUI: true,
                zoomControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                // styles:style,
                scrollwheel: false
            };

        bounds = new google.maps.LatLngBounds();
        map = new google.maps.Map($map[0],mapOptions);
        hashAreaId = window.location.hash.replace("#","");
        exhApplication.getPoints();
        bindEvents(map);

        google.maps.event.addListener(map, 'click', function () {
            map.setOptions({scrollwheel: true});
        });
    }

    exhApplication.disableSwitchView = function () {
        if (modeMap === 'desktop') {
            console.log($mapViewBtn);
            $mapViewBtn.off('click');
            $mapSchemeBtn.off('click');
            $('.switch-view').remove();
        }
    };

    exhApplication.getPoints = function () {
        $.ajax({
            url: '/ajax/ajax_get_points.php/',
            type: 'GET',
            dataType: 'json',
            beforeSend: function() {
                // $('.exhibition-wrapper > .overlay').addClass('active');
                $overlay.addClass('active');
            },
            success: function(data){
                if (!$.isEmptyObject(data)) {

                    pointsList = data;

                    var latLng = new google.maps.LatLng(pointsList[1]['lat'], pointsList[1]['long']);
                    map.panTo(latLng);

                    exhApplication.constructPoints(map, bounds);
                    exhApplication.buildPointsList();
                    if(hashAreaId) {
                        $pointsList.find('[data-area-id="' + hashAreaId + '"]').find('.point-info').trigger('click');
                    }

                    console.log(pointsList);

                }
                // $('.exhibition-wrapper > .overlay').removeClass('active');
                $overlay.removeClass('active');
            }
        })
    };

    exhApplication.resetBounds = function(){
        $.each(pointsList, function(idx, elt) {
            // Размещаем маркеры на карте
            var latLng = new google.maps.LatLng(elt.lat, elt.long);
            bounds.extend(latLng);
        });
        map.fitBounds(bounds);
    };

    exhApplication.constructPoints = function (map, bounds) {
        $.each(pointsList, function(idx, elt) {
        	// Размещаем маркеры на карте
            var latLng = new google.maps.LatLng(elt.lat, elt.long);
            var contentString = '';

            var marker = new google.maps.Marker({
                position: latLng,
                icon: markerSmall
            });

            bindMarkerEvents(marker);

            marker.setMap(map);
            bounds.extend(latLng);
            $.extend(marker, elt);
            markersList.push(marker);
        });
    };

    exhApplication.constructSwitchView = function(type) {
        var $switchView = '';

        if(type == 'extended') {
            $switchView = $($('#switch-view-extended').html());
        }
        if(type == 'default') {
            $switchView = $($('#switch-view').html());
        }

        $mapWrapper.append($switchView);
        bindSwitchView(type);
    };

    exhApplication.constructScheme = function (scheme) {

    	var schemeСontent = $mapWrapper.find('.scheme-content'),
    		schemeHtml = $('#scheme-tpl').html();

    	/* Очистить сехму */
    	schemeСontent.empty();

        if(scheme['scheme_img']) {

            schemeСontent.css('background', '#efefef url(\'' + scheme['scheme_img'] + '\') no-repeat left top');

            if(!$.isEmptyObject(scheme['objects'])) {
                $.each(scheme['objects'], function(ind, obj) {

                    var $scheme = $(schemeHtml),
                        $schemeItem = $($scheme[0]);

                    $schemeItem.css({
                        'width': obj.width + 'px',
                        'height': obj.height + 'px',
                        'top': obj.y + 'px',
                        'left': obj.x + 'px'
                    });

                    $schemeItem.attr('href', '#popup-on-scheme' + ind);
                    $schemeItem.attr('data-scheme-item-image', obj.scheme_item_image);

                    $($scheme[2]).attr('id', 'popup-on-scheme' + ind);
                    $scheme.find('.js-product-image').attr('src', obj.image);
                    $scheme.find('.js-product-image').text(obj.image);
                    $scheme.find('.js-product-name').text(obj.name);
                    $scheme.find('.js-product-technology').text(obj.technology);
                    $scheme.find('.js-product-area').text(obj.area);
                    $scheme.find('.js-product-size').text(obj.size);
                    $scheme.find('.js-product-old-price').text(obj.oldPrice);
                    $scheme.find('.js-product-price').text(obj.price);
                    $scheme.find('.js-product-link').attr('href', obj.href);

                    schemeСontent.append($scheme);
                })
            }
        } else {
            schemeСontent.css('background', '#efefef');
        }

        $schemeWrapper.addClass('active');
    	return false;

    };

    exhApplication.buildPointsList = function () {
        var listItems = $.map(markersList, function(elt, idx) {
            var pointId = elt.pointId,
                name = elt.name,
                address = elt.address,
                areaId = elt.areaId,
                active, 
                item;

            if (idx == 0) {
                active = 'active';
            } else {
                active = '';
            }

            item = '<li class="points-list-item" id="' + pointId + '" data-area-id="' + areaId + '">' +
                        '<a class="point-info" href="#">' +
                            '<div class="point-info-cnt">' +
                                '<p class="point-name">' + name + '</p>' +
                                '<p class="point-address">' + address + '</p>' + 
                            '</div>' +
                        '</a>' + 
                        '<a href="#" class="mobile-point-more">Подробнее</a>' + 
                    '</li>';

            return item;

        });

        $pointsList.empty().append(listItems);
        if (modeMap === 'desktop') {
            scrollableList.mCustomScrollbar();
        } else if (modeMap === 'mobile') {
            scrollableList.mCustomScrollbar({
                axis:"x",
                advanced:{autoExpandHorizontalScroll:true}
            });

            bindMoreMobile();
        }

    }

    exhApplication.activeInfoPlank = function (id) {
    	exhApplication.closeInfoPlank();
    	exhApplication.constructInfoPlank(id);
    	exhApplication.openInfoPlank();
    }

    exhApplication.closeInfoPlank = function () {
    	$infoPlank.removeClass('active');
    }

    exhApplication.openInfoPlank = function () {
    	$infoPlank.addClass('active');
    }

    exhApplication.constructInfoPlank = function (id) {
    	var infoPoint;

    	$.each(pointsList, function(ind, elt) {
    		if(elt.pointId == id) {
    			infoPoint = '<p class="point-name">' + elt.name + '</p>' + 
							'<div class="func">' + 
								'<div class="columns">';

                if(elt.virtual) {
                    infoPoint += '<div class="col2">' + 
                                    '<a href="' + elt.virtual + '" class="tour">' + 
                                        'Виртуальный тур по площадке' + 
                                    '</a>' + 
                                 '</div>';
                }

                infoPoint +=	'<div class="col2">' + 
										'<a href="#" class="print">' + 
											'Распечатать' + 
										'</a>' + 
									'</div>' + 
								'</div>' + 
							'</div>' + 
							'<p class="point-address">' + elt.address + '</p>' + 
							'<div class="point-timework">' + 
								'<div class="columns">' + 
									'<div class="col2">' + 
										'<div class="caption-exh-block">' + 
											'Режим работы' + 
										'</div>' + 
                                        '<p class="timework">' + elt.workTime + '<br />';

                if(elt.firstDay) {
                    infoPoint += elt.firstDay;
                    if(elt.lastDay) {
                        infoPoint += '-' + elt.lastDay;
                    }
                }

                infoPoint += '</p>';

				infoPoint +=    	'</div>' + 
									'<div class="col2">';

                if(elt.firstDayAdm.length > 0 || 
                   elt.lastDayAdm.length > 0|| 
                   elt.workTimeAdm.length > 0) {

                    infoPoint +=        '<div class="caption-exh-block">' + 
                                            'Администрация' + 
                                        '</div>' + 
                                        '<p class="timework">';
                    if(elt.firstDay) {
                        infoPoint += elt.firstDayAdm;

                        if(elt.lastDayAdm) {
                            infoPoint += '-' + elt.lastDayAdm
                        }
                    }

                    if(elt.workTimeAdm) {
                        if(elt.firstDayAdm.length > 0 || elt.lastDayAdm.length > 0) {
                            infoPoint += ',<br />' + elt.workTimeAdm;                            
                        } else {
                            infoPoint += elt.workTimeAdm;                            
                        }

                    }

                    infoPoint +=               '</p>' + 
                                            '</div>';
                }

                infoPoint += '</div></div>';


                if(!$.isEmptyObject(elt.phones)) {
                    infoPoint += '<div class="point-phones">' + 
                                    '<div class="caption-exh-block">' + 
                                        'Телефоны отделов' + 
                                    '</div>' + 
                                    '<ul class="point-list-phones">';

                    $.each(elt.phones, function(ind, elt) {
                        infoPoint += '  <li>' + 
                                            '<div class="columns">' + 
                                                '<div class="col2">' + 
                                                    ind + 
                                                '</div>' + 
                                                '<div class="col2">' + 
                                                    elt + 
                                                '</div>' + 
                                            '</div>' + 
                                        '</li>';
                    });

                    infoPoint +=   '</ul>' + 
                                 '</div>';

                }

                if(elt.driving_direction) {
                    infoPoint += '<div class="driving-direction">' + 
                                 '  <div class="caption-exh-block">' + 
                                 '    Как добраться' + 
                                 '  </div>' + 
                                 '  <p class="point-address">' + 
                                      elt.driving_direction + 
                                 '  </p>' + 
                                 '</div>';
                }

				return false;
    		}
    	});
		$('.info-exh').empty().append(infoPoint);

        // инициализация скролла
        $('.info-exh-for-scroll').mCustomScrollbar();

        bindPrint();
    }

    return exhApplication;

})(jQuery, window);
var buildsApplication = (function($, _window){
    var buildsApplication = {},
    	$exampleItem = $('.builds-item'),
    	$exampleList = $('.build-list'),
    	$seeMore = $('.show-stock'),        
    	buildItem = [],
    	buildList = [],
    	buildId,
        ajaxStatus = 0,
    	activePopup = false,
    	sliderPopup,
        overlayHtml = $('#overlay-tpl').html(),
        overlayTpl = $(overlayHtml),
        modeBuilds = '';

    var unbindEvents = function() {
        $exampleItem.unbind('click');
        // $seeMore.unbind('click');
    };

    var unbindSeeMore = function() {
        $seeMore.unbind('click');
    };

    var bindEvents = function() {
    	// клик на элементе списка
    	$exampleItem.on('click', function (e) {
    		e.preventDefault();
       		buildId = $(this).attr('href');
            $(this).append(overlayTpl);
    		buildsApplication.getBuildItem(buildId);
    	});
    };

    var bindSeeMore = function() {
        // клик на кнопке показать еще
        $seeMore.on('click', function (e) {
            e.preventDefault();
            if(!ajaxStatus) {
                buildsApplication.getBuildList();                
            }
        })
    };

    var bindControls = function() {
	    var $prevBtn = $('#example-prev'),
    		$nextBtn = $('#example-next'),
    		$popupContent = $('.popup-build-content');

    	$prevBtn.on('click', function(e) {
    		e.preventDefault();

    		var nextBuildsItem = $('a.builds-item[href=\'' + buildId + '\']').closest('li').prev().find('.builds-item');

    		if(nextBuildsItem.length) {
    			buildId = nextBuildsItem.attr('href');
    		} else {
    			buildId = $('.build-list li:last-child').find('.builds-item').attr('href');
    		}

			buildsApplication.showOverlay();
    		$popupContent.empty();
    		buildsApplication.getBuildItem(buildId);

    	});

    	$nextBtn.on('click', function(e) {
    		e.preventDefault();

    		var nextBuildsItem = $('a.builds-item[href=\'' + buildId + '\']').closest('li').next().find('.builds-item');

    		if(nextBuildsItem.length) {
    			buildId = nextBuildsItem.attr('href');
    		} else {
                buildId = $exampleList.find('li:first-child').find('.builds-item').attr('href');
    		}

			buildsApplication.showOverlay();
			$popupContent.empty();
    		buildsApplication.getBuildItem(buildId);
    	});
    };

    buildsApplication.init = function (mode) {
        if (!modeBuilds) {
            modeBuilds = mode;
        }

        $seeMore.hide();
        $exampleItem = $('.builds-item');
        buildsApplication.checkBuildList();
        unbindEvents();
        unbindSeeMore();
        bindEvents();
        bindSeeMore();
    };

    buildsApplication.checkBuildList = function() {
        var ids = $('#ids').val();
        $.ajax({
            url:'/ajax/ajax_get_build_list.php/',
            type: 'POST',
            dataType: 'json',
            data: $('#filter').serialize()+'&ids='+ids,
            beforeSend: function() {
                ajaxStatus = 1;
            },
            success: function(data) {
                if(data['build_list']['list']) {
                    $seeMore.show();
                }

                ajaxStatus = 0;
            }
        });
    };

    buildsApplication.getBuildList = function() {
    	var $newItems;       
        var ids = $('#ids').val();
        $.ajax({
            url:'/ajax/ajax_get_build_list.php/',
            type: 'POST',
            dataType: 'json',
            data: $('#filter').serialize()+'&ids='+ids,
            beforeSend: function() {
                $seeMore.addClass('disabled');
                ajaxStatus = 1;
            },
            success: function(data) {

                $('#ids').val(ids + ',' + data['build_list']['ids']);

                if(data['build_list']['list']) {
                    buildsList = data['build_list']['list'];
                    $newItems = buildsApplication.constructList(buildsList);
                    $exampleList.append($newItems);
                    buildsApplication.init();
                }

                $seeMore.removeClass('disabled');

                var itemsCount = parseInt($exampleList.find('li').length);

                if(parseInt(data['build_list']['count']) <= itemsCount) {
                    $seeMore.hide();
                }

                ajaxStatus = 0;

            }
        });
    };

    buildsApplication.getBuildItem = function(buildId) {
        $.ajax({
            url:'/ajax/ajax_get_build_item.php/',
            type: 'POST',
            data: {buildId: buildId},
            dataType: 'json',
            success: function(data) {
            	buildItem = data['build_item'];

                $('.builds-item[href=' + buildId + ']').find('.overlay').remove();

               	if(activePopup) {
       		    	var $popupContent = $('.popup-build-content');
       		    	$popupContent.empty();
               		popupTpl = buildsApplication.constructPopupContent(buildItem);
               		$popupContent.append(popupTpl);
              		buildsApplication.initSlider(popupTpl);
					buildsApplication.hideOverlay();
               	} else {
	               	popupTpl = buildsApplication.constructPopup(buildItem);

                    if (modeBuilds === 'desktop') {
                        $.fancybox.open([popupTpl], {
                            padding: 0,
                            scrolling: 'no',
                            fitToView: false,
                            'helpers': {
                                overlay: {
                                    locked: false
                                }
                            },
                            afterLoad: function() {

                                buildsApplication.showOverlay();
                            },
                            afterShow: function() {
                                // console.log('reload');
                                // sliderPopup.reloadSlider();
                                buildsApplication.initSlider(popupTpl);
                                buildsApplication.hideOverlay();
                                activePopup = true;
                            },
                            afterClose: function() {
                                activePopup = false;
                            }
                        });
                    } else if (modeBuilds === 'mobile') {

                        $.fancybox.open([popupTpl], {
                            padding: 0,
                            margin: [70, 0, 0, 0],
                            scrolling: 'no',
                            fitToView: false,
                            minWidth: 640,
                            maxWidth: 640,
                            tpl: {
                                closeBtn: '<div class="fancybox__close__exp"><a class="fbx__close" href="#"> </a></div>'
                            },
                            'helpers': {
                                overlay: {
                                    locked: false
                                }
                            },
                            afterLoad: function() {
                                buildsApplication.showOverlay();
                            },
                            afterShow: function() {
                                // console.log('reload');
                                // sliderPopup.reloadSlider();
                                buildsApplication.initSlider(popupTpl);
                                buildsApplication.hideOverlay();
                                activePopup = true;
                            },
                            afterClose: function() {
                                activePopup = false;
                            }
                        });

                    }

                    bindControls();

               	}

            }
        })
    };

    buildsApplication.constructPopup = function(buildsItem) {
    	var popupTplWrapper = $($('#build-popup-wrapper-tpl').html()),
    		popupTpl = buildsApplication.constructPopupContent(buildsItem);
    	popupTplWrapper.append(popupTpl);
    	return popupTplWrapper;
    };

    buildsApplication.constructList = function(buildsList) {
        var $builds = $('<div></div>');

        $.each(buildsList, function(ind, elt) {
            var listTpl = $($('#build-list-tpl').html());

            listTpl.find('.js-build-id').attr('href', '#' + elt.id);
            listTpl.find('.js-build-img').attr('src', elt.img);
            listTpl.find('.js-build-name').text(elt.name);

            $builds.append(listTpl);

        });

        return $builds.find('li');
    };

    buildsApplication.constructPopupContent = function(buildsItem) {
    	var popupHtml = $('#build-popup-tpl').html(),
    		popupTpl = $(popupHtml),
    		images = buildsItem['images'],
    		params = buildsItem['params'],
            series = buildsItem['series'],
    		nameParams = '',
    		valueParams = '',
            linkProduct =buildsItem['link']
    		// video = buildsItem['video'],
    		// review = buildsItem['review'],
    		// reviewText = ''
            ;

    	// вставка слайдера в шаблон
/*
        $.each(images, function (ind, elt) {

            var sliderListHtml = $('#slider-popup-tpl').html(),
                sliderListTpl = $(sliderListHtml),
                pagerSliderHtml = $('#pager-slider-tpl').html(),
                pagerSliderTpl = $(pagerSliderHtml);

            // Список для слайдера
            sliderListTpl.find('.js-img-slide').attr('src', elt['src']);
            sliderListTpl.find('.js-img-slide').addClass(elt['img_class']);
            popupTpl.find('.js-build-slider').append(sliderListTpl);

            // Список для пагинации 
            pagerSliderTpl.attr('data-slide-index', ind);
            pagerSliderTpl.find('.js-img-pgn').attr('src', elt['src']);
            popupTpl.find('#bx-pager-build').append(pagerSliderTpl);

        });
*/

        // cover image
    	$.each(images, function (ind, elt) {

    		var sliderListHtml = $('#slider-popup-tpl-bg').html(),
    			sliderListTpl = $(sliderListHtml),
    			pagerSliderHtml = $('#pager-slider-tpl').html(),
    			pagerSliderTpl = $(pagerSliderHtml);

    		// Список для слайдера
            sliderListTpl.find('.js-img-slide').css('background-image', 'url("' + elt['src'] + '")');
            // добавить класс full-width-image или full-height-image
            // sliderListTpl.find('.js-img-slide').addClass(elt['img_class']);
    		popupTpl.find('.js-build-slider').append(sliderListTpl);

    		// Список для пагинации 
    		pagerSliderTpl.attr('data-slide-index', ind);
    		pagerSliderTpl.find('.js-img-pgn').attr('src', elt['src']);
    		popupTpl.find('#bx-pager-build').append(pagerSliderTpl);

    	});

    	// Вставка имени
    	popupTpl.find('.js-build-name').text(buildsItem.name);

        // Вставка ссылки на все дома серии
        /*console.log(series);
        if(series == '/examlpes//') {
            console.log(series);
            series = '/examples/';
        }*/
///console.log(series == '/examples//');
        if(series == '/examples//') {
            series = '/examples/';
        }

        if(series) {
            popupTpl.find('.js-build-series').attr('href', series);
        } else {
            popupTpl.find('.js-build-series').remove();
        }

    	// Параметры дома
        if(!$.isEmptyObject(params)) {
            $.each(params, function (ind, elt) {
                nameParams  += '<p>' + ind + '</p>';
                valueParams += '<p>' + elt + '</p>';
            });
            popupTpl.find('.js-pp-left').append(nameParams);
            popupTpl.find('.js-pp-right').append(valueParams);
        } else {
            popupTpl.find('.product-params').remove();
        }

        // Ссылка нп продукт
        if(linkProduct) {
            popupTpl.find('.btn').attr('href', linkProduct);
        } else {
            popupTpl.find('.btn').remove();
        }

    	// Видео
    	// popupTpl.find('.js-video-link').attr('src', video['link']);
    	// popupTpl.find('.js-video-img').attr('src', video['img']);
    	// popupTpl.find('.js-video-capt').attr('src', video['text']);

    	// Отзыв
    	// reviewText += review['text'] + '<p class="sign">' + review['user'] + ', ' + 
    	// 			  '<span>' + review['city'] + '</span></p>';
    	// popupTpl.find('.js-review-text').append(reviewText);

    	return popupTpl;

    };

    buildsApplication.initSlider = function(popupTpl) {
        // console.log('initSlider');
    	var $slider = popupTpl.find('.build-slider'),
            controls = false,
            slideForView = 6,
            $bxNext,
            $bxPrev,
            slideCount = $('#bx-pager-build a').length;;

    	if($slider.length) {


            if (modeBuilds === 'desktop') {

                sliderPopup = $slider.bxSlider({
                    auto: false,
                    pagerCustom: '#bx-pager-build',
                    controls: false,
                    mode: 'fade',
                    onSliderLoad: function() {
                        // console.log('init onSliderLoad');


                        if(slideCount > 6) {
                            controls = true;

                            $('#bx-pager-build').bxSlider({
                                            'useCSS':true,
                                            'controls':controls,
                                            'pager':false,
                                            'infiniteLoop':false,
                                            'minSlides': slideForView,
                                            'maxSlides': slideForView + 1,
                                            'moveSlides': 1,
                                            'slideWidth': 130,
                                            'slideMargin': 22,
                                            hideControlOnEnd: true
                                            //onSliderLoad: function() {
                                            //    $bxNext = $('.bx-next'),
                                            //    $bxPrev = $('.bx-prev');
                                            //    $bxPrev.hide();
                                            //},
                                            //onSlideNext: function($slideElement, oldIndex, newIndex) {
                                            //    if(slideCount - slideForView == newIndex) {
                                            //        $bxNext.hide();
                                            //    }
                                            //    $bxPrev.show();
                                            //},
                                            //onSlidePrev: function($slideElement, oldIndex, newIndex) {
                                            //    if(newIndex == 0) {
                                            //        $bxPrev.hide();
                                            //    }
                                            //    $bxNext.show();
                                            //}
                                        });
                        } else {
                            $('.preview-slider').addClass('no-slider');
                        }

                    }
                });

            } else if (modeBuilds === 'mobile') {

                if(slideCount > 1) {
                    sliderPopup = $slider.bxSlider({
                        auto: false,
                        pager: false,
                        controls: false,
                        minSlides: 1,
                        maxSlides: 1,
                        slideWidth: 600,
                        slideMargin: 10,
                        onSliderLoad: function() {
                            $('.bx-wrapper').css('margin', '0');
                            $('.bx-wrapper').css('width', 'auto');
                            $('.bx-wrapper').css('max-width', '640px');
                        }
                    });
                }

            }

    	}
    };

    buildsApplication.showOverlay = function() {
    	var $overlay = $('.popup-build .overlay');

    	$overlay.addClass('active');
    };

    buildsApplication.hideOverlay = function() {
    	var $overlay = $('.popup-build .overlay');

    	$overlay.removeClass('active');
    };

    return buildsApplication;

})(jQuery, window);

// news time line application
var ntlApplication = (function($, _window){
    var ntlApplication = {},
        $newsTimelineWrapper = $('.news-timeline-wrapper'),
        $swiperContainer = $('.news-timeline-wrapper .swiper-container'),
        $swiperWrapper = $swiperContainer.find('.swiper-wrapper'),
        timeLineSlider = '',
        $prevSlideBtn = $('.ntl-prev'),
        $nextSlideBtn = $('.ntl-next'),
        newsList = [],
        $newsItems,
        $detailNewsContainer = $('.detail-news-container'),
        dateDelimeter = '.',

        widthTooltipNews = 300,
        sizeNews = 20,
        currPeriod = false;
        // centerCoord = $swiperContainer.width()/2;

        // сфоримировать слайды по месяцам
    var constructSlides = function() {
            $.each(newsList, function(index, month) {
                var $slide = $('<div>', {class: 'swiper-slide'}),
                    $slideContent = $('<div>', {class: 'slide-content'}),
                    $periodNews = $('<div>', {class: 'period-news'}),
                    css_sc = {},
                    arrMonth = strToArr(month['period'], dateDelimeter),
                    daysInMonth = countDaysInMonth(arrMonth[1], arrMonth[0]);

                css_sc = {
                    width: sizeNews * daysInMonth + 'px'
                }

                applyCss($slideContent, css_sc);

                // добавление подписи
                $periodNews.text(moment([arrMonth[1],arrMonth[0] - 1]).format("MMMM YYYY"));
                $slideContent.append($periodNews);

                // добавление новостей на слайд
                addNewsInSlide($slideContent, index, month['news']);

                if(index == newsList.length - 1) {
                    $slideContent.addClass('last');
                }

                $slide.append($slideContent);
                $swiperWrapper.append($slide);

            });
        },

        // добавить детальную новость
        constructDetailNews = function(monthId, newsId) {
            // console.log('constructDetailNews');

            $detailNewsContainer.empty();
            $detailNewsContainer.removeClass('active');

            var news = newsList[monthId]['news'][newsId],
                $newsDetail = $($('#detail-news').html()),
                dateInArray = strToArr(news['date'], dateDelimeter);

                // console.log('news', news);

                $newsDetail.find('.js-tl-link').attr('href', news['link']);

                if(!news['img']) {
                    $newsDetail.find('.image-wrapper').remove();
                    $newsDetail.find('.js-tl-image').remove();
                } else {
                    $newsDetail.find('.js-tl-image').attr('src', news['img']);
                }

                $newsDetail.find('.js-tl-title').html(news['title']);
                $newsDetail.find('.js-tl-date').text(getMonthInWords(dateInArray));
                // $newsDetail.find('.js-tl-text').text(news['text']);
                $newsDetail.find('.js-tl-text').html(news['text']);

                $detailNewsContainer.append($newsDetail);
                setTimeout(function() { 
                    $detailNewsContainer.addClass('active');                    
                }, 50);
        },

        // добавить новости на таймлайн
        addNewsInSlide = function($slideContent, monthId, news) {
            $.each(news, function(index, element) {
                var css_nws = {},
                    day = strToArr(element['date'], dateDelimeter),
                    dateNews = strToArr(element['date'], dateDelimeter),
                    $newsItem = $('<a>', {class: 'news-item', href: '#'}),
                    $ttNews = $('<div>', {class: 'tooltip-news'}),
                    $ttDate = $('<p>', {class: 'date'}),

                    // $ttImage = $('<div>', {class: 'cover-image'}),
                    $ttImage = $('<div>', {class: 'contain-image'}),
                    $ttImageWrp = $('<div>', {class: 'image-wrapper'}),


                    $ttTitle = $('<p>', {class: 'title'});

                $ttDate.html(getMonthInWords(dateNews));
                $ttTitle.html(element['title']);

                // $ttImage.attr('backgorund-image', 'url(' + element['img'] + ')');
                if(element['img']) {
                    $ttImage.attr('style', 'background-image: url(' + element['img'] + ')');
                    $ttImageWrp.append($ttImage);
                    $ttNews.append($ttImageWrp);
                }

                $ttNews.append($ttDate);
                $ttNews.append($ttTitle);

                $newsItem.append($ttNews);

                css_nws = {
                    left: (day[0] - 1) * sizeNews + 'px'
                }

                applyCss($newsItem, css_nws);
                $newsItem.attr('data-monthId', monthId);
                $newsItem.attr('data-newsId', index);

                $slideContent.append($newsItem);

            });
        },
 
        // инициализировать видимые тултипы
        initShowTooltip = function($news, position) {
            var content = $news.html();

            $news.tooltipster({
                content: $(content),
                theme: 'tooltipster-news',
                minWidth: widthTooltipNews,
                maxWidth: widthTooltipNews,
                position: position,
                interactive: true,
                speed: 50,
                hideOnClick: true,
                // trigger: 'hover',
                autoClose: false,
                offsetY: 20,
                // updateAnimation: true,
                functionReady: function(origin, tooltip) {
                    bindTooltipNewsClick(origin, tooltip);
                    if (position == 'top') {
                        var hTooltip = tooltip.outerHeight(),
                            arrowTop = tooltip.find('.tooltipster-arrow-top');

                        arrowTop.css('top', hTooltip);
                    }

                },
                functionBefore: function(origin, continueTooltip) {
                    // если детальная новость не показана, то
                    // показываем тултип
                    if(!origin.hasClass('active')) {
                        continueTooltip();
                    }
                }
            });            
        },

        // инициализировать скрытые тултипы
        initHideTooltip = function($news) {
            var content = $news.html();

            $news.tooltipster({
                content: $(content),
                theme: 'tooltipster-news',
                minWidth: widthTooltipNews,
                maxWidth: widthTooltipNews,
                position: 'top',
                // contentAsHTML: true,
                interactive: true,
                speed: 50,
                offsetY: 20,
                functionReady: function(origin, tooltip) {
                    bindTooltipNewsClick(origin, tooltip);

                    var hTooltip = tooltip.outerHeight(),
                        arrowTop = tooltip.find('.tooltipster-arrow-top');

                    arrowTop.css('top', hTooltip);

                },
                functionBefore: function(origin, continueTooltip) {
                    // если детальная новость показана, то
                    // показываем тултип
                    if(!origin.hasClass('active')) {
                        continueTooltip();
                    }

                    origin.addClass('show-hide-tooltip');

                    if(!origin.hasClass('show-tooltip')) {
                        hideCrossTooltip(origin);
                    }
                },
                functionAfter: function(origin) {
                    origin.removeClass('show-hide-tooltip');
                    showTooltip();

                }
            });
        },

        // инициализировать новостной слайдер
        initTimeLineSlider = function() {
            widthSwiperContainer = $newsTimelineWrapper.outerWidth() - $prevSlideBtn.outerWidth() - $nextSlideBtn.outerWidth();
            $swiperContainer.width(widthSwiperContainer);

            timeLineSlider = new Swiper('.news-timeline-wrapper .swiper-container', {
                freeMode: true,
                // freeModeFluid: true,
                loop: false,
                slidesPerView: "auto",
                centeredSlides: true,
                calculateHeight: true,
                resistance: '100%',
                noSwiping: true,
                onSwiperCreated: function(swiper) {

                    currPeriod = swiper.activeIndex;

                    controlPrevNextButton();

                    // $nextSlideBtn.show();
                    // if(swiper.activeIndex) {
                    //     $prevSlideBtn.show();
                    // }

                    // клик по стрелке назад
                    $prevSlideBtn.on('click', function(e) {
                        e.preventDefault();

                        // $nextSlideBtn.show(200);

                        // убрать активные тултипы
                        hideTooltip();

                        // к какому слайду двигать
                        var currSlide;
                        if(currPeriod && currPeriod != swiper.activeIndex) {
                            currSlide = currPeriod;
                        } else {
                            currSlide = swiper.activeIndex;
                        }
                        var toIndex = currSlide != 0 ? parseInt(currSlide) - 1 : 0;

                        // подвинуть слайд если он не нулевой
                        swiper.swipeTo(toIndex, 300, true);


                        if(toIndex == 0) {
                            $(this).hide(200);
                        }

                        // текущий период
                        currPeriod = toIndex;

                        controlPrevNextButton();

                        // сброс
                        resetActiveNews();

                    });

                    // клик по стрелке вперед
                    $nextSlideBtn.on('click', function(e) {
                        e.preventDefault();

                        // $prevSlideBtn.show(200);

                        // убрать активные тултипы
                        hideTooltip();

                        // к какому слайду двигать
                        var currSlide;
                        if(currPeriod && currPeriod != swiper.activeIndex) {
                            currSlide = currPeriod;
                        } else {
                            currSlide = swiper.activeIndex;
                        }
                        var toIndex = currSlide != swiper.slides.length - 1 ? parseInt(currSlide) + 1 : swiper.slides.length - 1;

                        // подвинуть слайд если он не последний
                        swiper.swipeTo(toIndex, 300, true);

                        // скрыть стрелку на последнем слайде
                        var lastSlideIndex = swiper.slides.length - 1;
                        if(lastSlideIndex == toIndex) {
                            $(this).hide(200);
                        }

                        // текущий период
                        currPeriod = toIndex;

                        controlPrevNextButton();

                        // сброс
                        resetActiveNews();

                    })
                },
                onTouchStart: function(swiper) {
                    resetActiveNews();
                    destroyTooltip();
                    $swiperWrapper.removeClass('duration');
                },
                onTouchEnd: function(swiper) {
                    currPeriod = swiper.activeIndex;
                    controlPrevNextButton();
                    setTimeout(function() { 
                        activeTooltip();                        
                    }, 350);
                    $swiperWrapper.addClass('duration');
                },
                onSlideChangeEnd: function(swiper) {
                    destroyTooltip();
                    activeTooltip();
                }
            });
        },

        controlPrevNextButton = function() {
            var lastIndex = '';

            if(timeLineSlider) {
                lastIndex = timeLineSlider.slides.length - 1;
            }

            if(currPeriod === 0) {
                $prevSlideBtn.hide(200);
                $nextSlideBtn.show(200);
            }

            if(currPeriod && currPeriod != lastIndex) {
                $prevSlideBtn.show(200);
                $nextSlideBtn.show(200);
            }

            if(currPeriod === lastIndex) {
                $nextSlideBtn.hide(200);
            }

        },

        resetActiveNews = function() {
            // сделать точку новости неактивной
            $newsItems.removeClass('active');
            // очистить детальную новость
            $detailNewsContainer.empty();
            // сделать детальную новость неактивной
            $detailNewsContainer.removeClass('active');

            $newsTimelineWrapper.removeClass('dtl-news');
        },

        bindTooltipNewsClick = function($origin, $tooltip) {
            var monthId = $origin.attr('data-monthId'),
                newsId = $origin.attr('data-newsId'),
                different;

            var swPos = $('.swiper-wrapper').position(),
                swLeft = swPos.left;

            $tooltip.on('click', function() {

                // console.log('monthId', monthId, 'newsId', newsId);

                // сделать все новости неактивными
                resetActiveNews();

                // текущий период 
                currPeriod = monthId;

                // разница между центром и тултипстером
                different = getDifferent($origin);

                // подвинуть слайд
                timeLineSlider.setWrapperTranslate(different, 0, 0);

                // событие после передвижения слайдера
                // timeLineSlider.wrapperTransitionEnd(function() {
                    // сформировать детальную новость
                    // constructDetailNews(monthId, newsId); 
                // });

                // скрыть тултипстер
                $origin.tooltipster('hide');

                // уничтожить тултипы
                destroyTooltip();

                // сделать все тултипы скрытыми
                // initAllTooltipHide();

                setTimeout(function() { 
                    constructDetailNews(monthId, newsId); 
                    activeTooltip();
                }, 400);

                // сделать активной точку
                $origin.addClass('active');

                //
                controlPrevNextButton();

                $newsTimelineWrapper.addClass('dtl-news');

            });
        },

        bind = function() {
            if($newsItems.length) {
                $newsItems.on('click', function(e) {
                    e.preventDefault();
                });
            }
        },

        strToArr = function(str, delimeter) {
            return str.toString().split(delimeter);
        },
        
        countDaysInMonth = function(month, year) {
            return moment("'" + month + " " + year + "'", "YYYY MM").daysInMonth();
        },

        applyCss = function(element, css) {
            $.each(css, function(index, value) {
                element.css(index, value);
            });
        },

        getMonthInWords = function(dateInArray) {
            return  moment(
                        [
                            dateInArray[2],
                            dateInArray[1] - 1,
                            dateInArray[0]
                        ]
                    ).format("D MMMM YYYY");
        },

        getDifferent = function($pointNews) {
            var pointNewsPos = $pointNews.position(),
                halfWidthPointNews = Math.round($pointNews.width()/2),
                pointNewsParent = $pointNews.closest('.swiper-slide'),
                pointNewsParentPos = pointNewsParent.position(),
                centerCoord = $swiperContainer.width()/2;

            return centerCoord - pointNewsParentPos.left - pointNewsPos.left - halfWidthPointNews;
        },

        getActiveNewsPoint = function() {
            var $activePoint= '';
            $.each($('.news-item'), function(index, element) {
                if($(element).hasClass('active')) {
                    $activePoint = $(element);
                    return false;
                }
            });
            return $activePoint;
        },


        hideCrossTooltip = function(newsPoint) {
            var $visiblePopupNews = $('.show-tooltip'),
                currPos = getPositionNewsOverSlider(newsPoint),
                $news,
                arrCrossTooltipTop = [];

            $.each($visiblePopupNews, function(index, news) {
                $news = $(news);

                if($news.hasClass('top')) {

                    var newsPos = getPositionNewsOverSlider($news);

                    if(Math.abs(currPos - newsPos) < widthTooltipNews + 5) {
                        arrCrossTooltipTop.push($news);
                    }

                }

            });

            if(arrCrossTooltipTop.length > 0) {
                $.each(arrCrossTooltipTop, function (index, news) {
                    news.tooltipster('hide');
                });
            }

        },

        activeTooltip = function() {
            var $tooltips = $('.news-item'),
                // prevNewsPos = 0,
                prevNewsPosTop = 0,
                prevNewsPosBottom = 0,
                $prevNews,
                $prevNewsTop,
                $prevNewsBottom,
                currPos,
                position;

            // console.log("$tooltips", $tooltips);

            $.each($tooltips, function(index, news) {

                // показываем тултипы только на активном слайде ...
                if($(news).attr('data-monthid') == currPeriod) {

                    currPos = getPositionNewsOverSlider($(news));

                    if(prevNewsPosBottom == 0
                       || currPos - prevNewsPosBottom > widthTooltipNews + 5 
                       || ($prevNewsBottom.length && $prevNewsBottom.hasClass('active'))
                       ) {
                            $(news).removeClass('bottom');
                            $(news).removeClass('top');

                            prevNewsPosBottom = currPos;
                            $prevNewsBottom = $(news);
                            $(news).addClass('show-tooltip');
                            $(news).addClass('bottom');
                            initShowTooltip($(news), 'bottom');
                            $(news).tooltipster('show', false);

                    } else {

                        if(prevNewsPosTop == 0
                           || currPos - prevNewsPosTop > widthTooltipNews + 5
                           || ($prevNewsTop.length && $prevNewsTop.hasClass('active')) 
                           ) {

                            $(news).removeClass('bottom');
                            $(news).removeClass('top');

                            prevNewsPosTop = currPos;
                            $prevNewsTop = $(news);
                            $(news).addClass('show-tooltip');
                            $(news).addClass('top');
                            initShowTooltip($(news), 'top');
                            $(news).tooltipster('show', false);

                        } else {

                            initHideTooltip($(news));

                        }

                    }

                } else {
                    // ... на остальных скрытые
                    initHideTooltip($(news));
                }

            });
        },

        initAllTooltipHide = function() {
            var $tooltips = $('.news-item');

            $.each($tooltips, function(index, news) {
                initHideTooltip($(news));
            });
        },

        showTooltip = function() {
            var $tooltips = $('.show-tooltip'),
                $showHideTooltip = $('.show-hide-tooltip');

            if($showHideTooltip.length == 0) {
                $tooltips.tooltipster('show');
            }

        },

        hideTooltip = function() {
            var $tooltips = $('.news-item');
            $.each($tooltips, function(index, news) {
                if($(news).hasClass('show-tooltip')) {
                    $($(news)).tooltipster('hide');                    
                }
            });
        },

        destroyTooltip = function() {
            var $tooltips = $('.news-item');
            $tooltips.removeClass('show-tooltip');
            $.each($tooltips, function(index, news) {
                $($(news)).tooltipster('destroy');
            });
        },

        getPositionNewsOverSlider = function($news) {
            var 
                // позиция слайда родителя
                $pointNewsParent = $news.closest('.swiper-slide'),
                pointNewsParentPos = $pointNewsParent.position(),

                // позиция новости относительно слайда
                tooltipPos = $news.position();

                // позиция новости относительно слайдера
                positionNews = tooltipPos.left + pointNewsParentPos.left;

                return positionNews;
        };

    ntlApplication.init = function() {
        moment.locale('ru');
        newsList = window.news;

        // console.log('news ', newsList);

        // построение слайдов
        constructSlides();

        // инициализация слайдера
        initTimeLineSlider();

        // активировать тултипы
        activeTooltip();

        $newsItems = $('.news-item');

        // инициализация событий
        bind();

    }

    return ntlApplication;

})(jQuery, window);

var filterApplication = (function($, _window){
    var filterApplication = {},
    	$showAllButtons = $('.show-all-filter'),
    	$resetFilterBlockBtn = $('.reset-filter-block'),
    	$resetFilterBtn = $('.reset-filter'),
        $inputsForm = $('.filter-wrapper input'),
        $form = $('#filter_form'),
        $range = $('#range'),
        rangeFrom = $('#range-from-value').data('price'),
        rangeTo = $('#range-to-value').data('price'),
        $pipFrom = $('.pip-from'),
        $pipTo = $('.pip-to'),
        pipFromPrice = $pipFrom.data('price'),
        pipToPrice = $pipTo.data('price'),
        timeViewPopup = 5000,
        popupFilterCountInterval,

        $productsListWrapper = $('.products-list-wrapper'),
        $showMoreBtn = $('.show-all'),

        initNoUiSlider = function() {
            if($range.length) {
                $pipFrom.text(accounting.formatNumber(pipFromPrice, 0, " ", ","));
                $pipTo.text(accounting.formatNumber(pipToPrice, 0, " ", ","));

                $range.noUiSlider({
                    start: [rangeFrom, rangeTo],
                    step: 100,
                    connect: true,
                    range: {
                        'min': pipFromPrice,
                        'max': pipToPrice
                    }
                });

                $range.Link('lower').to($('#range-from-value'), null, wNumb({
                    decimals: 0,
                    thousand: ' '

                }));
                $range.Link('upper').to($('#range-to-value'), null, wNumb({
                    decimals: 0,
                    thousand: ' '
                }));

                $('#range-from-value').on('click', function() {
                    $(this).select();
                });

                $('#range-to-value').on('click', function() {
                    $(this).select();
                });

                $range.on('change', function() {
                    $('#range-from-value, #range-to-value').trigger('change');
                })

            }

        },
        bindSubmitFilter = function() {
            var $init = $('.js-submit-filter');
            if($init.length) {
                $init.on('click', function(e) {
                    e.preventDefault();
                    $form.submit();
                });
            }
        },
        bindShowMore = function() {
            $showMoreBtn.find('.show-stock').on('click', function(e) {
                e.preventDefault();
                $productsListWrapper.find('.hide').eq(0).removeClass('hide');
                if(!$productsListWrapper.find('.hide').length) {
                    $showMoreBtn.hide();
                }
            });
        },
        bindEvents = function() {
            // кнопка показать все
            $showAllButtons.on('click', function(e) {
                var $this = $(this),
                    hideParams = $this.closest('.filter-block-content').find('.hide'),
                    $animParamFilterHide, $animParamFilterShow;
                e.preventDefault();

                if($this.hasClass('open')) {
                    $animParamFilterHide = new TweenMax(hideParams, 0.5, {css: {display: 'none', opacity: '0'}});
                    $animParamFilterHide.play();
                    $animParamFilterHide.eventCallback('onComplete', function() {
                        $this.text('Показать все');
                    });
                } else {
                    $animParamFilterShow = new TweenMax(hideParams, 0.5, {css: {display: 'block', opacity: '1'}});
                    $animParamFilterShow.play();
                    $animParamFilterShow.eventCallback('onComplete', function() {
                        $this.text('Скрыть');
                    });
                }

                $(this).toggleClass('open');
            });

            // сбросить фильтр для блока
            $resetFilterBlockBtn.on('click', function(e) {
                e.preventDefault();
                var $this = $(this);
                var $inputs_checked = $this.closest('.filter-block-content').find('input');
                $inputs_checked.each(function(ind, elt) {
                    $(elt).removeAttr('checked');
                    $(elt).removeClass('checked');
                });
                $this.fadeOut(200);
                $('.popup-filter-count').fadeOut(200);
            });

            // сбросить весь фильтр
            $resetFilterBtn.on('click', function(e) {
                e.preventDefault();
                var checkboxes = $(this).closest('.js-filter-container').find('input[type=checkbox], input[type=radio]');
                checkboxes.removeAttr('checked');
                checkboxes.removeClass('checked');
                $('.reset-filter-block').removeClass('active');
                $('.popup-filter-count').fadeOut(200);
                $range.val([pipFromPrice,pipToPrice]);
            });


            // подсчет количества домов, удовлетворяющих выбранным условиям
            $inputsForm.on('change', function() {
                var $this = $(this);
                clearTimeout(popupFilterCountInterval);

                hideShowResetBlockBtn($this);
                sendAjaxAndGetPopupCount($this);

            });
        },
        hideAllPopupCount = function() {
            $('.popup-filter-count').remove();
        },
        hideFilterPopupCount = function() {
            clearTimeout(popupFilterCountInterval);
            $('.popup-filter-count').fadeOut(400);
        },
        hideShowResetBlockBtn = function($input) {
            var $filterBlock = $input.closest('.filter-block-content'),
                $inputsFilterBlock = $filterBlock.find('input:checked');
            if(!$inputsFilterBlock.length) {
                hideResetBlockBtn($filterBlock);
            } else {
                var isActiveResetBlockBtn = getStatusResetBlockBtn($filterBlock);
                if(!isActiveResetBlockBtn) {
                    var $resetBlockBtn = $filterBlock.find('.reset-filter-block');
                    $resetBlockBtn.addClass('active');
                    $resetBlockBtn.fadeIn(200);
                }
            }
        },
        getStatusResetBlockBtn = function($filterBlock) {
            var resetBlockBtn = $filterBlock.find('.reset-filter-block');
            return !!resetBlockBtn.hasClass('active');
        },
        hideResetBlockBtn = function($filterBlock) {
            var $resetBlockBtn = $filterBlock.find('.reset-filter-block');
            $resetBlockBtn.removeClass('active');
            $resetBlockBtn.fadeOut(200);
        },
        sendAjaxAndGetPopupCount = function($input) {
            var $this = $input;
            if (!device.mobile()){
                $.ajax({
                    url:'/ajax/filter.php/',
                    type: 'POST',
                    dataType: 'json',
                    data: $form.serialize() + '&type=count',
                    beforeSend: function() {
                        hideAllPopupCount();
                    },
                    success: function(data) {
                        var html = '',
                            $fieldset,
                            hFieldSet,
                            $popupFilterCount,
                            hPopupContent,
                            topCss;

                        if(data['count']) {

                            html  = '<div class="popup-filter-count">';
                            html += data['count'] + '.';
                            html += '  <span class="text-container">';
                            html += '    <a href="#" class="js-submit-filter">';
                            html += '      Показать';
                            html += '    </a>';
                            html += '  </span>';
                            html += '</div>';

                            $fieldset = $this.closest('.js-level-popup');
                            hFieldSet = $fieldset.innerHeight();

                            $fieldset.append(html);
                            bindSubmitFilter();

                            $popupFilterCount = $('.popup-filter-count');
                            hPopupContent = $popupFilterCount.innerHeight();
                            topCss = (hFieldSet - hPopupContent)/2;
                            var wPopupCount = $popupFilterCount.innerWidth() + 20;

                            $popupFilterCount.css('right', '-' + wPopupCount + 'px');
                            $popupFilterCount.css('top', topCss + 'px');
                            $popupFilterCount.fadeIn(400);

                            popupFilterCountInterval = setTimeout(hideFilterPopupCount, timeViewPopup);

                        }
                    }
                });
            }
        };

    filterApplication.init = function() {
        if(!rangeFrom) {
            rangeFrom = 200000;
        }
        if(!rangeTo) {
            rangeTo = 7000000;
        }
        if(!pipFromPrice) {
            pipFromPrice = 200000;
        }
        if(!pipToPrice) {
            pipToPrice = 7000000;
        }

    	initNoUiSlider();
    	bindEvents();

        if($productsListWrapper.find('.hide').length > 0) {
            $showMoreBtn.show();
            bindShowMore();
        }
    };

    return filterApplication;

})(jQuery, window);

var favoritesApplication = (function($, _window){
    var favoritesApplication = {},
    	$btnDelete = $('.js-delete-favorites'),
    	$btnClose = $('.js-close-fav'),
    	$jsCount = $('.js-count'),
    	$btnShare = $('.js-share-link'),
    	$shareLinkCont = $('.share-link-cont'),
    	$favPanel = $('.favorites-panel'),
        $favoritesNoItems = $('.favorites-no-items'),
        $favoritesItemsWrapper = $('.favorites-items-wrapper'),

    	bindBtnClose = function() {
    		$btnClose.on('click', function(e) {
    			e.preventDefault();
    			var idFav = $(this).closest('.product-container').attr('data-fav-id');
    			sendAjaxDelete(idFav);
    		});
    	},

    	bindBtnDelete = function() {
    		$btnDelete.on('click', function(e) {
    			e.preventDefault();
    			var idFav = 'all';
    			sendAjaxDelete(idFav);
    		});
    	},

		bindBtnShare = function() {
			$btnShare.on('click', function(e) {
				e.preventDefault();
				if($shareLinkCont.hasClass('active')) {
					$shareLinkCont.removeClass('active');
					$shareLinkCont.fadeOut(200);
				} else {
					$shareLinkCont.addClass('active');
					$shareLinkCont.fadeIn(200);
					$shareLinkCont.find('input').focus().select();
				}
			});
		},

    	sendAjaxDelete = function(idFav) {
    		var dltObject = {},
    			ids = [],
    			currFav = '',
    			currLi = '';

    		if(idFav != 'all') {
				currFav = $('.product-container[data-fav-id=' + idFav + ']');
				currLi = currFav.closest('li');
				// ids.push(idFav);
                dltObject['id'] = idFav;
    		} else {
    			currFav = $('.product-container');
    			$.each(currFav, function(index, element) {
    				ids.push($(element).attr('data-fav-id'));
    			});
    			currLi = $('.favorites-list li');
                dltObject['ids'] = ids;
    		}
    		
    		dltObject['type'] = 'del';

	        $.ajax({
	            url: '/ajax/favorites.php/',
	            type: 'POST',
	            dataType: 'json',
	            data: dltObject,
	            beforeSend: function() {
	            	currFav.find('.overlay').addClass('active');
	            },
	            success: function(data) {
	                if (!$.isEmptyObject(data)) {
	                    if(data['success']) {
	                    	currLi.remove();
	                    	$jsCount.text(data['count']);

                            if($favPanel.find('span').length) {
                                if(parseInt(data['count']) == 0 || parseInt(data['count']) < 0) {
                                    $favPanel.find('span').remove();
                                    $favoritesNoItems.removeClass('hidden');
                                    $favoritesItemsWrapper.hide();
                                } else {
                                    $favPanel.find('span').text(data['count']);
                                }
                            } else {
                                var span = $('<span>');
                                span.text(data['count']);
                                $favPanel.append(span);
                            }
                            var s = window.location.hostname + '/favorits/?ids=';
                            $.each(data['stay_id'], function(idx, elt){
                                s += elt;
                                if (idx < data['stay_id'].length - 1) s+=',';
                            });
                            $btnShare.val(s);

	                    } else {
	                    	openModal('Ошибка!', data['message']);
	                    }
	                }

	                currFav.find('.overlay').removeClass('active');
	            }
	        });
    	};

    favoritesApplication.init = function() {
    	bindBtnClose();
    	bindBtnDelete();
    	bindBtnShare();
    };

    return favoritesApplication;

})(jQuery, window);

var comparisonApplication = (function($, _window){
    var comparisonApplication = {},
    	$comparisonList = $('.comparison-list'),
    	$headParamList = $('.head-param-list'),
    	$valueParamList = $('.value-param-list'),
    	$btnSwitchView = $('.switch-comparison input[type=radio]'),
    	$btnShare = $('.js-share-link'),
    	$shareLinkCont = $('.share-link-cont'),
    	$btnClose = $('.js-del-comparison'),
    	$btnDelete = $('.js-delete-comparison'),
    	$comparisonPanel = $('.comparison-panel-item'),
        $comparisonNoItems = $('.comparison-no-items'),
        $comparisonContentWrapper = $('.comparison-content-wrapper'),
        modeComparison = '',

    alignmentHeadValue = function() {
    	// позиция списка с параметрами относительно родителя
    	var valueParamListPos = $valueParamList.position(),
            heights = [];

        if (modeComparison === 'desktop') {
            // позиция наименований 
            $headParamList.css('top', valueParamListPos.top);
            // высота строк
            $.each($valueParamList, function(index, element) {
                $.each($(element).find('li'), function(ind, elt) {
                    if(!heights[ind] || (heights[ind] && heights[ind] < $(elt).outerHeight()) ) {
                        heights[ind] = $(elt).outerHeight();
                    }                
                });
            });

            $.each(heights, function(index, height) {
                $headParamList.find('li').eq(index).css('height', height);

                $.each($valueParamList, function(ind, elt) {
                    $(elt).find('li').eq(index).css('height', height)
                });

            });

            $headParamList.show(100);

            console.log('heights ', heights, heights.length);

        }
        
    },

	bindBtnShare = function() {
		$btnShare.on('click', function(e) {
			e.preventDefault();
			if($shareLinkCont.hasClass('active')) {
				$shareLinkCont.removeClass('active');
				$shareLinkCont.fadeOut(200);
			} else {
				$shareLinkCont.addClass('active');
				$shareLinkCont.fadeIn(200);
				$shareLinkCont.find('input').focus().select();
			}
		});
	},

	bindDelAllComparison = function() {
		$btnDelete.on('click', function(e) {
			e.preventDefault();
			var idComparison = 'all';
			sendAjaxDelete(idComparison);
		});
	},

	bindDelComparison = function() {
		$btnClose.on('click', function(e) {
			e.preventDefault();
			var idComparison = $(this).closest('.comparison-container').attr('data-comparison-id');
			sendAjaxDelete(idComparison);
		});
	},

	sendAjaxDelete = function(idComparison) {
		var dltObject = {},
			ids = [],
			currComparison = '',
			currLi = '';

		if(idComparison != 'all') {
			currComparison = $('.comparison-container[data-comparison-id=' + idComparison + ']');
			currLi = currComparison.closest('li');
			// ids.push(idComparison);
            dltObject['id'] = idComparison;
		} else {
			currComparison = $('.comparison-container');
			$.each(currComparison, function(index, element) {
				ids.push($(element).attr('data-comparison-id'));
			});
			currLi = $('.comparison-list li');
            dltObject['ids'] = ids;
		}

		// dltObject['ids'] = ids;
		dltObject['type'] = 'del';

        $.ajax({
            url: '/ajax/comparison.php/',
            type: 'POST',
            dataType: 'json',
            data: dltObject,
            beforeSend: function() {
            	currComparison.find('.overlay').addClass('active');
            },
            success: function(data) {
                if (!$.isEmptyObject(data)) {
                    if(data['success']) {
                    	currLi.remove();

                        if($comparisonPanel.find('span').length) {
                            $comparisonPanel.find('span').text(data['count']);
                        } else {
                            var span = $('<span>');
                            span.text(data['count']);
                            $comparisonPanel.append(span);
                        }

                    } else {
                    	openModal('Ошибка!', data['message']);
                    }
                }

                if(!$comparisonList.find('li').length) {
                	$headParamList.hide();
                }

                currComparison.find('.overlay').removeClass('active');

                if(!$comparisonList.find('li').length) {
                    $comparisonContentWrapper.remove();
                    $comparisonNoItems.removeClass('hidden');
                }

            }
        });
	},

    bindSwitchView = function() {
    	$btnSwitchView.on('change', function() {
    		var $this = $(this);

    		if($this.val() != 'all') {

                if($comparisonList.children('li').length > 1) {

                    $headParamList.find('li:not(.' + $this.val() + ')').fadeOut(500);

                    $.each($headParamList.find('li:not(.' + $this.val() + ')'), function(index, element) {

                        $.each($valueParamList, function(ind, elt) {
                            $(elt).find('li').eq($(element).index()).fadeOut(500);                      
                        });

                    });

                }
	
    		} else {

    			$headParamList.find('li').fadeIn(500);
    			$valueParamList.find('li').fadeIn(500);

    		}

    	});
    },

    bindHoverList = function() {
    	$valueParamList.find('li').on('hover', function() {
    		var $this = $(this),
    			currIndex = $this.index();

    		$.each($valueParamList, function(index, element) {
    			$(element).find('li').eq(currIndex).addClass('on-hover');
    		});

    		$headParamList.find('li').eq($this.index()).addClass('on-hover');
    	});

    	$valueParamList.find('li').on('mouseleave', function() {
    		var $this = $(this),
    			currIndex = $this.index();

    		$.each($valueParamList, function(index, element) {
    			$(element).find('li').eq(currIndex).removeClass('on-hover');
    		});

    		$headParamList.find('li').eq($this.index()).removeClass('on-hover');
    	});

    	$headParamList.find('li').on('hover', function() {
    		var $this = $(this),
    			currIndex = $this.index();

    		$this.addClass('on-hover');

    		$.each($valueParamList, function(index, element) {
    			$(element).find('li').eq(currIndex).addClass('on-hover');
    		});

    	});

    	$headParamList.find('li').on('mouseleave', function() {
    		var $this = $(this),
    			currIndex = $this.index();

    		$.each($valueParamList, function(index, element) {
    			$(element).find('li').eq(currIndex).removeClass('on-hover');
    		});

    		$this.removeClass('on-hover');
    	});


    };

    comparisonApplication.init = function(mode) {

        modeComparison = mode;

    	// инициализация скролла
    	$('.comparison-list-wrapper').mCustomScrollbar({
    		axis:"x",
    		advanced:{autoExpandHorizontalScroll:true},
    		theme: "rounded"
		});
		// выравнивание параметров со значениями
		alignmentHeadValue();
		// события - выбор отображения
		bindSwitchView();
		// события - поделиться ссылкой
		bindBtnShare();
		// события - удалить элемент из списка сравнения
		bindDelComparison();
		// события - удалить все элементы
		bindDelAllComparison();
		// события - наведение на список
		bindHoverList();
    }

    return comparisonApplication;

})(jQuery, window);

var chooseColorApplication = (function($, _window){
    var chooseColorApplication = {},
        $chooseColorBtn = $('.choose-color'),
        $roofSwiperContainer = $('.roof-slider .swiper-container'),
        $sidingSwiperContainer = $('.siding-slider .swiper-container'),
        $roofList = $('.roof-slider'),
        $sidingList = $('.siding-slider'),
        $productViewList = $('.product-view-list'),
        $productSlides = $('.product-view-list li'),
        $productViewContent = $('.product-view-content'),
        $panelChoose = $('.select-view-params'),
        $roofSliderNext = $('#sroof-next'),
        $sidingSliderNext = $('#ssiding-next'),
        productSlider,
        roofSlider,
        sidingSlider,
        modeColor,
        
        $productView = $('.product-view'),
        $changeView = $('.change-view');

    var bindEvents = function() {
        // Показать/скрыть панель со списком параметров
        $chooseColorBtn.on('click', function(e) {
            e.preventDefault();
            chooseColorApplication.showHideParamsPanel();
        });

        // следующий параметр кровли сделать акивным
        $roofSliderNext.on('click', function(e) {
            e.preventDefault();
            roofSlider.swipeNext();
            console.log('roofSliderNext click');
        });

        // следующий параметр сайдинга сделать активным
        $sidingSliderNext.on('click', function(e) {
            e.preventDefault();
            sidingSlider.swipeNext();
        });

        // отменить действие по умолчанию у параметра кровли
        $roofSwiperContainer.find('a').on('click', function(e) {
            e.preventDefault();
        });

        // отменить действие по умолчанию у параметра сайдинга
        $sidingSwiperContainer.find('a').on('click', function(e) {
            e.preventDefault();
        });
    }

    chooseColorApplication.init = function(mode) {
        modeColor = mode;
        /* инициализация слайда с видом дома */
        if (modeColor === 'desktop') {

            productSlider = $productViewList.bxSlider({
                                pagerCustom: '#bx-pager-product',
                                controls: false,
                                mode: 'fade',
                            });
bindEvents();
            /* инициализация слайдов с крышами и сайдингом */
            var countSlidesRoof = $roofSwiperContainer.find('.swiper-slide').length;
            $roofList.find('.swiper-slide').each(function(){

                imgPath = $(this).find('a').attr('data-img');
                $('.roof').append('<img src="' + imgPath + '" class="roof_'+ $roofList.find('.swiper-slide').index(this) +' img_roof" style="display:none" alt="">');   
                $(this).click(function(){
                    var index = $roofList.find('.swiper-slide').index(this);

                    if(index==chooseColorApplication.currentIndexRoof)
                    {
                        chooseColorApplication.currentIndexRoof ='-5';
                        chooseColorApplication.clearRoof();
                    }
                    else
                        chooseColorApplication.setParam($roofList, index);
                });
            });
             $sidingList.find('.swiper-slide').each(function(){
                 imgPath = $(this).find('a').attr('data-img');
               $('.siding').append('<img src="' + imgPath + '" class="siding_'+ $sidingList.find('.swiper-slide').index(this) +' img_siding" style="display:none" alt="">');  
                

                
                $(this).click(function(){
                    var index = $sidingList.find('.swiper-slide').index(this);

                    if(index==chooseColorApplication.currentIndexSiding)
                    {
                        chooseColorApplication.currentIndexSiding ='-5';
                        chooseColorApplication.clearSiding();
                    }
                    else
                        chooseColorApplication.setParam($sidingList, index);
                });
            });

        $('.choose-color').click(function(){  
        if($chooseColorBtn.hasClass('active')) {
            chooseColorApplication.clearSiding();
            chooseColorApplication.clearRoof();
            $('#bx-pager-product').find('a:eq(0)').trigger('click');
            $chooseColorBtn.removeClass('active');
            TweenMax.to($productViewContent, 0.4, {css: {marginBottom: '0'}});                    
            TweenMax.to($panelChoose, 0.4, {css: {bottom: '0', opacity: 0}});
        } else {
            $('bx-pager-product').find('a').bind('click',function(){
            chooseColorApplication.clearSiding();
            chooseColorApplication.clearRoof();
            $chooseColorBtn.removeClass('active');
            TweenMax.to($productViewContent, 0.4, {css: {marginBottom: '0'}});                    
            TweenMax.to($panelChoose, 0.4, {css: {bottom: '0', opacity: 0}});
            });
            $('#bx-pager-product a').last().trigger('click');
            $('.roof-slider').find('.swiper-slide:first').addClass('swiper-slide-active');
            $('.siding-slider').find('.swiper-slide:first').addClass('swiper-slide-active');
            chooseColorApplication.setParam($('.roof-slider'), 0);
            chooseColorApplication.setParam($('.siding-slider'), 0);

            $chooseColorBtn.addClass('active')
            TweenMax.to($productViewContent, 0.4, {css: {marginBottom: $panelChoose.outerHeight() + 15 + 'px'}});                    
            TweenMax.to($panelChoose, 0.4, {css: {bottom: '-' + $panelChoose.outerHeight() + 'px', opacity: 1}});
        }
        });
        //     roofSlider = new Swiper('.roof-slider .swiper-container', {
        //                     slidesPerView: 4,
        //                      loop: true,
        //                     onSlideClick: function(swiper) {
        //                         console.log(swiper);
        //                         var clickIndex = swiper.clickedSlideLoopIndex;
                                // if(clickIndex >= countSlidesRoof) {
        //                             clickIndex -= countSlidesRoof;
        //                         }
        //                         swiper.swipeTo(clickIndex, 200, false);

        //                         chooseColorApplication.setParam($roofList, swiper.activeIndex);
        //                         //chooseColorApplication.setParam($roofList, clickIndex);
        //                     },
        //                     onSlideChangeEnd: function(swiper) {
        //                         console.log('onSlideChangeEnd');
        //                         chooseColorApplication.setParam($roofList, swiper.activeIndex);
        //                         //chooseColorApplication.setParam($roofList, swiper.clickedSlideIndex);
        //                     },
        //                     onSlideNext: function(swiper) {
        //                         console.log('onSlideNext');
        //                     }
        //                  });

    /*
            rfSlider = $('.rf-slider').bxSlider({
                            minSlides: 4,
                            maxSlides: 4,
                            slideWidth: 41,
                            slideMargin: 10,
                            auto: false,
                            moveSlides: 1,
                            infiniteLoop: false,
                            nextSelector: '#sroof-next',
                            // prevSelector: '#prev-slider-nav',
                            nextText: '',
                            prevText: '',
                            pager: false
            });
*/


            // sidingSlider = new Swiper('.siding-slider .swiper-container', {
            //                 slidesPerView: 4,
            //                 loop: true,
            //                 onSlideClick: function(swiper) {
            //                     var clickIndex = swiper.clickedSlideLoopIndex;

            //                     if(clickIndex >= countSlidesSiding) {
            //                         clickIndex -= countSlidesSiding;
            //                     }
            //                     swiper.swipeTo(clickIndex, 200, false);

            //                     chooseColorApplication.setParam($sidingList, swiper.activeIndex);
            //                 },
            //                 onSlideChangeEnd: function(swiper) {
            //                     chooseColorApplication.setParam($sidingList, swiper.activeIndex);
            //                 }

            //              });
            

            // Установка значений по умолчанию по текущему индексу слайдеров
            // chooseColorApplication.setParam($roofList, roofSlider.activeIndex);
            // chooseColorApplication.setParam($sidingList, sidingSlider.activeIndex);

        } else if (modeColor === 'mobile') {
            productSlider = $productViewList.bxSlider({
                controls: true,
                pager: false,
                mode: 'horizontal',
                prevText: '',
                nextText: ''
            });
        }


        // события
        bindEvents();
    }

    chooseColorApplication.setParam = function(list, numParam) {


        
        var typeParam = '';
        if(list.hasClass('roof-slider')) {
            typeParam = 'roof';
            this.currentIndexRoof = numParam;
            chooseColorApplication.clearRoof();
          
        } 

        if(list.hasClass('siding-slider')) {
            typeParam = 'siding';
            
            this.currentIndexSiding = numParam;
            chooseColorApplication.clearSiding();
            $(this).addClass('swiper-slide-active');
        } 
        list.find('.swiper-slide:eq(' + numParam + ')').addClass('swiper-slide-active');
        $productSlides.each(function(ind, elt) {

                // imageWrapper = $(elt).find('.image-wrapper'),
                // imgPath = list.find('.swiper-slide:eq(' + numParam + ') a').data('img-' + ind),
    
                //imgPath = list.find('.swiper-slide:eq(' + numParam + ') a').data('img'),
                $('.'+typeParam+'_'+numParam).css('display','block');


            // if(imgPath && $paramElt) {
            //  var img = $paramElt.find('img');
            //  //alert(img.attr('src'));
            //  $paramElt.append('<img src="' + imgPath + '" alt="">'); 
            // }s
        });
    }

    chooseColorApplication.clearRoof = function() {
        //$('.roof').empty();
        //this.currentIndexRoof='';
        $('.roof-slider').find('.swiper-slide-active').removeClass('swiper-slide-active'); 
        $('.roof').find('img').each(function(){
            $(this).hide();
        });
    }

    chooseColorApplication.clearSiding = function() {
        //$('.siding').empty();
        //this.currentIndexSiding='';
        $('.siding-slider').find('.swiper-slide-active').removeClass('swiper-slide-active');
        $('.siding').find('img').each(function(){
            $(this).hide();
        });

    }

    chooseColorApplication.showHideParamsPanel = function() {
         if($chooseColorBtn.hasClass('active')) {
            chooseColorApplication.clearSiding();
            chooseColorApplication.clearRoof();
            $('#bx-pager-product').find('a:eq(0)').trigger('click');
            $chooseColorBtn.removeClass('active');
            TweenMax.to($productViewContent, 0.4, {css: {marginBottom: '0'}});                    
            TweenMax.to($panelChoose, 0.4, {css: {bottom: '0', opacity: 0}});
        } else {
            $('#bx-pager-product').find('a').bind('click',function(){
            chooseColorApplication.clearSiding();
            chooseColorApplication.clearRoof();
            $chooseColorBtn.removeClass('active');
            TweenMax.to($productViewContent, 0.4, {css: {marginBottom: '0'}});                    
            TweenMax.to($panelChoose, 0.4, {css: {bottom: '0', opacity: 0}});
            });
            $('#bx-pager-product a').last().trigger('click');
            $('.roof-slider').find('.swiper-slide:first').addClass('swiper-slide-active');
            $('.siding-slider').find('.swiper-slide:first').addClass('swiper-slide-active');
            chooseColorApplication.setParam($('.roof-slider'), 0);
            chooseColorApplication.setParam($('.siding-slider'), 0);

            $chooseColorBtn.addClass('active')
            TweenMax.to($productViewContent, 0.4, {css: {marginBottom: $panelChoose.outerHeight() + 15 + 'px'}});                    
            TweenMax.to($panelChoose, 0.4, {css: {bottom: '-' + $panelChoose.outerHeight() + 'px', opacity: 1}});
        }
    }

    return chooseColorApplication;

})(jQuery, window);

var menuApplication = (function ($, _window) {
    var menuApplication = {},
        btnMenu = $('.js-menu-trigger'),
        $itemMobileMenu = $('.mobile-menu-list > li > a'),
        $subMenu = $('.sub-menu-list'),
        backBtn = $('.js-back-menu'),
        hBackBtn = backBtn.height(),
        hHeaderMenu = $('.mobile-header').innerHeight() + 5,
        $mobilMainMenu = $('.mobile-main-menu'),
        $headerMenu = $('.mobile-header'),
        $scrollWrapper = $('.scroll-wrapper'),
        $scrollWrapperSub = $('.scroll-wrapper-sub'),
        $body = $('body'),
        $mobileMenuWrapper = $('.mobile-menu-wrapper'),
        mobileMenuHeight,
        mobileSubMenuHeight,
        $mobileMenu = $('.mobile-menu'),
        currPos = 0;
        // facadeSlider = null;


    menuApplication.init = function () {
        menuApplication.setHeight();
        $scrollWrapper.each(function(ind, elt) {
            $(elt).mCustomScrollbar();
        });

        $scrollWrapperSub.each(function(ind, elt) {
            $(elt).mCustomScrollbar();
        });

    };

    menuApplication.setHeight = function() {
        // Высота меню
        mobileMenuHeight = window.innerHeight - hHeaderMenu;
        // Высота списка подменю
        mobileSubMenuHeight = window.innerHeight - hHeaderMenu - hBackBtn;

        $scrollWrapper.each(function(ind, elt) {
            $(elt).css('height', mobileMenuHeight);
        });

        $scrollWrapperSub.each(function(ind, elt) {
            var currSubMenu = $(elt).find('.mobile-sub-menu'),
                hSubMenu = currSubMenu.height();
            if(hSubMenu < mobileSubMenuHeight) {
                currSubMenu.css('height', mobileSubMenuHeight + 1);
            }

            $(elt).css('height', mobileSubMenuHeight);
        });

        $mobileMenuWrapper.css('height', mobileMenuHeight);
        $mobileMenuWrapper.css('top', hHeaderMenu + 'px');
        if (device.mobile())
            $body.outerHeight(window.innerHeight);

    }

    menuApplication.bindEvents = function () {

        /* main menu */
        btnMenu.on('click', function (e) {
            e.preventDefault();
            menuApplication.setHeight();

            if ($(this).hasClass('active')) {

                TweenMax.to($subMenu, 0.4, {css: {left: '-640px'}});
                TweenMax.to($mobileMenuWrapper, 0.4, {left: -640});

                $('html').removeAttr('style');
                $('html').removeClass('noscroll');
                $(this).removeClass('active');
                $(document).scrollTop(currPos);
                //$(document).off('touchmove.menu');

            } else {
                currPos = $headerMenu.offset().top + hHeaderMenu;
                //$mobileMenuWrapper.css('top', currPos);
                TweenMax.to($mobileMenuWrapper, 0.4, {left: 0});
                $('html').outerHeight(window.innerHeight);

                $(this).addClass('active');
                $('html').addClass('noscroll');
                //$(document).on('touchmove.menu', function(e) {
                //    e.preventDefault();
                //});

            }
        });

        /* $subMenu */
        $itemMobileMenu.on('click', function (e) {
            var item$subMenu = $($(this).attr('href'));
            if (item$subMenu.length) {
                e.preventDefault();
                TweenMax.to($mobilMainMenu, 0.4, {left: '-640px'});
                TweenMax.to(item$subMenu, 0.4, {css: {left: 0}});
            }

            $scrollWrapper.mCustomScrollbar('scrollTo', 'top');
            $scrollWrapper.mCustomScrollbar('destroy');

        });

        /* back */
        backBtn.on('click', function (e) {
            e.preventDefault();
            var curr$subMenu = $(this).closest('.sub-menu-list');
            TweenMax.to(curr$subMenu, 0.4, {css: {left: '-640px'}});

            $scrollWrapper.mCustomScrollbar();
        });

    };

    return menuApplication;

})(jQuery, window);

var mobileDevice,
    // popupFilterCountInterval,
    initMainSlider = function () {
        var $init = $('.main-slider'),
            mainSlider;
        if ($init.length) {

            var oneSlide = false;
            if(($init.find('li').length - 1) > 0) {
                oneSlide = true;
            }

            mainSlider = $init.bxSlider({
                // auto: oneSlide,
                auto: true,
                infiniteLoop: true,
                autoHover: true,
                controls: false,
                pagerSelector: '#pager-main-slider',
                pager: oneSlide,
		pause: 6000,
                responsive: true
            });

            if (oneSlide) {
                $(document).on('scroll', function () {
                    if ($(document).scrollTop() == 0) {
                        mainSlider.startAuto();
                    } else {
                        mainSlider.stopAuto();
                    }
                });
            }

            $(window).on('load', function () {
                mainSlider.reloadSlider();
            });

            $(window).on('resize', function () {
                mainSlider.reloadSlider();
            });

        }
    },
    initMosaic = function () {
        var $init = $('.mosaic');
        if ($init.length) {
            $init.each(function () {
                var $subEach = $(this);

                var $items = $subEach.find('.item'),
                    initScheme;

                if($(document).width() > 640) {
                    initScheme = $subEach.data('scheme');
                } else {
                    initScheme = $subEach.data('mobile-scheme');
                }

                $subEach.css({'position': 'relative'});

                var counter = 0, subCounter = 0, subLine = 0;
                var param = initScheme[0], scheme = initScheme[1];
                $items.each(function () {
                    var $this = $(this);
                    $this.css({
                        'position': 'absolute',
                        'left': scheme[counter][0],
                        'top': scheme[counter][1] + (subCounter * param[0]),
                        'width': scheme[counter][2],
                        'height': scheme[counter][3]
                    });
                    counter++;
                    if (counter == scheme.length) {
                        counter = 0;
                        subCounter++;
                    }
                });
                if ((counter > 0) && (counter < scheme.length)) {
                    subLine = 1
                }
                $subEach.css({'width': param[1], 'height': param[0] * (subCounter + subLine)})
            });
        }
    },
    initNewsSlider = function () {

        var $init = $('.news-list');
        if ($init.length) {
            if($init.find('> li').length > 1) {
                $('#news-next').addClass('active');
                $('#news-prev').addClass('active');
                if($(document).width() > 640) {
                    $init.bxSlider({
                        pageSelector: '#pager-news',
                        nextSelector: '#news-next',
                        prevSelector: '#news-prev',
                        nextText: '>',
                        prevText: '<'
                    });
                } else {
                    $init.bxSlider({
                        pageSelector: '#pager-news',
                        controls: false,
                        adaptiveHeight: true
                    });
                }
            }
        }

    },
    initHoverMosaic = function () {
        var $mosaic = $('.mosaic'),
            $item = $('.mosaic .item'),
            $target = $('.mosaic .target'),
            $currItem,
            $imageItem;

        $mosaic.on('mouseover', function () {
            $target.css('display', 'block');
        });

        $mosaic.on('mouseleave', function () {
            $target.css('display', 'none');
            $('.zoom').removeClass('zoom');
        });


        $item.on('mouseover', function () {
            $currItem = $(this);
            var $this = $(this),
                _css = {
                    'top': $this.css('top'),
                    'left': $this.css('left'),
                    'width': $this.width(),
                    'height': $this.height()
                };

            $('.zoom').removeClass('zoom');

            $imageItem = $this.find('img');
            $imageItem.addClass('zoom');

            $target.css(_css);

        });

        $target.on('mouseleave', function () {
            $imageItem = $currItem.find('img');
            $imageItem.removeClass('zoom');
        });

        $target.on('click', function () {
            var href = $currItem.find('a').attr('href');
            location.href = href;
        });

    },
    initBlurMainSliderOnScroll = function () {
        var $imageBlur = $('.main-slider li .image-bg-blur'),
            heightBlur = $('.main-slider-wrapper').height() / 3,
            contentSlider = $('.content-slider'),
            oVal;

        if ($(document).width() > 640) {
            $(document).on('scroll', function () {
                if ($(document).scrollTop() == 0) {
                    $imageBlur.css('opacity', '0');
                    $imageBlur.css('z-index', '-1');
                    contentSlider.css("opacity", 1);
                } else {
                    $imageBlur.css('z-index', '0');

                    oVal = ($(window).scrollTop() / heightBlur);
                    $imageBlur.css("opacity", oVal);

                    contentSlider.css("opacity", 1 - oVal);

                }
            });
        }
    },
    initQuality = function() {
        var $quality = $('.js-quality');
        var position = calcQuality($quality);

        if($quality.length) {
            $(document).on('scroll', function () {
                position = calcQuality($quality);
                if ($(this).scrollTop() + $(window).height() == $(this).height()) {
                    $quality.removeClass('fixed');                    
                } else {
                    if(!$quality.hasClass('fixed')) {
                        $quality.addClass('fixed');
                    }
                }
            });
        }

    },
    calcQuality = function($quality) {
        if($quality.length) {
            var hWindow = $(window).height(),
                realWH = getViewport();
                WcontentWrapper = $('.content-wrapper').width(),
                topPos = hWindow - ($quality.height() + 60),
                $qualityWrapper = $('.quality-wrapper');
                if($(document).width() == 1280) {
                    leftPos = ($(window).width() - WcontentWrapper)/2 + WcontentWrapper - $quality.width() - 20;
                } else {
                    leftPos = ($(window).width() - WcontentWrapper)/2 + WcontentWrapper - $quality.width();
                }


            if($(document).scrollTop() + $(window).height() != $(document).height()) {
                $quality.css('top', topPos + 'px');
                $quality.css('left', leftPos + 'px');
                $quality.addClass('fixed');
            }

            $quality.css('left', leftPos + 'px');
            $quality.css('top', topPos + 'px');

            return [leftPos, topPos];
        } else {
            return false;
        }

    },
    initStickyHeader = function () {
        var $init = $('.sticky-header');

        if ($init.length) {
            $init.width($(document).width());
            if($(document).width() == 1280) {
                $('header .feedback').addClass('pr');
                $('.js-rs1280').addClass('pr');
            } else {
                $('header .feedback').removeClass('pr');
                $('.js-rs1280').removeClass('pr');
            }
        }
    },
    initSHHeaderOnScroll = function () {
        var $init = $('.sticky-header'),
            $mpHeader = $('.mp-header'),
            hMpHeader = $mpHeader.outerHeight(),
            docHeight = $(document).height() / 1500,
            tValCurr,
            tVal,
            tValMp,
            headerHeight = $init.innerHeight() + 5,
            posHeader = -headerHeight,
            msHeight = $('.main-slider-wrapper').height() / 250;

        if ($init.length) {
            if (!$init.hasClass('no-main')) {
                $(document).on('scroll', function () {

                    tValCurr = ($(window).scrollTop() / msHeight);
                    if (tValCurr > headerHeight) {
                        tVal = posHeader + headerHeight;
                    } else {
                        tVal = posHeader + tValCurr;
                    }

                    if(tValCurr == 0) {
                        TweenMax.to($mpHeader, 0.4, {css: {top: tValCurr + 'px'}});
                    } else {
                        TweenMax.to($mpHeader, 0.4, {css: {top: -hMpHeader + 'px'}});
                    }

                    TweenMax.to($init, 0.4, {css: {top: tVal + 'px'}});

                    if ($(document).scrollTop() >= msHeight - headerHeight) {
                        $init.addClass('shadow');
                    } else {
                        $init.removeClass('shadow');
                    }

                });
            }
        }
    },
    initInputsClick = function () {
        $('.css-label').on('click', function (e) {
            var $input = $(this).prev('input');
            var group = $input.attr('name');
            var $groupInputs = $('input[name="' + group + '"]');
            if ($input.is(':radio')) {
                if (!$input.hasClass('checked')) {
                    $groupInputs.removeClass('checked');
                    $input.addClass('checked');
                }
            } else {
                $input.toggleClass('checked');
            }
        });
    },
    initSelectChosen = function () {
        if ($(document).width() > 642) {
            var selects = $('.js-common-select');
            selects.each(function () {
                $(this).find('option:first').text('');
            });
            $('.js-common-select').chosen({
                disable_search: true,
                inherit_select_classes: true,
                width: '100%'
            });
        }


    },
    initFancybox = function() {
        var $init = $('.fancybox'),
            closeBtn = $('.close-fancybox');

        if($init.length) {

            if($init.hasClass('ex-popup')) {

                $init.each(function() {


                    $(this).fancybox({
                        closeBtn: true,
                        scrolling: 'no',
                        fitToView: false,
                        'helpers': {
                            overlay: {
                                locked: false
                            }
                        },
                        beforeShow: function() {

                            // console.log($(this));

                            var $idPopup = $(this).attr('href'),
                                idPopup = $idPopup.replace('#', ''),
                                sliderBefore = $($idPopup).find('.examples-list-before'),
                                sliderAfter = $($idPopup).find('.examples-list-after'),
                                pagerBefore = '#pager-before-' + idPopup,
                                pagerAfter = '#pager-after-' + idPopup,
                                ctrlAfterPrev = '#ctrl-ap-' + idPopup,
                                ctrlAfterNext = '#ctrl-an-' + idPopup,
                                ctrlBeforePrev = '#ctrl-bp-' + idPopup,
                                ctrlBeforeNext = '#ctrl-bn-' + idPopup; 

                                // console.log('idPopup ', idPopup);

                            sliderBefore.bxSlider({
                                pagerCustom: pagerBefore,
                                controls: false,
                                loop: false
                            });


                            var pagerSlider = $('#pager-before-' + idPopup);
                            if(pagerSlider.length) {
                                sliderBxBefore = pagerSlider.bxSlider({
                                    minSlides: 4,
                                    maxSlides: 4,
                                    slideMargin: 10,
                                    slideWidth: 125,
                                    pager: false,
                                    nextSelector: ctrlBeforeNext,
                                    prevSelector: ctrlBeforePrev,
                                    nextText: 'next',
                                    prevText: 'prev',
                                    infiniteLoop: false
                                });
                            }


                            sliderAfter.bxSlider({
                                pagerCustom: pagerAfter,
                                controls: false
                            });


                            var pagerSlider = $('#pager-after-' + idPopup);
                            if(pagerSlider.length) {
                                sliderBxAfter = pagerSlider.bxSlider({
                                    minSlides: 4,
                                    maxSlides: 4,
                                    slideMargin: 10,
                                    slideWidth: 125,
                                    pager: false,
                                    nextSelector: ctrlAfterNext,
                                    prevSelector: ctrlAfterPrev,
                                    nextText: 'next',
                                    prevText: 'prev',
                                    infiniteLoop: false
                                });
                            }

                        }
                    });

                });

            }

        }
    },

    initTooltip = function() {
        $init = $('.tooltip');

        if($init.length) {
            $init.tooltipster({
                position: 'bottom',
                theme: 'tooltipster-light'
            });
        }
    },

    initDevice = function() {
        if(device.ipad()) {
            $('meta[name="viewport"]').attr('content', 'width=1300');
        }
    },
    initHint = function() {
        $init = $('.hint');


        $init.each(function() {
            var $this = $(this),
                content = $this.next('.content-tooltip').html();

            $this.tooltipster({
                content: $(content),
                theme: 'tooltipster-hint',
                minWidth: 500,
                maxWidth: 500,
                position: 'right',
                contentAsHTML: true,
                interactive: true
            });

        });
    },
    initCalculator = function () {
        var $init = $('#credit_calculator');

        if($init.length) {
            var costField = $('#cost'),
                instalmentField = $('#instalment');

            costField.on('focus', function() {
                $(this).select();
            })
            instalmentField.on('focus', function() {
                $(this).select();
            })

            getSumCredit();

            $('#credit_submit').on('click', function(e) {
                e.preventDefault();
                sendAjaxCredit();
                getSumCredit();
            });

        }
    },
    initMap = function () {
        if($('#map-mobile').length || $('#map-desktop').length) {
            var mode;
            if (device.desktop() || device.ipad()) {
                mode = 'desktop';
            } else {
                mode = 'mobile';
            }

            exhApplication.init($('#map-' + mode), mode);

        }
    },
    initNumBox = function () {
        $inputs = $('.num-box');
        if($inputs.length) {
            $('.num-box').number(true, 0, '.', ' ');
        }
    },
    initVideoFancy = function () {
        if ($('.fancybox-video').length) {
            $('.fancybox-video').click(function (e) {
                e.preventDefault();

                var thisHref;
                thisHref = $(this).attr('href');

                $.fancybox({
                    'wrapCSS': 'fancy-video',
                    'padding': 2,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'width': 640,
                    'height': 480,
                    'helpers': {
                        overlay: {
                            locked: false
                        }
                    },
                    'href': thisHref.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                    'type': 'swf',
                    'swf': {
                        'wmode': 'transparent',
                        'allowfullscreen': 'true'
                    }
                });

            });
        }
    },
    initBuildSlider = function() {
        $init = $('.build-slider');
        if($init.length) {
            $init.bxSlider({
                auto: true,
                pagerCustom: '#bx-pager-build',
                controls: false,
                mode: 'fade'
            });
        }
    },
    initBuildsPopup = function() {
        $init = $('.builds-item');
        if($init.length) {
            var mode = ''
            if (device.desktop() || device.ipad()) {
                mode = 'desktop';
            } else {
                mode = 'mobile';
            }
            buildsApplication.init(mode);
        }
    },

    socialShare = function(){
        var $shareLink = $('[data-share-social]');
        if ($shareLink.length) {

            var shareThis = function($target,service){
                var $box = $target.closest('.js-social-box');

                var _title = $box.find('[data-social="title"]').text().replace(/^\s+|\s+$/g, '');
                var _details = $box.find('[data-social="text"]').text().replace(/^\s+|\s+$/g, '');
                var _image = location.origin + $box.find('[data-social="image"]').attr('src');
                var _url = location.origin + $box.find('[data-social="url"]').attr('href');
                var shareLink = 'http://share.yandex.ru/go.xml?service=' + service + '&url=' + _url + '&link=' + _url + '&title=' + _title + '&description=' + _details + '&image=' + _image;

                if (service=='twitter') {
                    shareLink = 'http://share.yandex.ru/go.xml?service=' + service + '&url=' + _url + '&title='+_title;
                }

                window.open(shareLink,'','toolbar=0,status=0,width=626,height=436');
                //console.log([_title,_details,_image,_url,shareLink]);
            };

            $shareLink.click(function(e){
                var $target = $(e.target);
                var service = $target.attr('data-share-social');
                if (service) { shareThis($target,service); }
            });
        }
    },
    hiddenLink  = function(){
        var $linkBox = $('.js-hidden-link');
        if ($linkBox.length) {
            var _html = '<div class="hidden-link-show"><a class="js-link-show a-show" href="#">Подробнее</a><a class="js-link-show a-hide" href="#">Скрыть</a></div>';
            $linkBox.each(function(i,e){
                var $thisBox = $(e);
                var defaultH = $thisBox.height();
                var thisHeight = $thisBox.data('height');

                if (defaultH < thisHeight - 30) { return true; }

                $thisBox.attr('data-h',defaultH);
                if (thisHeight) {
                    $thisBox.css({'height':thisHeight+'px'}).addClass('hidden-link-box').append(_html);
                }
            });
        }

        $linkBox.click(function(e){
            var $this = $(this);
            var $target = $(e.target);
            if ($target.hasClass('js-link-show')) {
                if ($this.hasClass('hidden-show')) {
                    $this.removeClass('hidden-show');
                    $this.height($this.data('height'));
                } else {
                    $this.addClass('hidden-show');
                    $this.height($this.data('h'));
                }
                return false;
            } else {
                return true;
            }
        });
    },

    initHonorsGallery = function() {
        if (device.desktop()) {
            var $init = $('.fancybox-button');
            if($init.length) {
                $init.fancybox({
                    maxWidth: 1280,
                    maxHeight: 780,
                    helpers     : {
                        title   : { type : 'inside' }
                    }
                });
            }            
        } else {
            var $honorsList = $('.honors');
            if ($honorsList.length) {
                $honorsList.bxSlider({
                    auto: false,
                    pager: false,
                    controls: false,
                    minSlides: 2,
                    maxSlides: 2,
                    slideWidth: 225,
                    slideMargin: 25
                });
            }
        }

    },

    getAjaxHonors = function(){
        var $honorBox = $('.js-honors-box');
        if ($honorBox.length) {
            var $showNext = $('.js-honors-show');
            $showNext.click(function(e){
                e.preventDefault();
                var $this = $(this);
                var thisActive = $this.hasClass('disabled');
                if (thisActive) { return false; }
                $this.addClass('disabled');
                var thisStep = $this.data('step');
                $honorBox.eq(thisStep).slideDown(300,function(){
                    // console.log('thisStep ', thisStep);
                    if(thisStep == 2) {
                        $('.js-honors-box-add a').each(function(ind, elt) {
                            $(elt).addClass('fancybox-button');
                            $(elt).attr('rel', 'fancybox-button');
                        });
                        initHonorsGallery();
                        // console.log('sdf');
                    }
                    $this.removeClass('disabled');
                });
                thisStep++;
                $this.data('step',thisStep);
                var hasNextBox = $honorBox.eq(thisStep).length;
                if (!hasNextBox) {
                    $this.hide(300).addClass('disabled');;
                }
            });

            // lets go
            $showNext.trigger('click');
        }
    },

    initTabsOptions = function () {

        if($('.qa-tabs a').length) {
            console.log('qa-tabs a');
            $('.qa-tabs a').tabs();
        }

        if($('.review-tabs a').length) {
            $('.review-tabs a').tabs();
        }

    },

    initSelectParamsHouse = function() {
        var $chooseColorBtn = $('.product-view-list');

        if($chooseColorBtn.length) {
            var mode;
            if (device.desktop() || device.ipad()) {
                mode = 'desktop';
            } else {
                mode = 'mobile';
            }

            chooseColorApplication.init(mode);
        }
    },

    initLayoutsSliders = function() {
        var $tabs = $('.layouts-tabs a'),
            $sliders = $('.layouts-slider'),
            $slidersLayouts = [],
            initTabs = function() {
                if ($tabs.length) {
                    if ($('.product-layouts-wrapper .tabs a.selected').length === 0) {
                        console.log('initTabs');
                        $tabs.tabs();                        
                    }
                }
            };

        if($sliders.length) {
            var countSliders = $sliders.length;
            $sliders.each(function(ind, elt) {
                var floor = $(elt).data('floor');

                $slidersLayouts[ind] = $(elt).bxSlider({
                    auto: false,
                    mode: 'fade',
                    pagerCustom: '#bx-pager-floor' + floor,
                    onSliderLoad: function() {
                        console.log('init slider ' + ind);
                        if(ind + 1 == countSliders) {
                            initTabs();
                        }
                    }
                });
            });
        }

        $('.product-layouts-wrapper .tabs a').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            console.log($(this).index());
            var $this = $(this),
                index = $this.index(),
                $tabCnt = $($this.attr('href'));

            if ($tabCnt.find('.bx-wrapper').height() == 0) {
                console.log('height 0');
                setTimeout(function() { 
                    $slidersLayouts[index].reloadSlider();
                }, 500);
            }

            // console.log($slidersLayouts[index]);
            // var slider = $slidersLayouts[index].reloadSlider();

            // $slidersLayouts[index].reloadSlider();
        })

        $tabs.on('click', function() {
            // yaCounter23269252.reachGoal('planirovka'); 
            // ga('send', 'event', 'ссылка', 'клик', 'Планировка');
        });
    },

    initFacadeSlider = function() {
        var $tabs = $('.description-tabs a'),
            $optionTabs = $('.option-tabs a'),
            $facadeSlider = $('.facade-slider'),
            facadeSlider,
            initCustomScroll = function(elementForScroll) {
                elementForScroll.mCustomScrollbar({
                    axis:"x",
                    advanced:{autoExpandHorizontalScroll:true},
                });
            },
            initTabsOptFcd = function() {
                if($optionTabs.length) {
                    $optionTabs.tabs();
                }

                if($tabs.length) {
                    initCustomScroll($('.description-tabs'));
                    $tabs.tabs();
                    console.log('tabs init');
                }
            };

        if ($tabs.length) {
            initCustomScroll($('.description-tabs'));
            $tabs.tabs();
        }

        $(document).on('tab-switch', function() {

            if ($('.description-tabs a.selected').attr('href') === '#tab-facade') {
                console.log('tab facade open');
                // facadeSlider.reloadSlider();

                if (!facadeSlider) {

                    if (device.desktop() || device.ipad()) {

                        facadeSlider = $facadeSlider.bxSlider({
                            auto: false,
                            mode: 'fade',
                            pagerCustom: '#bx-pager-facade',
                            onSliderLoad: function() {
                               }
                        });
         
                    } else {

                        facadeSlider = $facadeSlider.bxSlider({
                            auto: false,
                            controls: false,
                            pager: false,
                            onSliderLoad: function() {
                                // console.log('slider load');
                            }
                        });

                    }

                }

            }


        });

/*
        if($facadeSlider.length) {

            if (device.desktop() || device.ipad()) {

                facadeSlider = $facadeSlider.bxSlider({
                    auto: false,
                    mode: 'fade',
                    pagerCustom: '#bx-pager-facade',
                    onSliderLoad: function() {
                        initTabsOptFcd();
                    }
                });
 
            } else {

                facadeSlider = $facadeSlider.bxSlider({
                    auto: true,
                    // mode: 'horizontal',
                    controls: false,
                    pager: false,
                    onSliderLoad: function() {

                        console.log('slider load');

                        setTimeout(function() { 
                            initTabsOptFcd();
                        }, 500);

                    }
                });


            }
        } else {
            initTabsOptFcd();
        }

        $tabs.on('click', function() {
            // yaCounter23269252.reachGoal('podrobnosti');
            // ga('send', 'event', 'ссылка', 'клик', 'Подробности');
        });
*/
    },

    initGoToProductFromCatalog = function() {
        var $init = $('.product-container');

        if($init.length) {
            $init.on('click', function() {
                var self = $(this),
                    link = self.data('link');

                if(link) {
                    location.href = link;
                }
            });
        }
    },

    initValidation = function() {
        var formsToValidate = [
            'callback-form',
            'apply-form',
            'feedback-form',
            'send-question-form',
            'review-video-form',
            'review-text-form',
            // 'send-question-team-form'
        ];

        $.each(formsToValidate, function (idx, elt) {
            var $form = $('.' + elt).find('form');

            if ($form.length) {

                $form.parsley({
                    classHandler: function (ParsleyField) {
                        return ParsleyField.$element.closest('fieldset');
                    }
                }).subscribe('parsley:form:validate', function(formInstance) {

                    var $textarea = formInstance.$element.find('textarea');
					if(formInstance.$element.parents('#apply-form').length)
					{
						if(!formInstance.$element.find('.phone-input').val().length && !formInstance.$element.find('.email-input').val().length)
						{
							formInstance.$element.find('.phone-input, .email-input').addClass('parsley-error');
							formInstance.$element.find('.phone-input, .email-input').parents('fieldset').addClass('parsley-error');
							$('#phone-or-email').show();
							formInstance.submitEvent.preventDefault();
						}
						else
						{
							formInstance.$element.find('.phone-input, .email-input').removeClass('parsley-error');
							formInstance.$element.find('.phone-input, .email-input').parents('fieldset').removeClass('parsley-error');
							$('#phone-or-email').hide();
						}
					}
                    if (formInstance.$element.attr('name') == 'FEEDBACK_CALL') {

                        if (formInstance.isValid()) {
                            formInstance.submitEvent.preventDefault();

                            var $overlay = $('.callback-form .overlay');

                            $.each($textarea, function(index, element) {
                                if($(element).data('placeholder') == $(element).val()) {
                                    $(element).val('');
                                }
                            });

                            $.ajax({
                                url: '/ajax/ajax_callback.php/',
                                data: formInstance.$element.serialize(),
                                dataType: 'json',
                                type:'POST',
                                beforeSend: function() {
                                    $overlay.addClass('active');
                                },
                                success: function(data) {
                                    var phone =  formInstance.$element.find('input[name=form_text_34]').val();
                                    var current = getCookie('sbjs_current');
                                    $.get( "/js/call.php", { config: "52", token: "fSxb8iB88IHG5tZXTA", sbjs_current:current,form_type: 'callback',phone:phone} );
                                    $overlay.removeClass('active');
                                    $('.callback-form .message-form p').append(data['message']);
                                    $('.callback-form .message-form').addClass('active');
                                    dhtmlLoadScript("http://track.auditorius.ru/pixel?id=30966&type=js");
                                    yaCounter23269252.reachGoal('recall'); 
                                    ga('send', 'event', 'форма', 'отправка', 'Обратный звонок');
                                }
                            });
                        }
                    }
/*
<<<<<<< HEAD
                    if    (formInstance.$element.attr('name') == 'APPLY-FORM' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_CABINS' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_BYTOVKY' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_CHANGES' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_BATH' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_HOUSES' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_DOMA' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_ARBORS' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_COTTEGES' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK' 
                        || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_INDIVIDYALNYE' 
                        ) {
                        if (formInstance.isValid()) {
                            formInstance.submitEvent.preventDefault();

                            var $overlay = $('.apply-form .overlay');

                            $.each($textarea, function(index, element) {
                                if($(element).data('placeholder') == $(element).val()) {
                                    $(element).val('');
                                }
                            });

                            $.ajax({
                                url: '/ajax/ajax_apply.php/',
                                data: formInstance.$element.serialize(),
                                dataType: 'json',
                                type:'POST',
                                beforeSend: function() {
                                    $overlay.addClass('active');
                                },
                                success: function(data) {
                                    $overlay.removeClass('active');
                                    $.fancybox('<div class="apply-message-wrapper"><div class="text-container"><p class="apply-message">' + data['message'] + '</p></div></div>',
                                                {
                                                    minWidth: 400,
                                                    minHeight: 300
                                                });
                                    if(data['success']) {
                                        $('.js-clear-input').val('');
                                        // $form.find('input[type=text]').val('');
                                        // $form.find('input[type=email]').val('');
                                        // $form.find('textarea').val('');

                                    }

                                    if (formInstance.$element.attr('name') == 'APPLY-FORM') {
                                        try { var _mt_top_location = window.top.location.href; } catch (e) { _mt_top_location = window.location.href; }
                                        (new Image(1, 1)).src = "http://videoclick.ru/core/land.gif?ld=LDswWnDc&rl=" + escape( document.referrer ) + "&ll=" + escape( _mt_top_location ) + "&r=" + ( Math.floor( Math.random() * 1000000000 ) );
                                    }

                                }
                            });
                        }                        
                    }
=======*/
                    //if    (formInstance.$element.attr('name') == 'APPLY-FORM'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_CABINS'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_BYTOVKY'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_CHANGES'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_BATH'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_HOUSES'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_DOMA'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_ARBORS'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_COTTEGES'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK'
                    //    || formInstance.$element.attr('name') == 'CATALOG_FEEDBACK_INDIVIDYALNYE'
                    //    ) {
                    //    if (formInstance.isValid()) {
                    //        formInstance.submitEvent.preventDefault();
                    //
                    //        var $overlay = $('.apply-form .overlay');
                    //
                    //        $.each($textarea, function(index, element) {
                    //            if($(element).data('placeholder') == $(element).val()) {
                    //                $(element).val('');
                    //            }
                    //        });
                    //
                    //        $.ajax({
                    //            url: '/ajax/ajax_apply.php/',
                    //            data: formInstance.$element.serialize(),
                    //            dataType: 'json',
                    //            type:'POST',
                    //            beforeSend: function() {
                    //                $overlay.addClass('active');
                    //            },
                    //            success: function(data) {
                    //                $overlay.removeClass('active');
                    //                $.fancybox('<div class="apply-message-wrapper"><div class="text-container"><p class="apply-message">' + data['message'] + '</p></div></div>',
                    //                            {
                    //                                minWidth: 400,
                    //                                minHeight: 300
                    //                            });
                    //                if(data['success']) {
                    //                    $('.js-clear-input').val('');
                    //                    // $form.find('input[type=text]').val('');
                    //                    // $form.find('input[type=email]').val('');
                    //                    // $form.find('textarea').val('');
                    //
                    //                }
                    //
                    //                if (formInstance.$element.attr('name') == 'APPLY-FORM') {
                    //                    try { var _mt_top_location = window.top.location.href; } catch (e) { _mt_top_location = window.location.href; }
                    //                    (new Image(1, 1)).src = "http://videoclick.ru/core/land.gif?ld=LDswWnDc&rl=" + escape( document.referrer ) + "&ll=" + escape( _mt_top_location ) + "&r=" + ( Math.floor( Math.random() * 1000000000 ) );
                    //                }
                    //
                    //            }
                    //        });
                    //    }
                    //}
//>>>>>>> 8ccad805ca4263447973a00951a437334f9731a5

                    if (formInstance.$element.attr('name') == 'FEEDBACK-FORM') {
                        if (formInstance.isValid()) {
                            formInstance.submitEvent.preventDefault();

                            var $overlay = $('.feedback-form .overlay');

                            $.each($textarea, function(index, element) {
                                if($(element).data('placeholder') == $(element).val()) {
                                    $(element).val('');
                                }
                            });

                            $.ajax({
                                url: '/ajax/feedback.php/',
                                data: formInstance.$element.serialize(),
                                dataType: 'json',
                                type:'POST',
                                beforeSend: function() {
                                    $overlay.addClass('active');
                                },
                                success: function(data) {
                                     var current = getCookie('sbjs_current');
                                    $.get( "/js/call.php", { config: "52", token: "fSxb8iB88IHG5tZXTA", sbjs_current:current,form_type: 'feedback'} );
                                    $overlay.removeClass('active');
                                    $('.feedback-form .message-form p').append(data['message']);
                                    $('.feedback-form .message-form').addClass('active');

                                    if(data['success']) {
                                        $form.find('input[type=text]').val('');
                                        $form.find('textarea').val('');
                                    }

                                }
                            });
                        }                        
                    }

                    if (formInstance.$element.attr('name') == 'SEND-QUESTION-FORM') {
                        if (formInstance.isValid()) {
                            formInstance.submitEvent.preventDefault();

                            var $overlay = $('.send-question-form .overlay');

                            $.ajax({
                                url: '/ajax/sendquestion.php/',
                                data: formInstance.$element.serialize(),
                                dataType: 'json',
                                type:'POST',
                                beforeSend: function() {
                                    $overlay.addClass('active');
                                },
                                success: function(data) {
                                    var current = getCookie('sbjs_current');
                                      var email =  formInstance.$element.find('input[name=email]').val();
                                    $.get( "/js/call.php", { config: "52", token: "fSxb8iB88IHG5tZXTA", sbjs_current:current,form_type: 'sendquestion',email:email} );
                                    $overlay.removeClass('active');
                                    // $('.send-question-form .message-form p').append(data['message']);
                                    // $('.send-question-form .message-form').addClass('active');

                                    if(data['success']) {
                                        $form.find('input[type=text]').val('');
                                        $form.find('textarea').val('');
                                    }

                                    openModal(data['title'], data['message']);

                                }
                            });
                        }                        
                    }

                });
            }
        });
    }, 

    initTextarea = function() {
        var $init = $('textarea.js-ta-placeholder');
        if($init.length) {
            $init.each(function() {

                var $this = $(this),
                    placeholder = $this.data('placeholder');

                $this.val(placeholder);

                $this.on('focus', function() {
                    var self = $(this);

                    if(self.val() == placeholder) {
                        self.val('');
                    }
                });

                $this.on('focusout', function() {
                    var self = $(this),
                        value = $.trim(self.val());
                    if(!value) {
                        self.val(placeholder);
                    }
                });

            });
        }
    },

    initFormElement = function() {
        if ($('.js-masked').length) {
            $('.js-masked').mask('+7(999)-999-99-99');
        }

        if($('.input-calendar').length) {
            $('.input-calendar').datepicker({
              firstDay: 1
            });
            // var currDate = new Date();
            // $('.input-calendar').datepicker('setDate', currDate);

        }
    },

    initCallbackPopup = function() {
        var $init = $('.js-callback'),
            $callbackPopup = $('.callback-form'),
            $overlay = $('.callback-form .overlay'),
            $content = $('.callback-form .message-form');
        if($init.length) {
            $init.on('click', function(e) {
                e.preventDefault();

                    $.fancybox.open([$callbackPopup], {
                        padding: 0,
                        scrolling: 'no',
                        fitToView: false,
                        'helpers': {
                            overlay: {
                                locked: false
                            }
                        },
                        afterShow: function() {
                            $('.callback-form textarea').val('Текст сообщения');                            
                        },
                        afterClose: function() {
                            $overlay.removeClass('active');
                            $content.removeClass('active');
                            $content.find('p').empty();
                            $('.callback-form input[type=text]').each(function(ind, elt) {
                                $(elt).val('');
                                initPlaceholders();
                            });
                            $('.callback-form textarea').val('Текст сообщения');
                        }
                    });

            });
        }
    },

    initFeedbackPopup = function() {
        var $init = $('.js-feedback-form'),
            $feedbackPopup = $('.feedback-form'),
            $overlay = $('.feedback-form overlay'),
            $content = $('.feedback-form .message-form');
        if($init.length) {
            $init.on('click', function(e) {
                e.preventDefault();

                $.fancybox.open([$feedbackPopup], {
                    padding: 0,
                    scrolling: 'no',
                    fitToView: false,
                    'helpers': {
                        overlay: {
                            locked: false
                        }
                    },
                    afterClose: function() {
                        $overlay.removeClass('active');
                        $content.removeClass('active');
                        $content.find('p').empty();

                        $('.callback-form input[type=text]').each(function(ind, elt) {
                            $(elt).val('');
                            initPlaceholders();
                        });
                    }
                });
            });
        }
    },

    hasPlaceholderSupport = function () {
        var input = document.createElement('input');
        return ('placeholder' in input);
    },

    initPlaceholders = function () {
        if (!hasPlaceholderSupport()) {
            $('[placeholder]').focus(function () {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                    input.removeClass('placeholder');
                }
            }).blur(function () {
                var input = $(this);
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.addClass('placeholder');
                    input.val(input.attr('placeholder'));
                }
                if (input.val() == input.attr('placeholder')) {
                    input.attr('data-parsley-value', '');
                } else {
                    input.removeAttr('data-parsley-value');
                }
            }).blur();
        }
        return this;
    },

    initFancyOpen = function() {
    },

    stopPropProductContainer = function() {

        $('.product-container a').on('click', function(event) {
            if ($(this).hasClass('more-info-object')) {
                return true;
            }
            if(!$(this).hasClass('btn')) {
                event.preventDefault();
            }
            event.stopPropagation();

            var idPopup = $(this).attr('href'),
                $popup = $(idPopup);

            if($popup.length) {
                var closeBtn = $popup.find('.close-fancybox');

                $.fancybox.open([$popup], {
                    scrolling: 'no',
                    fitToView: false,
                    'helpers': {
                        overlay: {
                            locked: false
                        }
                    },
                    afterClose: function() {
                        closeBtn.unbind('click');
                    }
                });

                closeBtn.on('click', function(e) {
                    e.preventDefault();
                    $.fancybox.close();
                });

            }

        });
    },
    initLinkAnchor = function () {
        var $init = $('.pfl-scheme'),
            hHeader = $('.sticky-header').outerHeight();
        if($init.length) {
            $init.on('click', function (e) {
                e.preventDefault();
                var $target = $($init.attr('data-link'));
                var coord = $target.offset().top - hHeader;

                $(document).scrollTop(coord);
            });
        }
    },

    initFilter = function() {
        var $init = $('.filter-wrapper');
        if($init.length) {
            filterApplication.init();
        }
    },
    initCountDown = function() {
        var $init = $('.countdown');
        if($init.length) {
            var year = $init.data('year'),
                month = $init.data('mon') - 1,
                day = $init.data('day'),
                hour = $init.data('hour'),
                min = $init.data('min'),
                sec = $init.data('sec');

            // var dateStart = new Date();
            var dateStart = new Date(year, month, day, hour, min, sec); //);

            $init.countdown({until: dateStart});

        }
    },
    initPrint = function() {
        var $printBtn = $('.print');
        if($printBtn.length) {
            $printBtn.on('click', function(e) {
                e.preventDefault();
                setTimeout(function(){window.print();}, 300);
                yaCounter23269252.reachGoal('pechat'); 
                ga('send', 'event', 'кнопка', 'клик', 'печать');
            });
        }
    },
    footerDown = function() {
        var $content = $('.content-wrapper.no-main'),
            $footer = $('#footer');

        var hBody = $("body").height();
        var hWind = $(window).height();
        var hPosTop = $("body").position().top;

        if(hBody + hPosTop < hWind) {
            var d = hWind - hBody;
            var h = $content.height() + d - hPosTop;
            $($content).height(h);
        }

    },
    initStayBid = function() {
        var $init = $('.js-stay-bid');
        if($init.length) {
            $init.on('click', function() {
                dhtmlLoadScript("http://track.auditorius.ru/pixel?id=31546&type=js");
                yaCounter23269252.reachGoal('knopka_zayavka');
                ga('send', 'event', 'Кнопка', 'Клик', 'Оставить заявку');
            }); 
        }
    },
    initQA = function() {
        var $init = $('.sh-q');
        if($init.length) {

            $init.on('click', function(e) {
                e.preventDefault();
                var $this = $(this),
                    $btnRead = $this.find('.qa-read'),
                    $btnHide = $this.find('.qa-hide'),
                    $qaItem = $this.closest('.qa-item'),
                    $qaBody = $qaItem.find('.qa-body');

                if(!$qaItem.hasClass('active')) {
                    $qaItem.addClass('active');
                } else {
                    $qaItem.removeClass('active');
                }

            });

        }
    },
    initMCSBH = function() {
        var $mcsb = $('.mcsb-h');
        if($mcsb.length) {
            $mcsb.mCustomScrollbar({
                axis:"x",
                advanced:{autoExpandHorizontalScroll:true},
                theme:"dark-thin",
                // alwaysShowScrollbar: 2,
                // mouseWheel:{ enable: false }
            });
        }
    },
    initMCSB = function() {
        var $mcsb = $('.mcsb');
        if($mcsb.length) {
            $mcsb.mCustomScrollbar({
                    scrollButtons:{enable:true},
                    theme:"light-thick",
                    scrollbarPosition:"outside"

            });
        }
    },
    initCommand = function() {
        var $command = $('.command');
        if($command.length) {
            var $commandItem = $command.find('.c-item'),
                $commandQuestion = $command.find('.js-command-question'),
                $commandWrapper = $('.command-wrapper');

            $('.ci-content').on('click', function(e) {
                e.stopPropagation();
            });

            $commandItem.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                var $this = $(this);

                if (device.desktop()) {
                    console.log('desktop');
                        // текущий индекс
                    var currInd = $this.data('ind'),
                        // количество блоков в ряду
                        countThisRow = 4,
                        // кол-во рядов
                        countRows = Math.ceil($commandItem.length/countThisRow),
                        //текущий ряд
                        currRow = Math.floor(currInd/countThisRow) + 1,
                        // расстояние между блоками
                        commandML = parseInt($this.css('margin-left')),
                        // 
                        leftPos = '',
                        // всплывающий блок
                        $ciContent = $this.find('.ci-content'),
                        // ширина всплывающего блока
                        wCiContent = $ciContent.outerWidth(),
                        // ширина списка
                        wCommand = $command.width(),
                        // позиция списка
                        commandPos = $command.position().left,
                        // позиция фото левый верхний угол
                        thisPos = $this.position().left + commandML - commandPos,
                        // позиция фото правый верхний угол
                        endThisPos = thisPos + $this.outerWidth() + commandML;

                    if($this.hasClass('active')) {

                        $this.removeClass('active');
                        $command.removeClass('active');
                        $ciContent.find('.cic-wrp').mCustomScrollbar("destroy");

                    } else {

                        if($('.command .c-item.active').length) {
                            return false;
                        } else {
                            // высота всплывающего блока
                            if($commandItem.length > 4) {
                                $ciContent.css('height', $commandItem.height() * 2 + commandML + 'px');
                            }

                            $ciContent.find('.cic-wrp').mCustomScrollbar({
                                // theme:"dark-thin"
                            });

                            // положение всплывающего блока по горизонтали отностильено родителя
                            if(endThisPos + wCiContent > wCommand) {
                                leftPos = -(wCiContent + commandML) + 'px';
                            } else {
                                leftPos = $this.width() + commandML + 'px';                            
                            }
                            $ciContent.css('left', leftPos);

                            // положение всплывающего блока по вертикали
                            if(currRow > 1 && currRow == countRows) {
                                $ciContent.css('top', -$this.outerHeight() - commandML);
                            }

                            $this.addClass('active');
                            $command.addClass('active');
                        }

                    }

                } else {
                    console.log('no desktop');

                    if (!$commandItem.hasClass('active') || $this.hasClass('active')) {
                        // контейнер для всплывающего блока
                        var $ciContentCnt = $('.ci-content-mobile'),
                            $ciContent = $this.find('.ci-content').clone();

                        if($this.hasClass('active')) {
                            $this.removeClass('active');
                            $ciContentCnt.empty();
                            $command.removeClass('active');
                            $commandWrapper.removeClass('active');
                        } else {
                            $this.addClass('active');
                            $ciContentCnt.append($ciContent);
                            $command.addClass('active');
                            $commandWrapper.addClass('active');
                        }
                    }

                }

            });

            $commandQuestion.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                var $this = $(this),
                    $formContent = $this.closest('.cic-wrp').find('form');

                if($formContent.length) {
                    $.fancybox({
                        content: $formContent,
                        padding: 20,
                    });                    
                }

            });

        }
    },
    initSidePanel = function() {
        if(device.desktop()) {
            var $sidePanel = $('.panel-wrapper');
            if($sidePanel.length) {
                var hSidePanel = $sidePanel.height(),
                    realWH = getViewport(),
                    posTop = '';

                posTop = Math.round((realWH[1] - hSidePanel) / 2);
                $sidePanel.css('top', posTop + 'px');
                $sidePanel.show();
            }
        }
    },

    initNewsTimelineSlider = function() {
        var $init = $('.news-timeline-wrapper');
        if($init.length) {
            ntlApplication.init();
        }
    },

    initFavoritesPage = function() {
        var $init = $('.favorites-list');
        if($init.length) {
            favoritesApplication.init();
        }
    },

    initFavoritesAdd = function() {
        var $init = $('.js-favorites-add');
        if($init.length) {
            var $favPanel = $('.favorites-panel'),
                idFav = '',
                data = {};

            $init.on('click', function(e) {
                e.preventDefault();
                var $this = $(this);

                if (!$this.hasClass('js-favorites-add')) {
                    return false;
                }

                data['type'] = 'add';
                data['id'] = $(this).attr('data-id-favorit');

                $.ajax({
                    url: '/ajax/favorites.php/',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    beforeSend: function() {

                    },
                    success: function(answer) {
                        if (!$.isEmptyObject(answer)) {
                            console.log(answer['success']);
                            if(answer['success']) {

                                $('[data-id-favorit=' + data['id'] + ']').removeClass('js-favorites-add');
                                $('[data-id-favorit=' + data['id'] + ']').parent().addClass('active');

                                if($this.hasClass('tooltip')) {
                                    $this.tooltipster('destroy');
                                    $this.attr('title', 'В избранном');
                                    $this.tooltipster({
                                        position: 'bottom',
                                        theme: 'tooltipster-light'
                                    });
                                } else {
                                    $this.text('В избранном');
                                }

                                if($favPanel.find('span').length) {
                                    $favPanel.find('span').text(answer['count']);
                                } else {
                                    var span = $('<span>');
                                    span.text(answer['count']);
                                    $favPanel.append(span);
                                }
                            } else {

                            }
                        }
                    }
                });
            });

            // var setPreventDefault = function($link) {
            //     $link.on('click', function(e) {
            //         e.preventDefault();
            //     });
            // }

        }
    },

    initComparisonAdd = function() {
        var $init = $('.js-comparison-add');
        if($init.length) {
            var $favPanel = $('.comparison-panel-item'),
                idFav = '',
                data = {};

            $init.on('click', function(e) {
                e.preventDefault();
                $this = $(this);

                if (!$this.hasClass('js-comparison-add')) {
                    return false;
                }

                data['type'] = 'add';
                data['id'] = $(this).attr('data-id-comparison');

                $.ajax({
                    url: '/ajax/comparison.php/',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    beforeSend: function() {

                    },
                    success: function(answer) {
                        if (!$.isEmptyObject(answer)) {
                            console.log(answer['success']);
                            if(answer['success']) {

                                $('[data-id-comparison=' + data['id'] + ']').removeClass('js-comparison-add');
                                $('[data-id-comparison=' + data['id'] + ']').parent().addClass('active');

                                if($this.hasClass('tooltip')) {
                                    $this.tooltipster('destroy');
                                    $this.attr('title', 'В сравнении');
                                    $this.tooltipster({
                                        position: 'bottom',
                                        theme: 'tooltipster-light'
                                    });
                                } else {
                                    $this.text('В сравнении');
                                }

                                if($favPanel.find('span').length) {
                                    $favPanel.find('span').text(answer['count']);
                                } else {
                                    var span = $('<span>');
                                    span.text(answer['count']);
                                    $favPanel.append(span);
                                    $this.removeClass('js-comparison-add');
                                    $this.parent().addClass('active');
                                }
                            } else {

                            }
                        }
                    }
                });
            });
        }        
    },

    initSearch = function() {
        var $init = $('.js-search');
        if($init.length) {
            var $searchPanel = $('.header-search-wrp'),
                $btnCloseSearch = $('.js-close-search');

            $init.on('click', function(e) {
                e.preventDefault();
                $searchPanel.addClass('active');
            });

            $btnCloseSearch.on('click', function(e) {
                e.preventDefault();
                $searchPanel.removeClass('active'); 
            });

        }
    },
    initShowMoreQA = function() {
        var $init = $('.qa-tabs-wrapper .tab-content .show-stock');
        if($init.length) {
            $init.on('click', function(e) {
                e.preventDefault();
                var $tabContent = $(this).closest('.tab-content');

                $tabContent.find('.hide').eq(0).removeClass('hide');
                if(!$tabContent.find('.hide').length) {
                    $(this).closest('.show-all').hide();
                }
            });
        }
    },
    initJsPflCredit = function() {
        var $init = $('.js-pfl-credit');
        if($init.length) {
            $init.on('click', function() {
                yaCounter23269252.reachGoal('knopka_kredit'); 
                ga('send', 'event', 'кнопка', 'клик', 'Кнопка рассчитать кредит');
            });
        }
    },
    initComparison = function() {
        var $init = $('.comparison-content');
        if($init.length) {
            var mode = '';
            if (device.desktop() || device.ipad()) {
                mode = 'desktop';
            } else {
                mode = 'mobile';
            }
            comparisonApplication.init(mode);
        }
    },
    initFileUpload = function() {
        var wrapper = $( ".file_upload" );

        $.each(wrapper, function(index, element) {
            var inp = $(element).find( "input" ),
                btn = $(element).find( "button" ),
                lbl = $(element).find( "div" );

            btn.focus(function(){
                inp.focus()
            });
            // Crutches for the :focus style:
            inp.focus(function(){
                $(element).addClass( "focus" );
            }).blur(function(){
                $(element).removeClass( "focus" );
            });

            var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

            inp.change(function(){
                var file_name;
                if( file_api && inp[ 0 ].files[ 0 ] ) 
                    file_name = inp[ 0 ].files[ 0 ].name;
                else
                    file_name = inp.val().replace( "C:\\fakepath\\", '' );

                if( ! file_name.length )
                    return;

                if( lbl.is( ":visible" ) ){
                    lbl.text( file_name );
                    btn.text( "Выбрать" );
                }else
                    btn.text( file_name );
            }).change();
        });

    },
    initExamplesWorkTabs = function() {
        var $exmplWorkItem = $('.mobile-examples-work-wrapper .example-work-item'),
            $init = $('.mobile-examples-work-wrapper .tabs a');

        if($exmplWorkItem.length) {
            $.each($exmplWorkItem, function(index, element) {
                if ($(element).find('.tabs a').length) {
                    $(element).find('.tabs a').tabs();
                }
            });
        }
    },
    initJsToUp = function () {
        $init = $('.js-to-up');

        if ($init.length) {
            $init.on('click', function(e) {
                e.preventDefault();
                $('body').animate({"scrollTop":0},"slow");
            });
        }
    },
    initCommandMobileSlider = function () {
        if (device.mobile()) {
            var $init = $('.command');
            if ($init.length) {
                $init.bxSlider({
                    auto: false,
                    controls: false,
                    pager: false,
                    minSlides: 2,
                    maxSlides: 2,
                    slideWidth: 240,
                    slideMargin: 15
                })
            }
        }
    };

    function getViewport() {
        var viewPortWidth;
        var viewPortHeight;

        // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
        if (typeof window.innerWidth != 'undefined') {
            viewPortWidth = window.innerWidth,
                viewPortHeight = window.innerHeight
        }

        // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
        else if (typeof document.documentElement != 'undefined'
            && typeof document.documentElement.clientWidth !=
            'undefined' && document.documentElement.clientWidth != 0) {
            viewPortWidth = document.documentElement.clientWidth,
                viewPortHeight = document.documentElement.clientHeight
        }

        // older versions of IE
        else {
            viewPortWidth = document.getElementsByTagName('body')[0].clientWidth,
                viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
        }
        return [viewPortWidth, viewPortHeight];
    }

    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    function sendAjaxCredit () {
        var cost = $('#cost').val(),
            instalment = $('#instalment').val(),
            term = $('#term').val(),
            $overlay = $('.credit-message .overlay');

        $.ajax({
            url: '/ajax/calculator_new.php/',
            data: {calc: {cost: cost, term: term, instalment: instalment}},
            dataType: 'json',
            type:'POST',
            beforeSend: function() {
                $overlay.addClass('active');
            },
            success: function(data) {
                                    var current = getCookie('sbjs_current');
                                    $.get( "/js/call/php", { config: "52", token: "fSxb8iB88IHG5tZXTA", sbjs_current:current,form_type: 'calculator_new'} );
                $overlay.removeClass('active');
                if(data['message']) {
                    $('.credit-message .text-container').empty().append('<p>' + data['message'] + '</p>');
                }

                if(data['error']) {
                    $('.credit-message .text-container').empty().append('<p>' + data['error'] + '</p>');
                }

            }
        });
    }

    function getSumCredit () {
        var cost = $('#cost').val(),
            instalment = $('#instalment').val(),
            $sumCreditField = $('#sumcredit');

        $sumCreditField.val(cost - instalment);
    }

    var openModal = function(title,content) {
        var html = '<div class="modal-default modal-msg"><p class="msg-title">'+title+'</p><p class="msg-content">'+content+'</p></div>';
        $.fancybox({
            content: html,
            padding: 20,
        });
    };

$(document).ready(function () {
    // footerDown();
    ////initMainMenu();
    if (device.tablet()) {
        $( '.catalog-menu li:has(.catalog-submenu)' ).doubleTapToGo();
    }

    initSidePanel();
    initStayBid();
    socialShare();
    hiddenLink();
    getAjaxHonors();
    initInputsClick();
    initSHHeaderOnScroll();
    initMainSlider();
    initMosaic();
    initNewsSlider();
    initHoverMosaic();
    initBlurMainSliderOnScroll();
    initSelectChosen();
    initFancybox();
    initTooltip();
    initDevice();
    initHint();
    initCalculator();
    initMap();
    initNumBox();
    initBuildSlider();
    initVideoFancy();
    initBuildsPopup();
    initTabsOptions();
    initSelectParamsHouse();
    initLayoutsSliders();
    initGoToProductFromCatalog();
    initTextarea();
    initValidation();
    initFormElement();
    initCallbackPopup();
    initFeedbackPopup();
    initHonorsGallery();
    initPlaceholders();
    stopPropProductContainer();
    initFacadeSlider();
    initFilter();
    initLinkAnchor();
    initCountDown();
    initPrint();
    initFancyOpen();
    // initQuality();
    initQA();
    // initMCSB();
    initMCSBH();
    initCommand();
    initNewsTimelineSlider();
    initJsPflCredit();
    initFileUpload();
    initFavoritesPage();
    initFavoritesAdd();
    initComparisonAdd();
    initSearch();
    initShowMoreQA();
    initComparison();
    initExamplesWorkTabs();
    initCommandMobileSlider();
    initJsToUp();

});

$(window).on('load', function () {
    menuApplication.init();
    menuApplication.bindEvents();
    initStickyHeader();
    footerDown();
});

$(window).on('resize', function () {
    initStickyHeader();
    footerDown();
    // calcQuality($('.js-quality'));
    initSidePanel();
});


$(document).ready(function () {
  $('.main-slider-wrapper').bind("contextmenu", function(e){ return false; })
});

