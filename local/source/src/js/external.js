if (typeof define != 'function')
    define = function(){};

@@include('../../bower_components/jquery-form/jquery.form.js');
@@include('../../bower_components/Swiper/dist/js/swiper.min.js');

@@include('../../bower_components/jquery-ui/jquery-ui.min.js');
@@include('../../bower_components/jquery-ui/ui/core.js');
@@include('../../bower_components/jquery-ui/ui/widget.js');
@@include('../../bower_components/jquery-ui/ui/position.js');
@@include('../../bower_components/jquery-ui/ui/widgets/menu.js');
@@include('../../bower_components/jquery-ui/ui/widgets/autocomplete.js');
@@include('../../bower_components/jquery-ui/ui/widgets/tooltip.js');

@@include('../../bower_components/fancybox/source/jquery.fancybox.pack.js');
@@include('../../bower_components/jquery-mask-plugin/dist/jquery.mask.min.js');