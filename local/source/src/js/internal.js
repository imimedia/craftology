'use strict';

@@include('partials/dialog.js');

var app = {};

app.content = new function() {
    var klass = function() {
        this._init = false;
        this.callback = {};
        this.data = {};
    };

    klass.prototype.bind = function(cmp, clb, data) {
        if ($.isFunction(clb)) {
            this.callback[cmp] = clb;

            if (typeof data == 'object') {
                if (!this.data[cmp])
                    this.data[cmp] = [];

                for (var param in data) {
                    if (data.hasOwnProperty(param))
                        this.data[cmp][param] = data[param];
                }
            }
        }
    };

    klass.prototype.load = function(cmp) {
        var self = this;

        var cmps = $.isArray(cmp) ? cmp : (typeof cmp == 'string' ? [cmp] : null),
            data = {};
        if (cmps) {
            for (var i = 0; i < cmps.length; i++)
                data[cmps[i]] = this.getCmpData(cmps[i]);
        }
        else {
            for (var _cmp in this.data) {
                if (this.data.hasOwnProperty(_cmp))
                    data[_cmp] = this.getCmpData(_cmp);
            }
        }

        $.ajax({
            url: '/local/api/content/',
            data: data,
            dataType: 'json',
            method: 'post',
            beforeSend: function() {},
            success: function (res) {
                if (!res) return;
                for (var cmp in res) {
                    if (res.hasOwnProperty(cmp)) {
                        if (cmp == 'content') {
                            for (var selector in res[cmp]) {
                                if (res[cmp].hasOwnProperty(selector)) {
                                    var $elem = $(selector);
                                    if ($elem.length) {
                                        $elem.html(res[cmp][selector]);
                                    }
                                }
                            }
                        }
                        else if (self.callback.hasOwnProperty(cmp)) {
                            self.callback[cmp](res[cmp]);
                        }
                    }
                }
            },
            error: function() {},
            complete: function() {}
        });
    };

    klass.prototype.getCmpData = function(cmp) {
        var result = {};
        if (this.data.hasOwnProperty(cmp)) {
            for (var name in this.data[cmp]) {
                if (this.data[cmp].hasOwnProperty(name)) {
                    if ($.isFunction(this.data[cmp][name]))
                        result[name] = this.data[cmp][name]();
                    else
                        result[name] = this.data[cmp][name];
                }
            }
        }
        return result;
    };

    return new klass;
};

var getNumEnding = function(num, endings) {
    num = parseInt(num, 10) % 100;
    if (num >= 11 && num <= 19) {
        endings = endings[2];
    }
    else {
        switch (num % 10) {
            case (1): endings = endings[0]; break;
            case (2):
            case (3):
            case (4): endings = endings[1]; break;
            default: endings = endings[2];
        }
    }
    return endings;
};

(function($) {

    var o = $({});

    $.subscribe = function() {
        o.on.apply(o, arguments);
    };

    $.unsubscribe = function() {
        o.off.apply(o, arguments);
    };

    $.publish = function() {
        o.trigger.apply(o, arguments);
    };

}(jQuery));

var initFormFields = function() {
        initMask();
    },

    initMainBanner = function() {
        var sliders = $('.main-banner');
        if (sliders.length) {
            $(sliders).each(function(){
                var swiper = new Swiper(this, {
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    spaceBetween: 0,
                    autoplay: 4000
                });

                var $slider = $(swiper.wrapper).parent();
                var $btnPlay = $slider.find('.js-btnSliderPlay');
                if ($btnPlay.length) {
                    $btnPlay.click(function(){
                        if ($slider.hasClass('pause'))
                            swiper.startAutoplay();
                        else
                            swiper.stopAutoplay();
                        $slider.toggleClass('pause');
                    });
                }
            });
        }
    },
    initSlider = function() {
        var sliders = $('.slider');
        if (sliders.length) {
            $(sliders).each(function(){
                var $slider = $(this),
                    pagination = null,
                    nextButton = null,
                    prevButton = null,
                    $paginationWrap = $(this).data('pagination') && $($(this).data('pagination'));
                if (!$paginationWrap || !$paginationWrap.length)
                    $paginationWrap = $slider.find('.slider-nav');
                if ($paginationWrap && $paginationWrap.length) {
                    pagination = $paginationWrap.find('.swiper-pagination').length ? $paginationWrap.find('.swiper-pagination') : null;
                    nextButton = $paginationWrap.find('.swiper-button-next').length ? $paginationWrap.find('.swiper-button-next') : null;
                    prevButton = $paginationWrap.find('.swiper-button-prev').length ? $paginationWrap.find('.swiper-button-prev') : null;
                }
                else {
                    var _pagination = $slider.find('.swiper-pagination');
                    pagination = _pagination.length ? _pagination : null;
                    var _nextButton = $slider.find('.swiper-button-next');
                    nextButton = _nextButton.length ? _nextButton : null;
                    var _prevButton = $slider.find('.swiper-button-prev');
                    prevButton = _prevButton.length ? _prevButton : null;
                }

                var options = {
                    pagination: pagination,
                    paginationClickable: true,
                    nextButton: nextButton,
                    prevButton: prevButton,
                    spaceBetween: 30,
                    setWrapperSize: true,
                    autoplay: 2500,
                    onInit: function(swiper) {
                        if (swiper.slides.length > swiper.params.slidesPerView) {
                            if ($paginationWrap && $paginationWrap.length) {
                                $paginationWrap.show();
                            }
                            else {
                                $(swiper.paginationContainer).hide();
                                $(swiper.prevButton).hide();
                                $(swiper.nextButton).hide();
                            }
                        }
                    }
                };

                var rows = parseInt($(this).data('sliderRows'), 10) || 1;
                options.slidesPerColumn = rows;

                var cols = parseInt($(this).data('sliderCols'), 10) || 3;
                options.slidesPerView = cols;

                var swiper = new Swiper(this, options);
            });
        }
    },
    initMainNav = function() {
        var elements = $('.js-btnOpenSubMainnav');
        if (elements.length) {
            $(elements).each(function() {
                var $btn = $(this);
                if ($btn.data('initSubMainnav'))
                    return;

                var $wrap = $btn.parent(),
                    $content = $wrap.find('.sub-mainnav'),
                    prevent = false;

                function close(e) {
                    if (prevent || $(e.target).closest($content).length) {
                        prevent = false;
                        return;
                    }
                    $wrap.removeClass('open');
                    $(document).unbind('click', close);
                }

                $btn.click(function(e){
                    e.preventDefault();
                    if ($wrap.hasClass('open')) {
                        close(e);
                    }
                    else {
                        prevent = true;
                        $wrap.addClass('open');
                        $(document).bind('click', close);
                    }
                });
            });
        }

        var elements = $('.js-btnHoverSubMainnav');
        if (elements.length) {
            $(elements).each(function () {
                var $btn = $(this);
                if ($btn.data('initSubMainnav'))
                    return;

                var $wrap = $btn.parent(),
                    timerId;

                $wrap.hover(
                    function(e){
                        timerId = setTimeout(function(){
                            if (!$wrap.hasClass('open') && $wrap.is(':hover')) {
                                $wrap.addClass('open');
                            }
                        }, 200);
                    },
                    function(e){
                        timerId = setTimeout(function(){
                            if ($wrap.hasClass('open') && !$wrap.is(':hover')) {
                                $wrap.removeClass('open');
                            }
                        }, 100);
                    }
                );
            });
        }
    },
    initDialog = function() {
        $(document).on('click', '.js-dialog', function(e){
            e.preventDefault();
            var obj = $(this).data('appDialog');
            if (!obj) {
                obj = new Dialog(this, {
                    afterLoadSuccess: function(){
                        initFormFields();
                    }
                });
                $(this).data('appDialog', obj);
            }
            obj.open();
        });
    },
    initDialogCloseBtn = function() {
        var elements = $('.js-dialogCloseBtn');
        if (elements.length) {
            $(elements).each(function () {
                var $btn = $(this);
                if ($btn.data('initDialogCloseBtn'))
                    return;
                $btn.data('initDialogCloseBtn', true);

                $btn.click(function(e){
                    e.preventDefault();
                    Dialog.close();
                });
            });
        }
    },
    initQty = function() {
        var elements = $('.qty');
        if ($(elements).length) {
            $(elements).each(function(){
                var $qty = $(this);
                if ($qty.data('initQty'))
                    return;

                var max = parseInt($qty.data('max'), 10) || 99999,
                    $input = $qty.find('input[type=text]'),
                    $incr = $qty.find('.qty-incr'),
                    $decr = $qty.find('.qty-decr'),
                    calc = function($input, incr, max) {
                        var val = parseInt($input.val(), 10) || 1,
                            prevVal = $input.data('prevVal');
                        if (incr)
                            val = val + incr;

                        if (val < 1)
                            val = 1;
                        else if (val > max) {
                            $qty.addClass('qty-out-range');
                            setTimeout(function(){ $qty.removeClass('qty-out-range'); }, 2000);
                            val = prevVal;
                        }

                        $input.val(val);
                        $input.data('prevVal', val);
                    };
                $input.data('prevVal', $input.val());

                if ($input.length) {
                    $input.on('input', function(e){
                        if ($input.val() == '')
                            return;
                        calc($input, 0, max);
                    });
                    $input.on('focusout', function(e){
                        if ($input.val() == '')
                            $input.val(1);
                    });
                    if ($incr.length) {
                        $incr.click(function(e){
                            e.preventDefault();
                            calc($input, 1, max);
                            $input.trigger('input');
                        });
                    }
                    if ($decr.length) {
                        $decr.click(function(e){
                            e.preventDefault();
                            calc($input, -1, max);
                            $input.trigger('input');
                        });
                    }
                }
            });
        }
    },
    initRating = function() {
        var elements = $('.js-rating');
        if ($(elements).length) {
            $(elements).each(function(){
                var $rating = $(this);
                if ($rating.data('initRating'))
                    return;

                var lock = false,
                    id = $rating.closest('[data-id]').data('id'),
                    stars = $rating.children('span'),
                    rateActual = $rating.data('rate') || 0,
                    set = function(rate) {
                        rate = rate || rateActual;
                        $rating.data('rate', rate).attr('data-rate', rate);
                    };
                if (!id)
                    return;

                $(stars).each(function(i){
                    $(this).data('rate', i+1);
                });

                $(stars).mouseenter(function(e){
                    if (lock)
                        return;
                    var rate = $(this).data('rate');
                    set(rate);
                });

                $(stars).click(function(e){
                    if (lock)
                        return;

                    lock = true;
                    $rating.removeClass('js-rating');

                    var rate = $(this).data('rate'),
                        data = { id: id, action: 'set', vote: $(this).index() + 1 };

                    $.ajax({
                        url: '/local/api/rating/',
                        data: data,
                        dataType: 'json',
                        beforeSend: function(){
                            $rating.addClass('rating-loading');
                        },
                        success: function(data){
                            if (data.rating) {
                                rateActual = data.rating;
                            }
                        },
                        error: function(){},
                        complete: function() {
                            set();
                            $rating.removeClass('rating-loading');
                        }
                    });
                });

                $rating.mouseleave(function(e){
                    if (lock)
                        return;
                    set();
                });
            });
        }
    },
    initPreview = function() {
        var elements = $('.js-preview');
        if ($(elements).length) {
            $(elements).each(function(){
                var $wrap = $(this);
                if ($wrap.data('initPreview'))
                    return;

                var $large = $('.js-previewLarge'),
                    items = $wrap.find('.js-previewSliderItem'),
                    images = $(items).find('img'),
                    links = [];

                for (var i = 0; i < images.length; i++)
                    links.push($(images[i]).attr('data-src'));

                if (images.length == 1)
                    $wrap.find('.js-previewSlider').hide();

                $(images).eq(0).addClass('active');
                $large.data('imgIndex', 0);

                $(images).click(function(e) {
                    $(images).removeClass('active');
                    $(this).addClass('active');
                    $large.attr('src', $(this).data('large')).data('imgIndex', $(images).index(this));
                });

                $large.click(function() {
                    var $image = $(this);
                    $.fancybox.open(links, {
                        index: $image.data('imgIndex'),
                        prevEffect: 'none',
                        nextEffect: 'none',
                        padding: 0,
                        margin: 50
                    });
                });
                $large.siblings('.zoom').click(function(e) {
                    $large.trigger('click');
                });
            });
        }
    },
    initDropdown = function() {
        var elements = $('.dropdown');
        if (elements.length) {
            $(elements).each(function(){
                var $dropdown = $(this);
                if ($dropdown.data('initDropdown'))
                    return;

                var $trigger = $dropdown.find('.dropdown__trigger'),
                    $content = $dropdown.find('.dropdown__content'),
                    prevent = false;

                function close(e) {
                    if (prevent || $(e.target).closest($content).length) {
                        prevent = false;
                        return;
                    }
                    $dropdown.removeClass('dropdown-open');
                    $(document).unbind('click', close);
                }

                $trigger.click(function(e){
                    e.preventDefault();
                    if ($dropdown.hasClass('dropdown-open')) {
                        close(e);
                    }
                    else {
                        prevent = true;
                        $dropdown.addClass('dropdown-open');
                        $(document).bind('click', close);
                    }
                });
            });
        }
    },
    initMask = function() {
        var elements = $('.js-mask');
        if (elements.length) {
            $(elements).each(function() {
                var $this = $(this);

                if ($this.data('initMask'))
                    return;
                $this.data('initMask', true);

                var opts = {
                        clearIfNotMatch: true
                    },
                    mask = $this.attr('data-pattern'),
                    pattern = '';

                switch (mask) {
                    case 'phone':   pattern = '8 (000) 000-00-00'; break;
                    case 'date':    pattern = '00.00.0000'; break;
                    case 'time':    pattern = '00:00'; break;
                }

                if (!pattern)
                    pattern = mask;

                if (pattern)
                    $(this).mask(pattern, opts);
            });
        }
    },
    initFixedHeader = function() {
        var $element = $('.header-main'),
            $body = $('body'),
            top = $element.offset().top;
        $(document).scroll(function(e) {
            var scroll = window.pageYOffset;
            if (top < scroll && !$body.isFixed) {
                $body.addClass('header-fixed');
                $body.isFixed = true;
            }
            else if (top >= scroll && $body.isFixed) {
                $body.removeClass('header-fixed');
                $body.isFixed = false;
            }
        });
    },
    initBreakPhone = function() {
        setTimeout(function(){
            $('.header-phone').css('margin-top', 0);
        }, 200);
    },
    initTabs = function() {
        var tabs = $('.tabs');
        if (tabs.length) {
            $(tabs).each(function(){
                var $tabs = $(this),
                    tabs = $tabs.find('.tabs-tab'),
                    panes = $tabs.find('.tabs-pane');

                var select = function($tab) {
                    $(tabs).removeClass('active');
                    $tab.addClass('active');

                    $(panes).removeClass('active');
                    $(panes).eq($(tabs).index($tab)).addClass('active');
                };

                $(tabs).click(function(e){
                    e.preventDefault();
                    select($(this));
                });

                var $tab = $(tabs).filter('.active');
                if ($tab.length)
                    select($tab);
                else
                    select($(tabs).eq(0));
            });
        }
    },
    initSocialPopupBtn = function() {
        var elements = $('.js-socialPopupBtn');
        if (elements.length) {
            $(elements).each(function() {
                var $this = $(this);

                if ($this.data('initSocialPopupBtn'))
                    return;
                $this.data('initSocialPopupBtn', true);

                $this.click(function(e) {
                    e.preventDefault();
                    var width = $(this).data('width'),
                        height = $(this).data('height'),
                        left = screen.availWidth / 2 - width / 2,
                        top = screen.availHeight / 2 - height / 2,
                        win = window.open($(this).attr('href'),
                            'socialpopup',
                            'width=' + width + ',height=' + height + ',top=' + top + ',left=' + left + ',location=no'
                        );
                    win.focus();
                });
            });
        }
    },
    initFormHtml = function() {
        var elements = $('.js-formHtml');
        if (elements.length) {
            $(elements).each(function () {
                var $form = $(this);

                if ($form.data('initForm'))
                    return;
                $form.data('initForm', true);

                var $formWrap = $form.closest('.js-formWrap'),
                    $submit = $form.find('[type=submit]');
                if (!$formWrap.length)
                    $formWrap = $form;

                $form.ajaxForm({
                    data: { ajax: 'y' },
                    beforeSubmit: function(formData, jqForm, options){
                        $submit.prop('disabled', true);
                    },
                    success: function(responseText, statusText, xhr, $form){
                        $submit.prop('disabled', false);
                        $formWrap.html(responseText);
                    }
                });
            });
        }
    },
    initForm = function() {
        var elements = $('.js-form');
        if (elements.length) {
            $(elements).each(function () {
                $(this).on('submit', function(e){
                    e.preventDefault();

                    var $form = $(this),
                        $submit = $form.find('[type=submit]');

                    var options = {
                        data: { ajax: 'y' },
                        dataType: 'json',
                        beforeSubmit: function(formData, jqForm, options) {
                            $submit.prop('disabled', true);
                            $form.find('.error').removeClass('.error');
                            $form.find('.errors').removeClass('.errors').empty();
                            $form.find('.successes').removeClass('.successes').empty();
                        },
                        success: function(json, statusText, xhr, jqForm) {
                            $submit.prop('disabled', false);

                            if (json.success) {
                                if (json.reloadPage)
                                    window.location.reload();

                                if (json.successes && $.isArray(json.successes)) {
                                    if ($form.find('.js-form-msg-successes')) {
                                        $form.find('js-form-msg-successes').addClass('successes').html(json.successes.join('<br/>'));
                                    }
                                    else if ($form.find('.js-form-msg').length) {
                                        $form.find('.js-form-msg').addClass('successes').html(json.successes.join('<br/>'));
                                    }
                                }
                            }
                            else {
                                var errors = [];

                                if (json.errors && typeof json.errors == 'object') {
                                    for (var eid in json.errors) {
                                        if (json.errors.hasOwnProperty(eid)) {
                                            if (!$.isNumeric(eid) && $form.find('#' + eid).length) {
                                                $form.find('#' + eid).addClass('error');
                                            }
                                            else {
                                                errors.push(json.errors.eid);
                                            }
                                        }
                                    }
                                }

                                if (json.error) {
                                    errors.push(json.error);
                                }

                                if (errors.length) {
                                    if ($form.find('.js-form-msg-errors').length) {
                                        $form.find('js-form-msg-errors').addClass('errors').html(errors.join('<br/>'));
                                    }
                                    else if ($form.find('.js-form-msg').length) {
                                        $form.find('.js-form-msg').addClass('errors').html(errors.join('<br/>'));
                                    }
                                }
                            }

                            if (json.saveSize) {
                                $form.parent().outerWidth($form.parent().outerWidth());
                                $form.parent().outerHeight($form.parent().outerHeight());
                            }

                            if (json.content && typeof json.content == 'object') {
                                var $formWrap = $form.closest('.js-formWrapper');
                                for (var eid in json.content) {
                                    if (json.content.hasOwnProperty(eid)) {
                                        var $container;
                                        if ($formWrap.length && $formWrap.find('#' + eid).length)
                                            $container = $formWrap.find('#' + eid);
                                        else if ($form.parent().find('#' + eid).length)
                                            $container = $form.parent().find('#' + eid);
                                        if ($container && $container.length) {
                                            $container.html(json.content[eid]);
                                        }
                                    }
                                }
                            }
                        }
                    };

                    $form.ajaxSubmit(options);
                });
            });
        }
    },
    initFormReviews = function() {
        var elements = $('.js-formReviews');
        if (elements.length) {
            $(elements).each(function () {
                var $form = $(this);

                if ($form.data('initFormReviews'))
                    return;
                $form.data('initFormReviews', true);

                var $submit = $form.find('[type=submit]');

                $form.ajaxForm({
                    data: { ajax: 'y', action: 'add' },
                    dataType: 'json',
                    beforeSubmit: function(formData, jqForm, options){
                        $submit.prop('disabled', true);
                    },
                    success: function(data, statusText, xhr, jqForm){
                        $submit.prop('disabled', false);
                        if (data) {
                            if (data.SUCCESS == 'OK') {
                                app.content.load('reviews');
                                $form.find('[type=text], textarea').val('');
                            }
                            else {}
                        }
                    }
                });
            });
        }
    },
    initFileUpload = function() {
        var elements = $('.js-fileUpload');
        if (elements.length) {
            $(elements).each(function () {
                var $file = $(this);

                if ($file.data('initFileUpload'))
                    return;
                $file.data('initFileUpload', true);

                var $label = $file.parent().find('.js-fileUploadLabel'),
                    text = $label.text();
                $file.change(function(e){
                    if ($file[0].files.length)
                        $label.text($file[0].files[0].name);
                    else
                        $label.text(text);
                });
            });
        }
    },
    initAdd2Cart = function() {
        var buttons = $('.js-add2cart');
        if (buttons.length) {
            $(buttons).each(function () {
                var $btn = $(this);

                if ($btn.data('initAdd2Cart'))
                    return;
                $btn.data('initAdd2Cart', true);

                $btn.click(function(e){
                    e.preventDefault();

                    var $product = $btn.closest('[data-product]'),
                        productId = $product.length && parseInt($product.data('product'), 10) > 0 ? parseInt($product.data('product'), 10) : 0;
                    if (productId) {
                        var $qty = $product.find('.js-qtyInput'),
                            qty = $qty.length && !isNaN($qty.val()) && parseFloat($qty.val()) > 0 ? parseFloat($qty.val()) : 1,
                            available = !isNaN($qty.data('available')) ? parseFloat($qty.data('available')) : -1;
                        if (available >= qty) {
                            var data = {
                                action: 'ADD2BASKET',
                                id: productId,
                                quantity: qty
                            };
                            $.ajax({
                                url: '/local/api/catalog/',
                                data: data,
                                dataType: 'json',
                                beforeSend: function () {
                                    $btn.prop('disabled', true);
                                },
                                success: function (data) {
                                    if (data && data.hasOwnProperty('SUCCESS') && data.SUCCESS == 'OK') {
                                        var label = $btn.html();
                                        $btn.html('В корзине');
                                        setTimeout(function () {
                                            $btn.html(label);
                                        }, 2000);
                                    }

                                    $btn.addClass('is-added').find('path').eq(0).animate({'stroke-dashoffset': 0}, 300, function () {
                                        setTimeout(function () {
                                            $btn.removeClass('is-added');
                                            setTimeout(function () {
                                                $btn.find('path').css('stroke-dashoffset', '19.79');
                                            }, 1000);
                                        }, 600);
                                    });

                                    app.content.load('cart');
                                },
                                error: function () {
                                },
                                complete: function () {
                                    $btn.prop('disabled', false);
                                }
                            });
                        }
                        else {
                            var $notice = $('<div/>', {
                                'class': 'notice-no-available',
                                html: 'К сожалению, данного товара на складе осталось <br/>только ' + available + ' шт.'
                            });
                            $qty.val(available).after($notice);
                            setTimeout(function(){
                                $notice.remove();
                            }, 3000);
                        }
                    }
                });
            });
        }
    },
    initFavoriteBtn = function() {
        var buttons = $('.js-favoriteBtn');
        if (buttons.length) {
            $(buttons).each(function () {
                var $btn = $(this);

                if ($btn.data('initFavoriteBtn'))
                    return;
                $btn.data('initFavoriteBtn', true);

                $btn.click(function (e) {
                    e.preventDefault();

                    var $product = $btn.closest('[data-product]'),
                        productId = $product.length && parseInt($product.data('product'), 10) > 0 ? parseInt($product.data('product'), 10) : 0,
                        data = {
                            action: $btn.hasClass('in-favorite') ? 'remove' : 'add',
                            id: productId
                        };
                    if (productId) {
                        $.ajax({
                            url: '/local/api/favorite/',
                            data: data,
                            dataType: 'json',
                            beforeSend: function () {
                                $btn.prop('disabled', true);
                            },
                            success: function (data) {
                                if (data && data.hasOwnProperty('SUCCESS') && data.SUCCESS == 'OK') {
                                    $btn.toggleClass('in-favorite');
                                    app.content.load('favorite');

                                    if (!$btn.hasClass('in-favorite') && $btn.closest('.page-content-favorite').length) {
                                        $product.remove();
                                    }
                                }
                            },
                            error: function () {},
                            complete: function () {
                                $btn.prop('disabled', false);
                            }
                        });
                    }
                });
            });
        }
    },
    initProductSubscribeBtn = function() {
        var buttons = $('.js-productSubscribeBtn');
        if (buttons.length) {
            $(buttons).each(function () {
                var $btn = $(this);

                if ($btn.data('initProductSubscribeBtn'))
                    return;
                $btn.data('initProductSubscribeBtn', true);

                $btn.click(function (e) {
                    e.preventDefault();

                    var $product = $btn.closest('[data-product]'),
                        productId = $product.length && parseInt($product.data('product'), 10) > 0 ? parseInt($product.data('product'), 10) : 0,
                        data = {
                            action: $btn.hasClass('js-productSubscribeBtnOff') ? 'delete' : 'add',
                            product_id: productId
                        };
                    if (productId) {
                        $.ajax({
                            url: '/local/api/product_subscribe/',
                            data: data,
                            dataType: 'json',
                            beforeSend: function () {
                                $btn.prop('disabled', true);
                            },
                            success: function (data) {
                                if (data && data.hasOwnProperty('SUCCESS') && data.SUCCESS == 'OK') {
                                    app.content.load('product_subscribe');
                                }
                            },
                            error: function () {},
                            complete: function () {
                                $btn.prop('disabled', false);
                            }
                        });
                    }
                });
            });
        }
    },
    initCart = function() {
        var cart = $('.js-cart');
        if (cart.length) {
            $(cart).each(function () {
                var $cart = $(this);

                if ($cart.data('initCart'))
                    return;
                $cart.data('initCart', true);

                var $allQty = $cart.find('.js-cartAllQty'),
                    $allSum = $cart.find('.js-cartAllSum'),
                    timeout,
                    lock = false;

                var setCartFields = function($item, data, action) {
                    var itemId = $item.data('cartItem');
                    $allSum.html(data.BASKET_DATA.allSum_FORMATED);

                    var qty = 0;
                    for (var i = 0; i < data.BASKET_DATA.ITEMS.AnDelCanBuy.length; i++) {
                        var item = data.BASKET_DATA.ITEMS.AnDelCanBuy[i];
                        qty += item.QUANTITY;
                        if (item.ID == itemId) {
                            $item.find('.js-cartItemSum').html(item.SUM);
                        }
                    }
                    $allQty.html(qty + getNumEnding(qty, [' товар', ' товара', ' товаров']));
                };

                var sent = function($item, data, callback) {
                    $.ajax({
                        url: '/local/api/cart/',
                        data: data,
                        dataType: 'json',
                        method: 'post',
                        beforeSend: function () {},
                        success: function (data) {
                            if (data) {
                                if ($.isFunction(callback))
                                    callback($item, data);
                                setCartFields($item, data, 'qty');
                            }
                        },
                        error: function () {},
                        complete: function () {}
                    });
                };

                $cart.on('input', '.js-cartItemQty', function(e){
                    var $input = $(this),
                        qty = $input.length && !isNaN($input.val()) && parseFloat($input.val()) > 0 ? parseFloat($input.val()) : 1,
                        available = !isNaN($input.data('available')) ? parseFloat($input.data('available')) : -1;
                    if (available >= qty) {
                        if (!lock) {
                            lock = true;
                            timeout = setTimeout(function () {
                                lock = false;
                                var $item = $input.closest('[data-cart-item]'),
                                    cartId = $item.data('cartItem'),
                                    data = {
                                        action_var: 'basketAction',
                                        basketAction: 'recalculate',
                                        select_props: 'NAME,DISCOUNT,WEIGHT,DELETE,DELAY,TYPE,PRICE,QUANTITY'
                                    };
                                data['QUANTITY_' + cartId] = $input.val();
                                sent($item, data);
                            }, 0);
                        }
                    }
                    else {
                        var $notice = $('<div/>', {
                            'class': 'notice-no-available',
                            html: 'К сожалению, данного товара на складе осталось<br/> только ' + available + ' шт.'
                        });
                        $input.val(available).after($notice);
                        setTimeout(function(){
                            $notice.remove();
                        }, 3000);
                    }
                });

                $cart.on('click', '.js-cartItemDelete', function(e){
                    e.preventDefault();
                    var $btn = $(this),
                        $item = $btn.closest('[data-cart-item]'),
                        cartId = $item.data('cartItem'),
                        data = {
                            action_var: 'basketAction',
                            basketAction: 'recalculate',
                            select_props: 'NAME,DISCOUNT,WEIGHT,DELETE,DELAY,TYPE,PRICE,QUANTITY'
                        };
                    data['DELETE_' + cartId] = 'Y';
                    sent($item, data, function($item, data){
                        $item.remove();
                    });
                });

                $cart.click('.js-cartItemDelete', function(e){});
            });
        }
    },
    initSearchTitle = function() {
        var cart = $('.js-searchTitle');
        if (cart.length) {
            $(cart).each(function () {
                var $input = $(this);

                if ($input.data('initSearchTitle'))
                    return;
                $input.data('initSearchTitle', true);

                var $inputWrap = $input.closest('.js-searchTitleWrapper'),
                    $html,
                    close = function(e) {
                        if (e && $(e.target).closest($inputWrap).length)
                            return;
                        if ($html && $html.length)
                            $html.remove();
                        $(document).unbind('click', close);
                    };

                $input.on('input', function(e){
                    var val = $.trim($input.val()),
                        data = {
                            ajax_call: 'y',
                            q: val,
                            l: 3
                        };
                    if (val.length > 2) {
                        $.ajax({
                            url: window.location,
                            data: data,
                            dataType: 'html',
                            method: 'post',
                            beforeSend: function () {
                            },
                            success: function (html) {
                                close();
                                if ($.trim(html).length) {
                                    $html = $($('<div/>').html(html).contents());
                                    $html.outerWidth($input.outerWidth());
                                    $html.appendTo($inputWrap);
                                    $(document).on('click', close);
                                }
                            },
                            error: function () {
                            },
                            complete: function () {
                            }
                        });
                    }
                });
            });
        }
    },
    initTooltip = function() {
        var elements = $('.js-tooltip');
        if (elements.length) {
            $(elements).each(function () {
                var $elem = $(this);

                if ($elem.data('initTooltip'))
                    return;
                $elem.data('initTooltip', true);

                $elem.tooltip({
                    position: {
                        my: 'center top',
                        at: 'center bottom+9',
                        collision: 'none'
                    }
                });
            });
        }
    };

jQuery(function($) {
    initBreakPhone();
    initMainBanner();
    initSlider();
    initMainNav();
    initDialog();
    initDialogCloseBtn();
    initQty();
    initPreview();
    initDropdown();
    initMask();
    initFixedHeader();
    initTabs();
    initSocialPopupBtn();
    initFormHtml();
    initForm();
    initFormReviews();
    initFileUpload();
    // initAdd2Cart();
    initFavoriteBtn();
    initProductSubscribeBtn();
    initCart();
    initSearchTitle();
    initTooltip();
});