var Dialog = function(elem, options){
    var self = this;
    this.$elem = $(elem);
    this.options = $.extend({}, Dialog.options._default, options || {},  Dialog.options[this.$elem.data('dialog')] || {});

    var url = this.$elem.attr('href') || this.$elem.data('href') || '';
    if (url) {
        this.options.url = url;
        this.options.type = 'ajax';
    }

    this.type = this.options.type;

    this.init();
};

Dialog.stack = [];

Dialog.close = function() {
    for (var i = 0; Dialog.stack.length; i++) {
        Dialog.stack[i].close();
    }
};

Dialog.open = function(dialog, options) {
    var $elem = $('<span/>');
    $elem.data('dialog', dialog);

    (new Dialog($elem, options)).open();
};

Dialog.prototype.init = function() {
    var self = this;
    this.create();
};

Dialog.prototype.create = function() {
    var self = this;
    this.$dialog = $('<div/>', {
        'class': 'dialog ' + this.options.wrapCls,
        click: function(e){
            if ($(e.target).closest('.dialog-c').length)
                return;
            self.close();
        }
    });
    var a = $('<div class="dialog-a"></div>').appendTo(this.$dialog),
        b = $('<div class="dialog-b"></div>').appendTo(a),
        c = $('<div class="dialog-c"></div>').appendTo(b);
    this.$content = $('<div class="dialog-content"></div>').appendTo(c);
    this.$close = $('<div/>', {
        'class': 'dialog-close',
        //'text': '×',
        click: function() {
            self.close();
        }
    }).appendTo(c);
};

Dialog.prototype.open = function() {
    var self = this;

    $('body').addClass('dialog-lock');
    this.$content.empty();
    this.options.beforeOpen(self);

    if (this.type == 'ajax') {
        $.ajax({
            url: this.options.url,
            beforeSend: function() {
                self.$content.empty();
                self.$dialog.addClass('dialog-loading');
                self.$dialog.appendTo('body');
            },
            success: function(data) {
                self.$dialog.removeClass('dialog-loading');
                self.$content.html(data);

                self.options.afterLoadSuccess(self, data);
            }
        });
    }
    else {
        self.$dialog.appendTo('body');
    }

    Dialog.stack.push(this);
};

Dialog.prototype.close = function() {
    var self = this;
    this.$dialog.detach();
    Dialog.stack.pop();
    if (!Dialog.stack.length)
        $('body').removeClass('dialog-lock');
};

Dialog.options = {
    _default: {
        type: '',
        wrapCls: '',
        url: '',
        beforeOpen: function(){},
        afterLoadSuccess: function(){}
    },
    auth: {
        type: 'ajax',
        url: '/local/api/signin/',
        wrapCls: 'dialog-wrap__auth',
        beforeOpen: function(){
            Dialog.close();
        },
        afterLoadSuccess: function(){
            initForm();
            initMask();
            initSocialPopupBtn();
            initDialogCloseBtn();
        }
    },
    registration: {
        type: 'ajax',
        url: '/local/api/signup/',
        wrapCls: 'dialog-wrap__auth',
        beforeOpen: function(){
            Dialog.close();
        },
        afterLoadSuccess: function(){
            initForm();
            initMask();
            initSocialPopupBtn();
            initDialogCloseBtn();
        }
    },
    forgotpasswd: {
        type: 'ajax',
        url: '/local/api/forgotpasswd/',
        wrapCls: 'dialog-wrap__auth',
        beforeOpen: function(){
            Dialog.close();
        },
        afterLoadSuccess: function(){
            initForm();
            initMask();
            initSocialPopupBtn();
            initDialogCloseBtn();
        }
    }
};