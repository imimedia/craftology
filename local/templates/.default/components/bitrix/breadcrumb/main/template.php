<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $breadcrumbsIcon;
$icon = isset($breadcrumbsIcon) && trim($breadcrumbsIcon) ? ' '.$breadcrumbsIcon : '';

$s = '';
if ($arResult) {
    $s .= '<div class="breadcrumbs"><div class="container"><div class="breadcrumbs__items'.$icon.'">';
    $num_items = count($arResult);
    for ($index = 0, $itemSize = $num_items; $index < $itemSize; $index++) {
        $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
        if ($arResult[$index]["LINK"] != "" && $index != $itemSize-1)
            $s .= ' <a href="'.$arResult[$index]["LINK"].'">'.$title.'</a>';
        else
            $s .= ' <span>'.$title.'</span>';
    }
    $s .= '</div></div></div>';
}

return $s;