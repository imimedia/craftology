<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if ($arResult['SEARCH']) {
    $APPLICATION->SetTitle('Показаны результаты поиска по запросу<br>"'.$arResult['REQUEST']['QUERY'].'" <span class="text-xxs">(найдено&nbsp;-&nbsp;'.($arResult['NAV_RESULT']->NavRecordCount).')</span>');
}