<script>
    $(document).ready(function(){
        $('.js-quantity .qty input').val(1);
        $('.js-quantity .js-buy').removeClass("active");
        $('.js-quantity .js-buy').text("Купить");
        $('.js-quantity .js-bask').html('');
    });
</script>
<?
  global $arBasket;
  foreach ($arBasket["QUANTITY_BASKET"] as $key=>$Q)
  {
   ?>
     <script>
        $(document).ready(function(){
           $('.js-quantity[data-id="<?=$key?>"] .qty input[data-id="<?=$key?>"]').val(<?=$Q?>);
            <?if ($Q>0) :?>
               $('.js-quantity[data-id="<?=$key?>"] .js-buy').addClass("active");
               $('.js-quantity[data-id="<?=$key?>"] .js-buy').text("+");
               $('.js-quantity[data-id="<?=$key?>"] .js-bask').html('Добавлено в <a href="/personal/cart/">корзину</a>');
            <?else:?>
               $('.js-quantity[data-id="<?=$key?>"] .js-buy').removeClass("active");
               $('.js-quantity[data-id="<?=$key?>"] .js-buy').text("Купить");
               $('.js-quantity[data-id="<?=$key?>"] .js-bask').html('');
            <?endif?>
        });
     </script>
   <?
  }
?>