<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
foreach ($arResult['ITEMS'] as $key => $arItem) {
     if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")){
        $quantity_element = 0;
        $dbBasketItems = CSaleBasket::GetList(
                array(
                   "NAME" => "ASC",
                   "ID" => "ASC"
                ),
                array(
                  "FUSER_ID" => CSaleBasket::GetBasketUserID(false),
                  "LID" => SITE_ID,
                  "PRODUCT_ID" => $arItem["ID"],
                  "ORDER_ID" => "NULL"
                ),
                array("ID","QUANTITY")
             );
             if($arItems = $dbBasketItems->Fetch()){
                  $quantity_element = $arItems["QUANTITY"];
             }
         $arResult['ITEMS'][$key]["QUANTITY_BASKET"] = $quantity_element;
        }
}

?>