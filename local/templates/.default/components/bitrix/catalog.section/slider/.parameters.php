<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"TITLE" => Array(
		"PARENT" => "BASE",
		"NAME" => "Заголовок слайдера",
		"TYPE" => "STRING",
		"DEFAULT" => ""
	),
	"COLS" => Array(
		"PARENT" => "BASE",
		"NAME" => "Количество колонок",
		"TYPE" => "STRING",
		"DEFAULT" => "3"
	),
	"ROWS" => Array(
		"PARENT" => "BASE",
		"NAME" => "Количество строк",
		"TYPE" => "STRING",
		"DEFAULT" => "1"
	),
);