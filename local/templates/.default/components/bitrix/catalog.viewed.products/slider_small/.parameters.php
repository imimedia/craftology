<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"TITLE" => Array(
		"PARENT" => "BASE",
		"NAME" => "Заголовок слайдера",
		"TYPE" => "STRING",
		"DEFAULT" => ""
	)
);