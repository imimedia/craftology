<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['ITEMS'] as $key => $arItem) {
    if ($arItem['IBLOCK_SECTION_ID']) {
        $arResult['ITEMS'][$key]['SECTION'] = array();

        $rsSection = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $arItem['IBLOCK_ID'], 'ID' => $arItem['IBLOCK_SECTION_ID']), false, array('ID', 'NAME', 'SECTION_PAGE_URL'));
        $rsSection->SetUrlTemplates();
        while ($arSection = $rsSection->GetNext()) {
            $arResult['ITEMS'][$key]['SECTION'] = $arSection;
        }
    }
}