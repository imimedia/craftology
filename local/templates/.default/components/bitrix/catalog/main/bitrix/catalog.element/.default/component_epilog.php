<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Catalog\CatalogViewedProductTable as CatalogViewedProductTable;

Bitrix\Main\Loader::includeModule('catalog');
Bitrix\Main\Loader::includeModule('sale');

CatalogViewedProductTable::refresh($arResult['ID'], CSaleBasket::GetBasketUserID());

global $arBasket;
?>

<script>
    $(document).ready(function(){
       <?if ($arBasket["QUANTITY_BASKET"][$arResult['ID']]>0):?>
            $(".js-cart .qty input").val(<?=$arBasket["QUANTITY_BASKET"][$arResult['ID']]?>);
            $(".js-cart span p").text(<?=$arBasket["ITEM"][$arResult['ID']]["QUANTITY"]*$arBasket["ITEM"][$arResult['ID']]["PRICE"]?>);
            $(".wrap_basket").html('Добавлено в <a href="/personal/cart/">корзину</a>');
            $('.js-buy').addClass("active");
            $('.js-buy').text("+");
       <?else:?>
            $(".js-cart .qty input").val("1");
            $(".wrap_basket").html('');
            $('.js-buy').text("Купить");
            $('.js-buy').removeClass("active");
            $(".js-cart span p").text($(".product__price").data('price'));
       <?endif?>
    });
</script>