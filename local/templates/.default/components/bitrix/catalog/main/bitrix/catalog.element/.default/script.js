$(function(){
    function loader(){
        var preloader = $('#page-preloader'),
            spinner   = preloader.find('.spinner');
        preloader.show();
        preloader.delay(1600).fadeOut('slow');
    }
    $('.tabs-pane').removeClass('active');
    $('.tabs-pane:nth-child(1)').addClass('active');
    $('body').on('click', '.tabs-tabs .tabs-tab', function(){
        var index = $( this ).index();
        index +=1;
        $('.tabs-pane').removeClass('active');
        $('.tabs-pane:nth-child('+index+')').addClass('active');
    });
    // счетчик количества тоаров
    $('body').on('click', '.incr', function(e) {
        var item_id = $(this).attr('data-id');
        var val_e = $('.quatity_item_'+item_id).attr('value');//
        if(val_e == 1){
            sum = parseInt(val_e);
        } else {
            sum = parseInt(val_e) + 1;
        }
        $('.quatity_item_'+item_id).attr('value',sum);
    });

    $('body').on('click', '.decr', function(e) {
        var item_id = $(this).attr('data-id');
        var val_e = $('.quatity_item_'+item_id).attr('value');
        if(val_e > 0){
            sum = parseInt(val_e) - 1;
        }
        $('.quatity_item_'+item_id).attr('value',sum);
    });

    focus_update_personal = '';
    item_id_focus = '';
    quantity_focus = '';
    $('body').on('keyup', '.focus_click', function(e) {
        item_id_focus = $(this).attr('data-id');
        quantity_focus = $('.quatity_item_'+item_id_focus).val();
        focus_update_personal = 'Y';
    });

    $('body').on('mouseover', '.js-btnHoverSubMainnav', function(e) {
        if(focus_update_personal == "Y"){
            $.ajax({
                type: 'POST',
                url: '/ajax/basket_ubdate.php',
                data: {id:item_id_focus,quantity:quantity_focus},
                success: function(data){
                    loader();
                    $('.basket_up_'+item_id_focus).fadeIn();
                    $('.quatity_item_'+item_id_focus).val(data);
                    $('.basket_up_'+item_id_focus).html('Добавлено в <a href="/personal/cart/">корзину</a>');
                 //   setTimeout(function() { $('.basket_up_'+id).fadeOut()}, 3000);
                    $('.qcart_new').load(window.location.href + ' .qcart_new');
                    focus_update_personal = '';
                    item_id_focus = '';
                }
            });
            $('.add_basket_'+item_id_focus+' span').addClass('active');
            $('.add_basket_'+item_id_focus+' span').html('+');
        }
    });

    $('body').on('click', '.btn.js-add2cart', function(){
        var id = $(this).attr('data-id'),
            quantity = $('.quatity_item_'+id).val(),
            adding = true,
            date_basket_url = '';
        if(focus_update_personal == 'Y'){
            date_basket_url = "/ajax/basket_ubdate.php";
        } else {
            date_basket_url = "/ajax/basket_add.php";
        }
        $.ajax({
            type: 'POST',
            url: date_basket_url,
            data: {id:id,quantity:quantity,adding:adding},
            success: function(data){
                $('.basket_up_'+id).fadeIn();
                $('.quatity_item_'+id).val(data);
                $('.basket_up_'+id).html('Добавлено в <a href="/personal/cart/">корзину</a>');
                //   setTimeout(function() { $('.basket_up_'+id).fadeOut()}, 3000);
                $('.qcart_new').load(window.location.href + ' .qcart_new');
                focus_update_personal = '';
                loader();
            }
        });
        $(this).children('span').addClass('active').html('+');
    })

    $('body').on('focusout', '.js-cartItemQty', function(e) {
        var id = $(this).attr('data-id'),
        quantity = $('.quatity_item_'+id).val(),
        price = $(this).attr('data-price');
            $.ajax({
                type: 'POST',
                url: "/ajax/basket_ubdate.php",
                data: {id:id,quantity:quantity},
                success: function(data){
                    setTimeout(function() {
                        if(quantity < 1){
                            $('.basket_up_'+id).fadeOut(1);
                            $('.add_basket_'+id+' span').removeClass('active');
                            $('.add_basket_'+id+' span').html('Купить');
                            $('.basket_up_'+id).html('Удалено из <a href="/personal/cart/">корзины</a>');
                        } else {
                            $('.add_basket_'+id+' span').addClass('active');
                            $('.add_basket_'+id+' span').html('+');
                            $('.basket_up_'+id).html('Изменено в <a href="/personal/cart/">корзине</a>');
                            $('.basket_up_'+id).fadeIn();
                        }
                         $('.product__buy > span p').html(price * data);
                         $('.qcart_new').load(window.location.href + ' .qcart_new');
                         $('.quatity_item_'+id).val(data);
                         loader();
                    }, 1000);
                }
            });

    });
    $('body').on('click', '.incr', function(){
        var id = $(this).attr('data-id'),
        quantity = $('.quatity_item_'+id).val(),
        price = $(this).attr('data-price'),
        incr = true;

        $.ajax({
            type: 'POST',
            url: "/ajax/basket_add.php",
            data: {id:id,quantity:quantity,incr:incr},
            success: function(data){
                $('.add_basket_'+id+' span').addClass('active');
                $('.add_basket_'+id+' span').html('+');
                $('.quatity_item_'+id).val(data);
                $('.product__buy > span p').html(price * data);
                $('.basket_up_'+id).html('Добавлено в <a href="/personal/cart/">корзину</a>');
                $('.basket_up_'+id).fadeIn();
                $('.qcart_new').load(window.location.href + ' .qcart_new');
                loader();
            }
        });
    })
    $('body').on('click', '.decr', function(){
        var id = $(this).attr('data-id'),
        quantity = $('.quatity_item_'+id).val(),
        price = $(this).attr('data-price');
        $.ajax({
            type: 'POST',
            url: "/ajax/basket_delete.php",
            data: {id:id},
            success: function(data){
                $('.basket_up_'+id).fadeIn();
                if(data < 1){
                    $('.basket_up_'+id).fadeOut(1);
                    $('.add_basket_'+id+' span').removeClass('active');
                    $('.add_basket_'+id+' span').html('Купить');
                    $('.basket_up_'+id).html('Удалено из <a href="/personal/cart/">корзины</a>');
                } else {
                    $('.basket_up_'+id).html('Изменено в <a href="/personal/cart/">корзине</a>');
                }
                $('.product__buy > span p').html(price * data);
                $('.quatity_item_'+id).val(data);
                $('.qcart_new').load(window.location.href + ' .qcart_new');
                loader();
            }
        });
    })
})
