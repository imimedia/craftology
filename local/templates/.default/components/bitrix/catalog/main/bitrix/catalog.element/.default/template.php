<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$canBuy = $arResult['CAN_BUY'] && $arResult['MIN_PRICE']['CAN_BUY'] == 'Y';
$canToOrder = $arResult['CAN_BUY'] && (float)$arResult['CATALOG_QUANTITY'] <= 0;

$images = array();
$imageIds = array();
if ($arResult['DETAIL_PICTURE'])
	$imageIds[] = $arResult['DETAIL_PICTURE']['ID'];
if ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE']) {
	foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $imageId)
		$imageIds[] = $imageId;
}
if ($imageIds) {
	foreach ($imageIds as $imageId) {
		$imageOrig = CFile::GetFileArray($imageId);
		if ($imageOrig) {
			$imageLarge = CFile::ResizeImageGet($imageId, array("width" => 370, "height" => 370), BX_RESIZE_IMAGE_EXACT, true);
			$imageLarge['SRC'] = $imageLarge['src'];
			$imageSmall = CFile::ResizeImageGet($imageId, array("width" => 103, "height" => 103), BX_RESIZE_IMAGE_EXACT, true);
			$imageSmall['SRC'] = $imageSmall['src'];

			$images[] = array(
				'orig' => $imageOrig,
				'large' => $imageLarge,
				'small' => $imageSmall
			);
		}
	}
}
?>


<div class="title">
    <h1><?= $arResult['NAME'] ?></h1>
</div>

<div class="product-content<?= !$images ? ' noimage' : '' ?>" data-id="<?= $arResult['ID'] ?>" data-product="<?= $arResult['ID'] ?>">
	<? if ($images) { ?>
	<div class="product-left">
		<div class="preview js-preview">
			<div class="preview__large">
				<img src="<?= $images[0]['large']['SRC'] ?>" alt="" class="js-previewLarge" />
				<? if ($arResult['PROPERTIES']['NEW']['VALUE']) { ?>
					<div class="product__new"></div>
				<? }
				else if ($arResult['PROPERTIES']['HIT']['VALUE']) { ?>
					<div class="product__hit"></div>
                <? }
                else if ($arResult['PROPERTIES']['SALE']['VALUE'] || $arResult['SECTION_ID'] == 127) { ?>
                    <div class="product__sale"></div>
                <? } ?>
				<div class="zoom"></div>
			</div>
			<div class="preview__slider js-previewSlider"<?= count($images) < 2 ? ' style="display: none"' : '' ?>>
				<div class="swiper-container slider" data-pagination="#product_<?= $arResult['ID'] ?>_slider_nav">
					<div class="swiper-wrapper">
						<? foreach ($images as $image) { ?>
						<div class="swiper-slide">
							<div class="preview__item js-previewSliderItem">
								<img src="<?= $image['small']['SRC'] ?>" data-large="<?= $image['large']['SRC'] ?>" data-src="<?= $image['orig']['SRC'] ?>" alt=""/>
							</div>
						</div>
						<? } ?>
					</div>
				</div>

				<? if (count($images) > 3) { ?>
				<div class="slider-nav" id="product_<?= $arResult['ID'] ?>_slider_nav">
					<div class="swiper-button-prev"></div>
					<div class="swiper-pagination"></div>
					<div class="swiper-button-next"></div>
				</div>
				<? } ?>
			</div>
		</div>
	</div>
	<? } ?>
	
	<?
		global $USER;
		if ($USER->IsAdmin()) {
			//print_r($arResult);
		};
		?>

	<div class="product-right">
		
		<div class="product-line">
			<? if (trim($arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'])) { ?><div class="product__sky">Артикул <?= trim($arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']) ?></div><? } ?>
<!-- 			<div class="product__name"><?//= $arResult['NAME'] ?></div> -->
		</div>
		
		
        
		<div class="product-line">
			<? if ($arResult['MIN_PRICE'] && $arResult['MIN_PRICE']['CAN_ACCESS'] == 'Y') {
				?><div class="product__price" data-price="<?=$arResult['MIN_PRICE']["DISCOUNT_VALUE"]?>"><?
				if ($arResult['MIN_PRICE']['DISCOUNT_DIFF']) {
					?><div class="old-price"><?= $arResult['MIN_PRICE']['PRINT_VALUE'] ?></div><?
				} ?>


                <? echo $arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];?>

                <span>*</span>


                </div><?
			}
			?>
            <? if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")){
                // количестов товара в корзине пользователя
            $quantity_element = 0;
            $quantity_basket = 0;
            $dbBasketItems = CSaleBasket::GetList(
                    array(
                       "NAME" => "ASC",
                       "ID" => "ASC"
                    ),
                    array(
                      "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                      "LID" => SITE_ID,
                      "PRODUCT_ID" => $arResult["ID"],
                      "ORDER_ID" => "NULL"
                    ),
                    array("ID","QUANTITY")
                 );
                 if($arItems = $dbBasketItems->Fetch()){
                      $quantity_element = $arItems["QUANTITY"];
                      $quantity_basket = $arItems["QUANTITY"];
                 } else {
                     $quantity_element = 1;
                 }

            }?>
            <? if ($canBuy && $arResult['CATALOG_QUANTITY_TRACE'] == 'Y') { //&& !$canToOrder) { ?>
                <div class="product__buy js-cart">
                    <div class="qty">
                        <? if ($arResult['CATALOG_CAN_BUY_ZERO'] == 'N') { ?>
                        <input type="text" value="<?=($quantity_element == 0)?'1':$quantity_element?>" data-price="<?=$arResult['MIN_PRICE']["DISCOUNT_VALUE"]?>" data-id="<?=$arResult["ID"]?>" class=" js-cartItemQty focus_click quatity_item_<?=$arResult["ID"]?>" data-available="<?= (float) $arResult['CATALOG_QUANTITY'] ?>" />
                        <? } else {?>
                        <input type="text" value="<?=($quantity_element == 0)?'1':$quantity_element?>" data-price="<?=$arResult['MIN_PRICE']["DISCOUNT_VALUE"]?>" data-id="<?=$arResult["ID"]?>" class=" js-cartItemQty focus_click quatity_item_<?=$arResult["ID"]?>" data-available="999999" />
                        <?}?>
                        <div class="qty-control">
                            <div class="incr qty-incr" data-price="<?=$arResult['MIN_PRICE']["DISCOUNT_VALUE"]?>" data-id="<?=$arResult["ID"]?>"></div>
                            <div class="decr qty-decr" data-price="<?=$arResult['MIN_PRICE']["DISCOUNT_VALUE"]?>" data-id="<?=$arResult["ID"]?>"></div>
                        </div>
                    </div>
                    <span>= <ruble><span class="text">руб.</span></ruble><p><?=$quantity_element * $arResult['MIN_PRICE']["DISCOUNT_VALUE"]?></p></span>

                    <?if($quantity_basket > 0){?>
                        <span class="basket_up_<?=$arResult["ID"]?> wrap_basket">Добавлено в <a href="/personal/cart/">корзину</a></span>
                    <?}else {?>
                        <span class="basket_up_<?=$arResult["ID"]?> wrap_basket"></span>
                    <?}?>
                    <button type="submit" class="btn add2cart js-add2cart add_basket_<?=$arResult["ID"]?>" data-price="<?=$arResult['MIN_PRICE']["DISCOUNT_VALUE"]?>" data-id="<?=$arResult["ID"]?>">
                     <?if($quantity_basket > 0){
                        ?><span class="js-buy active">+</span><?
                    } else {
                        ?><span class="js-buy">Купить</span><?
                    }?>
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" height="32px" viewBox="0 0 32 32"><path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/></svg>
                    </button>
                </div>
            <? } else if ($canToOrder) { ?>
                <div class="product__buy">
                    <a href="<?= '/local/api/form/zakaz/?theme='.urlencode('Заказ товара "'.$arResult['NAME'].'", #'.$arResult['ID']) ?>" class="btn add2cart add2cart z js-dialog"><span>Под заказ</span></a>
                </div>
            <? } else { ?>
                    <div class="product-line">
                        <span class="btn btn-b">Нет в наличии</span><br>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:sale.notice.product",
                            "",
                            Array(
                                "NOTIFY_ID" => $arResult['ID'],
                                "NOTIFY_URL" => htmlspecialcharsback($arResult["SUBSCRIBE_URL"]),
                                "NOTIFY_USE_CAPTHA" => "N"
                            )
                        );?>
                    </div>
            <?}?>
		</div>
		
		<? if ($arResult['PROPERTIES']['LIMITATION']['VALUE'] == "Ограничена" ) {?>
        	<div class="catalog-item_forbidden limited js-dialog" data-href="/local/api/form/warnings/limited.php">
            	<a href="" class="btn_open-feedback">! Ограничения в пересылке</a>
            </div>
        <?}?>
        
        <? if ($arResult['PROPERTIES']['LIMITATION']['VALUE'] == "Запрещена" ) {?>
        	<div class="catalog-item_forbidden js-dialog" data-href="/local/api/form/warnings/forbidden.php">
            	<a href="" class="btn_open-feedback">! Пересылка невозможна</a>
            </div>	
        <?}?>
		
		
		<div class="product-line">

				</div>
			<div class="product__fq">
				<a href="" class="product__favorite js-favoriteBtn js-tooltip" title="В избранное">Добавить в избранное</a>
				<a href="<?= '/local/api/form/feedback/?theme='.urlencode('Вопрос о товаре "'.$arResult['NAME'].'", #'.$arResult['ID']) ?>" class="product__question js-dialog">Задать вопрос</a>
			</div>
		
<!--
		<div class="product__rating">
            <div class="rating-stars-lg js-ratingStars js-rating" data-rate="0">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
-->
		
		<? if ($arResult['CAN_BUY'] && $arResult['MIN_PRICE']['CAN_BUY'] == 'Y') { ?>
			<? if ($arParams['IS_AUTH']) { ?>
<!--
			<div class="product-line">
				<a href="" class="pseudo-link js-productSubscribeBtn">Уведомить меня о снижении цены</a>
			</div>
-->
			<? } ?>
		<? } ?>
        
	</div>
</div>

<div class="tabs-tabs">
            <? if (trim($arResult['DETAIL_TEXT'])) { ?><div class="tabs-tab">Описание</div><? } ?>
            <div class="tabs-tab">Наличие</div>
            <div class="tabs-tab">Отзывы</div>
        </div>

<div class="tabs new">

	<div class="tabs-panes new">
		<? if (trim($arResult['DETAIL_TEXT'])) { ?>
		<div class="tabs-pane">
			<h2>Описание товара</h2>
			<?= trim($arResult['DETAIL_TEXT']) ?>
		</div>
		<? } ?>
		
		<div class="tabs-pane">
			<h2>Наличие товара</h2>
			<?
				$APPLICATION->IncludeComponent(
					'bitrix:catalog.store.amount',
					'main',
					array(
						'ELEMENT_ID' => $arResult['ID'],
						'STORE_PATH' => $arParams['STORE_PATH'],
						'CACHE_TYPE' => 'A',
						'CACHE_TIME' => '3600',
						'MAIN_TITLE' => $arParams['MAIN_TITLE'],
						'USE_MIN_AMOUNT' =>  $arParams['USE_MIN_AMOUNT'],
						'MIN_AMOUNT' => $arParams['MIN_AMOUNT'],
						'STORES' => $arParams['STORES'],
						'SHOW_EMPTY_STORE' => "Y",
						'SHOW_GENERAL_STORE_INFORMATION' => $arParams['SHOW_GENERAL_STORE_INFORMATION'],
						'USER_FIELDS' => $arParams['USER_FIELDS'],
						'FIELDS' => array("TITLE", "ADDRESS", "DESCRIPTION", "={$arParams['FIELDS']}", "")
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);	
			?>
		</div>
		
		<div class="tabs-pane">
			<div class="reviews__list">
				<div class="reviews__items js-reviews"></div>
				<div class="reviews-form">
					<h2>Написать отзыв</h2>
					<form method="post" class="js-formReviews" action="/local/api/reviews/">
						<div class="form-row"><input type="text" name="review[name]" placeholder="ФИО" /></div>
						<div class="form-row"><textarea name="review[text]" rows="5" placeholder="Отзыв"></textarea></div>
						<div class="form-actions">
							<button type="submit" name="" class="btn">Отправить отзыв</button>
						</div>
						<input type="hidden" name="review[product_id]" value="<?= $arResult['ID'] ?>" />
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?
// Initial data
$productDetailParams = array(
	'ID' => $arResult['ID'],
	'NAME'=> $arResult['NAME'],
	'PRICE' => null,
	'CURRENCY' => null
);
if (is_array($arResult['MIN_PRICE'])) {// Non-SKU product
	$productDetailParams['PRICE'] =$arResult['MIN_PRICE']['DISCOUNT_VALUE'];
	$productDetailParams['CURRENCY'] =$arResult['MIN_PRICE']['CURRENCY'];
}
elseif(is_array($arResult['OFFERS'])){// SKU product
	foreach ($arResult['OFFERS'] as $offer){
		if(!is_array($offer['MIN_PRICE']))
		{
			continue;
		}
		if(null===$productDetailParams['PRICE'] ||$offer['MIN_PRICE']['DISCOUNT_VALUE'] <$productDetailParams['PRICE']) {
			$productDetailParams['PRICE'] =$offer['MIN_PRICE']['DISCOUNT_VALUE'];
			$productDetailParams['CURRENCY'] =$offer['MIN_PRICE']['CURRENCY'];
	 	}
	}
}
$APPLICATION->IncludeComponent(
	"intervolga:conversionpro.productdetail",
	"",
	$productDetailParams,
	$component
);?>