<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$curSection = array(
	'SECTION_ID' => 0,
	'HAS_CHILD' => false
);
$arFilter = array(
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"ACTIVE" => "Y",
	"GLOBAL_ACTIVE" => "Y",
);
if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
	$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
	$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog/subsections")) {
	$curSection = $obCache->GetVars();
}
elseif ($obCache->StartDataCache()) {
	if (Bitrix\Main\Loader::includeModule("iblock")) {
		$dbSection = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

		if (defined("BX_COMP_MANAGED_CACHE")) {
			global $CACHE_MANAGER;
			$CACHE_MANAGER->StartTagCache("/iblock/catalog/subsections");

			if ($curSection = $dbSection->Fetch()) {
				$CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);

				$arFilter['SECTION_ID'] = $curSection['ID'];
				unset($arFilter["ID"]);
				unset($arFilter["=CODE"]);
				$dbSections = CIBlockSection::GetList(array(), $arFilter, false, array("ID"), array('nTopCount' => 1));
				if ($dbSections->Fetch())
					$curSection['HAS_CHILD'] = true;
			}

			$CACHE_MANAGER->EndTagCache();
		}
		else {
			if ($curSection = $dbSection->Fetch()) {
				$arFilter['SECTION_ID'] = $curSection['ID'];
				unset($arFilter["ID"]);
				unset($arFilter["=CODE"]);
				$dbSections = CIBlockSection::GetList(array(), $arFilter, false, array("ID"), array('nTopCount' => 1));
				if ($dbSections->Fetch())
					$curSection['HAS_CHILD'] = true;
			}
		}
	}
	$obCache->EndDataCache($curSection);
}

?>
<div class="title">
    <h1><?$APPLICATION->ShowTitle()?></h1>
</div>
<?

if ($curSection['HAS_CHILD']) {
	
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list",
		"",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $curSection['ID'],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
			"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
			"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
			"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
			"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
			"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);
}
else { ?>
	<div class="catalog__meta">
        
		<div class="count">
			<span class="count__label">Показать:</span>
			<?
			$countItems = array(
				'60' => array('title' => '60', 'value' => 60),
				'120' => array('title' => '120', 'value' => 120),
				'all' => array('title' => 'все', 'value' => 999)
			);
			$countItem = $countItems['60'];
			if (isset($_REQUEST['count']) && in_array($_REQUEST['count'], array_keys($countItems))) {
				$countItem = $countItems[$_REQUEST['count']];
				$USER->SetParam('CATALOG_COUNT_ITEMS', $_REQUEST['count']);
			}
			else if ($USER->GetParam('CATALOG_COUNT_ITEMS') && in_array($USER->GetParam('CATALOG_COUNT_ITEMS'), array_keys($countItems))) {
				$countItem = $countItems[$USER->GetParam('CATALOG_COUNT_ITEMS')];
			}

			$n = 0;
			foreach ($countItems as $countKey => $_cntItem) {
				echo ($_cntItem['value'] == $countItem['value'])
					? '<span>'.$_cntItem['title'].'</span>'
					: '<a href="'.$APPLICATION->GetCurPageParam('count='.$countKey, array('count')).'">'.$_cntItem['title'].'</a>';
				echo (count($countItems) > ++$n) ? ', ' : '';
			} ?>
		</div>

		<div class="sorter">
			<div class="sorter__label">Сортировка: </div>
			<div class="dropdown sorter__dropdown">
				<?
				$sortItems = array(
                    'scu' => array('title' => 'По артикулу', 'FIELD' => 'PROPERTY_CML2_ARTICLE', 'ORDER' => 'ASC'),
					'name' => array('title' => 'По названию', 'FIELD' => 'NAME', 'ORDER' => 'ASC'),
					'priceup' => array('title' => 'По возрастанию цены', 'FIELD' => 'CATALOG_PRICE_'.CATALOG_PRICE, 'ORDER' => 'ASC,nulls'),
					'pricedown' => array('title' => 'По убыванию цены', 'FIELD' => 'CATALOG_PRICE_'.CATALOG_PRICE, 'ORDER' => 'DESC,nulls')
				);
				$sortName = 'scu';
				$sortItem = $sortItems[$sortName];
				if (isset($_REQUEST['sort']) && in_array($_REQUEST['sort'], array_keys($sortItems))) {
					$sortName = $_REQUEST['sort'];
					$sortItem = $sortItems[$sortName];
					$USER->SetParam('CATALOG_SORT_ITEMS', $_REQUEST['sort']);
				}
				else if ($USER->GetParam('CATALOG_SORT_ITEMS') && in_array($USER->GetParam('CATALOG_SORT_ITEMS'), array_keys($sortItems))) {
					$sortName = $USER->GetParam('CATALOG_SORT_ITEMS');
					$sortItem = $sortItems[$sortName];
				}
				?>
				<span class="dropdown__trigger"><?= $sortItem['title'] ?></span>
				<ul class="dropdown__content">
					<? foreach ($sortItems as $sortKey => $_sortItem) {
						echo '<li><a href="'.$APPLICATION->GetCurPageParam('sort='.$sortKey, array('sort')).'">'.$_sortItem['title'].'</a></li>';
					} ?>
				</ul>
			</div>
		</div>
	</div>
	
	<?
		if($_COOKIE["namber_order"] <> "null") {
			$arParams["HIDE_NOT_AVAILABLE"] = "Y";
	    }
	    else {
		    $arParams["HIDE_NOT_AVAILABLE"] = "L";
	    }
	?>
	
	<?
	$intSectionID = $APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"main",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ELEMENT_SORT_FIELD" => $sortItem['FIELD'],
			"ELEMENT_SORT_ORDER" => $sortItem['ORDER'],
			"ELEMENT_SORT_FIELD2" => $sortItem2['FIELD'],
			"ELEMENT_SORT_ORDER2" => $sortItem2['ORDER'],
			"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
			"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
			"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
			"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
			"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
			"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_FILTER" => $arParams["CACHE_FILTER"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SET_TITLE" => $arParams["SET_TITLE"],
			"MESSAGE_404" => $arParams["MESSAGE_404"],
			"SET_STATUS_404" => $arParams["SET_STATUS_404"],
			"SHOW_404" => $arParams["SHOW_404"],
			"FILE_404" => $arParams["FILE_404"],
			"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
			"PAGE_ELEMENT_COUNT" => $countItem['value'],
			"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
			"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
			"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

			"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
			"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
			"PAGER_TITLE" => $arParams["PAGER_TITLE"],
			"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
			"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
			"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
			"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
			"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
			"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
			"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
			"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
			"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
			"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
			"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
			"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
			"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

			'LABEL_PROP' => $arParams['LABEL_PROP'],
			'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
			'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

			'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
			'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
			'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
			'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
			'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
			'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
			'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
			'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

			'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
			'ADD_TO_BASKET_ACTION' => $basketAction,
			'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
			'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
			'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
			'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
		),
		$component
	);
}?>
<? $this->SetViewTarget('bottom'); ?>
<div class="lents">
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.bestsellers",
	"slider_small",
	array(
		"COMPONENT_TEMPLATE" => "slider_small",
		"HIDE_NOT_AVAILABLE" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_NAME" => "N",
		"SHOW_IMAGE" => "N",
		"MESS_BTN_BUY" => "РљСѓРїРёС‚СЊ",
		"MESS_BTN_DETAIL" => "РџРѕРґСЂРѕР±РЅРµРµ",
		"MESS_NOT_AVAILABLE" => "РќРµС‚ РІ РЅР°Р»РёС‡РёРё",
		"MESS_BTN_SUBSCRIBE" => "РџРѕРґРїРёСЃР°С‚СЊСЃСЏ",
		"PAGE_ELEMENT_COUNT" => "7",
		"LINE_ELEMENT_COUNT" => "3",
		"TEMPLATE_THEME" => "blue",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "86400",
		"BY" => "AMOUNT",
		"PERIOD" => "0",
		"FILTER" => array(
			0 => "F",
		),
		"DISPLAY_COMPARE" => "N",
		"SHOW_OLD_PRICE" => "N",
		"PRICE_CODE" => array(
			0 => "Р РѕР·РЅРёС‡РЅР°СЏ",
		),
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"SHOW_PRODUCTS_3" => "Y",
		"PROPERTY_CODE_3" => array(
			0 => "",
			1 => "",
		),
		"CART_PROPERTIES_3" => array(
			0 => "",
			1 => "",
		),
		"ADDITIONAL_PICT_PROP_3" => "MORE_PHOTO",
		"LABEL_PROP_3" => "-",
		"PROPERTY_CODE_4" => array(
			0 => "",
			1 => "",
		),
		"CART_PROPERTIES_4" => array(
			0 => "",
			1 => "",
		),
		"ADDITIONAL_PICT_PROP_4" => "MORE_PHOTO",
		"OFFER_TREE_PROPS_4" => array(
		),
		"TITLE" => "Популярные товары"
	),
	false
);?>
</div>
<? $this->EndViewTarget(); ?>
