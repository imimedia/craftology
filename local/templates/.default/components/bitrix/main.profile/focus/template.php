<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<div class="bx-auth-profile">

<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>
<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />

<h4>Информация о покупателе</h4>
<div class="profile-block-<?=strpos($arResult["opened"], "reg") === false ? "hidden" : "shown"?>" id="user_div_reg">
	<div class="clearfix">
		<div class="order-prop">
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('NAME')?></dt>
				<dd class="order-prop__control"><input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" /></dd>
			</dl>
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('LAST_NAME')?></dt>
				<dd class="order-prop__control"><input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" /></dd>
			</dl>
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('SECOND_NAME')?></dt>
				<dd class="order-prop__control"><input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" /></dd>
			</dl>
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('USER_PHONE')?></dt>
				<dd class="order-prop__control"><input type="text" name="PERSONAL_PHONE" maxlength="255" class="js-mask" data-pattern="phone" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" /></dd>
			</dl>
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('EMAIL')?><?if($arResult["EMAIL_REQUIRED"]):?><span class="starrequired">*</span><?endif?></dt>
				<dd class="order-prop__control"><input type="text" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" /></dd>
			</dl>
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('LOGIN')?><span class="starrequired">*</span></dt>
				<dd class="order-prop__control"><input type="text" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" /></dd>
			</dl>
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('USER_CITY')?></dt>
				<dd class="order-prop__control"><input type="text" name="PERSONAL_CITY" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" /></dd>
			</dl>
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('USER_ZIP')?></dt>
				<dd class="order-prop__control"><input type="text" name="PERSONAL_ZIP" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_ZIP"]?>" /></dd>
			</dl>
			
			<?if($arResult["arUser"]["EXTERNAL_AUTH_ID"] == ''):?>
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('NEW_PASSWORD_REQ')?></dt>
				<dd class="order-prop__control"><input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" class="bx-auth-input" /></dd>
				<?if($arResult["SECURE_AUTH"]):?>
					<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
						<div class="bx-auth-secure-icon"></div>
					</span>
					<noscript>
					<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
						<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
					</span>
					</noscript>
					<script type="text/javascript">
					document.getElementById('bx_auth_secure').style.display = 'inline-block';
					</script>
						
				<?endif?>
			</dl>
			<dl class="form-row">
				<dt class="order-prop__label"><?=GetMessage('NEW_PASSWORD_CONFIRM')?></dt>
				<dd class="order-prop__control"><input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" /></dd>
			</dl>
			
			<?endif?>
			
		</div>
	</div>
	
	
	
	
</div>



	<?// ******************** /User properties ***************************************************?>
	<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
	<p><input class="btn add2cart" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">&nbsp;&nbsp;<input type="reset" class="btn add2cart" value="<?=GetMessage('MAIN_RESET');?>"></p>
</form>

</div>