<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<? if ($arResult) { ?>
    <nav class="bottomnav">
        <? foreach ($arResult as $key => $arItem) {
            if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;

            $style = 'theme-link';
            if ($arItem['SELECTED'])
                $style .= ' active';
            ?><a href="<?= $arItem['LINK'] ?>" class="<?= $style ?>"><?= $arItem['TEXT'] ?></a><?
        } ?>
    </nav>
<? } ?>
