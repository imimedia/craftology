<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
   if ($arResult && CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
        $dbBasketItems = CSaleBasket::GetList(
                array(
                   "NAME" => "ASC",
                   "ID" => "ASC"
                ),
                array(
                  "FUSER_ID" => CSaleBasket::GetBasketUserID(false),
                  "LID" => SITE_ID,
                  "ORDER_ID" => "NULL",
                    "CAN_BUY" => "Y"
                ),
                array("ID", "CALLBACK_FUNC", "MODULE",
              "PRODUCT_ID", "QUANTITY", "DELAY",
              "CAN_BUY", "PRICE", "WEIGHT","NAME")
             );
        while($arPrice = $dbBasketItems->GetNext()){
            $arResult["TOTAL_PRICE"] += $arPrice["PRICE"] * $arPrice["QUANTITY"];
            $arResult["TOTAL_QUANTITY"] += count($arPrice["QUANTITY"]);
        }
   }