<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<? if ($arResult && CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {



    $dbBasketItems = CSaleBasket::GetList(
        array("ID" => "ASC"),
        array(
            'FUSER_ID' => CSaleBasket::GetBasketUserID(),
            'LID' => SITE_ID,
            'ORDER_ID' => 'NULL'
        ),
        false,
        false,
        array(
            'ID', 'NAME','PRODUCT_ID', 'QUANTITY', 'PRICE', 'DISCOUNT_PRICE', 'WEIGHT'
        )
    );

    $allSum = 0;
    $allWeight = 0;
    $arItems = array();
    $arItemsCheck = array();


    while ($arBasketItems = $dbBasketItems->Fetch())
    {
        $allSum += ($arBasketItems["PRICE"] * $arBasketItems["QUANTITY"]);
        $allWeight += ($arBasketItems["WEIGHT"] * $arBasketItems["QUANTITY"]);
        $arItems[$arBasketItems['ID']] = $arBasketItems;

    }


    if(!empty($arItems)){
        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(
            \Bitrix\Sale\Fuser::getId(),
            \Bitrix\Main\Context::getCurrent()->getSite()
        );
        $discounts = \Bitrix\Sale\Discount::loadByBasket($basket);
        $basket->refreshData(array('PRICE', 'COUPONS'));
        $discounts->calculate();
        $discountResult = $discounts->getApplyResult();

        $diskData = $discountResult['PRICES']['BASKET'];


        foreach ($arItems as $key => &$item ){

            $item['PRICE'] = round($diskData[$key]['PRICE']*$item['QUANTITY'], 2);
        }


    }

//
//
    unset($item);
    $flag = false;
    foreach ($diskData as $item ){

        if($item['DISCOUNT'] == 0){
            $flag = true;
        }
    }


//    ?>
<!--    --><?//if($USER->IsAdmin()):?>
<!--        <pre>-->
<!--        --><?//print_r($arItems)?>
<!--    </pre>-->
<!--    --><?//endif;?>
<!--    --><?//

    ?>
    <div class="mainnav__item mainnav__item_cart">
        <a href="/personal/cart/" class="mainnav__title js-btnHoverSubMainnav"><span>Корзина<?/*<br/><span class="js-mainnavCartTotalPrice"></span>*/?></span></a>
        <div class="qcart_new sub-mainnav qcart-empty" <?if(CSite::InDir('/personal/cart/')):?>style="display: none" <?endif;?>>
            <?$key = 0;?>
            <div class="qcart__title">В корзине <?=$arResult["TOTAL_QUANTITY"]?> товара - <?=$arResult["TOTAL_PRICE"]?> <ruble><span class="text">руб.</span></ruble></div>
            <div class="qcart__items-wrap">
                <div class="qcart__items">
                    <div id="page-preloader">
                        <span class="spinner"></span>
                    </div>
                    <?
                    foreach ($arItems as $arBasket){
                        $sum =round($arBasket["PRICE"],2);
                        $cartPrice += round($arBasket["PRICE"],2);

                        if(!$arBasket["SET_PARENT_ID"]>0) {
//                          if ($arBasket["CAN_BUY"] == 'Y') {
                            $element = CIBlockElement::GetByID($arBasket["PRODUCT_ID"])->Fetch();
                            $picture = (CFile::GetPath($element["PREVIEW_PICTURE"])) ? CFile::GetPath($element["PREVIEW_PICTURE"]) : CFile::GetPath($element["DETAIL_PICTURE"]) ?>
                            <div class="qcart__item">
                                <div class="qcart__image">
                                    <a href="<?= $arBasket["DETAIL_PAGE_URL"] ?>"><img src="<?= $picture ?>"
                                                                                       alt=""></a>
                                </div>
                                <div class="qcart__details">
                                    <div class="qcart__name">
                                        <a href="<?= $arBasket["DETAIL_PAGE_URL"] ?>"><?= $arBasket["NAME"] ?></a>
                                    </div>
                                </div>
                                <div class="qcart__price"><?= $arBasket["QUANTITY"] ?> шт.</div>
                                <div class="qcart__price">

                                    <?= round($arBasket["PRICE"],2,PHP_ROUND_HALF_ODD) ?>
                                    <ruble><span class="text">руб.</span></ruble>
                                </div>
                            </div>
                            <?
//                          }
                        }
                        ?>
                    <?}?>
                </div>
            </div>

            <?
            $maxDiscSum = 3500;
            $toDisc = 0;
            if($cartPrice > 0){
                $perc = round($cartPrice/3500*100);
            }
            if($cartPrice < 1500){
                $percent = 1;
                $toDisc = 1500 -$cartPrice;
            }elseif($cartPrice > 1500 && $cartPrice < 2500){
                $percent = 2;
                $toDisc = 2500 -$cartPrice;
            }elseif($cartPrice > 2500 && $cartPrice< 3500){
                $percent = 3;
                $toDisc = 3500 -$cartPrice;
            }elseif($cartPrice > 3500){
                $toDisc = 999;
            }

            ?>
<!--            --><?//if($flag)?><!-- style="display: none;"--><?//endif;?>
            <div class="qcart__actions">

                <?if($cartPrice > 0):?>
                    <div class="progress__wrap" >
                        <div class="progress__head">
                            <?if($toDisc != 999):?>
                                <?if(($percent-1) > 0):?>Ваша скидка <?=$percent-1?>%. <?endif;?>До скидки <?=$percent?>% осталось <span><?=$toDisc;?></span> ₽
                            <?else:?>
                                <?if($toDisc > 0):?>Ваша скидка 3%<?endif;?>
                            <?endif;?>
                        </div>
                        <div class="progress">
                            <div class="progress__bar" role="progressbar" style="width:<?=$perc?>%;">
                                <span class="progress__bar"></span>
                                <span class="progress__info progress__info--begin">
                                    <i>Скидка</i>
                                </span>
                                <span class="progress__count progress__count--one"></span>
                                <span class="progress__info progress__info--one">
                                    <i>1%</i>(1500 ₽)
                                </span>
                                <span class="progress__count progress__count--two"></span><span class="progress__info progress__info--two"><i>2%</i>(2500 ₽)</span><span class="progress__count progress__count--three"></span><span class="progress__info progress__info--three"><i>3%</i>(3500 ₽)</span></div>
                        </div>
                    </div>
                <?endif;?>

                <a href="/personal/cart/" class="btn go-to-cart">Перейти в корзину</a>
                <!--                --><?// if ($cartPrice > 1000) { ?>
                <a href="/order/" class="btn go-to-order">Оформить заказ</a>
                <!--                --><?// } else { ?>
                <!--                    <a href="/order/" class="btn go-to-order" disabled onclick="return false;">Оформить заказ</a>-->
                <!--                --><?// } ?>
                <!--                --><?//if($cartPrice < 1000):?>
                <!--                    <div style="clear: both; margin-top: 10px"></div><br>-->
                <!--                    <p>Минимальная сумма заказа : 1000<ruble><span class="text">руб.</span></ruble>.-->
                <!--                        Добавьте товаров на --><?//= (1000-$cartPrice)?><!--  <ruble><span class="text">руб.</span></ruble> <a href="/catalog/" style="text-decoration: underline">Перейти в каталог</a></p>-->
                <!--                --><?//endif;?>
            </div>
        </div>
    </div>
<? } ?>

