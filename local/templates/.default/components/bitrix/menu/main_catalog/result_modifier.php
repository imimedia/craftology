<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if ($arResult) {
    $idKey = array();
    foreach ($arResult as $key => $arItem) {
        $id = array_key_exists('ID', $arItem['PARAMS']) ? intval($arItem['PARAMS']['ID']) : 0;
        $pid = array_key_exists('PARENT_ID', $arItem['PARAMS']) ? intval($arItem['PARAMS']['PARENT_ID']) : 0;

        if (!array_key_exists('CNT_CHILD', $arItem))
            $arResult[$key]['CNT_CHILD'] = 0;

        if ($id)
            $idKey[$id] = $key;

        if ($pid)
            $arResult[$idKey[$pid]]['CNT_CHILD']++;
    }
}