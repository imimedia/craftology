<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<? if (!$arResult) return; ?>
<?
$html = '';
$inFavorites = false;
$inColumns = false;

$cols = 2;
$col = 0;
$rows = 0;
$row = 0;

$topLevelCode = '';

$htmlBanners = '';

foreach ($arResult as $key => $arItem) {
    $next = array_key_exists($key + 1, $arResult) ? $arResult[$key + 1] : null;
    $isFavorite = array_key_exists('IS_FAVORITE', $arItem['PARAMS']) && $arItem['PARAMS']['IS_FAVORITE'];
    $nextIsFavorite = $next && array_key_exists('IS_FAVORITE', $next['PARAMS']) && $next['PARAMS']['IS_FAVORITE'];

    if ($arItem['DEPTH_LEVEL'] == 1) {
        $topLevelCntChild = $arItem['CNT_CHILD'];
        $topLevelCode = array_key_exists('CODE', $arItem['PARAMS']) ? $arItem['PARAMS']['CODE'] : '';
        $html .= '<div class="mainnav__item'.(isset($arItem['PARAMS']['STYLE']) ? ' '.$arItem['PARAMS']['STYLE'] : '').'">';
        $html .= '<a href="'.$arItem['LINK'].'" class="mainnav__title js-btnHoverSubMainnav"><span>'.$arItem['TEXT'].'</span></a>';
        if ($arItem['IS_PARENT']) {
            $html .= '<div class="mainnavsub">';
        }
        if (isset($arItem['PARAMS']['HTML_BANNERS'])) {
            $htmlBanners = $arItem['PARAMS']['HTML_BANNERS'];
        }
    }
    else if ($arItem['DEPTH_LEVEL'] == 2 && $isFavorite) {
        if (!$inFavorites) {
            $inFavorites = true;
            $html .= '<ul class="top-line">';
        }
        $html .= '<li><a href="'.$arItem['LINK'].'">'.$arItem['TEXT'].'</a></li>';
        if ($inFavorites && (!$next || !$nextIsFavorite || $next['DEPTH_LEVEL'] != 2 || $next['IS_PARENT'])) {
            $inFavorites = false;
            $html .= '</ul>';
        }
    }
    else if ($arItem['DEPTH_LEVEL'] == 2) {
        if (!$inColumns) {
            $inColumns = true;
            $rows = ceil($topLevelCntChild / $cols);
            $row = 0;
            $html .= '<div class="columns">';
        }
        if ($inColumns) {
            $row++;
            if ($row == 1) {
                $html .= '<div class="mainnavsub_categories"><ul>';
            }
        }

        if ($arItem['IS_PARENT']) {
            $html .= '<li class="is-parent">';
            $html .= '<div class="mainnavsub__sub">';
            $html .= '<a href="'.$arItem['LINK'].'">'.$arItem['TEXT'].'</a>';
            $html .= '<ul' . ($arItem['CNT_CHILD'] > 8 ? ' class="mainnavsub__sub__2-columns"' : '') . '>';
        }
        else {
            $html .= '<li><a href="'.$arItem['LINK'].'">'.$arItem['TEXT'].'</a></li>';
        }
    }
    else if ($arItem['DEPTH_LEVEL'] == 3) {
        $html .= '<li><a href="'.$arItem['LINK'].'">'.$arItem['TEXT'].'</a></li>';
    }


    // Закрываем


    if ($arItem['DEPTH_LEVEL'] > 2) {
        if (!$next || $next['DEPTH_LEVEL'] < 3) {
            $html .= '</ul>';
            $html .= '</div>'; // .mainnavsub__sub
            $html .= '</li>'; // .is-parent
        }
    }

    if ($arItem['DEPTH_LEVEL'] > 1) {
        if ($inColumns && (!$next || $next['DEPTH_LEVEL'] < 2 || ($next['DEPTH_LEVEL'] < 3 && $row == $rows))) {
            $row = 0;
            $html .= '</ul></div>'; // .third
            if ($next['DEPTH_LEVEL'] < 2) {
                $html .= '<div class="mainnavsub_banners">'.$htmlBanners.'</div>';
                $htmlBanners = '';
            }
        }
        if (!$next || $next['DEPTH_LEVEL'] < 2) {
            if ($inColumns) {
                $inColumns = false;
                $html .= '</div>'; // .columns
            }
        }
    }

    if (!$next || $next['DEPTH_LEVEL'] == 1) {
        $html .= '</div>'; // .mainnavsub
        $html .= '</div>'; // .mainnav__item
    }
}

echo $html;