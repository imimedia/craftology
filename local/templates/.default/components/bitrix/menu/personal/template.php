<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<? if ($arResult) { ?>
    <nav class="personal-menu">
        <? foreach ($arResult as $key => $arItem) {
            if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;

            $style = '';
            if ($arItem['SELECTED'])
                $style .= ' active';
            ?><a href="<?= $arItem['LINK'] ?>" class="personal-menu__item<?= $style ?>"><?= $arItem['TEXT'] ?></a><?
        } ?>
    </nav>
<? } ?>
