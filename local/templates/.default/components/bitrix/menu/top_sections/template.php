<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<? if ($arResult) { ?>
    <div class="footcatnav">
        <div class="footcatnav__title h1">Дополнительная информация</div>
        <ul>
            <? foreach ($arResult as $key => $arItem) {
                $next = isset($arResult[$key+1]) ? $arResult[$key+1] : null;

                $style = '';
                if ($arItem['SELECTED'])
                    $style .= 'active';
                $style = $style ? ' class="'.$style.'"' : '';

                if ($arItem['DEPTH_LEVEL'] == 1) {
                    echo '<li'.$style.'><a href="'.$arItem['LINK'].'">'.$arItem['TEXT'].'</a>';
                    echo $arItem['IS_PARENT'] ? '<ul>' : '</li>';
                }
                else {
                    echo '<li><a href="'.$arItem['LINK'].'" class="'.$style.'">'.$arItem['TEXT'].'</a></li>';
                }

                if (!$next || $next['DEPTH_LEVEL'] < $arItem['DEPTH_LEVEL'] || ($next['DEPTH_LEVEL'] == $arItem['DEPTH_LEVEL'] && $arItem['DEPTH_LEVEL'] == 1))
                    echo '</ul></li>';
            } ?>
        </ul>
    </div>
<? } ?>