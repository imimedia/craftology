<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult['SECTIONS'] = array();
foreach ($arResult['ITEMS'] as $key => $arItem) {
    $arResult['ITEMS'][$key]['IBLOCK_SECTION_ID'] = array();
    $rs = CIBlockElement::GetElementGroups($arItem['ID'], false, array('ID'));
    while ($ar = $rs->GetNext())
        $arResult['ITEMS'][$key]['IBLOCK_SECTION_ID'][] = $ar['ID'];

    foreach ($arResult['ITEMS'][$key]['IBLOCK_SECTION_ID'] as $sid) {
        if (!array_key_exists($sid, $arResult['SECTIONS'])) {
            $rsSections = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $sid), false, false, array('ID', 'NAME', 'SECTION_PAGE_URL'));
            $rsSections->SetUrlTemplates();
            if ($section = $rsSections->GetNext()) {
                $arResult['SECTIONS'][$section['ID']] = $section;
            }
        }
    }
}