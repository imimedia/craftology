<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="delivery-step">
    <div class="row">
    <? foreach ($arResult['ITEMS'] as $key=>$arItem):?>
        <div class="col-md-3 delivery-step__item">
            <div class="row">
                <div class="col-md-6">
                    <div class="delivery-step__item-num">
                        <?=($key+1)?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="delivery-step__item-icon">
                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="Заказ">
                    </div>
                    <div class="delivery-step__item-title"><?=$arItem['NAME']?></div>
                    <div class="delivery-step__item-descr"><?=$arItem['~PREVIEW_TEXT']?></div>
                </div>
            </div>
        </div>
    <?endforeach;?>
    </div>
</div>


