<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$cnt = 0;
foreach ($arResult['ITEMS'] as $arItem)
    if ($arItem['DETAIL_PICTURE'])
        $cnt++;

if (!$cnt)
    return;
?>

<div class="swiper-container 444 main-banner main-banner--sub" data-autoplay="true">
    <div class="swiper-wrapper">
        <? foreach ($arResult['ITEMS'] as $arItem) {
            if (!$arItem['DETAIL_PICTURE'])
                continue;
            ?>
            <a href="<?=$arItem['CODE']?>" class="swiper-slide" style="background-image: url('<?= $arItem['DETAIL_PICTURE']['SRC'] ?>')"></a>
        <? } ?>

    </div>

    <div class="swiper-button-prev paginate paginate--left"><i></i><i></i></div>
    <div class="swiper-button-next paginate paginate--right"><i></i><i></i></div>
    <div class="slider-nav 345345">
        <div class="swiper-pagination"></div>
    </div>
</div>