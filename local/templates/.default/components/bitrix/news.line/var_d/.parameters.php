<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"TITLE" => Array(
		"NAME" => "Заголовок",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"HTML_CONTAINER" => Array(
		"NAME" => "HTML контейнер",
		"TYPE" => "LIST",
		"DEFAULT" => "N",
		"VALUES" => array(
			"BOTH" => "Оба",
			"NO" => "Нет",
			"TOP" => "Открывающийся",
			"BOTTOM" => "Закрывающийся",
		)
	),
);