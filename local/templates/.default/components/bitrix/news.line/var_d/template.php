<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="row row--35">
    <? foreach ($arResult['ITEMS'] as $key=>$arItem):?>

        <div class="col-md-24 col--35 delivery-options__item text-center">
            <div class="delivery-options__icon">
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="Почта России">
            </div>
            <h4 class="delivery-options__title"><?=$arItem['NAME']?></h4>
            <p><?=$arItem['~PREVIEW_TEXT']?></p>
        </div>

    <?endforeach;?>
</div>



