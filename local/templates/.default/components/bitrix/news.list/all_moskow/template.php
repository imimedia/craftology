<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>







<div class="tabs  vertical">

    <ul class="tabs__caption">
        <?foreach($arResult["ITEMS"] as $arItem):?>
        <li class="active">
            <div class="tabs__link">
                <div class="tabs__icon">
                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt="<?=$arItem['NAME'];?>">
                </div>
                <div class="tabs__title"><span><?=$arItem['~NAME'];?></span></div>
            </div>
        </li>
        <?endforeach;?>


    </ul>
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="tabs__content <?if( $key == 0 ):?>active<?endif;?>">
        <div style="padding-left:290px;">
            <div class="row">
                <div class="col-md-8">
                    <h3><?=$arItem['~NAME']?></h3>
                    <?=$arItem['PREVIEW_TEXT']?>
                </div>
                <div class="col-md-4">
                    <?=$arItem['PROPERTIES']['HOW']['~VALUE']['TEXT']?>
                </div>
            </div>

            <p class="text-red"><strong><?=$arItem['PROPERTIES']['TYPE_DELIV']['~VALUE']?></strong></p>

            <div class="row">
                <?=$arItem['PROPERTIES']['DELIVERY']['~VALUE']['TEXT']?>
            </div>

            <hr>
        </div>
        <div style="padding-left:110px">
        <?=$arItem['DETAIL_TEXT']?>
            <?foreach($arItem['PROPERTIES']['RED']['~VALUE'] as $arProp):?>
            <p>
                <sup class="text-red">*</sup> <?=$arProp['TEXT'];?>
            </p>
            <?endforeach;?>
        </div>
    </div>
    <?endforeach;?>

</div><!-- .tabs -->
