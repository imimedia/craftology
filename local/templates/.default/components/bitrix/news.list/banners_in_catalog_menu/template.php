<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!$arResult['ITEMS'])
    return;

foreach ($arResult['ITEMS'] as $arItem) {
    $image = CFile::ResizeImageGet($arItem['~DETAIL_PICTURE'], array("width" => 310, "height" => 500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    ?>
    <? if (empty($arItem['PROPERTIES']['LINK']['VALUE']) == false) {?>
    <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">
	<?}?>
	    <img src="<?= $image['src'] ?>" alt="" />
	<? if (empty($arItem['PROPERTIES']['LINK']['VALUE']) == false) {?>
	</a>
	<?}?>
<? }