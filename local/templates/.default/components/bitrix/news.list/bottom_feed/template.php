<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arParams['TITLE'] = isset($arParams['TITLE']) ? trim($arParams['TITLE']) : '';
$arParams['HTML_CONTAINER'] = isset($arParams['HTML_CONTAINER']) && in_array($arParams['HTML_CONTAINER'], array('BOTH', 'NO', 'TOP', 'BOTTOM')) ? $arParams['HTML_CONTAINER'] : 'BOTH';
?>
<? if ($arParams['HTML_CONTAINER'] == 'BOTH' || $arParams['HTML_CONTAINER'] == 'TOP') { ?>
    <div class="lents">
    <div class="container">
<? } ?>
<? if ($arResult['ITEMS']) { ?>
    <? if ($arParams['TITLE']) { ?><div class="lents__title"><?= $arParams['TITLE'] ?></div><? } ?>
    <div class="lent">
        <div class="swiper-container slider">
            <div class="swiper-wrapper">
                <? foreach ($arResult['ITEMS'] as $arItem) {
                    $image = array();
                    if ($arItem['~DETAIL_PICTURE'])
                        $image = CFile::ResizeImageGet($arItem['~DETAIL_PICTURE'], array("width" => 120, "height" => 120), BX_RESIZE_IMAGE_PROPORTIONAL, true);

                    $text = '';
                    if (isset($arItem['PREVIEW_TEXT']) && trim($arItem['PREVIEW_TEXT']))
                        $text = trim($arItem['PREVIEW_TEXT']);
                    else if (isset($arItem['DETAIL_TEXT']) && trim($arItem['DETAIL_TEXT']))
                        $text = trim($arItem['DETAIL_TEXT']);
                    if ($text) {
                        $obParser = new CTextParser;
                        $text = $obParser->html_cut($text, 100);
                    }
                    ?>
                    <div class="swiper-slide">
                        <div class="item<?= !$image ? ' noimage' : '' ?>">
                            <? if ($image) { ?><div class="lent__image"><img src="<?= $image['src'] ?>" alt="" /></div><? } ?>
                            <div class="lent__descr">
                                <div class="lent__name"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
                                <div class="lent__meta">
                                    <div class="lent__date"><?= $arItem['DISPLAY_ACTIVE_FROM'] ?></div>
                                    <? if ($arItem['IBLOCK_SECTION_ID']) {
                                        foreach ($arItem['IBLOCK_SECTION_ID'] as $sid) {
                                            if (array_key_exists($sid, $arResult['SECTIONS'])) { ?>
                                                <a href="<?= $arResult['SECTIONS'][$sid]['SECTION_PAGE_URL'] ?>" class="tag"><?= $arResult['SECTIONS'][$sid]['NAME'] ?></a>
                                                <?
                                                break;
                                            }
                                        }
                                    } ?>
                                </div>
                                <div class="lent__text"><?= $text ?></div>
                                <div class="lent__more"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span class="theme-link">Читать далее</span></a></div>
                            </div>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>
    </div>
<? } ?>
<? if ($arParams['HTML_CONTAINER'] == 'BOTH' || $arParams['HTML_CONTAINER'] == 'BOTTOM') { ?>
    </div>
    </div>
<? } ?>