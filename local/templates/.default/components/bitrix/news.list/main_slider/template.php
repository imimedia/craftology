<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$cnt = 0;
foreach ($arResult['ITEMS'] as $arItem)
    if ($arItem['DETAIL_PICTURE'])
        $cnt++;

if (!$cnt)
    return;
?>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
<style>
    body,
    body .page__index, body .page__index *,
    .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
        font-family: 'Open Sans', sans-serif;
    }
</style>

<div class="swiper-container 444 main-banner main-banner--sub" data-autoplay="true">
    <div class="swiper-wrapper">
        <? foreach ($arResult['ITEMS'] as $arItem) {
            if (!$arItem['DETAIL_PICTURE'])
                continue;
            ?>
            <div class="swiper-slide" style="background-image: url('<?= $arItem['DETAIL_PICTURE']['SRC'] ?>')"></div>
        <? } ?>

    </div>

    <div class="swiper-button-prev paginate paginate--left"><i></i><i></i></div>
    <div class="swiper-button-next paginate paginate--right"><i></i><i></i></div>
    <div class="slider-nav 345345">
        <div class="swiper-pagination"></div>
    </div>
</div>