<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="d-flex flex-row justify-content-center b-point">
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	if($key == 3) break;
    ?>
    <div class="b-point__item p-1 text-center" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="b-point__icon">
            <?
            echo file_get_contents($_SERVER['DOCUMENT_ROOT'].''.CFile::GetPath($arItem['PROPERTIES']['SVG']['VALUE']));
            ?>
        </div>
        <h3 class="b-point__title">
            <?=$arItem['NAME']?>
        </h3>
        <div class="b-point__info"> <?=$arItem['PREVIEW_TEXT']?></div>
    </div>

<?endforeach;?>
</div>
<div class="d-flex flex-row justify-content-center b-point">
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    if($key < 3) continue;
    ?>
    <div class="b-point__item p-1 text-center" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="b-point__icon">
            <?
            echo file_get_contents($_SERVER['DOCUMENT_ROOT'].''.CFile::GetPath($arItem['PROPERTIES']['SVG']['VALUE']));
            ?>
        </div>
        <h3 class="b-point__title">
            <?=$arItem['NAME']?>
        </h3>
        <div class="b-point__info"> <?=$arItem['PREVIEW_TEXT']?></div>
    </div>
<?endforeach;?>
</div>
