<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult['SECTIONS'] = array();
$arResult['IBLOCK_SECTION_ID'] = array();
$rs = CIBlockElement::GetElementGroups($arResult['ID'], false, array('ID'));
while ($ar = $rs->GetNext())
    $arResult['IBLOCK_SECTION_ID'][] = $ar['ID'];

foreach ($arResult['IBLOCK_SECTION_ID'] as $sid) {
    if (!array_key_exists($sid, $arResult['SECTIONS'])) {
        $rsSections = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $sid), false, false, array('ID', 'NAME', 'SECTION_PAGE_URL'));
        $rsSections->SetUrlTemplates();
        if ($section = $rsSections->GetNext()) {
            $arResult['SECTIONS'][$section['ID']] = $section;
        }
    }
}