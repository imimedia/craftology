<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="news-detail">
	<?
	$image = array();
	if ($arResult['~DETAIL_PICTURE']) {
		$image = CFile::ResizeImageGet($arResult['~DETAIL_PICTURE'], array("width" => 370, "height" => 370), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		?>
		<img src="<?= $image['src'] ?>" alt="" class="news-detail__image" />
	<? } ?>
	<h1><?= $arResult['NAME'] ?></h1>
	<div class="news-detail__meta">
		<? if ($arResult['IBLOCK_SECTION_ID']) {
			foreach ($arResult['IBLOCK_SECTION_ID'] as $sid) {
				if (array_key_exists($sid, $arResult['SECTIONS'])) { ?>
		<a href="/news/<?= $arResult['SECTIONS'][$sid]['SECTION_PAGE_URL'] ?>" class="tag"><?= $arResult['SECTIONS'][$sid]['NAME'] ?></a>
				<? }
			}
		} ?>
		<div class="news-detail__date">Дата <?= $arResult["DISPLAY_ACTIVE_FROM"] ?></div>
	</div>
	<?= $arResult['DETAIL_TEXT'] ?>
	<div class="news-detail__meta">
		<a href="<?= App::getFacebookShareLink() ?>" class="share_fb js-socialPopupBtn" data-width="660" data-height="425">Поделиться в facebook</a>
		<? if (isset($arResult['PROPERTIES']['AUTHOR']) && trim($arResult['PROPERTIES']['AUTHOR']['VALUE'])) { ?>
		<span class="author"><?= $arResult['PROPERTIES']['AUTHOR']['VALUE'] ?></span>
		<? } ?>
	</div>
</div>