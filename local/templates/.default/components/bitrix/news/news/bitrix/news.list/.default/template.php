<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
	<div class="h1 center">Новости магазина</div>
	<div class="news-items">
		<? foreach ($arResult['ITEMS'] as $arItem) {
			$image = array();
			if ($arItem['~DETAIL_PICTURE'])
				$image = CFile::ResizeImageGet($arItem['~DETAIL_PICTURE'], array("width" => 120, "height" => 120), BX_RESIZE_IMAGE_PROPORTIONAL, true);

			$text = '';
			if (isset($arItem['PREVIEW_TEXT']) && trim($arItem['PREVIEW_TEXT']))
				$text = trim($arItem['PREVIEW_TEXT']);
			else if (isset($arItem['DETAIL_TEXT']) && trim($arItem['DETAIL_TEXT']))
				$text = trim($arItem['DETAIL_TEXT']);
			if ($text) {
				$obParser = new CTextParser;
				$text = $obParser->html_cut($text, 330);
			}
			?>
			<div class="news-item<?= !$image ? ' noimage' : '' ?>">
				<? if ($image) { ?>
					<div class="news-item__image"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $image['src'] ?>" alt="" /></a></div>
				<? } ?>
				<div class="news-item__text">
					<div class="news-item__name"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
					<div class="news-item__meta">
						<span class="news-item__date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						<? if ($arItem['IBLOCK_SECTION_ID']) {
							foreach ($arItem['IBLOCK_SECTION_ID'] as $sid) {
								if (array_key_exists($sid, $arResult['SECTIONS'])) { ?>
						<a href="/news/<?= $arResult['SECTIONS'][$sid]['SECTION_PAGE_URL'] ?>" class="tag"><?= $arResult['SECTIONS'][$sid]['NAME'] ?></a>
								<? }
							}
						} ?>
					</div>
					<div class="news-item__preview"><?= $text ?></div>
					<div class="news-item__more"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><span class="theme-link">Читать далее</span></a></div>
				</div>
			</div>
		<? } ?>
	</div>

<?= $arResult["NAV_STRING"] ?>