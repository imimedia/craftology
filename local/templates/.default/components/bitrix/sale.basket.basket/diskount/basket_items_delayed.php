<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>




<?
//echo "<pre>"; print_r($arResult['ITEMS'][nAnCanBuy]); echo "</pre>";

if ($delayCount || $naCount) { ?>
<div class="cart js-cart">
	<div class="cart-before">
		<div class="cart-before__text">Чтобы продолжить оформление нажмите кнопку Оформить заказ"</div>
		<? //if ($USER->IsAuthorized()) { ?>
			<a href="/order/" class="bbtn cart-before__to-order">Оформить заказ</a>
		<?/* } else { ?>
			<a href="" class="bbtn cart-before__to-order js-dialog" data-dialog="auth">Оформить заказ</a>
		<? }*/ ?>
	</div>
	<div class="cart__items">
		<?
		$qty = 0;
        $price=0;
//        $arResult['ITEMS']['DelDelCanBuy']=array_unique(array_merge($arResult['ITEMS']['DelDelCanBuy'],$arResult['ITEMS']['nAnCanBuy']));
        $arResult['ITEMS']['DelDelCanBuy']=array_merge($arResult['ITEMS']['DelDelCanBuy'],$arResult['ITEMS']['nAnCanBuy']);
		foreach ($arResult['ITEMS']['DelDelCanBuy'] as $arItem) {
			$qty += $arItem["QUANTITY"];
            $price+=$arItem["PRICE"]*$arItem['QUANTITY'];
        }
//        echo "<pre>"; print_r($arResult['ITEMS'][DelDelCanBuy]); echo "</pre>";


		foreach ($arResult['ITEMS']['DelDelCanBuy'] as $arItem) {
			$G_CART_PRODUCTS[] = $arItem['PRODUCT_ID'];
			?>
			<div class="cart__item js-cartItem" data-cart-item="<?= $arItem['ID'] ?>">
				<div class="cart__image"><? if ($arItem['DETAIL_PICTURE']) {
						$image = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array("width" => 60, "height" => 60), BX_RESIZE_IMAGE_PROPORTIONAL, true);
						?><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $image['src'] ?>" alt="" /></a><?
					} ?></div>
				<div class="cart__name"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
				<div class="cart__price"><?=$arItem["PRICE_FORMATED"]?></div>

                <div class="cart__qty">
					<div class="qty">
						<input type="text" value="<?=$arItem["QUANTITY"]?>" class="js-cartItemQty" data-available="<?= (float) $arItem['AVAILABLE_QUANTITY'] ?>" />
						<div class="qty-control">
							<div class="qty-incr"></div>
							<div class="qty-decr"></div>
						</div>
					</div>
				</div>
				<div class="cart__sum js-cartItemSum"><?=$arItem["SUM"]?></div>
				<div class="cart__delete"><a href="" class="js-cartItemDelete"></a></div>
			</div>
		<? } ?>
	</div>

	<div class="cart__total">
		<div class="cart__total__text">Итого: <span class="js-cartAllQty"><?= $qty ?> <?= App::getNumEnding($qty, array('товар', 'товара', 'товаров')) ?></span> на сумму <span class="js-cartAllSum">
            <?= CCurrencyLang::CurrencyFormat($price, $arResult['ITEMS']['DelDelCanBuy'][0]["CURRENCY"], true); // $arResult['allSum_FORMATED'] ?></span>
        </div>
		<? //if ($USER->IsAuthorized()) { ?>
			<a href="/order/" class="bbtn cart__total__to-order">Оформить заказ</a>
		<? /*} else { ?>
			<a href="" class="bbtn cart__total__to-order js-dialog" data-dialog="auth">Оформить заказ</a>
		<? }*/ ?>
	</div>
</div>
<? } else { ?>
	<div>Ваша корзина пустая!</div>
	<div>- Положите товар в корзину при помощи кнопки "Купить" указав необходимое кол-во</div>
	<div>- После этого нажмите кнопку "Оформить заказ"</div>
<? } ?>