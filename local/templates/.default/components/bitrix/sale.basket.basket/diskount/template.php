<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<?
$cartPrice = $arResult['allSum'];
$maxDiscSum = 3500;
$toDisc = 0;
if($cartPrice > 0){
    $perc = round($cartPrice/3500*100);
}
if($cartPrice < 1500){
    $percent = 1;
    $toDisc = 1500 -$cartPrice;
}elseif($cartPrice > 1500 && $cartPrice < 2500){
    $percent = 2;
    $toDisc = 2500 -$cartPrice;
}elseif($cartPrice > 2500 && $cartPrice< 3500){
    $percent = 3;
    $toDisc = 3500 -$cartPrice;
}elseif($cartPrice > 3500){
    $toDisc = 999;
}



?>

<?if($cartPrice > 0):?>

        <div class="progress__head">
            <?if($toDisc != 999):?>
                <?if(($percent-1) > 0):?>Ваша скидка <?=$percent-1?>%. <?endif;?>До скидки <?=$percent?>% осталось <span><?=round($toDisc,2);?></span> ₽
            <?else:?>
                <?if($toDisc > 0):?>Ваша скидка 3%<?endif;?>
            <?endif;?>
        </div>
        <div class="progress">
            <div class="progress__bar" role="progressbar" style="width:<?=$perc?>%;">
                <span class="progress__bar"></span>
                <span class="progress__info progress__info--begin">
                                    <i>Скидка</i>
                                </span>
                <span class="progress__count progress__count--one"></span>
                <span class="progress__info progress__info--one">
                                    <i>1%</i>(1500 ₽)
                                </span>
                <span class="progress__count progress__count--two"></span><span class="progress__info progress__info--two"><i>2%</i>(2500 ₽)</span><span class="progress__count progress__count--three"></span><span class="progress__info progress__info--three"><i>3%</i>(3500 ₽)</span></div>
        </div>

<?endif;?>