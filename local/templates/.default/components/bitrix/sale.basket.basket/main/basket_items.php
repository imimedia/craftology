<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<? if ($normalCount) { ?>
    <div class="cart js-cart">
        <div class="cart-before">
            <div class="cart-before__text">Чтобы продолжить оформление нажмите кнопку Оформить заказ"</div>
            <!--        --><?// if ($arResult['allSum'] >= 1000) { ?>
            <a href="/order/" class="bbtn cart__total__to-order">Оформить заказ</a>
            <!--        --><?// } else { ?>
            <!--            <a href="/order/" onclick="return false;" disabled class="bbtn cart__total__to-order" style="cursor: default;background-color: #555555;">Оформить заказ</a>-->
            <!--        --><?// } ?>
        </div>
        <div class="cart__items">
            <?
            $qty = 0;
            foreach ($arResult['ITEMS']['AnDelCanBuy'] as $arItem) {
                $G_CART_PRODUCTS[] = $arItem['PRODUCT_ID'];
                $qty += $arItem["QUANTITY"];
                ?>
                <div class="cart__item js-cartItem" data-cart-item="<?= $arItem['ID'] ?>">
                    <? $ar_res = CCatalogProduct::GetByID($arItem['PRODUCT_ID']);?>

                    <div class="cart__image"><? if ($arItem['DETAIL_PICTURE']) {
                            $image = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array("width" => 60, "height" => 60), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                            ?><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $image['src'] ?>" alt="" /></a><?
                        } ?></div>
                    <div class="cart__name"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
                    <div class="cart__price" data-price="<?=$arItem["PRICE"]?>"><?=$arItem["PRICE_FORMATED"]?></div>
                    <div class="cart__qty">
                        <div class="qty">

                            <? if ($ar_res['CAN_BUY_ZERO'] == 'N') { ?>
                                <input type="text" value="<?=$arItem["QUANTITY"]?>" class="js-cartItemQty" data-available="<?= (float) $arItem['AVAILABLE_QUANTITY'] ?>" />
                            <? } else {?>
                                <input type="text" value="<?=$arItem["QUANTITY"]?>" class="js-cartItemQty" data-available="999999" />
                            <?}?>
                            <div class="qty-control">
                                <div class="qty-incr"></div>
                                <div class="qty-decr"></div>
                            </div>
                        </div>
                    </div>
                    <div class="cart__sum js-cartItemSum"><?=$arItem["SUM"]?></div>
                    <div class="cart__delete">
                        <a href="/personal/cart/?basketAction=delete&id=<?=$arItem["ID"]?>" class="js-cartItemDelete2"></a>
                        <!--
					<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>"
										onclick="return deleteProductRow(this)">

									</a>
-->
                    </div>
                </div>
            <? }

            $cartPrice = $arResult['allSum'];
            ?>
        </div>

        <div class="cart__total">
            <div class="cart__total__text">Итого: <span class="js-cartAllQty"><?= $qty ?> <?= App::getNumEnding($qty, array('товар', 'товара', 'товаров')) ?></span> на сумму
                <span class="js-cartAllSum"><?= $arResult['allSum_FORMATED'] ?></span></div>
            <!--		--><?// if ($arResult['allSum'] >= 1000) { ?>
            <a href="/order/" class="bbtn cart__total__to-order">Оформить заказ</a>
            <!--		--><?// } else { ?>
            <!--            <a href="/order/" onclick="return false;" class="bbtn cart__total__to-order" style="cursor: default;background-color: #555555;">Оформить заказ</a>-->
            <!--		--><?// } ?>
        </div>
        <!--    --><?//if($arResult['allSum'] < 1000):?>
        <!--    <div class="cart__total">-->
        <!---->
        <!--            <p style="font-size: 18px;">Минимальная сумма заказа : 1000<ruble><span class="text">руб.</span></ruble>.-->
        <!--                Вам необходимо добавить товаров на сумму --><?//= (1000-$arResult['allSum'])?><!--  <ruble><span class="text">руб.</span></ruble> для оформления заказа</p>-->
        <!---->
        <!--    </div>-->
        <!--    --><?//endif;?>
    </div>
<? } else { ?>
    <div>Ваша корзина пустая!</div>
    <div>- Положите товар в корзину при помощи кнопки "Купить" указав необходимое кол-во</div>
    <div>- После этого нажмите кнопку "Оформить заказ"</div>
<? } ?>