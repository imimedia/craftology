<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<div class="cart js-cart">
	<div class="cart__items">
		<?
		$qty = 0;
		foreach ($arResult['ITEMS']['nAnCanBuy'] as $arItem) {
			$G_CART_PRODUCTS[] = $arItem['PRODUCT_ID'];
			$qty += $arItem["QUANTITY"];
			?>
			<div class="cart__item js-cartItem" data-cart-item="<?= $arItem['ID'] ?>">
				<div class="cart__image"><? if ($arItem['DETAIL_PICTURE']) {
						$image = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array("width" => 60, "height" => 60), BX_RESIZE_IMAGE_PROPORTIONAL, true);
						?><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $image['src'] ?>" alt="" /></a><?
					} ?></div>
				<div class="cart__name"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
				<div class="cart__price"><?=$arItem["PRICE_FORMATED"]?></div>
				<div class="cart__qty">
					<div class="qty">
						<input type="text" value="<?=$arItem["QUANTITY"]?>" class="js-cartItemQty" />
						<div class="qty-control">
							<div class="qty-incr"></div>
							<div class="qty-decr"></div>
						</div>
					</div>
				</div>
				<div class="cart__sum js-cartItemSum"><?=$arItem["SUM"]?></div>
				<div class="cart__delete"><a href="" class="js-cartItemDelete"></a></div>
			</div>
		<? } ?>
	</div>
</div>