<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
use Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Sale,
    Bitrix\Currency;
Loader::includeModule('sale');
Loader::includeModule('shop');
global $G_CART_PRODUCTS;
$G_CART_PRODUCTS = array();
if (strlen($arResult["ERROR_MESSAGE"]) <= 0) {

    $normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
    $normalHidden = ($normalCount == 0) ? 'style="display:none;"' : '';

    $delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
    $delayHidden = ($delayCount == 0) ? 'style="display:none;"' : '';

    $subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
    $subscribeHidden = ($subscribeCount == 0) ? 'style="display:none;"' : '';

    $naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
    $naHidden = ($naCount == 0) ? 'style="display:none;"' : '';

    /*
        echo '<pre>';
        print_r($arResult["ITEMS"]);
        echo '</pre>';
    */



    $dbBasketItems = CSaleBasket::GetList(
        array("ID" => "ASC"),
        array(
            'FUSER_ID' => CSaleBasket::GetBasketUserID(),
            'LID' => SITE_ID,
            'ORDER_ID' => 'NULL'
        ),
        false,
        false,
        array(
            'ID', 'NAME','PRODUCT_ID', 'QUANTITY', 'PRICE', 'DISCOUNT_PRICE', 'WEIGHT'
        )
    );

    $allSum = 0;
    $allWeight = 0;
    $arItems = array();
    $arItemsCheck = array();

    while ($arBasketItems = $dbBasketItems->Fetch())
    {
        $arBasketItemsCheck = $arBasketItems;
        $arBasketItemsCheck["QUANTITY"] = ceil(1500/$arBasketItemsCheck["PRICE"]);
        $arItemsCheck[] = $arBasketItemsCheck;

    }

    $arOrder2 = array(
        'SITE_ID' => SITE_ID,
        'USER_ID' => $GLOBALS["USER"]->GetID(),
        'ORDER_PRICE' => $allSum,
        'ORDER_WEIGHT' => $allWeight,
        'BASKET_ITEMS' => $arItemsCheck
    );

    $arOptions = array(
        'COUNT_DISCOUNT_4_ALL_QUANTITY' => 'Y',
    );

    $arErrors = array();

    CSaleDiscount::DoProcessOrder($arOrder2, $arOptions, $arErrors);

    $flag = false;
    foreach ($arOrder2['BASKET_ITEMS'] as $item ){

        if($item['DISCOUNT_PRICE'] == 0){
            $flag = true;
        }
    }
    ?>
    <div class="tabs">
        <? if ((!!$normalCount + !!$delayCount+!!$naCount) > 1) { ?>
            <div class="tabs-tabs">
                <div class="tabs-tab">Готовые к заказу (<?= $normalCount ?>)</div>
                <? if ($delayCount || $naCount) { ?>
                    <div class="tabs-tab">Временно нет в наличии (<?= $delayCount+$naCount ?>)</div>
                <? } ?>
            </div>
            <br/>
            <br/>
        <? } ?>
        <div class="tabs-panes">
            <div class="tabs-pane"><? include('basket_items.php'); ?></div>
            <? if ($delayCount || $naCount) { ?>
                <div class="tabs-pane"><?include('basket_items_delayed.php'); ?></div>
            <? } ?>
        </div>
    </div>
    <?
}
else {
    ShowError($arResult["ERROR_MESSAGE"]);
}
?>

<?
$maxDiscSum = 3500;
$toDisc = 0;
if($cartPrice > 0){
    $perc = round($cartPrice/3500*100);
}
if($cartPrice < 1500){
    $percent = 1;
    $toDisc = 1500 -$cartPrice;
}elseif($cartPrice > 1500 && $cartPrice < 2500){
    $percent = 2;
    $toDisc = 2500 -$cartPrice;
}elseif($cartPrice > 2500 && $cartPrice< 3500){
    $percent = 3;
    $toDisc = 3500 -$cartPrice;
}elseif($cartPrice > 3500){
    $toDisc = 999;
}

?>

    <div class="row" >
        <div class="col-md-6"></div>
        <div class="col-md-6">

            <div class="progress__wrap cart-progress pull-right">
                <?if($cartPrice > 0):?>

                    <div class="progress__head">
                        <?if($toDisc != 999):?>
                            <?if(($percent-1) > 0):?>Ваша скидка <?=$percent-1?>%. <?endif;?>До скидки <?=$percent?>% осталось <span><?=$toDisc;?></span> ₽
                        <?else:?>
                            <?if($toDisc > 0):?>Ваша скидка 3%<?endif;?>
                        <?endif;?>
                    </div>
                    <div class="progress">
                        <div class="progress__bar" role="progressbar" style="width:<?=$perc?>%;">
                            <span class="progress__bar"></span>
                            <span class="progress__info progress__info--begin">
                                        <i>Скидка</i>
                                    </span>
                            <span class="progress__count progress__count--one"></span>
                            <span class="progress__info progress__info--one">
                                        <i>1%</i>(1500 ₽)
                                    </span>
                            <span class="progress__count progress__count--two"></span><span class="progress__info progress__info--two"><i>2%</i>(2500 ₽)</span><span class="progress__count progress__count--three"></span><span class="progress__info progress__info--three"><i>3%</i>(3500 ₽)</span></div>
                    </div>

                <?endif;?>
            </div>

        </div>
    </div>
<?

$discountSelect = array(
    'ID', 'PRIORITY', 'SORT', 'LAST_DISCOUNT', 'LAST_LEVEL_DISCOUNT', 'UNPACK', 'APPLICATION', 'USE_COUPONS', 'EXECUTE_MODULE',
    'NAME', 'CONDITIONS_LIST', 'ACTIONS_LIST'
);
$discountOrder = array();

//$discountIterator = Bitrix\Sale\Internals\DiscountTable::getList(array(
//    'select' => $discountSelect,
//    'filter' => $discountFilter,
//    'order' => $discountOrder
//));


$discountFilter = array(
    '@ID' => '23',
    '=LID' => 's1'
);
use Bitrix\Sale\Internals\DiscountTable;
$res = DiscountTable::getList(array(    'select' => $discountSelect,
    'filter' => $discountFilter,
    'order' => $discountOrder))->fetchAll();
?>