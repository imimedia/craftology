<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (!$arResult['ITEMS'])
	return;

$arParams['TITLE'] = array_key_exists('TITLE', $arParams) && trim($arParams['TITLE']) ? trim($arParams['TITLE']) : '';
$arParams['COLS'] = array_key_exists('COLS', $arParams) && intval($arParams['COLS']) > 0 ? trim($arParams['COLS']) : 3;
$arParams['ROWS'] = array_key_exists('ROWS', $arParams) && intval($arParams['ROWS']) > 0 ? trim($arParams['ROWS']) : 1;
?>
<? if ($arParams['TITLE']) { ?>
	<div class="h1 center"><?= $arParams['TITLE'] ?></div>
<? } ?>
<div class="catalog__items">
	<div class="swiper-container slider" data-slider-cols="<?= $arParams['COLS'] ?>" data-slider-rows="<?= $arParams['ROWS'] ?>">
		<div class="swiper-wrapper">

			<? foreach ($arResult['ITEMS'] as $arItem) {
				$canBuy = $arItem['CAN_BUY'] && $arItem['MIN_PRICE']['CAN_BUY'] == 'Y';
				$canToOrder = $arItem['CAN_BUY'] && (float)$arItem['CATALOG_QUANTITY'] <= 0;
				$image = array();
				if ($arItem['PREVIEW_PICTURE']) {
					$image = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array("width" => 200, "height" => 200), BX_RESIZE_IMAGE_EXACT, true);
				}
				else if (!$image && $arItem['DETAIL_PICTURE']) {
					$image = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], array("width" => 200, "height" => 200), BX_RESIZE_IMAGE_EXACT, true);
				}
				?>
				<div class="swiper-slide">
					<div class="item" data-product="<?= $arItem['ID'] ?>">
						<div class="item-inner">
							<div class="catalog-item__image"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><? if ($image) { ?><img src="<?= $image['src'] ?>" alt="" /><? } ?></a></div>
							<div class="catalog-item__name"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
							<div class="catalog-item__price justify-items">
								<span class="justify-item"></span>
								<? if ($arItem['MIN_PRICE'] && $arItem['MIN_PRICE']['CAN_ACCESS'] == 'Y') { ?>
									<span class="justify-item"><?= $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] ?></span>
									<? if ($arItem['MIN_PRICE']['DISCOUNT_DIFF']) { ?>
										<span class="old-price justify-item"><?= $arItem['MIN_PRICE']['PRINT_VALUE'] ?></span>
									<? } ?>
								<? } ?>
								<span class="justify-item"></span>
							</div>
							<div class="catalog-item__buy js-cart">
			                <? if ($canBuy && $arItem['CATALOG_QUANTITY_TRACE'] == 'Y') { //!$canToOrder) { ?>
			
			                     <nav class="js-quantity" data-id="<?=$arItem["ID"]?>">
			                        <div class="qty">
			
			                            <? if ($arItem['CATALOG_CAN_BUY_ZERO'] == 'N') { ?>
			                            <input type="text" value="<?=($arItem["QUANTITY_BASKET"] == 0)?'1':$arItem["QUANTITY_BASKET"]?>" data-id="<?=$arItem["ID"]?>" class="focus_click js-cartItemQty quatity_item_<?=$arItem["ID"]?>" data-available="<?= (float) $arItem['CATALOG_QUANTITY'] ?>" />
			                            <? } else {?>
			                            <input type="text" value="<?=($arItem["QUANTITY_BASKET"] == 0)?'1':$arItem["QUANTITY_BASKET"]?>" data-id="<?=$arItem["ID"]?>" class="focus_click js-cartItemQty quatity_item_<?=$arItem["ID"]?>" data-available="999999" />   <?//js-qtyInput js-cartItemQty?>
			                            <?}?>
			                            <div class="qty-control">
			                                <div class="incr qty-incr" data-id="<?=$arItem["ID"]?>"></div>
			                                <div class="decr qty-decr" data-id="<?=$arItem["ID"]?>"></div>
			                            </div>
			                        </div>
			
			                        <button type="submit" class="btn add2cart js-add2cart add_basket_<?=$arItem["ID"]?>" data-id="<?=$arItem["ID"]?>">
			
			
			                        <?if($arItem["QUANTITY_BASKET"] > 0){
			                            ?><span class="js-buy active">+</span><?
			                        } else {
			                            ?><span class="js-buy">Купить</span><?
			                        }?>
			                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32"><path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/></svg>
			                        </button>
			                        <?if($arItem["QUANTITY_BASKET"] > 0){?>
			                            <span class="js-bask basket_up_<?=$arItem["ID"]?> wrap_basket">Добавлено в <a href="/personal/cart/">корзину</a></span>
			                        <?}else {?>
			                            <span class="js-bask basket_up_<?=$arItem["ID"]?> wrap_basket"></span>
			                        <?}?>
			                    </nav>
			
			                <? } else if ($canToOrder) { ?>
			                    <a href="<?= '/local/api/form/zakaz/?theme='.urlencode('Заказ товара "'.$arItem['NAME'].'", #'.$arItem['ID']) ?>" class="btn add2cart js-dialog"><span>Под заказ</span></a>
			                <? } else {?>
			                            <span class="btn btn-b add2cart">Нет в наличии</span>
			                            <?/*$APPLICATION->IncludeComponent(
			                                "bitrix:sale.notice.product",
			                                "",
			                                Array(
			                                    "NOTIFY_ID" => $arItem['ID'],
			                                    "NOTIFY_URL" => htmlspecialcharsback($arItem["SUBSCRIBE_URL"]),
			                                    "NOTIFY_USE_CAPTHA" => "N"
			                                )
			                            );*/?>
			                <? } ?>
			            </div>
							
							<? if ($arItem['PROPERTIES']['NEW']['VALUE']) { ?>
								<div class="catalog-item__new"></div>
							<? }
							else if ($arItem['PROPERTIES']['HIT']['VALUE']) { ?>
								<div class="catalog-item__hit"></div>
							<? } ?>
							<a href="" class="catalog-item__favorite js-favoriteBtn"></a>
						</div>
					</div>
				</div>
			<? } ?>

		</div>

		<div class="slider-nav">
			<div class="swiper-button-prev"></div>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-next"></div>
		</div>
	</div>
</div>