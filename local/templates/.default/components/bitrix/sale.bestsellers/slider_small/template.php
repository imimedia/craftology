<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (!$arResult['ITEMS'])
	return;

$arParams['TITLE'] = array_key_exists('TITLE', $arParams) && trim($arParams['TITLE']) ? trim($arParams['TITLE']) : '';
$sliderId = 'viewedProduct_'.randString();
?>
<? if ($arResult['ITEMS']) { ?>
	<? if ($arParams['TITLE']) { ?><div class="lents__title"><?= $arParams['TITLE'] ?></div><? } ?>

    <div class="container">
	<div class="lent lent-catalog">
		<div class="swiper-container slider" data-pagination="#<?= $sliderId ?>">
			<div class="swiper-wrapper">

				<? foreach ($arResult['ITEMS'] as $arItem) {
					$image = array();
					if ($arItem['PREVIEW_PICTURE']) {
						$image = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array("width" => 200, "height" => 200), BX_RESIZE_IMAGE_EXACT, true);
					}
					else if (!$image && $arItem['DETAIL_PICTURE']) {
						$image = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], array("width" => 200, "height" => 200), BX_RESIZE_IMAGE_EXACT, true);
					}
					?>
					<div class="swiper-slide">
						<div class="item" data-product="<?= $arItem['ID'] ?>">
							<div class="lent__image"><? if ($image) { ?><img src="<?= $image['src'] ?>" alt="" /><? } ?></div>
							<div class="lent__descr">
								<div class="lent__name"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
								<? if ($arItem['SECTION']) { ?><a href="<?= $arItem['SECTION']['SECTION_PAGE_URL'] ?>" class="tag"><?= $arItem['SECTION']['NAME'] ?></a><? } ?>
								<div class="lent__price"><? if ($arItem['MIN_PRICE'] && $arItem['MIN_PRICE']['CAN_ACCESS'] == 'Y') {
										echo $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
									} ?><a href="" class="lent__favorite js-favoriteBtn"></a></div>
							</div>
						</div>
					</div>
				<? } ?>

			</div>
		</div>
	</div>

	<div class="slider-nav" id="<?= $sliderId ?>">
		<div class="swiper-button-prev"></div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button-next"></div>
	</div>
    </div>
<? } ?>