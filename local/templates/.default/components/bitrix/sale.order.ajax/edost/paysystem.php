<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<? /* шаблон edost - НАЧАЛО */ ?>
<? if (isset($arResult['edost']['format'])) { ?>

	<script type="text/javascript">
		function changePaySystem(param) {
			if (BX("account_only") && BX("PAY_CURRENT_ACCOUNT"))
				if (BX("account_only").value == 'Y') {
					if (param == 'account') {
						if (BX("PAY_CURRENT_ACCOUNT").checked) BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
						else BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");

						var el = document.getElementsByName("PAY_SYSTEM_ID");
						for(var i = 0; i < el.length; i++) el[i].checked = false;
					}
					else {
						BX("PAY_CURRENT_ACCOUNT").checked = false;
						BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
					}
				}
				else if (BX("account_only").value == 'N') {
					if (param == 'account') {
						if (BX("PAY_CURRENT_ACCOUNT").checked) BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
						else BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
					}
				}

			submitForm();
		}
	</script>

	<? if ((!empty($arResult['PAY_SYSTEM']) || $arResult['PAY_FROM_ACCOUNT'] == 'Y') && !empty($arResult['edost']['format']['active']['id'])) { ?>
		<div class="edost edost_main edost_template_div"<?=(!empty($arResult['edost']['format']['active']['cod_tariff']) ? ' style="display: none;"' : '')?>>
			<?
			if (!isset($table_width)) $table_width = 645;
			$hide_radio = (count($arResult['PAY_SYSTEM']) == 1 && $arResult['PAY_FROM_ACCOUNT'] != 'Y' ? true : false);
			$ico_default = $templateFolder.'/images/logo-default-ps.gif';
			?>
			<h4><?=GetMessage("SOA_TEMPL_PAY_SYSTEM")?></h4>

			<div class="columns">
				<div class="col-7">
				<?
				if ($arResult['PAY_FROM_ACCOUNT'] == 'Y') {
					$id = 'PAY_CURRENT_ACCOUNT';
					$accountOnly = ($arParams['ONLY_FULL_PAY_FROM_ACCOUNT'] == 'Y') ? 'Y' : 'N';
					?>
					<input type="hidden" id="account_only" value="<?=$accountOnly?>">
					<input type="hidden" name="PAY_CURRENT_ACCOUNT" value="N">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="edost_format_ico" width="95">
								<input class="edost_format_radio" type="checkbox" name="<?=$id?>" id="<?=$id?>" value="Y" <?=($arResult['USER_VALS']['PAY_CURRENT_ACCOUNT'] == 'Y' ? 'checked="checked"' : '')?> onclick="changePaySystem('account');">

								<? if (!empty($ico_default)) { ?>
									<label class="edost_format_radio" for="<?=$id?>"><img class="edost_ico edost_ico_normal" src="<?=$ico_default?>" border="0"></label>
								<? } else { ?>
									<div class="edost_ico"></div>
								<? } ?>
							</td>
							<td class="edost_format_tariff">
								<label for="<?=$id?>">
									<span class="edost_format_tariff"><?=GetMessage('SOA_TEMPL_PAY_ACCOUNT')?></span>

									<div class="edost_format_description edost_description"><?=GetMessage('SOA_TEMPL_PAY_ACCOUNT1').' <b>'.$arResult['CURRENT_BUDGET_FORMATED']?></b></div>
									<div class="edost_format_description edost_description">
										<?=($arParams['ONLY_FULL_PAY_FROM_ACCOUNT'] == 'Y' ? GetMessage('SOA_TEMPL_PAY_ACCOUNT3') : GetMessage('SOA_TEMPL_PAY_ACCOUNT2'))?>
									</div>
								</label>
							</td>
						</tr>
					</table>
				<? }
				$activePaySystemId = null;
				foreach($arResult['PAY_SYSTEM'] as $v) {
					$id = 'ID_PAY_SYSTEM_ID_'.$v['ID'];
					$value = $v['ID'];
					$checked = ($v['CHECKED'] == 'Y' && !($arParams['ONLY_FULL_PAY_FROM_ACCOUNT'] == 'Y' && $arResult['USER_VALS']['PAY_CURRENT_ACCOUNT'] == 'Y') ? true : false);

					if ($checked)
						$activePaySystemId = $v['ID'];

					if (!empty($v['PSA_LOGOTIP']['SRC'])) $ico = $v['PSA_LOGOTIP']['SRC'];
					else if (!empty($ico_default)) $ico = $ico_default;
					else $ico = false;
					?>
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="edost_format_ico" width="<?=($hide_radio ? '70' : '95')?>">
								<input class="edost_format_radio" <?=($hide_radio ? 'style="display: none;"' : '')?> type="radio" id="<?=$id?>" name="PAY_SYSTEM_ID" value="<?=$value?>" <?=($checked ? 'checked="checked"' : '')?> onclick="changePaySystem();">

								<? if ($ico !== false) { ?>
									<label class="edost_format_radio" for="<?=$id?>"><img class="edost_ico edost_ico_normal" src="<?=$ico?>" border="0"></label>
								<? } else { ?>
									<div class="edost_ico"></div>
								<? } ?>
							</td>

							<td class="edost_format_tariff">
								<label for="<?=$id?>">
									<span class="edost_format_tariff"><?=$v['PSA_NAME']?></span>

									<? if (!empty($v['DESCRIPTION'])) { ?>
										<div class="edost_format_description edost_description"><?=nl2br($v['DESCRIPTION'])?></div>
									<? } ?>

									<? if (!empty($v['PRICE'])) { ?>
										<div class="edost_format_description edost_warning">
											<?=str_replace('#PAYSYSTEM_PRICE#', SaleFormatCurrency(roundEx($v['PRICE'], SALE_VALUE_PRECISION), $arResult['BASE_LANG_CURRENCY']), GetMessage('SOA_TEMPL_PAYSYSTEM_PRICE'))?>
										</div>
									<? } ?>

									<? if (!empty($v['codplus'])) { ?>
										<div class="edost_format_description edost_description"><?=$v['codplus']?></div>
									<? } ?>

									<? if (!empty($v['transfer'])) { ?>
										<div class="edost_format_description edost_warning"><?=$v['transfer']?></div>
									<? } ?>

									<? if (!empty($v['codtotal'])) { ?>
										<div class="edost_format_description edost_description"><?=$v['codtotal']?></div>
									<? } ?>
								</label>
							</td>
						</tr>
					</table>
				<? } ?>
				</div>
				<div class="col-5">
					<? if ($activePaySystemId == 2) { ?>
						<div class="edost-format__descr">
							Яндекс.Касса. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nunc erat, egestas quis consequat at, euismod sollicitudin lacus. Nunc rhoncus odio vel fermentum interdum. Cras lobortis rhoncus nunc eu gravida.
						</div>
					<? } else if ($activePaySystemId == 5) { ?>
						<div class="edost-format__descr">
							Наличными при получении. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nunc erat, egestas quis consequat at, euismod sollicitudin lacus. Nunc rhoncus odio vel fermentum interdum. Cras lobortis rhoncus nunc eu gravida. Duis faucibus bibendum ex, eget cursus turpis congue et. Integer vestibulum nisi ligula, sit amet congue diam commodo ut.
						</div>
					<? } else if ($activePaySystemId == 6) { ?>
						<div class="edost-format__descr">
							Безналичная оплата для Юр лиц. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nunc erat, egestas quis consequat at, euismod sollicitudin lacus. Nunc rhoncus odio vel fermentum interdum. Cras lobortis rhoncus nunc eu gravida. Duis faucibus bibendum ex, eget cursus turpis congue et. Integer vestibulum nisi ligula, sit amet congue diam commodo ut.
						</div>
					<? } ?>
				</div>
			</div>

		</div>
	<? } ?>

<? } ?>
<? /* шаблон edost - КОНЕЦ */ ?>




<? /* шаблон bitrix (на базе 16) - НАЧАЛО */ ?>
<? if (!isset($arResult['edost']['format'])) { ?>

	<div class="section">
		<script type="text/javascript">
			function changePaySystem(param)
			{
				if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
				{
					if (param == 'account')
					{
						if (BX("PAY_CURRENT_ACCOUNT"))
						{
							BX("PAY_CURRENT_ACCOUNT").checked = true;
							BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
							BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

							// deselect all other
							var el = document.getElementsByName("PAY_SYSTEM_ID");
							for(var i=0; i<el.length; i++)
								el[i].checked = false;
						}
					}
					else
					{
						BX("PAY_CURRENT_ACCOUNT").checked = false;
						BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
						BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
					}
				}
				else if (BX("account_only") && BX("account_only").value == 'N')
				{
					if (param == 'account')
					{
						if (BX("PAY_CURRENT_ACCOUNT"))
						{
							BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

							if (BX("PAY_CURRENT_ACCOUNT").checked)
							{
								BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
								BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
							}
							else
							{
								BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
								BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
							}
						}
					}
				}

				submitForm();
			}
		</script>
		<div class="bx_section">
			<?
			if (!empty($arResult["PAY_SYSTEM"]) && is_array($arResult["PAY_SYSTEM"]) || $arResult["PAY_FROM_ACCOUNT"] == "Y")
			{
				?><h4><?=GetMessage("SOA_TEMPL_PAY_SYSTEM")?></h4><?
			}
			if ($arResult["PAY_FROM_ACCOUNT"] == "Y")
			{
				$accountOnly = ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y") ? "Y" : "N";
				?>
				<input type="hidden" id="account_only" value="<?=$accountOnly?>" />
				<div class="bx_block w100 vertical">
					<div class="bx_element">
						<input type="hidden" name="PAY_CURRENT_ACCOUNT" value="N">
						<label for="PAY_CURRENT_ACCOUNT" id="PAY_CURRENT_ACCOUNT_LABEL" onclick="changePaySystem('account');" class="<?if($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y") echo "selected"?>">
							<input type="checkbox" name="PAY_CURRENT_ACCOUNT" id="PAY_CURRENT_ACCOUNT" value="Y"<?if($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y") echo " checked=\"checked\"";?>>
							<div class="bx_logotype">
								<span style="background-image:url(<?=$templateFolder?>/images/logo-default-ps.gif);"></span>
							</div>
							<div class="bx_description">
								<strong><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT")?></strong>
								<p>
								<div><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT1")." <b>".$arResult["CURRENT_BUDGET_FORMATED"]?></b></div>
								<? if ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y"):?>
									<div><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT3")?></div>
								<? else:?>
									<div><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT2")?></div>
								<? endif;?>
								</p>
							</div>
						</label>
						<div class="clear"></div>
					</div>
				</div>
				<?
			}

			uasort($arResult["PAY_SYSTEM"], "cmpBySort"); // resort arrays according to SORT value

			foreach($arResult["PAY_SYSTEM"] as $arPaySystem)
			{
				if (strlen(trim(str_replace("<br />", "", $arPaySystem["DESCRIPTION"]))) > 0 || intval($arPaySystem["PRICE"]) > 0)
				{
					if (count($arResult["PAY_SYSTEM"]) == 1)
					{
						?>
						<div class="bx_block w100 vertical">
							<div class="bx_element">
								<input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">
								<input type="radio"
									   id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
									   name="PAY_SYSTEM_ID"
									   value="<?=$arPaySystem["ID"]?>"
									<?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
									   onclick="changePaySystem();"
								/>
								<label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
									<?
									if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
										$arFileTmp = CFile::ResizeImageGet(
											$arPaySystem["PSA_LOGOTIP"]['ID'],
											array("width" => "95", "height" =>"55"),
											BX_RESIZE_IMAGE_PROPORTIONAL,
											true
										);
										$imgUrl = $arFileTmp["src"];
									else:
										$imgUrl = $templateFolder."/images/logo-default-ps.gif";
									endif;
									?>
									<div class="bx_logotype">
										<span style="background-image:url(<?=$imgUrl?>);"></span>
									</div>
									<div class="bx_description">
										<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
											<strong class="bitrix_title"><?=$arPaySystem["PSA_NAME"];?></strong>
										<?endif;?>
										<div class="bitrix_description">
											<p>
												<?
												if (intval($arPaySystem["PRICE"]) > 0)
													echo str_replace("#PAYSYSTEM_PRICE#", SaleFormatCurrency(roundEx($arPaySystem["PRICE"], SALE_VALUE_PRECISION), $arResult["BASE_LANG_CURRENCY"]), GetMessage("SOA_TEMPL_PAYSYSTEM_PRICE"));
												else
													echo $arPaySystem["DESCRIPTION"];
												?>
											</p>
										</div>
									</div>
								</label>
								<div class="clear"></div>
							</div>
						</div>
						<?
					}
					else // more than one
					{
						?>
						<div class="bx_block w100 vertical">
							<div class="bx_element">
								<input type="radio"
									   id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
									   name="PAY_SYSTEM_ID"
									   value="<?=$arPaySystem["ID"]?>"
									<?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
									   onclick="changePaySystem();" />
								<label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
									<?
									if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
										$arFileTmp = CFile::ResizeImageGet(
											$arPaySystem["PSA_LOGOTIP"]['ID'],
											array("width" => "95", "height" =>"55"),
											BX_RESIZE_IMAGE_PROPORTIONAL,
											true
										);
										$imgUrl = $arFileTmp["src"];
									else:
										$imgUrl = $templateFolder."/images/logo-default-ps.gif";
									endif;
									?>
									<div class="bx_logotype">
										<span style='background-image:url(<?=$imgUrl?>);'></span>
									</div>
									<div class="bx_description">
										<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
											<strong class="bitrix_title"><?=$arPaySystem["PSA_NAME"];?></strong>
										<?endif;?>
										<div class="bitrix_description">
											<p>
												<?
												if (intval($arPaySystem["PRICE"]) > 0)
													echo str_replace("#PAYSYSTEM_PRICE#", SaleFormatCurrency(roundEx($arPaySystem["PRICE"], SALE_VALUE_PRECISION), $arResult["BASE_LANG_CURRENCY"]), GetMessage("SOA_TEMPL_PAYSYSTEM_PRICE"));
												else
													echo $arPaySystem["DESCRIPTION"];
												?>
											</p>
										</div>
									</div>
								</label>
								<div class="clear"></div>
							</div>
						</div>
						<?
					}
				}

				if (strlen(trim(str_replace("<br />", "", $arPaySystem["DESCRIPTION"]))) == 0 && intval($arPaySystem["PRICE"]) == 0)
				{
					if (count($arResult["PAY_SYSTEM"]) == 1)
					{
						?>
						<div class="bx_block horizontal">
							<div class="bx_element">
								<input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">
								<input type="radio"
									   id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
									   name="PAY_SYSTEM_ID"
									   value="<?=$arPaySystem["ID"]?>"
									<?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
									   onclick="changePaySystem();"
								/>
								<label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
									<?
									if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
										$arFileTmp = CFile::ResizeImageGet(
											$arPaySystem["PSA_LOGOTIP"]['ID'],
											array("width" => "95", "height" =>"55"),
											BX_RESIZE_IMAGE_PROPORTIONAL,
											true
										);
										$imgUrl = $arFileTmp["src"];
									else:
										$imgUrl = $templateFolder."/images/logo-default-ps.gif";
									endif;
									?>
									<div class="bx_logotype">
										<span style='background-image:url(<?=$imgUrl?>);'></span>
									</div>
									<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
										<div class="bx_description">
											<div class="clear"></div>
											<strong class="bitrix_title"><?=$arPaySystem["PSA_NAME"];?></strong>
										</div>
									<?endif;?>
								</label>
							</div>
						</div>
						<?
					}
					else // more than one
					{
						?>
						<div class="bx_block horizontal">
							<div class="bx_element">

								<input type="radio"
									   id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
									   name="PAY_SYSTEM_ID"
									   value="<?=$arPaySystem["ID"]?>"
									<?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
									   onclick="changePaySystem();" />

								<label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
									<?
									if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
										$arFileTmp = CFile::ResizeImageGet(
											$arPaySystem["PSA_LOGOTIP"]['ID'],
											array("width" => "95", "height" =>"55"),
											BX_RESIZE_IMAGE_PROPORTIONAL,
											true
										);
										$imgUrl = $arFileTmp["src"];
									else:
										$imgUrl = $templateFolder."/images/logo-default-ps.gif";
									endif;
									?>
									<div class="bx_logotype">
										<span style='background-image:url(<?=$imgUrl?>);'></span>
									</div>
									<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
										<div class="bx_description">
											<div class="clear"></div>
											<strong class="bitrix_title">
												<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
													<?=$arPaySystem["PSA_NAME"];?>
												<?else:?>
													<?="&nbsp;"?>
												<?endif;?>
											</strong>
										</div>
									<?endif;?>
								</label>
							</div>
						</div>
						<?
					}
				}
			}
			?>
			<div style="clear: both;"></div>
		</div>
	</div>

<? } ?>
<? /* шаблон bitrix - КОНЕЦ */ ?>
