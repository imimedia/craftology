<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->SetAdditionalCSS($templateFolder."/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");

CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
?>

<a name="order_form"></a>

<div id="order_form_div" class="order-checkout">
	<NOSCRIPT>
		<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
	</NOSCRIPT>

	<?
	if (!function_exists("getColumnName"))
	{
		function getColumnName($arHeader)
		{
			return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
		}
	}

	if (!function_exists("cmpBySort"))
	{
		function cmpBySort($array1, $array2)
		{
			if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
				return -1;

			if ($array1["SORT"] > $array2["SORT"])
				return 1;

			if ($array1["SORT"] < $array2["SORT"])
				return -1;

			if ($array1["SORT"] == $array2["SORT"])
				return 0;
		}
	}
	?>

	<div class="bx_order_make">
		<?
		if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
		{
			if(!empty($arResult["ERROR"]))
			{
				foreach($arResult["ERROR"] as $v)
					echo ShowError($v);
			}
			elseif(!empty($arResult["OK_MESSAGE"]))
			{
				foreach($arResult["OK_MESSAGE"] as $v)
					echo ShowNote($v);
			}

			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
		}
		else
		{
			if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
			{
			if(strlen($arResult["REDIRECT_URL"]) > 0)
			{
				?>
				<script type="text/javascript">
					window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';

				</script>
			<?
			die();
			}
			else
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
			}
			}
			else { ?>
				<script type="text/javascript">
					function submitForm(val) {
						jQuery('<div class="order-overlay" />').appendTo('#order_form_content');

						if(val != 'Y')
							BX('confirmorder').value = 'N';

						var orderForm = BX('ORDER_FORM');

						BX.ajax.submitComponentForm(orderForm, 'order_form_content', true);
						BX.submit(orderForm);

						return true;
					}

					function SetContact(profileId) {
						BX("profile_change").value = "Y";
						submitForm();
					}
				</script>
			<? if($_POST["is_ajax_post"] != "Y") { ?>
				<form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
					<?=bitrix_sessid_post()?>
					<div id="order_form_content">
						<?
						}
						else
						{
							$APPLICATION->RestartBuffer();
						}
						?>
						<div class="columns">
							<div class="col-9">
								<?
								if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
								{
									foreach($arResult["ERROR"] as $v)
										echo ShowError($v);

									?>
									<script type="text/javascript">
										top.BX.scrollToNode(top.BX('ORDER_FORM'));
									</script>
									<?
								}

								include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
								include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
								if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d")
								{
									include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
									include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
								}
								else
								{
									include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
									include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
								}

								include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");

								include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
								?>
							</div><!-- .span-9 -->
							<div class="col-3">
								<? include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary_bill.php"); ?>
							</div>
						</div><!-- .columns -->
					</div>

					<? if($_POST["is_ajax_post"] != "Y") { ?>
					<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
					<input type="hidden" name="profile_change" id="profile_change" value="N">
					<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
					<input type="hidden" name="order-bill-fixed-width" id="order-bill-fixed-width" value="0">
				</form>
			<? if($arParams["DELIVERY_NO_AJAX"] == "N") {
				$APPLICATION->AddHeadScript("/bitrix/js/main/cphttprequest.js");
				$APPLICATION->AddHeadScript("/bitrix/components/bitrix/sale.ajax.delivery.calculator/templates/.default/proceed.js");
			}
			}
			else { ?>
				<script type="text/javascript">
					top.BX('confirmorder').value = 'Y';
					top.BX('profile_change').value = 'N';
				</script>
				<?
				die();
			}
			}
			?>
			<script>
				jQuery(function($) {
					var $edostFormatDefault = $('.js-edostFormatDefault');
					if ($edostFormatDefault.length) {
						$edostFormatDefault.click();
						$('#order_form_content').children().css('opacity', 0);
					}

					var $wrap = $('#order_form_div'),
						$body = $('body'),
						$element = $('#order-bill'),
						$input = $('#order-bill-fixed-width'),
						top = $element.offset().top,
						showScroll = 0;
					$(document).scroll(function(e) {
						$element = $('#order-bill');
						var scroll = $body.scrollTop() + 100,
							isFixed = $element.hasClass('order-bill-fixed');

						if (top < scroll && !isFixed) {
							var width = $element.outerWidth();
							$element.outerWidth(width);
							$element.addClass('order-bill-fixed');
							$input.val(width);
						}
						else if (top >= scroll && isFixed) {
							$element.removeClass('order-bill-fixed');
							$element.outerWidth('auto');
							$input.val('N');
						}

						if (isFixed) {
							var wb = $wrap.offset().top + $wrap.outerHeight(),
								eb = $element.offset().top + $element.outerHeight();
							if (eb > wb) {
								showScroll = scroll;
								$element.hide();
							}
							else if (showScroll > scroll) {
								setTimeout(function(){
									showScroll = 0;
									$element.show();
								}, 250);
							}
						}
					});
				});
			</script>
		<? } ?>
	</div>
</div>