<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ORDER"]))
{
     UserEx::SetBuyerValues($arResult["ORDER"]["ID"]);
	?>
    <script>
        fbq('track', 'Purchase', {value: '<?=$arResult['ORDER']['PRICE']?>', currency: 'RUB'});
    </script>
	<div class="center" style="padding-top: 20px;">
		<h1>Ваш заказа № <?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?></h1>
		
		<p style="padding: 20px; margin-bottom: 20px; font-size: 20px; color: white; background-image: url(/images/line.png);background-position: center center; background-repeat: no-repeat;"><b>Принят и поступил в обработку</b></p>
		
		<p>

						- Мы проверим наличие и возможность пересылки всех позиций заказа. <br>
						- После этого сразу отправим на Ваш e-mail письмо с данными для оплаты. <br>
						- Пожалуйста не отключайте телефон, чтобы мы могли связаться с Вами для подтверждения заказа.
		</p>
		
		<h2>Вступайте в сообщество из 45000 кондитеров 
и пользуйтесь <a href="https://vk.com/club_craftology?w=page-39622201_52375335" target="_blank">справочником</a> бесплатно!</h2>
		<a href="https://vk.com/club_craftology?w=page-39622201_52375335" target="_blank"><img src="/images/SgoFOdaCBpc.jpg"></a>
		
		
	</div>
	<?
	
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>
