<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column
?>

<div class="bx_section">
	<div class="bx_section form-row">
		<h4>Дополнительная информация</h4>
	</div>
	<div class="clearfix">
		<div class="bx_section form-row">
			<? PrintPropFormByCode('COMMUNICATION', $arResult["ORDER_PROP"]["USER_PROPS_N"], $arResult["ORDER_PROP"]["USER_PROPS_Y"]); ?>
		</div>
	</div>
	<div class="clearfix">
		<div class="bx_section form-row">
			<? PrintPropFormByCode('DISCOUNT_CART', $arResult["ORDER_PROP"]["USER_PROPS_N"], $arResult["ORDER_PROP"]["USER_PROPS_Y"]); ?>
			<a href="/skidki/" target="_blank" style="font-size: 12px; float: right ;">Условия дисконтной программы</a>
		</div>
	</div>
</div>

<div class="bx_ordercart">
	
	<div class="bx_section form-row">
		<h4><?=GetMessage("SOA_TEMPL_SUM_COMMENTS")?></h4>
		<div class="bx_block w100"><textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" style="max-width:100%;min-height:120px"><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea></div>
		<input type="hidden" name="" value="">
		<div style="clear: both;"></div><br />
	</div>
</div>
<?
$delivID = $arResult['ORDER_DATA']['DELIVERY_ID'];
$arFreeDeliv = array(184,185,202,204);
if($arResult['ORDER_PRICE'] < 1000 && (array_search($delivID,$arFreeDeliv) === false)):?>
    <div class="cart__total">

        <p style="font-size: 18px;text-align: center">Минимальная сумма заказа: 1000<ruble><span class="text">руб.</span></ruble>.
            Добавьте товары на <?= (1000-$arResult['ORDER_PRICE'])?><ruble><span class="text">руб.</span></ruble>. <a style="text-decoration: underline" href="/catalog/katalog_konditera/">Перейти в каталог.</a></p>

    </div>
<?endif;?>
<div class="bx_section">
	<div class="order-bill-actions">

        <?

        if($arResult['ORDER_PRICE'] < 1000 && (array_search($delivID,$arFreeDeliv) === false)):?>
            <a href="javascript:void();" onClick="return false;" class="btn" style="cursor: default;background-color: #555555;">Отправить заказ</a>
        <?else:?>
            <a href="javascript:void();" onClick="submitForm('Y'); return false;" class="btn" >Отправить заказ</a>
        <?endif;?>



	</div>
</div>
