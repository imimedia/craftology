<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$orderBillFixed = isset($_REQUEST['order-bill-fixed-width']) && intval($_REQUEST['order-bill-fixed-width']) > 0;
?>
<div class="order-bill<?= $orderBillFixed ? ' order-bill-fixed' : '' ?>" id="order-bill"<?= $orderBillFixed ? ' style="width: '.intval($_REQUEST['order-bill-fixed-width']).'px"' : '' ?>>
	<table class="order-bill_table"><tbody>
		<tr>
			<td class="order-bill_col-1" colspan="<?=$colspan?>" class="itog"><?=GetMessage("SOA_TEMPL_SUM_WEIGHT_SUM")?></td>
			<td class="order-bill_col-2" class="price"><?=$arResult["ORDER_WEIGHT_FORMATED"]?></td>
		</tr>
		<tr>
			<td class="order-bill_col-1" colspan="<?=$colspan?>" class="itog"><?=GetMessage("SOA_TEMPL_SUM_SUMMARY")?></td>
			<td class="order-bill_col-2" class="price"><?=$arResult["ORDER_PRICE_FORMATED"]?></td>
		</tr>
		<? if (doubleval($arResult["DISCOUNT_PRICE"]) > 0) { ?>
			<tr>
				<td class="order-bill_col-1" colspan="<?=$colspan?>" class="itog"><?=GetMessage("SOA_TEMPL_SUM_DISCOUNT")?><?if (strLen($arResult["DISCOUNT_PERCENT_FORMATED"])>0):?> (<?echo $arResult["DISCOUNT_PERCENT_FORMATED"];?>)<?endif;?>:</td>
				<td class="order-bill_col-2" class="price"><?echo $arResult["DISCOUNT_PRICE_FORMATED"]?></td>
			</tr>
		<? }
		if (!empty($arResult["arTaxList"])) {
			foreach($arResult["arTaxList"] as $val) { ?>
				<tr>
					<td class="order-bill_col-1" colspan="<?=$colspan?>" class="itog"><?=$val["NAME"]?> <?=$val["VALUE_FORMATED"]?>:</td>
					<td class="order-bill_col-2" class="price"><?=$val["VALUE_MONEY_FORMATED"]?></td>
				</tr>
			<? }
		}
		if (doubleval($arResult["DELIVERY_PRICE"]) > 0) { ?>
			<tr>
				<td class="order-bill_col-1" colspan="<?=$colspan?>" class="itog"><?=GetMessage("SOA_TEMPL_SUM_DELIVERY")?></td>
				<td class="order-bill_col-2" class="price"><?=$arResult["DELIVERY_PRICE_FORMATED"]?></td>
			</tr>
		<? }
		if (strlen($arResult["PAYED_FROM_ACCOUNT_FORMATED"]) > 0) { ?>
			<tr>
				<td class="order-bill_col-1" colspan="<?=$colspan?>" class="itog"><?=GetMessage("SOA_TEMPL_SUM_PAYED")?></td>
				<td class="order-bill_col-2" class="price"><?=$arResult["PAYED_FROM_ACCOUNT_FORMATED"]?></td>
			</tr>
		<? } ?>
		</tbody>
	</table>

	<div class="order-bill-total">
		<div class="order-bill-total__title"><?=GetMessage("SOA_TEMPL_SUM_IT")?></div>
		<div class="order-bill-total__price"><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></div>
		<? if ($bUseDiscount) { ?>
			<div class="order-bill-total__price-without-discount"><?=$arResult["PRICE_WITHOUT_DISCOUNT"]?></div>
		<? } ?>
		<? if ($arResult["PAY_SYSTEM_PRICE"] <> 0) { ?>
			<div class="order-bill_col-1" data-toggle="tooltip1" title="Размер комиссии, уплачиваемой при получении заказа за услуги денежного перевода">
				Комиссии за перевод денег (при получении):<br>~ <?=$arResult["PAY_SYSTEM_PRICE_FORMATTED"]?>
			</div>
		<? } ?>
	</div>

	<div class="order-bill-actions">
        <?
        $delivID = $arResult['ORDER_DATA']['DELIVERY_ID'];
        $arFreeDeliv = array(184,185,202,204);
        if($arResult['ORDER_PRICE'] < 1000 && (array_search($delivID,$arFreeDeliv) === false)):?>
            <a href="javascript:void();" onClick="return false;" class="btn" style="cursor: default;background-color: #555555;">Отправить заказ</a>
        <?else:?>
            <a href="javascript:void();" onClick="submitForm('Y'); return false;" class="btn"> Отправить заказ</a>
        <?endif;?>
	</div>
    <?

    if($arResult['ORDER_PRICE'] < 1000 && (array_search($delivID,$arFreeDeliv) === false)):?>
        <div class="cart__total">

            <p style="font-size: 18px;">Минимальная сумма заказа: 1000<ruble><span class="text">руб.</span></ruble>.
                Добавьте товары на <?= (1000-$arResult['ORDER_PRICE'])?><ruble><span class="text">руб.</span></ruble>. <a style="text-decoration: underline" href="/catalog/katalog_konditera/">Перейти в каталог.</a></p>

        </div>
    <?endif;?>
</div>