<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

function GetLocation()
{
  $city=UserEx::getInstance()->getField('PERSONAL_CITY');

  if (isset($city) && !empty($city))
  {
    $rsLocation = CSaleLocation::GetList(
        array(),
        array("CITY_NAME" => $city),
        false,
        false,
        array("ID","CODE","COUNTRY_ID","REGION_ID")
    );

    if ($arLocation=$rsLocation->Fetch())  {
          return $arLocation;
    }
  }
  return array();
}

function GetLocationByID($ID)
{
  if (isset($ID) && !empty($ID))
  {
    $rsLocation = CSaleLocation::GetList(
        array(),
        array("ID" => $ID),
        false,
        false,
        array("ID","CODE","COUNTRY_ID","REGION_ID")
    );

    if ($arLocation=$rsLocation->Fetch())  {
          return $arLocation;
    }
  }
}

$loc=GetLocation();
if ($loc["CODE"])
{
  $arCity[]=$loc["CODE"];
  if ($loc["REGION_ID"])
  {
      $loc=GetLocationByID($loc["REGION_ID"]);
      $arCity[]=$loc["CODE"];
  }
  if ($loc["COUNTRY_ID"])
  {
      $loc=GetLocationByID($loc["COUNTRY_ID"]);
      $arCity[]=$loc["CODE"];
  }
}

$arResult['DELIVERY_GROUPS'] = array();
foreach ($arResult['DELIVERY'] as $delivery_id => $delivery) {

    $isRectriction=false;
    $rsDLoc=$DB->Query("select * from b_sale_delivery2location where delivery_id=".$DB->ForSQL($delivery_id));
    while ($arDLoc=$rsDLoc ->Fetch())
    {
        foreach ($arCity as $city)
        {
          if ($city==$arDLoc['LOCATION_CODE'])
          {
            $isRectriction=false;
            break;
          } else {
            $isRectriction=true;
          }
        }
    }

    if ($isRectriction) {
      unset($arResult['DELIVERY'][$delivery_id]);
      continue;
    }

    $arResult['DELIVERY'][$delivery_id]['PARENT_ID'] = 0;
    $arResult['DELIVERY'][$delivery_id]['PARENT_SORT'] = 0;

    $deliveryRow = $DB->Query("SELECT PARENT_ID, SORT FROM b_sale_delivery_srv WHERE ID={$delivery['ID']} ORDER BY SORT ASC")->Fetch();
    if ($deliveryRow && $deliveryRow['PARENT_ID']) {
        $parentId = intval($deliveryRow['PARENT_ID']);
        $groupRow = array();
        if ($parentId) {
            while (true) {
                $groupRow = $DB->Query("SELECT * FROM b_sale_delivery_srv WHERE ID={$parentId}")->Fetch();
                $parentId = $groupRow && intval($groupRow['PARENT_ID']) && $parentId != intval($groupRow['PARENT_ID']) ? intval($groupRow['PARENT_ID']) : 0;
                if (!$parentId) {
                    if (!array_key_exists($groupRow['ID'], $arResult['DELIVERY_GROUPS'])) {
                        $groupRow['CHECKED'] = 'N';
                        $arResult['DELIVERY_GROUPS'][$groupRow['ID']] = $groupRow;
                    }
                    $arResult['DELIVERY'][$delivery_id]['PARENT_ID'] = $groupRow['ID'];
                    $arResult['DELIVERY'][$delivery_id]['PARENT_SORT'] = $groupRow['SORT'];
                    break;
                }
            }
        }
    }

    if ($delivery_id !== 0 && intval($delivery_id) <= 0) {
        foreach ($delivery["PROFILES"] as $profile_id => $arProfile) {
            if ($arProfile["CHECKED"] == "Y")
                $arResult['DELIVERY_GROUPS'][$arResult['DELIVERY'][$delivery_id]['PARENT_ID']]['CHECKED'] = 'Y';
        }
    }
    else {
        if ($delivery["CHECKED"] == "Y")
            $arResult['DELIVERY_GROUPS'][$arResult['DELIVERY'][$delivery_id]['PARENT_ID']]['CHECKED'] = 'Y';
    }
}



$arResult['DELIVERY_GROUPS'][0]['ID'] = 0;
$arResult['DELIVERY_GROUPS'][0]['NAME'] = 'Согласовать с менеджером по телефону';

uasort($arResult['DELIVERY'], function ($a, $b){
    if ($a['PARENT_ID'] == 0 || $b['PARENT_ID'] == 0) {
        if ($a['PARENT_ID'] < $b['PARENT_ID'])
            return 1;
        if ($a['PARENT_ID'] > $b['PARENT_ID'])
            return -1;
    }

    if ($a['PARENT_ID'] > $b['PARENT_ID'])
        return 1;

    if ($a['PARENT_ID'] < $b['PARENT_ID'])
        return -1;

    if ($a['PARENT_ID'] == $b['PARENT_ID']) {
        if ($a['SORT'] == $b['SORT'])
            return 0;

        if ($a['SORT'] > $b['SORT'])
            return 1;

        if ($a['SORT'] < $b['SORT'])
            return -1;
    }
});