<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

    <script type="text/javascript">
        function fShowStore(id, showImages, formWidth, siteId) {
            var strUrl = '<?=$templateFolder?>' + '/map.php';
            var strUrlPost = 'delivery=' + id + '&showImages=' + showImages + '&siteId=' + siteId;

            var storeForm = new BX.CDialog({
                'title': '<?=GetMessage('SOA_ORDER_GIVE')?>',
                head: '',
                'content_url': strUrl,
                'content_post': strUrlPost,
                'width': formWidth,
                'height': 450,
                'resizable': false,
                'draggable': false
            });

            var button = [
                {
                    title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
                    id: 'crmOk',
                    'action': function () {
                        GetBuyerStore();
                        BX.WindowManager.Get().Close();
                    }
                },
                BX.CDialog.btnCancel
            ];
            storeForm.ClearButtons();
            storeForm.SetButtons(button);
            storeForm.Show();
        }

        function GetBuyerStore() {
            BX('BUYER_STORE').value = BX('POPUP_STORE_ID').value;
            //BX('ORDER_DESCRIPTION').value = '<?=GetMessage("SOA_ORDER_GIVE_TITLE")?>: '+BX('POPUP_STORE_NAME').value;
            BX('store_desc').innerHTML = BX('POPUP_STORE_NAME').value;
            BX.show(BX('select_store'));
        }

        function showExtraParamsDialog(deliveryId) {
            var strUrl = '<?=$templateFolder?>' + '/delivery_extra_params.php';
            var formName = 'extra_params_form';
            var strUrlPost = 'deliveryId=' + deliveryId + '&formName=' + formName;

            if (window.BX.SaleDeliveryExtraParams) {
                for (var i in window.BX.SaleDeliveryExtraParams) {
                    strUrlPost += '&' + encodeURI(i) + '=' + encodeURI(window.BX.SaleDeliveryExtraParams[i]);
                }
            }

            var paramsDialog = new BX.CDialog({
                'title': '<?=GetMessage('SOA_ORDER_DELIVERY_EXTRA_PARAMS')?>',
                head: '',
                'content_url': strUrl,
                'content_post': strUrlPost,
                'width': 500,
                'height': 200,
                'resizable': true,
                'draggable': false
            });

            var button = [
                {
                    title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
                    id: 'saleDeliveryExtraParamsOk',
                    'action': function () {
                        insertParamsToForm(deliveryId, formName);
                        BX.WindowManager.Get().Close();
                    }
                },
                BX.CDialog.btnCancel
            ];

            paramsDialog.ClearButtons();
            paramsDialog.SetButtons(button);
            //paramsDialog.adjustSizeEx();
            paramsDialog.Show();
        }

        function insertParamsToForm(deliveryId, paramsFormName) {
            var orderForm = BX("ORDER_FORM"),
                paramsForm = BX(paramsFormName);
            wrapDivId = deliveryId + "_extra_params";

            var wrapDiv = BX(wrapDivId);
            window.BX.SaleDeliveryExtraParams = {};

            if (wrapDiv)
                wrapDiv.parentNode.removeChild(wrapDiv);

            wrapDiv = BX.create('div', {props: {id: wrapDivId}});

            for (var i = paramsForm.elements.length - 1; i >= 0; i--) {
                var input = BX.create('input', {
                        props: {
                            type: 'hidden',
                            name: 'DELIVERY_EXTRA[' + deliveryId + '][' + paramsForm.elements[i].name + ']',
                            value: paramsForm.elements[i].value
                        }
                    }
                );

                window.BX.SaleDeliveryExtraParams[paramsForm.elements[i].name] = paramsForm.elements[i].value;

                wrapDiv.appendChild(input);
            }

            orderForm.appendChild(wrapDiv);

            BX.onCustomEvent('onSaleDeliveryGetExtraParams', [window.BX.SaleDeliveryExtraParams]);
        }
    </script>

    <input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?= $arResult["BUYER_STORE"] ?>"/>

<? if (!empty($arResult["DELIVERY"])) {
    $width = ($arParams["SHOW_STORES_IMAGES"] == "Y") ? 850 : 700;
    ?>
    <div class="section">
        <div class="bx_section">
            <h4><?= GetMessage("SOA_TEMPL_DELIVERY") ?></h4>
            <div class="columns">
                <div class="col-7">
                    <table class="order-delivery-list">
                        <tbody>
                        <?
                        $prevDeliveryGroupId = -1;
                        $deliveryGroupDescr = '';
                        $main_desc = "";

                        foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery) {
                            $deliveryGroup = $arResult['DELIVERY_GROUPS'][$arDelivery['PARENT_ID']];

                            if ($arDelivery["CHECKED"] == "Y")
                                $main_desc = trim($arDelivery["DESCRIPTION"]);

                            if (count($arDelivery["LOGOTIP"]) > 0) {
                                $arFileTmp = CFile::ResizeImageGet($arDelivery["LOGOTIP"]["ID"], array("width" => "95", "height" => "55"), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                                $image = $arFileTmp["src"];
                            } else {
                                $image = $templateFolder . "/images/logo-default-d.gif";
                            }

                            if ($delivery_id !== 0 && intval($delivery_id) <= 0) {
                                foreach ($arDelivery["PROFILES"] as $profile_id => $arProfile) {
                                    $extraParams = $arDelivery["ISNEEDEXTRAINFO"] == "Y" ? "showExtraParamsDialog('" . $delivery_id . ":" . $profile_id . "');" : '';
                                    ?>
                                    <tr>
                                        <td class="order-delivery__cell-control"><input
                                                    type="radio"
                                                    id="ID_DELIVERY_<?= $delivery_id ?>_<?= $profile_id ?>"
                                                    name="<?= htmlspecialcharsbx($arProfile["FIELD_NAME"]) ?>"
                                                    value="<?= $delivery_id . ":" . $profile_id; ?>"
                                                <?= $arProfile["CHECKED"] == "Y" ? "checked=\"checked\"" : ""; ?>
                                                    onclick="BX('ID_DELIVERY_<?= $delivery_id ?>_<?= $profile_id ?>').checked=true;<?= $extraParams ?>IPOLSDEK_pvz.selectPVZ('<?=$delivery_id?>','PVZ');"
                                            /></td>

                                        <td class="order-delivery__cell-image"><label
                                                    for="ID_DELIVERY_<?= $delivery_id ?>_<?= $profile_id ?>"><img
                                                        src="<?= $image ?>" alt=""/></label></td>
                                        <td class="order-delivery__cell-name"><label
                                                    for="ID_DELIVERY_<?= $delivery_id ?>_<?= $profile_id ?>"><?= htmlspecialcharsbx($arDelivery["TITLE"]) . " (" . htmlspecialcharsbx($arProfile["TITLE"]) . ")"; ?></label>
                                        </td>
                                        <td class="order-delivery__cell-period"></td>
                                        <td class="order-delivery__cell-price">
                                            <? if ($arProfile["CHECKED"] == "Y" && doubleval($arResult["DELIVERY_PRICE"]) > 0) { ?>
                                                <?= $arResult["DELIVERY_PRICE_FORMATED"] ?>
                                                <?
                                                if ((isset($arResult["PACKS_COUNT"]) && $arResult["PACKS_COUNT"]) > 1) {
                                                    echo '<br/>' . GetMessage('SALE_PACKS_COUNT') . ': <b>' . $arResult["PACKS_COUNT"] . '</b>';
                                                }
                                            } else {
                                                $APPLICATION->IncludeComponent('bitrix:sale.ajax.delivery.calculator', '', array(
                                                    "NO_AJAX" => $arParams["DELIVERY_NO_AJAX"],
                                                    "DELIVERY" => $delivery_id,
                                                    "PROFILE" => $profile_id,
                                                    "ORDER_WEIGHT" => $arResult["ORDER_WEIGHT"],
                                                    "ORDER_PRICE" => $arResult["ORDER_PRICE"],
                                                    "LOCATION_TO" => $arResult["USER_VALS"]["DELIVERY_LOCATION"],
                                                    "LOCATION_ZIP" => $arResult["USER_VALS"]["DELIVERY_LOCATION_ZIP"],
                                                    "CURRENCY" => $arResult["BASE_LANG_CURRENCY"],
                                                    "ITEMS" => $arResult["BASKET_ITEMS"],
                                                    "EXTRA_PARAMS_CALLBACK" => $extraParams
                                                ), null, array('HIDE_ICONS' => 'Y'));
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <? if (trim($arProfile["DESCRIPTION"]) || trim($arDelivery["DESCRIPTION"])) { ?>
                                        <tr data-delivery-group-parent="<?= $arDelivery['PARENT_ID'] ?>"<?= $deliveryGroup['CHECKED'] == 'Y' ? '' : ' style="display: none"' ?>>
                                            <td colspan="2"></td>
                                            <td colspan="3"
                                                class="order-delivery__cell-descr"><?= trim($arProfile["DESCRIPTION"]) ? nl2br($arProfile["DESCRIPTION"]) : nl2br($arDelivery["DESCRIPTION"]) ?></td>
                                        </tr>
                                    <? } ?>
                                    <?
                                }
                            } else {
                                $clickHandler = count($arDelivery["STORE"]) > 0
                                    ? $clickHandler = "onClick = \"fShowStore('" . $arDelivery["ID"] . "','" . $arParams["SHOW_STORES_IMAGES"] . "','" . $width . "','" . SITE_ID . "')\";"
                                    : $clickHandler = "onClick = \"BX('ID_DELIVERY_ID_" . $arDelivery["ID"] . "').checked=true;submitForm();\"";

                                $period = trim($arDelivery["PERIOD_TEXT"]);
                                $price = doubleval($arDelivery["PRICE"]) ? $arDelivery["PRICE_FORMATED"] : "Тариф ТК";
                                $descr = trim($arDelivery["DESCRIPTION"]);
                                ?>
                                <tr>
                                    <? if ($arDelivery['PARENT_ID'] > 0): ?>
                                        <td class="order-delivery__cell-control"><input type="radio"
                                                                                        id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
                                                                                        name="<?= htmlspecialcharsbx($arDelivery["FIELD_NAME"]) ?>"
                                                                                        value="<?= $arDelivery["ID"] ?>"
                                                <? if ($arDelivery["CHECKED"] == "Y") echo " checked"; ?>
                                                    <?if($delivery_id==198){?>                                                                                        onclick="IPOLSDEK_pvz.selectPVZ('<?=$delivery_id?>','PVZ');"
                                        <?}else{?> onclick="submitForm();"<?}?>
                                            /></td>
                                        <td class="order-delivery__cell-image"><label
                                                    for="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>" <?= $clickHandler ?>><img
                                                        src="<?= $image ?>" alt=""/></label></td>
                                        <td class="<?= (($arDelivery['PARENT_ID'] > 0) ? 'order-delivery__cell-name' : 'order-delivery-group__title') ?>">
                                            <label for="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>" <?= $clickHandler ?>><?= htmlspecialcharsbx($arDelivery["NAME"]) ?></label>
                                        </td>
                                        <td class="order-delivery__cell-period"><?= $period ?></td>
                                        <td class="order-delivery__cell-price"><?= $price ?></td>
                                        <?
                                    else: ?>
                                        <td colspan="5" class="order-delivery-group__title">
                                            <input type="radio"
                                                   id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
                                                   name="<?= htmlspecialcharsbx($arDelivery["FIELD_NAME"]) ?>"
                                                   value="<?= $arDelivery["ID"] ?>"
                                                <? if ($arDelivery["CHECKED"] == "Y") echo " checked"; ?>
                                                   onclick="submitForm();"
                                            />
                                            <label for="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>" <?= $clickHandler ?>><?= htmlspecialcharsbx($arDelivery["NAME"]) ?></label>
                                        </td>
                                    <? endif ?>
                                </tr>
                                <? /*if ($descr) { ?>
									<tr data-delivery-group-parent="<?= $arDelivery['PARENT_ID'] ?>"<?= $deliveryGroup['CHECKED'] == 'Y' ? '' : ' style="display: none"' ?>>
										<td colspan="2"></td>
										<td colspan="3" class="order-delivery__cell-descr"><?= $descr ?></td>
									</tr>
								<? } */ ?>
                                <?
                            }
                        } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-5">
                    <? if ($main_desc) { ?>
                        <div class="order-delivery__descr"><?= $main_desc ?></div>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        function IPOLSDEK_DeliveryChangeEvent(id) { //название принципиально
            $('#'+id).prop('checked', 'Y');
            submitForm();
        }

    </script>
<? } ?>