<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if ($arResult["PAY_SYSTEM"]) { ?>
<div class="section">
	<script type="text/javascript">
		function changePaySystem(param)
		{
			if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
			{
				if (param == 'account')
				{
					if (BX("PAY_CURRENT_ACCOUNT"))
					{
						BX("PAY_CURRENT_ACCOUNT").checked = true;
						BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
						BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

						// deselect all other
						var el = document.getElementsByName("PAY_SYSTEM_ID");
						for(var i=0; i<el.length; i++)
							el[i].checked = false;
					}
				}
				else
				{
					BX("PAY_CURRENT_ACCOUNT").checked = false;
					BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
					BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
				}
			}
			else if (BX("account_only") && BX("account_only").value == 'N')
			{
				if (param == 'account')
				{
					if (BX("PAY_CURRENT_ACCOUNT"))
					{
						BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

						if (BX("PAY_CURRENT_ACCOUNT").checked)
						{
							BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
							BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
						}
						else
						{
							BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
							BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
						}
					}
				}
			}

			submitForm();
		}
	</script>
	<div class="bx_section">
		<h4><?=GetMessage("SOA_TEMPL_PAY_SYSTEM")?></h4>
		<?
		uasort($arResult["PAY_SYSTEM"], "cmpBySort"); // resort arrays according to SORT value
		?>
		<div class="columns">
			<div class="col-7">
			<table class="order-paysystem-list">
				<tbody>
				<? foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
					$price = intval($arPaySystem["PRICE"]) > 0 ? SaleFormatCurrency(roundEx($arPaySystem["PRICE"], SALE_VALUE_PRECISION), $arResult["BASE_LANG_CURRENCY"]) : null;
					$image = !empty($arPaySystem["PSA_LOGOTIP"]["SRC"]) ? $arPaySystem["PSA_LOGOTIP"]["SRC"] : $templateFolder."/images/logo-default-ps.gif";

					$nameCols = 1;
					if (!$price)
						$nameCols++;
					?>
					<tr>
						<td class="order-paysystem__cell-control">

                        <input type="radio"
								   id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
								   name="PAY_SYSTEM_ID"
								   value="<?=$arPaySystem["ID"]?>"
								<?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
								   onclick="changePaySystem();" />
                         <label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"><?= $arPaySystem["PSA_NAME"] ?></label>
                        </td>
                        
                        <? if ($price) { ?><td class="order-paysystem__cell-price"><label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>">+ <?= $price ?></label></td><? } ?>

						<?/*<td<?= $nameCols > 1 ? ' colspan="'.$nameCols.'"' : '' ?> class="order-paysystem__cell-name">
                        <label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"><?= $arPaySystem["PSA_NAME"] ?></label>
                        </td>
						<? if ($price) { ?><td class="order-paysystem__cell-price"><label for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"><?= $price ?></label></td><? } */?>
					</tr>
				<? } ?>
				</tbody>
			</table>
			</div>
			<div class="col-5">
				<? foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
					$descr = trim(str_replace("<br />", "", $arPaySystem["DESCRIPTION"])) ? $arPaySystem["DESCRIPTION"] : null;
					if ($arPaySystem["CHECKED"]=="Y" && $descr) { ?>
						<div class="order-paysystem__descr"><?= $descr ?></div>
					<? }
				} ?>
			</div>
		</div>
	</div>
</div>
<? } ?>