<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arAuthServices = $arPost = array();
if (is_array($arParams["~AUTH_SERVICES"])) {
    $arAuthServices = $arParams["~AUTH_SERVICES"];
}
if (is_array($arParams["~POST"])) {
    $arPost = $arParams["~POST"];
}
?>
<? if ($arAuthServices) { ?>
    <div class="dialog_auth-social" style="display: none;">
        <div class="dialog-title">Вход через социальные сети</div>
        <div class="auth-social">
            <? foreach ($arAuthServices as $service) {
                $ar = App::socservOnclickToArray($service['ONCLICK']);
                ?>
                <a href="<?= $ar['url'] ?>" title="<?= $service['NAME'] ?>" class="auth-social_<?= $service['ICON'] ?> js-socialPopupBtn" data-width="<?= $ar['width'] ?>" data-height="<?= $ar['height'] ?>"></a>
            <? } ?>
        </div>
    </div>
<? } ?>
