<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<? if ($arResult["NavPageCount"] > 1) {
    $arParams["COUNT_EDGE"] = 0;
    $arParams["COUNT_CENTER"] = 7;
    $countEdge = !empty($arParams["COUNT_EDGE"]) && intval($arParams["COUNT_EDGE"]) > 0 ? intval($arParams["COUNT_EDGE"]) : 1;
    $countCenter = !empty($arParams["COUNT_CENTER"]) && intval($arParams["COUNT_CENTER"]) >= 3 ? intval($arParams["COUNT_CENTER"]) : 3;
    $countCenter = $countCenter % 2 ? $countCenter : $countCenter - 1;
    $countCenterHalf = $countCenter % 2 ? ($countCenter - 1) / 2 : $countCenter / 2;

    $pages = $arResult["NavPageCount"];
    $num = $arResult["NavPageNomer"];

    $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
    $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
    $strNavQueryString = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=";
    ?>
    <div class="pagination">
        <? if ($num != 1) { ?>
            <a class="prev" href="<?= $strNavQueryString.($num-1)?>"></a>
        <? } ?>

        <?
        $nextPage = 1;
        if ( ($pages > $countEdge * 2 + $countCenter) && ($num > $countEdge + 2 + $countCenterHalf) ) {
            $nextPage = $countEdge + 1;
            ?>
        <? } ?>

        <?
        if ($nextPage == 1)
            $start = 1;
        else if ($num > $pages - $countEdg - $countCenter - 1)
            $start = $pages - $countEdge - $countCenter;
        else
            $start = $num - $countCenterHalf;

        $nextPage = $start + $countCenter;

        if ($pages <= ($num + $countCenterHalf + $countEdge + 1))
            $end = $pages;
        else if ($num <= $countEdge + $countCenter - $countCenterHalf)
            $end = $countEdge + 1 + $countCenter;
        else
            $end = $num + $countCenterHalf;

        for ($i = $start; $i <= $end; $i++) {
            if ($i == $arResult["NavPageNomer"]) { ?>
                <span class="active"><?= $i ?></span>
            <? } else { ?>
                <a class="" href="<?= $strNavQueryString.$i ?>"><?= $i ?></a>
            <? }
        } ?>

        <? if ($num != $pages) { ?>
            <a class="next" href="<?= $strNavQueryString.($arResult["NavPageNomer"]+1)?>"></a>
        <? } ?>
    </div>
<? } ?>