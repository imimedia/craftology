<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    'IS_AJAX' => array(
        'NAME' => 'Запрос через Ajax',
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N',
    ),
    'FORM_ID' => array(
        'NAME' => 'ID формы',
        'TYPE' => 'STRING',
    ),
    'SENT_FORM_ID' => array(
        'NAME' => 'Отправленная ID формы',
        'TYPE' => 'STRING',
    )
);