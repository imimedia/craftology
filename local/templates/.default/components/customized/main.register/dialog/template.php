<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$formId = isset($arParams['FORM_ID']) ? $arParams['FORM_ID'] : '';
$isAjax = isset($arParams['IS_AJAX']) && $arParams['IS_AJAX'] == 'Y';
$sentFormId = isset($arParams['SENT_FORM_ID']) ? $arParams['SENT_FORM_ID'] : '';
if ($isAjax && $formId && $sentFormId == $formId) {
	$APPLICATION->RestartBuffer();

	$data = array();
	if (!$arResult['ERRORS']) {
		$data['success'] = true;
		$data['reloadPage'] = true;
	}
	else {
		foreach ($arResult["ERRORS"] as $key => $error)
			if (intval($key) == 0 && $key !== 0)
				$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

		$data['success'] = false;
		$data['error'] = implode('<br/>', $arResult['ERRORS']);
	}

	echo json_encode($data);
	die();
}
?>
<div class="dialog_auth">
	<form method="post" action="/local/api/signup/" name="regform" enctype="multipart/form-data" class="js-form">
		<div class="dialog-title">Регистрация</div>
		<div class="js-form-msg"></div>
		<div class="form-row"><input name="REGISTER[NAME]" value="<?=$arResult["VALUES"]['NAME']?>" type="text" placeholder="ФИО" /></div>
		<div class="form-row"><input name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]['EMAIL']?>" type="text" placeholder="Эл. почта" /></div>
		<div class="form-row"><input name="REGISTER[PERSONAL_PHONE]" value="<?=$arResult["VALUES"]['EMAIL']?>" type="tel" placeholder="Телефон" class="js-mask" data-pattern="phone"/></div>
		<div class="form-row"><input name="REGISTER[PASSWORD]" value="" type="password" placeholder="Пароль" autocomplete="off" /></div>
		<div class="form-row"><input name="REGISTER[CONFIRM_PASSWORD]" type="password" placeholder="Пароль ещё раз" autocomplete="off" /></div>
		<div class="form-registration__noconfirm-label">Подтверждение электронной почты не потребуется</div>
		<div class="form-actions center">
			<div class="terms-of-service"><input type="hidden" name="TermsOfService" value="N" /><input type="checkbox" name="TermsOfService" id="TermsOfService" value="Y" /> <label for="TermsOfService">Я согласен с <a href="">правилами ресурса</a></label></div>
			<button type="submit" name="register_submit_button" value="1" class="btn">Регистрация</button>
		</div>
		<input type="hidden" name="FORM_ID" value="<?= $formId ?>">
		<input type="hidden" name="save" value="1">
		<input type="hidden" name="register_submit_button" value="1">
	</form>
</div>
<? if ($arResult["AUTH_SERVICES"]) {
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
		array(
			"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
			"AUTH_URL"=>$arResult["AUTH_URL"],
			"POST"=>$arResult["POST"],
			"POPUP"=>"Y",
			"SUFFIX"=>"form",
		),
		$component,
		array("HIDE_ICONS"=>"Y")
	);
} ?>
<div class="open-other-dialogs">
	<div class="justify-items">
		<span class="justify-item"></span>
		<a href="" class="open-forgot justify-item js-dialog" data-dialog="forgotpasswd">Забыли пароль</a>
		<a href="" class="open-register justify-item btn js-dialog" data-dialog="auth">Войти</a>
		<span class="justify-item"></span>
	</div>
</div>