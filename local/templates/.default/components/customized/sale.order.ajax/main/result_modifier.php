<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$arResult['DELIVERY_GROUPS'] = array();
foreach ($arResult['DELIVERY'] as $delivery_id => $delivery) {
    $arResult['DELIVERY'][$delivery_id]['PARENT_ID'] = 0;
    $arResult['DELIVERY'][$delivery_id]['PARENT_SORT'] = 0;

    $deliveryRow = $DB->Query("SELECT PARENT_ID, SORT FROM b_sale_delivery_srv WHERE ID={$delivery['ID']} ORDER BY SORT ASC")->Fetch();
    if ($deliveryRow && $deliveryRow['PARENT_ID']) {
        $parentId = intval($deliveryRow['PARENT_ID']);
        $groupRow = array();
        if ($parentId) {
            while (true) {
                $groupRow = $DB->Query("SELECT * FROM b_sale_delivery_srv WHERE ID={$parentId}")->Fetch();
                $parentId = $groupRow && intval($groupRow['PARENT_ID']) && $parentId != intval($groupRow['PARENT_ID']) ? intval($groupRow['PARENT_ID']) : 0;
                if (!$parentId) {
                    if (!array_key_exists($groupRow['ID'], $arResult['DELIVERY_GROUPS'])) {
                        $groupRow['CHECKED'] = 'N';
                        $arResult['DELIVERY_GROUPS'][$groupRow['ID']] = $groupRow;
                    }
                    $arResult['DELIVERY'][$delivery_id]['PARENT_ID'] = $groupRow['ID'];
                    $arResult['DELIVERY'][$delivery_id]['PARENT_SORT'] = $groupRow['SORT'];
                    break;
                }
            }
        }
    }

    if ($delivery_id !== 0 && intval($delivery_id) <= 0) {
        foreach ($delivery["PROFILES"] as $profile_id => $arProfile) {
            if ($arProfile["CHECKED"] == "Y")
                $arResult['DELIVERY_GROUPS'][$arResult['DELIVERY'][$delivery_id]['PARENT_ID']]['CHECKED'] = 'Y';
        }
    }
    else {
        if ($delivery["CHECKED"] == "Y")
            $arResult['DELIVERY_GROUPS'][$arResult['DELIVERY'][$delivery_id]['PARENT_ID']]['CHECKED'] = 'Y';
    }
}



$arResult['DELIVERY_GROUPS'][0]['ID'] = 0;
$arResult['DELIVERY_GROUPS'][0]['NAME'] = 'Согласовать с менеджером по телефону';

uasort($arResult['DELIVERY'], function ($a, $b){
    if ($a['PARENT_ID'] == 0 || $b['PARENT_ID'] == 0) {
        if ($a['PARENT_ID'] < $b['PARENT_ID'])
            return 1;
        if ($a['PARENT_ID'] > $b['PARENT_ID'])
            return -1;
    }

    if ($a['PARENT_ID'] > $b['PARENT_ID'])
        return 1;

    if ($a['PARENT_ID'] < $b['PARENT_ID'])
        return -1;

    if ($a['PARENT_ID'] == $b['PARENT_ID']) {
        if ($a['SORT'] == $b['SORT'])
            return 0;

        if ($a['SORT'] > $b['SORT'])
            return 1;

        if ($a['SORT'] < $b['SORT'])
            return -1;
    }
});