<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
Bitrix\Main\Loader::includeModule('iblock');

if (!empty($arResult["SEARCH"])) { ?>
	<div class="title-search-result">
		<? foreach ($arResult["SEARCH"] as $i => $arItem) {
			$arElement = CIBlockElement::GetList(
				array(),
				array('IBLOCK_ID' => $arItem['PARAM2'], 'ID' => $arItem['ITEM_ID']),
				false,
				false,
				array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PICTURE', 'CATALOG_GROUP_'.CATALOG_PRICE)
			)->Fetch();

			if ($arElement) {
				$image = array();
				if ($arElement['DETAIL_PICTURE'])
					$image = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array("width" => 50, "height" => 50), BX_RESIZE_IMAGE_PROPORTIONAL, true);

				$price = CCurrencyLang::CurrencyFormat($arElement['CATALOG_PRICE_'.CATALOG_PRICE], $arElement['CATALOG_CURRENCY_'.CATALOG_PRICE]);
				?>
				<div class="title-search-item">
					<? if ($image) { ?><img src="<?= $image['src'] ?>" alt="" /><? } ?>
					<a href="<?= $arItem["URL" ]?>"><?= $arElement["NAME"] ?></a> - <span class="nowrap"><?= $price ?></span>
				</div>
				<?
			}
		} ?>
		<div class="title-search-all"><a href="<?= $arParams['PAGE'] ?>?q=<?= $arResult['query'] ?>">Все результаты</div>
	</div>
<? } ?>