<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?
$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
	$INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
	$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

if ($arParams["SHOW_INPUT"] !== "N") { ?>
	<div class="search js-searchTitleWrapper" id="<?echo $CONTAINER_ID?>">
		<form action="<?= $arResult["FORM_ACTION"]?>" method="get">
			<input id="<?= $INPUT_ID?>" type="text" name="q" value="" class="js-searchTitle" size="40" maxlength="50" autocomplete="off" placeholder="Поиск по каталогу товаров" />
			<button name="s" value="1" type="submit">Поиск</button>
		</form>
		
		<?// флаг "товары в наличии", проверка в catalog/index.php?>
        <form method="POST" action="<?$APPLICATION->GetCurPage()?>" name="orders_filter">
            <div class="access_check fl" data-toggle="tooltip1" title="Будут отображатся товары которые есть в наличии">
                <input value="" name="namber_order"  onclick="filter_click();" type="checkbox" id="cat_for_access" class="fl" <?=($_COOKIE["namber_order"]=='null')?'':'checked'?>>
                <label for="cat_for_access">Показать товары только в наличии</label>
            </div>
        </form>
		
	</div>
<? } ?>

<script>
// после загрузки страницы
$(function () {
  // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
  $('[data-toggle="tooltip1"]').tooltip()
})
</script>
