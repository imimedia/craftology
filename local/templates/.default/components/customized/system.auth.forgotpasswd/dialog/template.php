<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$formId = isset($arParams['FORM_ID']) ? $arParams['FORM_ID'] : '';
$isAjax = isset($arParams['IS_AJAX']) && $arParams['IS_AJAX'] == 'Y';
$sentFormId = isset($arParams['SENT_FORM_ID']) ? $arParams['SENT_FORM_ID'] : '';
if ($isAjax && $formId && $sentFormId == $formId && !empty($arResult['JSON'])) {
    $APPLICATION->RestartBuffer();

    $data = array();
    if ($arResult['JSON']['success']) {
        $data['success'] = true;
        $data['content'] = array(
            'forgotPasswordForm' => '<br/>'.$arResult['JSON']['message']
        );
    }
    else {
        $data['success'] = false;
        $data['error'] = $arResult['JSON']['error'];
    }

    echo json_encode($data);
    die();
}
?>
<div class="dialog_auth">
    <form name="bform" method="post" target="_top" action="/local/api/forgotpasswd/" class="js-form" id="forgotPasswordForm">
        <div class="dialog-title">Забыли пароль</div>
        <div class="dialog-descr">В форму ниже введите свой электронный адрес, указанный при регистрации, и через несколько минут на Ваш E-mail придет письмо с паролем</div>
        <div class="js-form-msg"></div>
        <div class="form-row"><input name="EMAIL" value="" type="text" placeholder="Эл. почта"/></div>
        <div class="form-actions">
            <div class="justify-items">
                <span class="justify-item"></span>
                <button class="btn justify-item js-dialogCloseBtn">Отмена</button>
                <button type="submit" name="send_account_info" value="1" class="btn justify-item">Восстановить</button>
                <span class="justify-item"></span>
            </div>
        </div>
        <input type="hidden" name="AUTH_FORM" value="Y">
        <input type="hidden" name="TYPE" value="SEND_PWD">
        <input type="hidden" name="FORM_ID" value="<?= $formId ?>">
        <input type="hidden" name="forgotpasswd" value="y">
    </form>
</div>
<? if ($arResult["AUTH_SERVICES"]) {
    $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
        array(
            "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
            "AUTH_URL"=>$arResult["AUTH_URL"],
            "POST"=>$arResult["POST"],
            "POPUP"=>"Y",
            "SUFFIX"=>"form",
        ),
        $component,
        array("HIDE_ICONS"=>"Y")
    );
} ?>
<div class="open-other-dialogs">
    <div class="justify-items">
        <span class="justify-item"></span>
        <a href="" class="open-forgot justify-item js-dialog" data-dialog="auth">Войти</a>
        <a href="" class="open-register justify-item btn js-dialog" data-dialog="registration">Регистрация</a>
        <span class="justify-item"></span>
    </div>
</div>