<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$formId = isset($arParams['FORM_ID']) ? $arParams['FORM_ID'] : '';
$isAjax = isset($arParams['IS_AJAX']) && $arParams['IS_AJAX'] == 'Y';
$sentFormId = isset($arParams['SENT_FORM_ID']) ? $arParams['SENT_FORM_ID'] : '';
if ($isAjax && $formId && $sentFormId == $formId) {
    $APPLICATION->RestartBuffer();

    $data = array();
    if (!$arResult['ERROR']) {
        $data['success'] = true;
        $data['reloadPage'] = true;
    }
    else {
        $data['success'] = false;
        $data['error'] = $arResult['ERROR_MESSAGE']['MESSAGE'];
    }

    echo json_encode($data);
    die();
}
?>
<div class="dialog_auth">
    <form method="post" action="/local/api/signin/" class="js-form">
        <div class="dialog-title">Вход</div>
        <div class="js-form-msg"></div>
        <div class="form-row"><input name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>" id="login" type="text" placeholder="Эл. почта" /></div>
        <div class="form-row"><input name="USER_PASSWORD" id="password" type="password" placeholder="Пароль" autocomplete="off" /></div>
        <div class="form-actions">
            <div class="justify-items">
                <span class="justify-item"></span>
                <? if ($arResult["STORE_PASSWORD"] == "Y") { ?>
                    <span class="remember-me justify-item"><input name="USER_REMEMBER" value="Y" type="checkbox" id="rememberMe" checked="checked"> <label for="rememberMe">Запомнить меня</label></span>
                <? } ?>
                <button type="submit" name="Login" value="1" class="btn justify-item">Войти</button>
                <span class="justify-item"></span>
            </div>
        </div>
        <div class="form-row" style="text-align: center;">Если у Вас не получается войти под учетной записью старого сайта - просьба зарегистрироваться заново. <br>Все скидки при этом за Вами сохранятся.</div>
        <input type="hidden" name="AUTH_FORM" value="Y" />
        <input type="hidden" name="TYPE" value="AUTH" />
        <input type="hidden" name="FORM_ID" value="<?= $formId ?>">
        <input type="hidden" name="Login" value="1" />
    </form>
</div>
<? if ($arResult["AUTH_SERVICES"]) {
    $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
        array(
            "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
            "AUTH_URL"=>$arResult["AUTH_URL"],
            "POST"=>$arResult["POST"],
            "POPUP"=>"Y",
            "SUFFIX"=>"form",
        ),
        $component,
        array("HIDE_ICONS"=>"Y")
    );
} ?>
<div class="open-other-dialogs">
    <div class="justify-items">
        <span class="justify-item"></span>
        <a href="" class="open-forgot justify-item js-dialog" data-dialog="forgotpasswd">Забыли пароль</a>
        <a href="" class="open-register justify-item btn js-dialog" data-dialog="registration">Регистрация</a>
        <span class="justify-item"></span>
    </div>
</div>