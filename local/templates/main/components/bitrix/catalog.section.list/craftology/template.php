<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="catalog-categories">
	<? foreach ($arResult['SECTIONS'] as $arSection) {
		$image = array();
		if ($arSection['PICTURE']) {
			$image = CFile::ResizeImageGet($arSection['~PICTURE'], array("width" => 200, "height" => 200), BX_RESIZE_IMAGE_EXACT, true);
		}
		?>
	<div class="catalog-categories__item">
		<div class="catalog-categories__image"><a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><? if ($image) { ?><img src="<?= $image['src'] ?>" alt="" /><? } ?></a></div>
		<div class="catalog-categories__name"><a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME'] ?></a></div>
	</div>
	<? } ?>
</div>
