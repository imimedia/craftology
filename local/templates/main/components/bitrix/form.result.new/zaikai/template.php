<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$isPost = !empty($_REQUEST['web_form_submit']);
$isAjax = !empty($_REQUEST['ajax']) && $_REQUEST['ajax'] == 'y' && $_REQUEST['WEB_FORM_ID'] == $arResult['arForm']['ID'];
?>
<div class="feedback js-formWrap">
    <? if ($isAjax) $APPLICATION->RestartBuffer(); ?>
    <? if ($arResult['isFormNote'] == 'Y') { ?>
        <div style="padding: 50px 0; text-align: center;"><?= !empty($arParams['SUCCESS_MESSAGE']) ? $arParams['SUCCESS_MESSAGE'] : $arResult["FORM_NOTE"]; ?></div>
    <? } else { ?>
        <div class="feedback__title h1">Заказ товара</div>
        <? if (trim($arResult['arForm']['DESCRIPTION'])) { ?><p><?= $arResult['arForm']['DESCRIPTION']; ?></p><? } ?>

        <form method="post" name="<?= $arResult["arForm"]["SID"] ?>" action="<?= POST_FORM_ACTION_URI ?>" id="<?= $arResult["arForm"]["SID"] ?>" enctype="multipart/form-data" class="js-formHtml">
            <?= bitrix_sessid_post() ?>
            <input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>" />

            <?
            foreach ($arResult['QUESTIONS'] as $FIELD_SID => $arQuestion) {
                $fieldName = 'form_'.$arQuestion['STRUCTURE'][0]['FIELD_TYPE'].'_'.$arQuestion['STRUCTURE'][0]['ID'];
                $fieldTitle = $arQuestion['STRUCTURE'][0]['MESSAGE'];
                $fieldId = "form{$arParams['WEB_FORM_ID']}_{$FIELD_SID}";

                $val = isset($arResult['arrVALUES'][$fieldName]) ? $arResult['arrVALUES'][$fieldName] : '';

                $isRequired = $arQuestion['REQUIRED'] == 'Y';
                $attrRequired = $isRequired ? ' required' : '';
                $requiredLabel = $isRequired ? ' *' : '';

                $hasError       = !empty($arResult["FORM_ERRORS"][$FIELD_SID]);
                $errStyle       = $hasError ? 'error' : '';
                $errStyleCls    = $hasError ? ' class="'.trim($errStyle).'"' : '';
                $errMsg         = $hasError ? '<p id="'.$fieldName.'-error" class="error-form">'.$arResult["FORM_ERRORS"][$FIELD_SID].'</p>' : '';
                ?>
                <?
                if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
                    echo $arQuestion["HTML_CODE"];
                }
                else if ($FIELD_SID == 'name') {
                    if ($USER->IsAuthorized() && !$isPost)
                        $val = $USER->GetFullName();
                    ?>
                    <dl class="form-row">
                        <dt><label for="<?= $fieldId ?>"><?= $fieldTitle ?><?= $requiredLabel ?></label></dt>
                        <dd><input type="text" id="<?= $fieldId ?>" name="<?= $fieldName ?>" value="<?= htmlspecialcharsEx($val) ?>"<?= $errStyleCls ?> placeholder="Укажите Имя и Фамилию" /></dd>
                    </dl>
                <? }
                else if ($FIELD_SID == 'contact') { ?>
                    <dl class="form-row">
                        <dt><label for="<?= $fieldId ?>"><?= $fieldTitle ?><?= $requiredLabel ?></label></dt>
                        <dd><input type="text" id="<?= $fieldId ?>" name="<?= $fieldName ?>" value="<?= htmlspecialcharsEx($val) ?>"<?= $errStyleCls ?> placeholder="Укажите как с Вами можно связаться" /></dd>
                    </dl>
                <? }
                if ($FIELD_SID == 'theme') {
                    if (!$val)
                        $val = isset($arParams['THEME']) ? htmlspecialcharsback(trim($arParams['THEME'])) : '';
                    if (!$val)
                        continue;
                    ?>
                    <dl class="form-row">
                        <dt><label for="<?= $fieldId ?>"><?= $fieldTitle ?><?= $requiredLabel ?></label></dt>
                        <dd><input type="text" id="<?= $fieldId ?>" name="<?= $fieldName ?>" value="<?= htmlspecialcharsEx($val) ?>"<?= $errStyleCls ?> placeholder="Укажите тему вопроса" /></dd>
                    </dl>
                <?
                $a = 4;
                }
                else if ($FIELD_SID == 'message') { ?>
                    <dl class="form-row">
                        <dt><label for="<?= $fieldId ?>"><?= $fieldTitle ?><?= $requiredLabel ?></label></dt>
                        <dd><textarea id="<?= $fieldId ?>" name="<?= $fieldName ?>" rows="5"<?= $errStyleCls ?> placeholder="Тут можно написать сообщение для нас"><?= htmlspecialcharsEx($val) ?></textarea></dd>
                    </dl>
                <? }
                else if ($FIELD_SID == 'file') {
                    $fileHtml = '<label class="input-file-styled"><input type="file" name="'.$fieldName.'" class="js-fileUpload"><span class="theme-link js-fileUploadLabel">Выберите файл</span>'.$errMsg.'</label>';
                } ?>
            <? } ?>

            <div class="form-actions">
                <?= isset($fileHtml) ? $fileHtml : '' ?>
                <button type="submit" name="web_form_submit" value="1" class="btn btn-b"><?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?></button>
                <input type="hidden" name="web_form_submit" value="1" />
            </div>
        </form>
        <script>
            initForm();
            initFileUpload();
        </script>
    <? } ?>
    <? if ($isAjax) die(); ?>
</div>
