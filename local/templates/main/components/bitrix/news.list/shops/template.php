<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

    ?>

    <div class="d-flex flex-row align-items-center our-shop__item">
        <div class="our-shop__img">
            <div class="d-flex flex-row justify-content-between">
                <div>
                    <?
                    $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width'=>510, 'height'=>340), BX_RESIZE_IMAGE_PROPORTIONAL, true);

                    ?>
                    <img src="<?=$file['src']?>">
                </div>
                <div class="d-flex flex-column justify-content-between">
                <?foreach($arItem["PROPERTIES"]['MORE_PHOTO']['VALUE'] as $img):?>
                    <?
                    $file = CFile::ResizeImageGet($img, array('width'=>194, 'height'=>110), BX_RESIZE_IMAGE_PROPORTIONAL, true);

                    ?>
                    <div>
                        <img src="<?=$file['src']?>">
                    </div>
                <?endforeach;?>
                </div>
            </div>
            <!-- <img src="img-1-all.jpg"> -->
        </div>
        <div>
            <div class="our-shop__contact">
                <div class="media">
                    <div class="media-left">
                        <div class="svg-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70.87 70.87">
                                <defs>
                                    <style>
                                        .icon-city__cls-2 {
                                            fill: #fff
                                        }
                                    </style>
                                </defs>
                                <g>
                                    <g>
                                        <circle cx="35.43" cy="35.43" r="35.43" transform="rotate(-9.25 35.453 35.467)" fill="#ec1a63" />
                                        <path class="icon-city__cls-2" d="M34 4.31A31.64 31.64 0 0 1 52.75 9.4a31.14 31.14 0 0 1-16.87 57.18A31.13 31.13 0 0 1 7.3 21.42 31.49 31.49 0 0 1 34 4.31zm.74 4a27.62 27.62 0 0 0-19.92 9.32 26.85 26.85 0 0 0 0 35.67 27.53 27.53 0 0 0 45.71-6.94 26.81 26.81 0 0 0 1.65-16.87 27.23 27.23 0 0 0-10.59-15.94A27.57 27.57 0 0 0 34.7 8.31z"
                                        />
                                        <path class="icon-city__cls-2" d="M31.4 18.86h17.79a1 1 0 0 1 .87 1v30.15a10.37 10.37 0 0 1 1.25.05 1 1 0 0 1 .69.76v.34a1 1 0 0 1-.8.8H19.67a1 1 0 0 1-.81-.82v-.25a1 1 0 0 1 .75-.84c.4-.06.81 0 1.21 0V29.54a2 2 0 0 1 .12-.91 1 1 0 0 1 .91-.5h8.72v-8.3a1 1 0 0 1 .83-1zm1.11 1.95v29.25h2.94v-6.84a1 1 0 0 1 1-.94h7.83a1 1 0 0 1 1 .94v6.86h2.92V20.84zm-9.74 9.27v20h7.79v-20zm14.62 14.13v5.85h2v-5.84zm3.89 5.85h2v-5.85h-1.99c-.01 1.95 0 3.9-.01 5.85z"
                                        />
                                        <path class="icon-city__cls-2" d="M35.13 23.3a2.27 2.27 0 0 1 .69-.05h2.52a1 1 0 1 1 0 1.95h-3a1 1 0 0 1-.24-1.9zM41.93 23.31a1.13 1.13 0 0 1 .36-.06h3a1 1 0 0 1-.06 1.94h-3a1 1 0 0 1-.29-1.89zM35.32 28.14h3a1 1 0 0 1 .81.41 1 1 0 0 1 0 1.15 1 1 0 0 1-.79.38h-2.91a1 1 0 0 1-.93-.73 1 1 0 0 1 .82-1.21zM42.11 28.14a8.88 8.88 0 0 1 .89 0h2.32a1 1 0 0 1 0 1.92h-3.11a1 1 0 0 1-.12-1.93zM42.06 33h3.15a1 1 0 0 1 0 2h-3a1 1 0 0 1-.15-2zM24.33 33.08A1.44 1.44 0 0 1 25 33h3.69a1 1 0 0 1 0 2h-3.94a1 1 0 0 1-.42-1.87zM35.07 33.07a1.51 1.51 0 0 1 .56-.07h2.78a1 1 0 0 1-.06 2h-2.92a1 1 0 0 1-.92-.67 1 1 0 0 1 .56-1.26zM24.4 37.93a1.32 1.32 0 0 1 .49-.06h3.81a1 1 0 0 1-.06 1.94h-3.95a1 1 0 0 1-.29-1.89zM35.19 37.91a2.85 2.85 0 0 1 .51 0h2.64a1 1 0 1 1 0 2h-2.91a1 1 0 0 1-.24-1.91zM41.93 37.93a1.07 1.07 0 0 1 .3-.06h3a1 1 0 0 1 .88 1.18 1 1 0 0 1-.93.76h-3a1 1 0 0 1-.3-1.89zM24.51 42.77H28.19a1.63 1.63 0 0 1 1 .21 1 1 0 0 1 .16 1.39 1 1 0 0 1-.86.36h-3.8a1 1 0 0 1-.18-1.93z"
                                        />
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class="media-body media-middle">
                        Город: <?=$arItem["PROPERTIES"]['CITY']['VALUE']?>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <div class="svg-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70.87 70.87">
                                <defs>
                                    <style>
                                        .icon-phone__cls-2 {
                                            fill: #fff
                                        }
                                    </style>
                                </defs>
                                <g>
                                    <g>
                                        <circle cx="35.43" cy="35.43" r="35.43" transform="rotate(-9.22 35.401 35.434)" fill="#ec1a63" />
                                        <path class="icon-phone__cls-2" d="M29.87 21.31a2 2 0 0 1 1.43.2 6.09 6.09 0 0 1 2.06 2.13 7.78 7.78 0 0 1 .9 1.94 2.45 2.45 0 0 1 .09 1.14 5.29 5.29 0 0 1-1.06 2.15 8.89 8.89 0 0 1-1.68 1.5 4 4 0 0 0-1.37 1.42 2.78 2.78 0 0 0-.1 1.8 10.39 10.39 0 0 0 .9 2.34 23.89 23.89 0 0 0 2.6 4.07 7.88 7.88 0 0 0 1.73 1.68 2.43 2.43 0 0 0 1.5.42 5.26 5.26 0 0 0 1.85-.69 6.89 6.89 0 0 1 2.37-.82 5.57 5.57 0 0 1 1.91.11 2.24 2.24 0 0 1 1 .56 7.38 7.38 0 0 1 1.25 1.48 7.2 7.2 0 0 1 1.07 2.46 2.27 2.27 0 0 1-.21 1.64 6.21 6.21 0 0 1-3 2.3 6.85 6.85 0 0 1-4.64.1 13.6 13.6 0 0 1-5.22-3.41 33.18 33.18 0 0 1-5.2-6.83 31.36 31.36 0 0 1-2.5-5.27 14.26 14.26 0 0 1-.93-6.49A6.94 6.94 0 0 1 26.77 23a6.52 6.52 0 0 1 3.1-1.69z"
                                        />
                                        <path class="icon-phone__cls-2" d="M34 4.31A31.64 31.64 0 0 1 52.75 9.4a31.14 31.14 0 0 1-16.87 57.18A31.13 31.13 0 0 1 7.3 21.42 31.49 31.49 0 0 1 34 4.31zm.74 4a27.62 27.62 0 0 0-19.92 9.32 26.85 26.85 0 0 0 0 35.67 27.53 27.53 0 0 0 45.71-6.94 26.81 26.81 0 0 0 1.65-16.87 27.23 27.23 0 0 0-10.59-15.94A27.57 27.57 0 0 0 34.7 8.31z"
                                        />
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class="media-body media-middle">
                        Телефон:
                        <a href="tel:#"><?=$arItem["PROPERTIES"]['PHONE']['VALUE']?></a>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <div class="svg-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70.87 70.87">
                                <defs>
                                    <style>
                                        .icon-location__cls-2 {
                                            fill: #fff
                                        }
                                    </style>
                                </defs>
                                <g>
                                    <g>
                                        <circle cx="35.43" cy="35.43" r="35.43" transform="rotate(-9.22 35.401 35.434)" fill="#ec1a63" />
                                        <path class="icon-location__cls-2" d="M34 4.31A31.64 31.64 0 0 1 52.75 9.4a31.14 31.14 0 0 1-16.87 57.18A31.13 31.13 0 0 1 7.3 21.42 31.49 31.49 0 0 1 34 4.31zm.74 4a27.62 27.62 0 0 0-19.92 9.32 26.85 26.85 0 0 0 0 35.67 27.53 27.53 0 0 0 45.71-6.94 26.81 26.81 0 0 0 1.65-16.87 27.23 27.23 0 0 0-10.59-15.94A27.57 27.57 0 0 0 34.7 8.31z"
                                        />
                                        <path class="icon-location__cls-2" d="M26.32 16.67a14.21 14.21 0 0 1 10.08-3.56 14.44 14.44 0 0 1 10.33 5.54 13.93 13.93 0 0 1 2.9 9.11 10.72 10.72 0 0 1-1.68 5c-2.19 3.55-5.6 6.16-7.79 9.72a41.37 41.37 0 0 0-4.56 13.63A42.61 42.61 0 0 0 31.35 43c-2.15-3.81-5.77-6.5-8.07-10.2a10.45 10.45 0 0 1-1.72-5.8 14 14 0 0 1 4.76-10.32zm8.16 6a4.16 4.16 0 0 0-3 4.79 4.25 4.25 0 0 0 5.15 3.18 4.14 4.14 0 0 0 3.08-4.93 4.24 4.24 0 0 0-5.23-3.07z"
                                        />
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class="media-body media-middle">
                        Адрес: <?=$arItem["PROPERTIES"]['ADDRESS']['VALUE']?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?endforeach;?>
