<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$isDefPage = ($curPage == '/index.php') ? true : false;
?>
</div>
<?/* .layout-main */?>


<? if (!$layoutWide) { ?>
<!--    <div class="layout-sidebar">-->
<!--        --><?//$APPLICATION->IncludeComponent(
//            "bitrix:main.include",
//            ".default",
//            array(
//                "AREA_FILE_SHOW" => "sect",
//                "AREA_FILE_SUFFIX" => "right",
//                "EDIT_TEMPLATE" => "standard.php",
//                "COMPONENT_TEMPLATE" => ".default",
//                "AREA_FILE_RECURSIVE" => "Y"
//            ),
//            false
//        );?>
<!--    </div>-->
<? } ?>
</div><?/* .layout */?>
</div><?/* .container */?>
<? if ($isDefPage) { ?>

    <?$APPLICATION->IncludeComponent(
        "bitrix:news.line",
        "bottom_feed",
        array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "CACHE_GROUPS" => "N",
            "CACHE_TIME" => "86400",
            "CACHE_TYPE" => "A",
            "DETAIL_URL" => "/news/#SECTION_CODE#/#ELEMENT_CODE#/",
            "FIELD_CODE" => array(
                0 => "NAME",
                1 => "PREVIEW_TEXT",
                2 => "DETAIL_TEXT",
                3 => "DETAIL_PICTURE",
                4 => "IBLOCK_NAME",
                5 => "",
            ),
            "IBLOCKS" => array(
                0 => "5",
            ),
            "IBLOCK_TYPE" => "news",
            "NEWS_COUNT" => "5",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_BY2" => "SORT",
            "SORT_ORDER1" => "DESC",
            "SORT_ORDER2" => "ASC",
            "TITLE" => "Новости магазина",
            "COMPONENT_TEMPLATE" => "bottom_feed",
            "HTML_CONTAINER" => "TOP",
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO"
        ),
        false
    );?>
    <?$APPLICATION->IncludeComponent("bitrix:news.line", "bottom_feed1", Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
        "CACHE_GROUPS" => "N",	// Учитывать права доступа
        "CACHE_TIME" => "86400",	// Время кеширования (сек.)
        "CACHE_TYPE" => "A",	// Тип кеширования
        "DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
        "FIELD_CODE" => array(	// Поля
            0 => "NAME",
            1 => "PREVIEW_TEXT",
            2 => "DETAIL_TEXT",
            3 => "DETAIL_PICTURE",
            4 => "IBLOCK_NAME",
            5 => "",
        ),
        "IBLOCKS" => array(	// Код информационного блока
            0 => "6",
        ),
        "IBLOCK_TYPE" => "recipes",	// Тип информационного блока
        "NEWS_COUNT" => "5",	// Количество новостей на странице
        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        "TITLE" => "Рецепты магазина",	// Заголовок
        "COMPONENT_TEMPLATE" => "bottom_feed",
        "HTML_CONTAINER" => "BOTTOM",	// HTML контейнер
    ),
        false
    );?>

<?}?>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "page",
        "AREA_FILE_SUFFIX" => "bottom",
        "EDIT_TEMPLATE" => "standard.php"
    )
);?>
<? $APPLICATION->ShowViewContent('bottom'); ?>

<footer class="footer">
    <div class="footer-nav">
        <div class="container clearfix">
            <div class="row">
                <div class="col-md-6">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "top_sections2",
                        array(
                            "ROOT_MENU_TYPE" => "top2",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "86400",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MAX_LEVEL" => "3",
                            "CHILD_MENU_TYPE" => "",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "COMPONENT_TEMPLATE" => "top_sections2",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO"
                        ),
                        false
                    );?>

                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "bottom",
                        array(
                            "ROOT_MENU_TYPE" => "top",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "86400",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "COMPONENT_TEMPLATE" => "bottom"
                        ),
                        false,
                        array(
                            "ACTIVE_COMPONENT" => "N"
                        )
                    );?>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
							<!--<iframe src="/local/templates/main/inwidget/index.php" data-inwidget scrolling="no" frameborder="no" style="border:none;width:267px;height:330px;overflow:hidden;display:block;margin:0 auto"></iframe>-->
                        </div>
                        <div class="col-md-6">
                            <div id="vkWidgetGroup" style="margin: 0 auto"></div>

                            <div style="margin-top:7px">
                                <div class="fb-page" data-href="https://www.facebook.com/club.craftology/" data-tabs="timeline" data-width="275" data-height="100" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/club.craftology/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/club.craftology/">Интернет-магазин Craftology.ru</a></blockquote></div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="footcatnav__title h1">Наши контакты:</div>

                    <p>
                        E-mail: <a href="mailto:info@craftology.ru">info@craftology.ru</a>
                    </p>
                    <p>
                        Группа Вконтакте:&nbsp;<a href="http://vk.com/club_craftology">http://vk.com/club_craftology</a>
                    </p>
                    <p>
                        Тел.: 8-800-500-30-47
                    </p>
                    <p>_</p>
                    <p>Если у Вас есть замечания по работе новой версии сайта - мы будем крайне благодарны Вам за отзыв</p> -->
                    <?/*
						$APPLICATION->IncludeComponent(
							"bitrix:form.result.new",
							"feedback_footer",
							array(
								"COMPONENT_TEMPLATE" => "feedback_footer",
								"WEB_FORM_ID" => "1",
								"IGNORE_CUSTOM_TEMPLATE" => "N",
								"USE_EXTENDED_ERRORS" => "Y",
								"SEF_MODE" => "N",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "86400",
								"LIST_URL" => "?ajax=y",
								"EDIT_URL" => "",
								"SUCCESS_URL" => "?ajax=y",
								"CHAIN_ITEM_TEXT" => "",
								"CHAIN_ITEM_LINK" => "",
								"SUCCESS_MESSAGE" => "В ближайшее время мы с вами свяжемся.",
								"COMPOSITE_FRAME_MODE" => "A",
								"COMPOSITE_FRAME_TYPE" => "AUTO",
								"VARIABLE_ALIASES" => array(
									"WEB_FORM_ID" => "WEB_FORM_ID",
									"RESULT_ID" => "RESULT_ID",
								)
							),
							false
						);
					*/?>
                </div>
            </div>
            <!-- <div class="footer-nav_left"></div> -->
            <!-- <a class="add_palitra" href="/order/#DELIVERYID"><img src="/images/ribbon.png"></a> -->
            <!-- <div class="footer-nav_right"></div> -->
        </div>
    </div>
    <div class="footer-main">
        <div class="container clearfix">
            <div class="footer-logo"><a href="/"><img src="/images/logo.png" alt="" /></a></div>
            <div class="footer-slogan">
                <div class="slogan__title">Craftology</div>
                <div class="slogan__text">Магазин для мастеров</div>
            </div>
            <div class="footer__download-price">
                <!--                 <a href="/upload/price.xlsx">Скачать прайс лист</a> -->
            </div>
            <div class="footer__phone js-dialog" data-href="/local/api/form/feedback/">
                <span>8 (800) 500 30 47</span>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container clearfix">
            <div class="legal-name">ООО "Крафтология"</div>
            <div class="copyright">Все права защищены, <?= date('Y') ?></div>
            <div class="powerby"><noindex><a href="https://studio-prm.ru" target="_blank" rel="nofollow">Сайт разработан в PRM-Studio</a></noindex></div>
        </div>
    </div>
</footer>

<script>
    //Show in availible or hide
    function filter_click(){
        var url = window.location.pathname,
            tagList = url.split('/');
        tagList.pop();
        lastItem = tagList[tagList.length - 1];
        var date = new Date(new Date().getTime() + 3600000 * 1000);
        if($("#cat_for_access").prop("checked")){
            document.cookie = "namber_order=checked; path=/; expires=" + date.toUTCString();
            //document.cookie = 'namber_order = checked;path=/';
            // document.cookie = 'pages = ' + lastItem;
            location.reload();
        }else{
            document.cookie = "namber_order=null; path=/; expires=" + date.toUTCString();
            //document.cookie = 'namber_order = null;path=/';
            location.reload();

        }
    }
</script>

<script>
    jQuery(function($){
        app.content.bind('favorite', function(data){
            data = $.isArray(data) ? data : [];

            for (var i = 0; i < data.length; i++) {
                $('[data-product=' + data[i] + '] .js-favoriteBtn').addClass('in-favorite').attr('title', 'Убрать из избранного');
            }

            $('.js-mainnavFavoriteCnt').text('(' + data.length + ' ' + getNumEnding(data.length, ['товар', 'товара', 'товаров']) + ')');
        });

        app.content.bind('cart', function(data){
            if (typeof data != 'object') return;

            $('.js-mainnavCartTotalPrice').html(parseFloat(data.total_price) > 0 ? '(' + data.total_price + ' <span class="rub">Р</span>)' : '');

            var $qcart = $('.qcart'),
                $items = $qcart.find('.qcart__items').empty();

            if (data.items.length) {
                $qcart.removeClass('qcart-empty');

                $qcart.find('.qcart__title').html('В корзине ' + data.quantity + ' ' + getNumEnding(data.quantity, ['товар', 'товара', 'товаров']) + ' — ' + data.total_price_format + '');

                for (var i = 0; i < data.items.length; i++) {
                    $items.append('<div class="qcart__item">' +
                        '<div class="qcart__image">' + (data.items[i].PRODUCT.IMAGE_SM.src ? '<a href=""><img src="' + data.items[i].PRODUCT.IMAGE_SM.src + '" alt="" /></a>' : '' ) + '</div>' +
                        '<div class="qcart__details">' +
                        '<div class="qcart__name"><a href="' + data.items[i].PRODUCT.DETAIL_PAGE_URL + '">' + data.items[i].PRODUCT.NAME + '</a></div>' +
                        '</div>' +
                        '<div class="qcart__price">' + data.items[i].QUANTITY + ' шт.</div>' +
                        '<div class="qcart__price">' + data.items[i].PRICE_FORMAT + '</div>' +
                        '</div>');
                }
            }
            else {
                $qcart.addClass('qcart-empty');
                $items.append('<div class="qcart__item"><div class="qcart__empty">Ваша корзина пуста.</div></div>');
            }
        });

        app.content.bind('rating',
            function(data){
                data = typeof data == 'object' ? data : {};

                if (data.rating) {
                    for (var id in data.rating) {
                        if (data.rating.hasOwnProperty(id)) {
                            $('[data-product=' + id + '] .js-ratingStars').data('rate', data.rating[id]).attr('data-rate', data.rating[id]);
                        }
                    }
                }
                if (data.user_rating) {
                    for (var i = 0; i < data.user_rating.length; i++) {
                        $('[data-product=' + data.user_rating[i] + '] .js-rating').removeClass('js-rating');
                    }
                }
                initRating();
            }
            , {
                id: function() {
                    var ids = [];
                    $('.js-ratingStars').each(function(){
                        var id = $(this).closest('[data-product]').data('product');
                        if (id) {
                            ids.push(id);
                        }
                    });
                    return ids;
                }
            }
        );

        app.content.bind('reviews',
            function(data){
                data = typeof data == 'object' ? data : {};

                if (data) {
                    for (var productId in data) {
                        if (data.hasOwnProperty(productId)) {
                            $('[data-product=' + productId + '] .js-reviews').each(function(){
                                var $reviews = $(this);
                                $reviews.empty();
                                for (var i = 0; i < data[productId].length; i++) {
                                    $reviews.append(
                                        '<div class="reviews__item">' +
                                        '<div class="reviews__meta">' +
                                        '<span class="reviews__name">' + data[productId][i].name + '</span><span class="reviews__date">' + data[productId][i].date + '</span>' +
                                        '</div>' +
                                        '<div class="reviews__text">' + data[productId][i].text + '</div>' +
                                        '</div>');
                                }
                            });
                        }
                    }
                }
                initRating();
            }
            , {
                productId: function() {
                    return $('.js-reviews').closest('[data-product]').data('product');
                }
            }
        );

        app.content.bind('product_subscribe',
            function(data){
                $('.js-productSubscribeBtn.js-productSubscribeBtnOff').removeClass('js-productSubscribeBtnOff').text('Уведомить меня о снижении цены');
                if (data.length) {
                    for (var i = 0; i < data.length; i++) {
                        $('[data-product=' + data[i] + '] .js-productSubscribeBtn').addClass('js-productSubscribeBtnOff').text('Убрать подписку о снижении цены');
                    }
                }
            }
            , {
                productId: function() {
                    var ids = [];
                    $('.js-productSubscribeBtn').each(function(){
                        ids.push($(this).closest('[data-product]').data('product'));
                    });
                    return ids;
                }
            }
        );

        app.content.load();
    });
</script>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>
<div id="vk_groups"></div>
<script type="text/javascript">
    VK.Widgets.Group("vkWidgetGroup", {mode: 1, width: "275", no_cover: 1}, 39622201);
</script>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-11232-8ppKr';</script>

<!-- Sliza.ru - Widget -->
<!-- <script type="text/javascript" src="https://sliza.ru/widget.php?id=3128&h=cc8677fa708a27dd8a1971bf4b699766&t=s" async defer></script> -->
<!-- /// -->

<!-- Yandex.Metrika counter -->
<!--<script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter13778644 = new Ya.Metrika({ id:13778644, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, ecommerce:"dataLayer" }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/13778644" style="position:absolute; left:-9999px;" alt="" /></div></noscript>-->
<!-- /Yandex.Metrika counter -->

<!--<script> -->
<!--(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ -->
<!--(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), -->
<!--m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) -->
<!--})(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); -->
<!---->
<!--ga('create', 'UA-101646308-1', 'auto'); -->
<!--ga('send', 'pageview'); -->
<!---->
<!--</script>-->

<!— /Google An. counter —>
<!— /VK retarget counter —>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=UKybmC0sgnA4VzElxQrlL7EpdHTWxSn68oy7E*UidkBDNXsUqT7seCntMRD6i*31XyWMnVweZovwRl1TPTScZgzxZL0P7g2JYu8XLaBbGnJTjMQH20qzLqaR*nJZ3j7OAuuFbC8/Zp/kT7YJHOX7Ou*92DzkwiNNDawMKAoNPO4-';</script>


<script type="text/javascript" src="//vk.com/js/api/openapi.js?152"></script>

<!— VK Widget —>
<div id="vk_community_messages"></div>
<script type="text/javascript">
VK.Widgets.CommunityMessages("vk_community_messages", 39622201, {tooltipButtonText: "Есть вопрос?"});
</script>

<!— Global site tag (gtag.js) - AdWords: 844891523 —>
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-844891523"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-844891523');
</script>
</body>
</html>