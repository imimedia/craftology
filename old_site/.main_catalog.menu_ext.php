<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;

$maxLevel = 3;

$aMenuLinks = array();
$obCache = new CPHPCache();
if ($obCache->InitCache(86400, serialize(1), '/menu/mainmenu')) {
    $aMenuLinks = $obCache->GetVars();
}
elseif ($obCache->StartDataCache()) {
    Bitrix\Main\Loader::includeModule('iblock');

    // Меню кондитера

    $rsSection = CIBlockSection::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG, 'ID' => SECTION_CONFECTIONER, 'ACTIVE' => 'Y'), false, array('ID', 'CODE', 'NAME', 'DEPTH_LEVEL', 'LEFT_MARGIN', 'RIGHT_MARGIN', 'SECTION_PAGE_URL'));
    $rsSection->SetUrlTemplates();
    if ($arTopSection = $rsSection->GetNext()) {
        ob_start();
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "banners_in_catalog_menu",
            Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "86400",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "N",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array("NAME", "DETAIL_PICTURE", ""),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "8",
                "IBLOCK_TYPE" => "content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "20",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_TITLE" => "",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "katalog_konditera",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array("LINK", ""),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC"
            )
        );
        $htmlBanners = ob_get_contents();
        ob_end_clean();

        $aMenuLinks[] = array(
            'Каталог',
            $arTopSection['SECTION_PAGE_URL'],
            array(),
            array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'ID' => $arTopSection['ID'], 'PARENT_ID' => 0, 'CODE' => $arTopSection['CODE'], 'DEPTH_LEVEL' => $arTopSection['DEPTH_LEVEL'], 'IS_FAVORITE' => false, 'STYLE' => 'mainnav__item_for-pastry', 'HTML_BANNERS' => $htmlBanners)
        );

        $aMenuLinks[] = array(
            "Новинки",
            "/novinki-konditera.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );

        $aMenuLinks[] = array(
            "Хиты продаж",
            "/khit-prodazh-konditera.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $aMenuLinks[] = array(
            "Суперцены",
            "/supertseny-konditera.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $aMenuLinks[] = array(
            "Пищевая печать",
            "/pishchevaya-pechat-po-maketu-klienta",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $aMenuLinks[] = array(
            "Новый год",
            "/novyy-god-konditera.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $aMenuLinks[] = array(
            "День Св. Валентина",
            "/den-sv-valentina-konditera.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $aMenuLinks[] = array(
            "23 февраля",
            "/23-fevralya-konditera.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $aMenuLinks[] = array(
            "8 марта",
            "/8-marta-konditera.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $aMenuLinks[] = array(
            "Пасха",
            "/paskha-konditera.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $aMenuLinks[] = array(
            "1 Сентября",
            "/1-sentyabrya-konditera.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $aMenuLinks[] = array(
            "Свадьба",
            "/svadba.php",
            Array(),
            Array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, 'IS_FAVORITE' => true),
            ""
        );
        $rsSections = CIBlockSection::GetList(
            array('left_margin'=>'asc'),
            array('IBLOCK_ID' => IBLOCK_CATALOG, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', '>LEFT_MARGIN' => $arTopSection['LEFT_MARGIN'], '<RIGHT_MARGIN' => $arTopSection['RIGHT_MARGIN']),
            false,
            array('ID', 'IBLOCK_SECTION_ID', 'NAME', 'CODE', 'SECTION_PAGE_URL', 'DEPTH_LEVEL', 'LEFT_MARGIN', 'RIGHT_MARGIN')
        );
        $rsSections->SetUrlTemplates();
        while ($section = $rsSections->GetNext()) {
            if ($maxLevel < $section['DEPTH_LEVEL'] || $arTopSection['ID'] == $section['ID'])
                continue;
            $isParent = $maxLevel != $section['DEPTH_LEVEL'] && ($section['RIGHT_MARGIN'] - $section['LEFT_MARGIN']) > 1;
            $aMenuLinks[] = array(
                $section['NAME'],
                $section['SECTION_PAGE_URL'],
                array(),
                array('FROM_IBLOCK' => true, 'IS_PARENT' => $isParent, 'ID' => $section['ID'], 'PARENT_ID' => $section['IBLOCK_SECTION_ID'], 'DEPTH_LEVEL' => $section['DEPTH_LEVEL'], 'IS_FAVORITE' => false)
            );
        }
    }

    // Меню мыловара



    $obCache->EndDataCache($aMenuLinks);
}