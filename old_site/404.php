<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$title = "Страница не найдена";
$APPLICATION->SetTitle($title);
$APPLICATION->SetPageProperty("title", $title);
?>
    <br>
 <b>К сожалению, эта страница уже не актуальна</b><br>
 <br>
 <br>
<ul>
	<li>Товар снят с продажи&nbsp;<img width="310" alt="Котик страница 404" src="/upload/medialibrary/dcb/dcb724408d5b297ea6845d874f5a93a6.png" height="289" title="Котик страница 404" align="right"> </li>
 <br>
	<li>Категория поменяла место в каталоге</li>
</ul>
 <br>
 Воспользуйтесь меню, чтобы найти нужную Вам информацию.<br>
 <br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>