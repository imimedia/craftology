<?$APPLICATION->IncludeComponent(
	"bitrix:news.line",
	"main_slider",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "86400",
		"CACHE_TYPE" => "A",
		"DETAIL_URL" => "",
		"FIELD_CODE" => array("NAME","DETAIL_TEXT","DETAIL_PICTURE",""),
		"IBLOCKS" => array("7"),
		"IBLOCK_TYPE" => "content",
		"NEWS_COUNT" => "7",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC"
	)
);?>