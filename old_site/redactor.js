//// deploy.js
//var ghpages = require('gh-pages'),
//    path = require('path');

//ghpages.publish(__dirname, function (err) {
//   if (err) console.error(err);
//   else console.log('Successfully deployed to gh-pages');
//});
// croppie.js
!function(e,t){"function"==typeof define&&define.amd?define(["exports"],t):t("object"==typeof exports&&"string"!=typeof exports.nodeName?exports:e.commonJsStrict={})}(this,function(e){function t(e){if(e in O)return e;for(var t=e[0].toUpperCase()+e.slice(1),n=N.length;n--;)if((e=N[n]+t)in O)return e}function n(e,t){e=e||{};for(var i in t)t[i]&&t[i].constructor&&t[i].constructor===Object?(e[i]=e[i]||{},n(e[i],t[i])):e[i]=t[i];return e}function i(e){if("createEvent"in document){var t=document.createEvent("HTMLEvents");t.initEvent("change",!1,!0),e.dispatchEvent(t)}else e.fireEvent("onchange")}function o(e,t,n){if("string"==typeof t){var i=t;(t={})[i]=n}for(var o in t)e.style[o]=t[o]}function r(e,t){e.classList?e.classList.add(t):e.className+=" "+t}function a(e,t){e.classList?e.classList.remove(t):e.className=e.className.replace(t,"")}function l(e){return parseInt(e,10)}function s(e,t){var n=t||new Image;return n.style.opacity=0,new Promise(function(t){n.src===e?t(n):(n.removeAttribute("crossOrigin"),e.match(/^https?:\/\/|^\/\//)&&n.setAttribute("crossOrigin","anonymous"),n.onload=function(){setTimeout(function(){t(n)},1)},n.src=e)})}function u(e,t){window.EXIF||t(0),EXIF.getData(e,function(){var e=EXIF.getTag(this,"Orientation");t(e)})}function c(e,t,n){var i=t.width,o=t.height,r=e.getContext("2d");switch(e.width=t.width,e.height=t.height,r.save(),n){case 2:r.translate(i,0),r.scale(-1,1);break;case 3:r.translate(i,o),r.rotate(180*Math.PI/180);break;case 4:r.translate(0,o),r.scale(1,-1);break;case 5:e.width=o,e.height=i,r.rotate(90*Math.PI/180),r.scale(1,-1);break;case 6:e.width=o,e.height=i,r.rotate(90*Math.PI/180),r.translate(0,-o);break;case 7:e.width=o,e.height=i,r.rotate(-90*Math.PI/180),r.translate(-i,o),r.scale(1,-1);break;case 8:e.width=o,e.height=i,r.translate(0,i),r.rotate(-90*Math.PI/180)}r.drawImage(t,0,0,i,o),r.restore()}function h(){var e,t,n,i,a,l=this,s=l.options.viewport.type?"cr-vp-"+l.options.viewport.type:null;l.options.useCanvas=l.options.enableOrientation||p.call(l),l.data={},l.elements={},e=l.elements.boundary=document.createElement("div"),t=l.elements.viewport=document.createElement("div"),l.elements.img=document.createElement("img"),n=l.elements.overlay=document.createElement("div"),l.options.useCanvas?(l.elements.canvas=document.createElement("canvas"),l.elements.preview=l.elements.canvas):l.elements.preview=l.elements.img,r(e,"cr-boundary"),i=l.options.boundary.width,a=l.options.boundary.height,o(e,{width:i+(isNaN(i)?"":"px"),height:a+(isNaN(a)?"":"px")}),r(t,"cr-viewport"),s&&r(t,s),o(t,{width:l.options.viewport.width+"px",height:l.options.viewport.height+"px"}),t.setAttribute("tabindex",0),r(l.elements.preview,"cr-image"),r(n,"cr-overlay"),l.element.appendChild(e),e.appendChild(l.elements.preview),e.appendChild(t),e.appendChild(n),r(l.element,"croppie-container"),l.options.customClass&&r(l.element,l.options.customClass),w.call(this),l.options.enableZoom&&d.call(l)}function p(){return this.options.enableExif&&window.EXIF}function m(e){if(this.options.enableZoom){var t=this.elements.zoomer,n=F(e,4);t.value=Math.max(t.min,Math.min(t.max,n))}}function d(){function e(){f.call(n,{value:parseFloat(o.value),origin:new D(n.elements.preview),viewportRect:n.elements.viewport.getBoundingClientRect(),transform:T.parse(n.elements.preview)})}function t(t){var i,o;i=t.wheelDelta?t.wheelDelta/1200:t.deltaY?t.deltaY/1060:t.detail?t.detail/-60:0,o=n._currentZoom+i*n._currentZoom,t.preventDefault(),m.call(n,o),e.call(n)}var n=this,i=n.elements.zoomerWrap=document.createElement("div"),o=n.elements.zoomer=document.createElement("input");r(i,"cr-slider-wrap"),r(o,"cr-slider"),o.type="range",o.step="0.0001",o.value=1,o.style.display=n.options.showZoomer?"":"none",n.element.appendChild(i),i.appendChild(o),n._currentZoom=1,n.elements.zoomer.addEventListener("input",e),n.elements.zoomer.addEventListener("change",e),n.options.mouseWheelZoom&&(n.elements.boundary.addEventListener("mousewheel",t),n.elements.boundary.addEventListener("DOMMouseScroll",t))}function f(e){function t(){var e={};e[H]=i.toString(),e[z]=a.toString(),o(n.elements.preview,e)}var n=this,i=e?e.transform:T.parse(n.elements.preview),r=e?e.viewportRect:n.elements.viewport.getBoundingClientRect(),a=e?e.origin:new D(n.elements.preview);if(n._currentZoom=e?e.value:n._currentZoom,i.scale=n._currentZoom,t(),n.options.enforceBoundary){var l=v.call(n,r),s=l.translate,u=l.origin;i.x>=s.maxX&&(a.x=u.minX,i.x=s.maxX),i.x<=s.minX&&(a.x=u.maxX,i.x=s.minX),i.y>=s.maxY&&(a.y=u.minY,i.y=s.maxY),i.y<=s.minY&&(a.y=u.maxY,i.y=s.minY)}t(),q.call(n),b.call(n)}function v(e){var t=this,n=t._currentZoom,i=e.width,o=e.height,r=t.elements.boundary.clientWidth/2,a=t.elements.boundary.clientHeight/2,l=t.elements.preview.getBoundingClientRect(),s=l.width,u=l.height,c=i/2,h=o/2,p=-1*(c/n-r),m=-1*(h/n-a),d=1/n*c,f=1/n*h;return{translate:{maxX:p,minX:p-(s*(1/n)-i*(1/n)),maxY:m,minY:m-(u*(1/n)-o*(1/n))},origin:{maxX:s*(1/n)-d,minX:d,maxY:u*(1/n)-f,minY:f}}}function g(){var e=this,t=e._currentZoom,n=e.elements.preview.getBoundingClientRect(),i=e.elements.viewport.getBoundingClientRect(),r=T.parse(e.elements.preview.style[H]),a=new D(e.elements.preview),l=i.top-n.top+i.height/2,s=i.left-n.left+i.width/2,u={},c={};u.y=l/t,u.x=s/t,c.y=(u.y-a.y)*(1-t),c.x=(u.x-a.x)*(1-t),r.x-=c.x,r.y-=c.y;var h={};h[z]=u.x+"px "+u.y+"px",h[H]=r.toString(),o(e.elements.preview,h)}function w(){function e(e,t){var n=d.elements.preview.getBoundingClientRect(),i=p.y+t,o=p.x+e;d.options.enforceBoundary?(h.top>n.top+t&&h.bottom<n.bottom+t&&(p.y=i),h.left>n.left+e&&h.right<n.right+e&&(p.x=o)):(p.y=i,p.x=o)}function t(e){var t=37,i=38,o=39,r=40;if(!e.shiftKey||e.keyCode!=i&&e.keyCode!=r){if(e.keyCode>=37&&e.keyCode<=40){e.preventDefault();var a=function(e){switch(e){case t:return[1,0];case i:return[0,1];case o:return[-1,0];case r:return[0,-1]}}(e.keyCode);p=T.parse(d.elements.preview),document.body.style[P]="none",h=d.elements.viewport.getBoundingClientRect(),n(a)}}else{var l=0;l=e.keyCode==i?parseFloat(d.elements.zoomer.value,10)+parseFloat(d.elements.zoomer.step,10):parseFloat(d.elements.zoomer.value,10)-parseFloat(d.elements.zoomer.step,10),d.setZoom(l)}}function n(t){var n={};e(t[0],t[1]),n[H]=p.toString(),o(d.elements.preview,n),y.call(d),document.body.style[P]="",g.call(d),b.call(d),c=0}function r(e){if(e.preventDefault(),!f){if(f=!0,s=e.pageX,u=e.pageY,e.touches){var t=e.touches[0];s=t.pageX,u=t.pageY}p=T.parse(d.elements.preview),window.addEventListener("mousemove",a),window.addEventListener("touchmove",a),window.addEventListener("mouseup",l),window.addEventListener("touchend",l),document.body.style[P]="none",h=d.elements.viewport.getBoundingClientRect()}}function a(t){t.preventDefault();var n=t.pageX,r=t.pageY;if(t.touches){var a=t.touches[0];n=a.pageX,r=a.pageY}var l=n-s,h=r-u,f={};if("touchmove"==t.type&&t.touches.length>1){var v=t.touches[0],g=t.touches[1],w=Math.sqrt((v.pageX-g.pageX)*(v.pageX-g.pageX)+(v.pageY-g.pageY)*(v.pageY-g.pageY));c||(c=w/d._currentZoom);var b=w/c;return m.call(d,b),void i(d.elements.zoomer)}e(l,h),f[H]=p.toString(),o(d.elements.preview,f),y.call(d),u=r,s=n}function l(){f=!1,window.removeEventListener("mousemove",a),window.removeEventListener("touchmove",a),window.removeEventListener("mouseup",l),window.removeEventListener("touchend",l),document.body.style[P]="",g.call(d),b.call(d),c=0}var s,u,c,h,p,d=this,f=!1;d.elements.overlay.addEventListener("mousedown",r),d.elements.viewport.addEventListener("keydown",t),d.elements.overlay.addEventListener("touchstart",r)}function y(){var e=this,t=e.elements.boundary.getBoundingClientRect(),n=e.elements.preview.getBoundingClientRect();o(e.elements.overlay,{width:n.width+"px",height:n.height+"px",top:n.top-t.top+"px",left:n.left-t.left+"px"})}function b(){var e=this,t=e.get();if(x.call(e))if(e.options.update.call(e,t),e.$&&"undefined"==typeof Prototype)e.$(e.element).trigger("update",t);else{var n;window.CustomEvent?n=new CustomEvent("update",{detail:t}):(n=document.createEvent("CustomEvent")).initCustomEvent("update",!0,!0,t),e.element.dispatchEvent(n)}}function x(){return this.elements.preview.offsetHeight>0&&this.elements.preview.offsetWidth>0}function C(){var e,t,n,r,a,l=this,s=0,u=1.5,c=1,h={},p=l.elements.preview,d=l.elements.zoomer,f=new T(0,0,c),v=new D;if(x.call(l)&&!l.data.bound){if(l.data.bound=!0,h[H]=f.toString(),h[z]=v.toString(),h.opacity=1,o(p,h),e=p.getBoundingClientRect(),t=l.elements.viewport.getBoundingClientRect(),n=l.elements.boundary.getBoundingClientRect(),l._originalImageWidth=e.width,l._originalImageHeight=e.height,l.options.enableZoom){l.options.enforceBoundary&&(r=t.width/e.width,a=t.height/e.height,s=Math.max(r,a)),s>=u&&(u=s+1),d.min=F(s,4),d.max=F(u,4);var w=Math.max(n.width/e.width,n.height/e.height);c=null!==l.data.boundZoom?l.data.boundZoom:w,m.call(l,c),i(d)}else l._currentZoom=c;f.scale=l._currentZoom,h[H]=f.toString(),o(p,h),l.data.points.length?E.call(l,l.data.points):_.call(l),g.call(l),y.call(l)}}function E(e){if(4!=e.length)throw"Croppie - Invalid number of points supplied: "+e;var t=this,n=e[2]-e[0],i=t.elements.viewport.getBoundingClientRect(),r=t.elements.boundary.getBoundingClientRect(),a={left:i.left-r.left,top:i.top-r.top},l=i.width/n,s=e[1],u=e[0],c=-1*e[1]+a.top,h=-1*e[0]+a.left,p={};p[z]=u+"px "+s+"px",p[H]=new T(h,c,l).toString(),o(t.elements.preview,p),m.call(t,l),t._currentZoom=l}function _(){var e=this,t=e.elements.preview.getBoundingClientRect(),n=e.elements.viewport.getBoundingClientRect(),i=e.elements.boundary.getBoundingClientRect(),r=n.left-i.left,a=n.top-i.top,l=r-(t.width-n.width)/2,s=a-(t.height-n.height)/2,u=new T(l,s,e._currentZoom);o(e.elements.preview,H,u.toString())}function B(e){var t=this,n=t.elements.canvas,i=t.elements.img,o=n.getContext("2d"),r=p.call(t),e=t.options.enableOrientation&&e;o.clearRect(0,0,n.width,n.height),n.width=i.width,n.height=i.height,r?u(i,function(t){c(n,i,l(t,10)),e&&c(n,i,e)}):e&&c(n,i,e)}function R(e){var t=e.points,n=l(t[0]),i=l(t[1]),o=l(t[2])-n,r=l(t[3])-i,a=e.circle,s=document.createElement("canvas"),u=s.getContext("2d"),c=o,h=r,p=0,m=0,d=c,f=h,v=1;return e.outputWidth&&e.outputHeight&&(d=e.outputWidth,f=e.outputHeight,v=d/c),s.width=d,s.height=f,e.backgroundColor&&(u.fillStyle=e.backgroundColor,u.fillRect(0,0,c,h)),n<0&&(p=Math.abs(n),n=0),i<0&&(m=Math.abs(i),i=0),1!==v&&(p*=v,m*=v,c*=v,h*=v),u.drawImage(this.elements.preview,n,i,o,r,p,m,c,h),a&&(u.fillStyle="#fff",u.globalCompositeOperation="destination-in",u.beginPath(),u.arc(c/2,h/2,c/2,0,2*Math.PI,!0),u.closePath(),u.fill()),s}function Z(e){var t=e.points,n=document.createElement("div"),i=document.createElement("img"),a=t[2]-t[0],l=t[3]-t[1];return r(n,"croppie-result"),n.appendChild(i),o(i,{left:-1*t[0]+"px",top:-1*t[1]+"px"}),i.src=e.url,o(n,{width:a+"px",height:l+"px"}),n}function L(e){return R.call(this,e).toDataURL(e.format,e.quality)}function I(e){var t=this;return new Promise(function(n,i){R.call(t,e).toBlob(function(e){n(e)},e.format,e.quality)})}function M(e,t){var n,i=this,o=[],r=null;if("string"==typeof e)n=e,e={};else if(Array.isArray(e))o=e.slice();else{if(void 0===e&&i.data.url)return C.call(i),b.call(i),null;n=e.url,o=e.points||[],r=void 0===e.zoom?null:e.zoom}return i.data.bound=!1,i.data.url=n||i.data.url,i.data.boundZoom=r,s(n,i.elements.img).then(function(n){if(o.length)i.options.relative&&(o=[o[0]*n.naturalWidth/100,o[1]*n.naturalHeight/100,o[2]*n.naturalWidth/100,o[3]*n.naturalHeight/100]),i.data.points=o.map(function(e){return parseFloat(e)});else{var r,a,l=n.naturalWidth,s=n.naturalHeight,u=i.elements.viewport.getBoundingClientRect(),c=u.width/u.height;l/s>c?r=(a=s)*c:a=(r=l)/c;var h=(l-r)/2,p=(s-a)/2,m=h+r,d=p+a;i.data.points=[h,p,m,d]}i.options.useCanvas&&(i.elements.img.exifdata=null,B.call(i,e.orientation||1)),C.call(i),b.call(i),t&&t()})}function F(e,t){return parseFloat(e).toFixed(t||0)}function X(){var e=this,t=e.elements.preview.getBoundingClientRect(),n=e.elements.viewport.getBoundingClientRect(),i=n.left-t.left,o=n.top-t.top,r=(n.width-e.elements.viewport.offsetWidth)/2,a=(n.height-e.elements.viewport.offsetHeight)/2,l=i+e.elements.viewport.offsetWidth+r,s=o+e.elements.viewport.offsetHeight+a,u=e._currentZoom;(u===1/0||isNaN(u))&&(u=1);var c=e.options.enforceBoundary?0:Number.NEGATIVE_INFINITY;return i=Math.max(c,i/u),o=Math.max(c,o/u),l=Math.max(c,l/u),s=Math.max(c,s/u),{points:[F(i),F(o),F(l),F(s)],zoom:u}}function k(e){var t=this,i=X.call(t),o=n(U,n({},e)),r="string"==typeof e?e:o.type||"base64",a=o.size,l=o.format,s=o.quality,u=o.backgroundColor,c="boolean"==typeof o.circle?o.circle:"circle"===t.options.viewport.type,h=t.elements.viewport.getBoundingClientRect(),p=h.width/h.height;return"viewport"===a?(i.outputWidth=h.width,i.outputHeight=h.height):"object"==typeof a&&(a.width&&a.height?(i.outputWidth=a.width,i.outputHeight=a.height):a.width?(i.outputWidth=a.width,i.outputHeight=a.width/p):a.height&&(i.outputWidth=a.height*p,i.outputHeight=a.height)),$.indexOf(l)>-1&&(i.format="image/"+l,i.quality=s),i.circle=c,i.url=t.data.url,i.backgroundColor=u,new Promise(function(e,n){switch(r.toLowerCase()){case"rawcanvas":e(R.call(t,i));break;case"canvas":case"base64":e(L.call(t,i));break;case"blob":I.call(t,i).then(e);break;default:e(Z.call(t,i))}})}function W(){C.call(this)}function Y(e){if(!this.options.useCanvas)throw"Croppie: Cannot rotate without enableOrientation";var t=this,n=t.elements.canvas,i=(t.elements.img,document.createElement("canvas")),o=1;i.width=n.width,i.height=n.height,i.getContext("2d").drawImage(n,0,0),90!==e&&-270!==e||(o=6),-90!==e&&270!==e||(o=8),180!==e&&-180!==e||(o=3),c(n,i,o),f.call(t)}function j(){var e=this;e.element.removeChild(e.elements.boundary),a(e.element,"croppie-container"),e.options.enableZoom&&e.element.removeChild(e.elements.zoomerWrap),delete e.elements}function S(e,t){if(this.element=e,this.options=n(n({},S.defaults),t),"img"===this.element.tagName.toLowerCase()){var i=this.element;r(i,"cr-original-image");var o=document.createElement("div");this.element.parentNode.appendChild(o),o.appendChild(i),this.element=o,this.options.url=this.options.url||i.src}if(h.call(this),this.options.url){var a={url:this.options.url,points:this.options.points};delete this.options.url,delete this.options.points,M.call(this,a)}}"function"!=typeof Promise&&function(e){function t(e,t){return function(){e.apply(t,arguments)}}function n(e){if("object"!=typeof this)throw new TypeError("Promises must be constructed via new");if("function"!=typeof e)throw new TypeError("not a function");this._state=null,this._value=null,this._deferreds=[],s(e,t(o,this),t(r,this))}function i(e){var t=this;return null===this._state?void this._deferreds.push(e):void c(function(){var n=t._state?e.onFulfilled:e.onRejected;if(null!==n){var i;try{i=n(t._value)}catch(t){return void e.reject(t)}e.resolve(i)}else(t._state?e.resolve:e.reject)(t._value)})}function o(e){try{if(e===this)throw new TypeError("A promise cannot be resolved with itself.");if(e&&("object"==typeof e||"function"==typeof e)){var n=e.then;if("function"==typeof n)return void s(t(n,e),t(o,this),t(r,this))}this._state=!0,this._value=e,a.call(this)}catch(e){r.call(this,e)}}function r(e){this._state=!1,this._value=e,a.call(this)}function a(){for(var e=0,t=this._deferreds.length;t>e;e++)i.call(this,this._deferreds[e]);this._deferreds=null}function l(e,t,n,i){this.onFulfilled="function"==typeof e?e:null,this.onRejected="function"==typeof t?t:null,this.resolve=n,this.reject=i}function s(e,t,n){var i=!1;try{e(function(e){i||(i=!0,t(e))},function(e){i||(i=!0,n(e))})}catch(e){if(i)return;i=!0,n(e)}}var u=setTimeout,c="function"==typeof setImmediate&&setImmediate||function(e){u(e,1)},h=Array.isArray||function(e){return"[object Array]"===Object.prototype.toString.call(e)};n.prototype.catch=function(e){return this.then(null,e)},n.prototype.then=function(e,t){var o=this;return new n(function(n,r){i.call(o,new l(e,t,n,r))})},n.all=function(){var e=Array.prototype.slice.call(1===arguments.length&&h(arguments[0])?arguments[0]:arguments);return new n(function(t,n){function i(r,a){try{if(a&&("object"==typeof a||"function"==typeof a)){var l=a.then;if("function"==typeof l)return void l.call(a,function(e){i(r,e)},n)}e[r]=a,0==--o&&t(e)}catch(e){n(e)}}if(0===e.length)return t([]);for(var o=e.length,r=0;r<e.length;r++)i(r,e[r])})},n.resolve=function(e){return e&&"object"==typeof e&&e.constructor===n?e:new n(function(t){t(e)})},n.reject=function(e){return new n(function(t,n){n(e)})},n.race=function(e){return new n(function(t,n){for(var i=0,o=e.length;o>i;i++)e[i].then(t,n)})},n._setImmediateFn=function(e){c=e},"undefined"!=typeof module&&module.exports?module.exports=n:e.Promise||(e.Promise=n)}(this),"function"!=typeof window.CustomEvent&&function(){function e(e,t){t=t||{bubbles:!1,cancelable:!1,detail:void 0};var n=document.createEvent("CustomEvent");return n.initCustomEvent(e,t.bubbles,t.cancelable,t.detail),n}e.prototype=window.Event.prototype,window.CustomEvent=e}(),HTMLCanvasElement.prototype.toBlob||Object.defineProperty(HTMLCanvasElement.prototype,"toBlob",{value:function(e,t,n){for(var i=atob(this.toDataURL(t,n).split(",")[1]),o=i.length,r=new Uint8Array(o),a=0;a<o;a++)r[a]=i.charCodeAt(a);e(new Blob([r],{type:t||"image/png"}))}});var z,H,P,N=["Webkit","Moz","ms"],O=document.createElement("div").style;H=t("transform"),z=t("transformOrigin"),P=t("userSelect");var A={translate3d:{suffix:", 0px"},translate:{suffix:""}},T=function(e,t,n){this.x=parseFloat(e),this.y=parseFloat(t),this.scale=parseFloat(n)};T.parse=function(e){return e.style?T.parse(e.style[H]):e.indexOf("matrix")>-1||e.indexOf("none")>-1?T.fromMatrix(e):T.fromString(e)},T.fromMatrix=function(e){var t=e.substring(7).split(",");return t.length&&"none"!==e||(t=[1,0,0,1,0,0]),new T(l(t[4]),l(t[5]),parseFloat(t[0]))},T.fromString=function(e){var t=e.split(") "),n=t[0].substring(S.globals.translate.length+1).split(","),i=t.length>1?t[1].substring(6):1,o=n.length>1?n[0]:0,r=n.length>1?n[1]:0;return new T(o,r,i)},T.prototype.toString=function(){var e=A[S.globals.translate].suffix||"";return S.globals.translate+"("+this.x+"px, "+this.y+"px"+e+") scale("+this.scale+")"};var D=function(e){if(!e||!e.style[z])return this.x=0,void(this.y=0);var t=e.style[z].split(" ");this.x=parseFloat(t[0]),this.y=parseFloat(t[1])};D.prototype.toString=function(){return this.x+"px "+this.y+"px"};var q=function(e,t,n){var i;return function(){var o=this,r=arguments,a=function(){i=null,n||e.apply(o,r)},l=n&&!i;clearTimeout(i),i=setTimeout(a,t),l&&e.apply(o,r)}}(y,500),U={type:"canvas",format:"png",quality:1,size:"viewport"},$=["jpeg","webp","png"];if(window.jQuery){var Q=window.jQuery;Q.fn.croppie=function(e){if("string"===typeof e){var t=Array.prototype.slice.call(arguments,1),n=Q(this).data("croppie");return"get"===e?n.get():"result"===e?n.result.apply(n,t):"bind"===e?n.bind.apply(n,t):this.each(function(){var n=Q(this).data("croppie");if(n){var i=n[e];if(!Q.isFunction(i))throw"Croppie "+e+" method not found";i.apply(n,t),"destroy"===e&&Q(this).removeData("croppie")}})}return this.each(function(){var t=new S(this,e);t.$=Q,Q(this).data("croppie",t)})}}S.defaults={viewport:{width:100,height:100,type:"square"},boundary:{},orientationControls:{enabled:!0,leftClass:"",rightClass:""},customClass:"",showZoomer:!0,enableZoom:!0,mouseWheelZoom:!0,enableExif:!1,enforceBoundary:!0,enableOrientation:!1,update:function(){}},S.globals={translate:"translate3d"},n(S.prototype,{bind:function(e,t){return M.call(this,e,t)},get:function(){var e=X.call(this),t=e.points;return this.options.relative&&(t[0]/=this.elements.img.naturalWidth/100,t[1]/=this.elements.img.naturalHeight/100,t[2]/=this.elements.img.naturalWidth/100,t[3]/=this.elements.img.naturalHeight/100),e},result:function(e){return k.call(this,e)},refresh:function(){return W.call(this)},setZoom:function(e){m.call(this,e),i(this.elements.zoomer)},rotate:function(e){Y.call(this,e)},destroy:function(){return j.call(this)}}),e.Croppie=window.Croppie=S,"object"==typeof module&&module.exports&&(module.exports=S)});
// prism
/* http://prismjs.com/download.html?themes=prism-coy&languages=markup+css+clike+javascript&plugins=line-highlight+line-numbers */
var _self = "undefined" != typeof window ? window : "undefined" != typeof WorkerGlobalScope && self instanceof WorkerGlobalScope ? self : {}, Prism = function () { var e = /\blang(?:uage)?-(\w+)\b/i, t = 0, n = _self.Prism = { util: { encode: function (e) { return e instanceof a ? new a(e.type, n.util.encode(e.content), e.alias) : "Array" === n.util.type(e) ? e.map(n.util.encode) : e.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/\u00a0/g, " ") }, type: function (e) { return Object.prototype.toString.call(e).match(/\[object (\w+)\]/)[1] }, objId: function (e) { return e.__id || Object.defineProperty(e, "__id", { value: ++t }), e.__id }, clone: function (e) { var t = n.util.type(e); switch (t) { case "Object": var a = {}; for (var r in e) e.hasOwnProperty(r) && (a[r] = n.util.clone(e[r])); return a; case "Array": return e.map && e.map(function (e) { return n.util.clone(e) }) } return e } }, languages: { extend: function (e, t) { var a = n.util.clone(n.languages[e]); for (var r in t) a[r] = t[r]; return a }, insertBefore: function (e, t, a, r) { r = r || n.languages; var i = r[e]; if (2 == arguments.length) { a = arguments[1]; for (var l in a) a.hasOwnProperty(l) && (i[l] = a[l]); return i } var o = {}; for (var s in i) if (i.hasOwnProperty(s)) { if (s == t) for (var l in a) a.hasOwnProperty(l) && (o[l] = a[l]); o[s] = i[s] } return n.languages.DFS(n.languages, function (t, n) { n === r[e] && t != e && (this[t] = o) }), r[e] = o }, DFS: function (e, t, a, r) { r = r || {}; for (var i in e) e.hasOwnProperty(i) && (t.call(e, i, e[i], a || i), "Object" !== n.util.type(e[i]) || r[n.util.objId(e[i])] ? "Array" !== n.util.type(e[i]) || r[n.util.objId(e[i])] || (r[n.util.objId(e[i])] = !0, n.languages.DFS(e[i], t, i, r)) : (r[n.util.objId(e[i])] = !0, n.languages.DFS(e[i], t, null, r))) } }, plugins: {}, highlightAll: function (e, t) { for (var a, r = document.querySelectorAll('code[class*="language-"], [class*="language-"] code, code[class*="lang-"], [class*="lang-"] code'), i = 0; a = r[i++]; ) n.highlightElement(a, e === !0, t) }, highlightElement: function (t, a, r) { for (var i, l, o = t; o && !e.test(o.className); ) o = o.parentNode; o && (i = (o.className.match(e) || [, ""])[1], l = n.languages[i]), t.className = t.className.replace(e, "").replace(/\s+/g, " ") + " language-" + i, o = t.parentNode, /pre/i.test(o.nodeName) && (o.className = o.className.replace(e, "").replace(/\s+/g, " ") + " language-" + i); var s = t.textContent, u = { element: t, language: i, grammar: l, code: s }; if (!s || !l) return n.hooks.run("complete", u), void 0; if (n.hooks.run("before-highlight", u), a && _self.Worker) { var c = new Worker(n.filename); c.onmessage = function (e) { u.highlightedCode = e.data, n.hooks.run("before-insert", u), u.element.innerHTML = u.highlightedCode, r && r.call(u.element), n.hooks.run("after-highlight", u), n.hooks.run("complete", u) }, c.postMessage(JSON.stringify({ language: u.language, code: u.code, immediateClose: !0 })) } else u.highlightedCode = n.highlight(u.code, u.grammar, u.language), n.hooks.run("before-insert", u), u.element.innerHTML = u.highlightedCode, r && r.call(t), n.hooks.run("after-highlight", u), n.hooks.run("complete", u) }, highlight: function (e, t, r) { var i = n.tokenize(e, t); return a.stringify(n.util.encode(i), r) }, tokenize: function (e, t) { var a = n.Token, r = [e], i = t.rest; if (i) { for (var l in i) t[l] = i[l]; delete t.rest } e: for (var l in t) if (t.hasOwnProperty(l) && t[l]) { var o = t[l]; o = "Array" === n.util.type(o) ? o : [o]; for (var s = 0; s < o.length; ++s) { var u = o[s], c = u.inside, g = !!u.lookbehind, f = 0, h = u.alias; u = u.pattern || u; for (var p = 0; p < r.length; p++) { var d = r[p]; if (r.length > e.length) break e; if (!(d instanceof a)) { u.lastIndex = 0; var m = u.exec(d); if (m) { g && (f = m[1].length); var y = m.index - 1 + f, m = m[0].slice(f), v = m.length, b = y + v, k = d.slice(0, y + 1), w = d.slice(b + 1), _ = [p, 1]; k && _.push(k); var P = new a(l, c ? n.tokenize(m, c) : m, h); _.push(P), w && _.push(w), Array.prototype.splice.apply(r, _) } } } } } return r }, hooks: { all: {}, add: function (e, t) { var a = n.hooks.all; a[e] = a[e] || [], a[e].push(t) }, run: function (e, t) { var a = n.hooks.all[e]; if (a && a.length) for (var r, i = 0; r = a[i++]; ) r(t) } } }, a = n.Token = function (e, t, n) { this.type = e, this.content = t, this.alias = n }; if (a.stringify = function (e, t, r) { if ("string" == typeof e) return e; if ("Array" === n.util.type(e)) return e.map(function (n) { return a.stringify(n, t, e) }).join(""); var i = { type: e.type, content: a.stringify(e.content, t, r), tag: "span", classes: ["token", e.type], attributes: {}, language: t, parent: r }; if ("comment" == i.type && (i.attributes.spellcheck = "true"), e.alias) { var l = "Array" === n.util.type(e.alias) ? e.alias : [e.alias]; Array.prototype.push.apply(i.classes, l) } n.hooks.run("wrap", i); var o = ""; for (var s in i.attributes) o += (o ? " " : "") + s + '="' + (i.attributes[s] || "") + '"'; return "<" + i.tag + ' class="' + i.classes.join(" ") + '" ' + o + ">" + i.content + "</" + i.tag + ">" }, !_self.document) return _self.addEventListener ? (_self.addEventListener("message", function (e) { var t = JSON.parse(e.data), a = t.language, r = t.code, i = t.immediateClose; _self.postMessage(n.highlight(r, n.languages[a], a)), i && _self.close() }, !1), _self.Prism) : _self.Prism; var r = document.currentScript || [].slice.call(document.getElementsByTagName("script")).pop(); return r && (n.filename = r.src, document.addEventListener && !r.hasAttribute("data-manual") && document.addEventListener("DOMContentLoaded", n.highlightAll)), _self.Prism } (); "undefined" != typeof module && module.exports && (module.exports = Prism), "undefined" != typeof global && (global.Prism = Prism);
Prism.languages.markup = { comment: /<!--[\w\W]*?-->/, prolog: /<\?[\w\W]+?\?>/, doctype: /<!DOCTYPE[\w\W]+?>/, cdata: /<!\[CDATA\[[\w\W]*?]]>/i, tag: { pattern: /<\/?(?!\d)[^\s>\/=.$<]+(?:\s+[^\s>\/=]+(?:=(?:("|')(?:\\\1|\\?(?!\1)[\w\W])*\1|[^\s'">=]+))?)*\s*\/?>/i, inside: { tag: { pattern: /^<\/?[^\s>\/]+/i, inside: { punctuation: /^<\/?/, namespace: /^[^\s>\/:]+:/} }, "attr-value": { pattern: /=(?:('|")[\w\W]*?(\1)|[^\s>]+)/i, inside: { punctuation: /[=>"']/} }, punctuation: /\/?>/, "attr-name": { pattern: /[^\s>\/]+/, inside: { namespace: /^[^\s>\/:]+:/}}} }, entity: /&#?[\da-z]{1,8};/i }, Prism.hooks.add("wrap", function (a) { "entity" === a.type && (a.attributes.title = a.content.replace(/&amp;/, "&")) }), Prism.languages.xml = Prism.languages.markup, Prism.languages.html = Prism.languages.markup, Prism.languages.mathml = Prism.languages.markup, Prism.languages.svg = Prism.languages.markup;
Prism.languages.css = { comment: /\/\*[\w\W]*?\*\//, atrule: { pattern: /@[\w-]+?.*?(;|(?=\s*\{))/i, inside: { rule: /@[\w-]+/} }, url: /url\((?:(["'])(\\(?:\r\n|[\w\W])|(?!\1)[^\\\r\n])*\1|.*?)\)/i, selector: /[^\{\}\s][^\{\};]*?(?=\s*\{)/, string: /("|')(\\(?:\r\n|[\w\W])|(?!\1)[^\\\r\n])*\1/, property: /(\b|\B)[\w-]+(?=\s*:)/i, important: /\B!important\b/i, "function": /[-a-z0-9]+(?=\()/i, punctuation: /[(){};:]/ }, Prism.languages.css.atrule.inside.rest = Prism.util.clone(Prism.languages.css), Prism.languages.markup && (Prism.languages.insertBefore("markup", "tag", { style: { pattern: /(<style[\w\W]*?>)[\w\W]*?(?=<\/style>)/i, lookbehind: !0, inside: Prism.languages.css, alias: "language-css"} }), Prism.languages.insertBefore("inside", "attr-value", { "style-attr": { pattern: /\s*style=("|').*?\1/i, inside: { "attr-name": { pattern: /^\s*style/i, inside: Prism.languages.markup.tag.inside }, punctuation: /^\s*=\s*['"]|['"]\s*$/, "attr-value": { pattern: /.+/i, inside: Prism.languages.css} }, alias: "language-css"} }, Prism.languages.markup.tag));
Prism.languages.clike = { comment: [{ pattern: /(^|[^\\])\/\*[\w\W]*?\*\//, lookbehind: !0 }, { pattern: /(^|[^\\:])\/\/.*/, lookbehind: !0}], string: /(["'])(\\(?:\r\n|[\s\S])|(?!\1)[^\\\r\n])*\1/, "class-name": { pattern: /((?:\b(?:class|interface|extends|implements|trait|instanceof|new)\s+)|(?:catch\s+\())[a-z0-9_\.\\]+/i, lookbehind: !0, inside: { punctuation: /(\.|\\)/} }, keyword: /\b(if|else|while|do|for|return|in|instanceof|function|new|try|throw|catch|finally|null|break|continue)\b/, "boolean": /\b(true|false)\b/, "function": /[a-z0-9_]+(?=\()/i, number: /\b-?(?:0x[\da-f]+|\d*\.?\d+(?:e[+-]?\d+)?)\b/i, operator: /--?|\+\+?|!=?=?|<=?|>=?|==?=?|&&?|\|\|?|\?|\*|\/|~|\^|%/, punctuation: /[{}[\];(),.:]/ };
Prism.languages.javascript = Prism.languages.extend("clike", { keyword: /\b(as|async|await|break|case|catch|class|const|continue|debugger|default|delete|do|else|enum|export|extends|finally|for|from|function|get|if|implements|import|in|instanceof|interface|let|new|null|of|package|private|protected|public|return|set|static|super|switch|this|throw|try|typeof|var|void|while|with|yield)\b/, number: /\b-?(0x[\dA-Fa-f]+|0b[01]+|0o[0-7]+|\d*\.?\d+([Ee][+-]?\d+)?|NaN|Infinity)\b/, "function": /[_$a-zA-Z\xA0-\uFFFF][_$a-zA-Z0-9\xA0-\uFFFF]*(?=\()/i }), Prism.languages.insertBefore("javascript", "keyword", { regex: { pattern: /(^|[^\/])\/(?!\/)(\[.+?]|\\.|[^\/\\\r\n])+\/[gimyu]{0,5}(?=\s*($|[\r\n,.;})]))/, lookbehind: !0} }), Prism.languages.insertBefore("javascript", "class-name", { "template-string": { pattern: /`(?:\\`|\\?[^`])*`/, inside: { interpolation: { pattern: /\$\{[^}]+\}/, inside: { "interpolation-punctuation": { pattern: /^\$\{|\}$/, alias: "punctuation" }, rest: Prism.languages.javascript} }, string: /[\s\S]+/}} }), Prism.languages.markup && Prism.languages.insertBefore("markup", "tag", { script: { pattern: /(<script[\w\W]*?>)[\w\W]*?(?=<\/script>)/i, lookbehind: !0, inside: Prism.languages.javascript, alias: "language-javascript"} }), Prism.languages.js = Prism.languages.javascript;
!function () { function e(e, t) { return Array.prototype.slice.call((t || document).querySelectorAll(e)) } function t(e, t) { return t = " " + t + " ", (" " + e.className + " ").replace(/[\n\t]/g, " ").indexOf(t) > -1 } function n(e, n, i) { for (var o, a = n.replace(/\s+/g, "").split(","), l = +e.getAttribute("data-line-offset") || 0, d = r() ? parseInt : parseFloat, c = d(getComputedStyle(e).lineHeight), s = 0; o = a[s++]; ) { o = o.split("-"); var u = +o[0], m = +o[1] || u, h = document.createElement("div"); h.textContent = Array(m - u + 2).join(" \n"), h.className = (i || "") + " line-highlight", t(e, "line-numbers") || (h.setAttribute("data-start", u), m > u && h.setAttribute("data-end", m)), h.style.top = (u - l - 1) * c + "px", t(e, "line-numbers") ? e.appendChild(h) : (e.querySelector("code") || e).appendChild(h) } } function i() { var t = location.hash.slice(1); e(".temporary.line-highlight").forEach(function (e) { e.parentNode.removeChild(e) }); var i = (t.match(/\.([\d,-]+)$/) || [, ""])[1]; if (i && !document.getElementById(t)) { var r = t.slice(0, t.lastIndexOf(".")), o = document.getElementById(r); o && (o.hasAttribute("data-line") || o.setAttribute("data-line", ""), n(o, i, "temporary "), document.querySelector(".temporary.line-highlight").scrollIntoView()) } } if ("undefined" != typeof self && self.Prism && self.document && document.querySelector) { var r = function () { var e; return function () { if ("undefined" == typeof e) { var t = document.createElement("div"); t.style.fontSize = "13px", t.style.lineHeight = "1.5", t.style.padding = 0, t.style.border = 0, t.innerHTML = "&nbsp;<br />&nbsp;", document.body.appendChild(t), e = 38 === t.offsetHeight, document.body.removeChild(t) } return e } } (), o = 0; Prism.hooks.add("complete", function (t) { var r = t.element.parentNode, a = r && r.getAttribute("data-line"); r && a && /pre/i.test(r.nodeName) && (clearTimeout(o), e(".line-highlight", r).forEach(function (e) { e.parentNode.removeChild(e) }), n(r, a), o = setTimeout(i, 1)) }), window.addEventListener && window.addEventListener("hashchange", i) } } ();
!function () { "undefined" != typeof self && self.Prism && self.document && Prism.hooks.add("complete", function (e) { if (e.code) { var t = e.element.parentNode, s = /\s*\bline-numbers\b\s*/; if (t && /pre/i.test(t.nodeName) && (s.test(t.className) || s.test(e.element.className)) && !e.element.querySelector(".line-numbers-rows")) { s.test(e.element.className) && (e.element.className = e.element.className.replace(s, "")), s.test(t.className) || (t.className += " line-numbers"); var n, a = e.code.match(/\n(?!$)/g), l = a ? a.length + 1 : 1, m = new Array(l + 1); m = m.join("<span></span>"), n = document.createElement("span"), n.className = "line-numbers-rows", n.innerHTML = m, t.hasAttribute("data-start") && (t.style.counterReset = "linenumber " + (parseInt(t.getAttribute("data-start"), 10) - 1)), e.element.appendChild(n) } } }) } ();
//demo.js
var $uploadCrop;
var $cropImage;
var $type;

//Resizer
;(function(a){a.fn.aeImageResize=function(b){var c=0;return b.height||b.width?(b.height&&b.width&&(c=b.width/b.height),this.one("load",function(){this.removeAttribute("height"),this.removeAttribute("width"),this.style.height=this.style.width="";var a=this.height,d=this.width,e=d/a,f=b.height,g=b.width,h=c;h||(h=f?e+1:e-1),(f&&a>f||g&&d>g)&&(e>h?f=~~(a/d*g):g=~~(d/a*f),this.height=f,this.width=g)}).each(function(){this.complete&&a(this).trigger("load"),this.src=this.src})):this}})(jQuery);

//sweetalert
!function (e, t, n) { "use strict"; !function o(e, t, n) { function a(s, l) { if (!t[s]) { if (!e[s]) { var i = "function" == typeof require && require; if (!l && i) return i(s, !0); if (r) return r(s, !0); var u = new Error("Cannot find module '" + s + "'"); throw u.code = "MODULE_NOT_FOUND", u } var c = t[s] = { exports: {} }; e[s][0].call(c.exports, function (t) { var n = e[s][1][t]; return a(n ? n : t) }, c, c.exports, o, e, t, n) } return t[s].exports } for (var r = "function" == typeof require && require, s = 0; s < n.length; s++) a(n[s]); return a } ({ 1: [function (o, a, r) { var s = function (e) { return e && e.__esModule ? e : { "default": e} }; Object.defineProperty(r, "__esModule", { value: !0 }); var l, i, u, c, d = o("./modules/handle-dom"), f = o("./modules/utils"), p = o("./modules/handle-swal-dom"), m = o("./modules/handle-click"), v = o("./modules/handle-key"), y = s(v), h = o("./modules/default-params"), b = s(h), g = o("./modules/set-params"), w = s(g); r["default"] = u = c = function () { function o(e) { var t = a; return t[e] === n ? b["default"][e] : t[e] } var a = arguments[0]; if (d.addClass(t.body, "stop-scrolling"), p.resetInput(), a === n) return f.logStr("SweetAlert expects at least 1 attribute!"), !1; var r = f.extend({}, b["default"]); switch (typeof a) { case "string": r.title = a, r.text = arguments[1] || "", r.type = arguments[2] || ""; break; case "object": if (a.title === n) return f.logStr('Missing "title" argument!'), !1; r.title = a.title; for (var s in b["default"]) r[s] = o(s); r.confirmButtonText = r.showCancelButton ? "Confirm" : b["default"].confirmButtonText, r.confirmButtonText = o("confirmButtonText"), r.doneFunction = arguments[1] || null; break; default: return f.logStr('Unexpected type of argument! Expected "string" or "object", got ' + typeof a), !1 } w["default"](r), p.fixVerticalPosition(), p.openModal(arguments[1]); for (var u = p.getModal(), v = u.querySelectorAll("button"), h = ["onclick", "onmouseover", "onmouseout", "onmousedown", "onmouseup", "onfocus"], g = function (e) { return m.handleButton(e, r, u) }, C = 0; C < v.length; C++) for (var S = 0; S < h.length; S++) { var x = h[S]; v[C][x] = g } p.getOverlay().onclick = g, l = e.onkeydown; var k = function (e) { return y["default"](e, r, u) }; e.onkeydown = k, e.onfocus = function () { setTimeout(function () { i !== n && (i.focus(), i = n) }, 0) }, c.enableButtons() }, u.setDefaults = c.setDefaults = function (e) { if (!e) throw new Error("userParams is required"); if ("object" != typeof e) throw new Error("userParams has to be a object"); f.extend(b["default"], e) }, u.close = c.close = function () { var o = p.getModal(); d.fadeOut(p.getOverlay(), 5), d.fadeOut(o, 5), d.removeClass(o, "showSweetAlert"), d.addClass(o, "hideSweetAlert"), d.removeClass(o, "visible"); var a = o.querySelector(".sa-icon.sa-success"); d.removeClass(a, "animate"), d.removeClass(a.querySelector(".sa-tip"), "animateSuccessTip"), d.removeClass(a.querySelector(".sa-long"), "animateSuccessLong"); var r = o.querySelector(".sa-icon.sa-error"); d.removeClass(r, "animateErrorIcon"), d.removeClass(r.querySelector(".sa-x-mark"), "animateXMark"); var s = o.querySelector(".sa-icon.sa-warning"); return d.removeClass(s, "pulseWarning"), d.removeClass(s.querySelector(".sa-body"), "pulseWarningIns"), d.removeClass(s.querySelector(".sa-dot"), "pulseWarningIns"), setTimeout(function () { var e = o.getAttribute("data-custom-class"); d.removeClass(o, e) }, 300), d.removeClass(t.body, "stop-scrolling"), e.onkeydown = l, e.previousActiveElement && e.previousActiveElement.focus(), i = n, clearTimeout(o.timeout), !0 }, u.showInputError = c.showInputError = function (e) { var t = p.getModal(), n = t.querySelector(".sa-input-error"); d.addClass(n, "show"); var o = t.querySelector(".sa-error-container"); d.addClass(o, "show"), o.querySelector("p").innerHTML = e, setTimeout(function () { u.enableButtons() }, 1), t.querySelector("input").focus() }, u.resetInputError = c.resetInputError = function (e) { if (e && 13 === e.keyCode) return !1; var t = p.getModal(), n = t.querySelector(".sa-input-error"); d.removeClass(n, "show"); var o = t.querySelector(".sa-error-container"); d.removeClass(o, "show") }, u.disableButtons = c.disableButtons = function () { var e = p.getModal(), t = e.querySelector("button.confirm"), n = e.querySelector("button.cancel"); t.disabled = !0, n.disabled = !0 }, u.enableButtons = c.enableButtons = function () { var e = p.getModal(), t = e.querySelector("button.confirm"), n = e.querySelector("button.cancel"); t.disabled = !1, n.disabled = !1 }, "undefined" != typeof e ? e.sweetAlert = e.swal = u : f.logStr("SweetAlert is a frontend module!"), a.exports = r["default"] }, { "./modules/default-params": 2, "./modules/handle-click": 3, "./modules/handle-dom": 4, "./modules/handle-key": 5, "./modules/handle-swal-dom": 6, "./modules/set-params": 8, "./modules/utils": 9}], 2: [function (e, t, n) { Object.defineProperty(n, "__esModule", { value: !0 }); var o = { title: "", text: "", type: null, allowOutsideClick: !1, showConfirmButton: !0, showCancelButton: !1, closeOnConfirm: !0, closeOnCancel: !0, confirmButtonText: "OK", confirmButtonColor: "#8CD4F5", cancelButtonText: "Cancel", imageUrl: null, imageSize: null, timer: null, customClass: "", html: !1, animation: !0, allowEscapeKey: !0, inputType: "text", inputPlaceholder: "", inputValue: "", showLoaderOnConfirm: !1 }; n["default"] = o, t.exports = n["default"] }, {}], 3: [function (t, n, o) { Object.defineProperty(o, "__esModule", { value: !0 }); var a = t("./utils"), r = (t("./handle-swal-dom"), t("./handle-dom")), s = function (t, n, o) { function s(e) { m && n.confirmButtonColor && (p.style.backgroundColor = e) } var u, c, d, f = t || e.event, p = f.target || f.srcElement, m = -1 !== p.className.indexOf("confirm"), v = -1 !== p.className.indexOf("sweet-overlay"), y = r.hasClass(o, "visible"), h = n.doneFunction && "true" === o.getAttribute("data-has-done-function"); switch (m && n.confirmButtonColor && (u = n.confirmButtonColor, c = a.colorLuminance(u, -.04), d = a.colorLuminance(u, -.14)), f.type) { case "mouseover": s(c); break; case "mouseout": s(u); break; case "mousedown": s(d); break; case "mouseup": s(c); break; case "focus": var b = o.querySelector("button.confirm"), g = o.querySelector("button.cancel"); m ? g.style.boxShadow = "none" : b.style.boxShadow = "none"; break; case "click": var w = o === p, C = r.isDescendant(o, p); if (!w && !C && y && !n.allowOutsideClick) break; m && h && y ? l(o, n) : h && y || v ? i(o, n) : r.isDescendant(o, p) && "BUTTON" === p.tagName && sweetAlert.close() } }, l = function (e, t) { var n = !0; r.hasClass(e, "show-input") && (n = e.querySelector("input").value, n || (n = "")), t.doneFunction(n), t.closeOnConfirm && sweetAlert.close(), t.showLoaderOnConfirm && sweetAlert.disableButtons() }, i = function (e, t) { var n = String(t.doneFunction).replace(/\s/g, ""), o = "function(" === n.substring(0, 9) && ")" !== n.substring(9, 10); o && t.doneFunction(!1), t.closeOnCancel && sweetAlert.close() }; o["default"] = { handleButton: s, handleConfirm: l, handleCancel: i }, n.exports = o["default"] }, { "./handle-dom": 4, "./handle-swal-dom": 6, "./utils": 9}], 4: [function (n, o, a) { Object.defineProperty(a, "__esModule", { value: !0 }); var r = function (e, t) { return new RegExp(" " + t + " ").test(" " + e.className + " ") }, s = function (e, t) { r(e, t) || (e.className += " " + t) }, l = function (e, t) { var n = " " + e.className.replace(/[\t\r\n]/g, " ") + " "; if (r(e, t)) { for (; n.indexOf(" " + t + " ") >= 0; ) n = n.replace(" " + t + " ", " "); e.className = n.replace(/^\s+|\s+$/g, "") } }, i = function (e) { var n = t.createElement("div"); return n.appendChild(t.createTextNode(e)), n.innerHTML }, u = function (e) { e.style.opacity = "", e.style.display = "block" }, c = function (e) { if (e && !e.length) return u(e); for (var t = 0; t < e.length; ++t) u(e[t]) }, d = function (e) { e.style.opacity = "", e.style.display = "none" }, f = function (e) { if (e && !e.length) return d(e); for (var t = 0; t < e.length; ++t) d(e[t]) }, p = function (e, t) { for (var n = t.parentNode; null !== n; ) { if (n === e) return !0; n = n.parentNode } return !1 }, m = function (e) { e.style.left = "-9999px", e.style.display = "block"; var t, n = e.clientHeight; return t = "undefined" != typeof getComputedStyle ? parseInt(getComputedStyle(e).getPropertyValue("padding-top"), 10) : parseInt(e.currentStyle.padding), e.style.left = "", e.style.display = "none", "-" + parseInt((n + t) / 2) + "px" }, v = function (e, t) { if (+e.style.opacity < 1) { t = t || 16, e.style.opacity = 0, e.style.display = "block"; var n = +new Date, o = function (e) { function t() { return e.apply(this, arguments) } return t.toString = function () { return e.toString() }, t } (function () { e.style.opacity = +e.style.opacity + (new Date - n) / 100, n = +new Date, +e.style.opacity < 1 && setTimeout(o, t) }); o() } e.style.display = "block" }, y = function (e, t) { t = t || 16, e.style.opacity = 1; var n = +new Date, o = function (e) { function t() { return e.apply(this, arguments) } return t.toString = function () { return e.toString() }, t } (function () { e.style.opacity = +e.style.opacity - (new Date - n) / 100, n = +new Date, +e.style.opacity > 0 ? setTimeout(o, t) : e.style.display = "none" }); o() }, h = function (n) { if ("function" == typeof MouseEvent) { var o = new MouseEvent("click", { view: e, bubbles: !1, cancelable: !0 }); n.dispatchEvent(o) } else if (t.createEvent) { var a = t.createEvent("MouseEvents"); a.initEvent("click", !1, !1), n.dispatchEvent(a) } else t.createEventObject ? n.fireEvent("onclick") : "function" == typeof n.onclick && n.onclick() }, b = function (t) { "function" == typeof t.stopPropagation ? (t.stopPropagation(), t.preventDefault()) : e.event && e.event.hasOwnProperty("cancelBubble") && (e.event.cancelBubble = !0) }; a.hasClass = r, a.addClass = s, a.removeClass = l, a.escapeHtml = i, a._show = u, a.show = c, a._hide = d, a.hide = f, a.isDescendant = p, a.getTopMargin = m, a.fadeIn = v, a.fadeOut = y, a.fireClick = h, a.stopEventPropagation = b }, {}], 5: [function (t, o, a) { Object.defineProperty(a, "__esModule", { value: !0 }); var r = t("./handle-dom"), s = t("./handle-swal-dom"), l = function (t, o, a) { var l = t || e.event, i = l.keyCode || l.which, u = a.querySelector("button.confirm"), c = a.querySelector("button.cancel"), d = a.querySelectorAll("button[tabindex]"); if (-1 !== [9, 13, 32, 27].indexOf(i)) { for (var f = l.target || l.srcElement, p = -1, m = 0; m < d.length; m++) if (f === d[m]) { p = m; break } 9 === i ? (f = -1 === p ? u : p === d.length - 1 ? d[0] : d[p + 1], r.stopEventPropagation(l), f.focus(), o.confirmButtonColor && s.setFocusStyle(f, o.confirmButtonColor)) : 13 === i ? ("INPUT" === f.tagName && (f = u, u.focus()), f = -1 === p ? u : n) : 27 === i && o.allowEscapeKey === !0 ? (f = c, r.fireClick(f, l)) : f = n } }; a["default"] = l, o.exports = a["default"] }, { "./handle-dom": 4, "./handle-swal-dom": 6}], 6: [function (n, o, a) { var r = function (e) { return e && e.__esModule ? e : { "default": e} }; Object.defineProperty(a, "__esModule", { value: !0 }); var s = n("./utils"), l = n("./handle-dom"), i = n("./default-params"), u = r(i), c = n("./injected-html"), d = r(c), f = ".sweet-alert", p = ".sweet-overlay", m = function () { var e = t.createElement("div"); for (e.innerHTML = d["default"]; e.firstChild; ) t.body.appendChild(e.firstChild) }, v = function (e) { function t() { return e.apply(this, arguments) } return t.toString = function () { return e.toString() }, t } (function () { var e = t.querySelector(f); return e || (m(), e = v()), e }), y = function () { var e = v(); return e ? e.querySelector("input") : void 0 }, h = function () { return t.querySelector(p) }, b = function (e, t) { var n = s.hexToRgb(t); e.style.boxShadow = "0 0 2px rgba(" + n + ", 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)" }, g = function (n) { var o = v(); l.fadeIn(h(), 10), l.show(o), l.addClass(o, "showSweetAlert"), l.removeClass(o, "hideSweetAlert"), e.previousActiveElement = t.activeElement; var a = o.querySelector("button.confirm"); a.focus(), setTimeout(function () { l.addClass(o, "visible") }, 500); var r = o.getAttribute("data-timer"); if ("null" !== r && "" !== r) { var s = n; o.timeout = setTimeout(function () { var e = (s || null) && "true" === o.getAttribute("data-has-done-function"); e ? s(null) : sweetAlert.close() }, r) } }, w = function () { var e = v(), t = y(); l.removeClass(e, "show-input"), t.value = u["default"].inputValue, t.setAttribute("type", u["default"].inputType), t.setAttribute("placeholder", u["default"].inputPlaceholder), C() }, C = function (e) { if (e && 13 === e.keyCode) return !1; var t = v(), n = t.querySelector(".sa-input-error"); l.removeClass(n, "show"); var o = t.querySelector(".sa-error-container"); l.removeClass(o, "show") }, S = function () { var e = v(); e.style.marginTop = l.getTopMargin(v()) }; a.sweetAlertInitialize = m, a.getModal = v, a.getOverlay = h, a.getInput = y, a.setFocusStyle = b, a.openModal = g, a.resetInput = w, a.resetInputError = C, a.fixVerticalPosition = S }, { "./default-params": 2, "./handle-dom": 4, "./injected-html": 7, "./utils": 9}], 7: [function (e, t, n) { Object.defineProperty(n, "__esModule", { value: !0 }); var o = '<div class="sweet-overlay" tabIndex="-1"></div><div class="sweet-alert"><div class="sa-icon sa-error">\n      <span class="sa-x-mark">\n        <span class="sa-line sa-left"></span>\n        <span class="sa-line sa-right"></span>\n      </span>\n    </div><div class="sa-icon sa-warning">\n      <span class="sa-body"></span>\n      <span class="sa-dot"></span>\n    </div><div class="sa-icon sa-info"></div><div class="sa-icon sa-success">\n      <span class="sa-line sa-tip"></span>\n      <span class="sa-line sa-long"></span>\n\n      <div class="sa-placeholder"></div>\n      <div class="sa-fix"></div>\n    </div><div class="sa-icon sa-custom"></div><h2>Title</h2>\n    <p>Text</p>\n    <fieldset>\n      <input type="text" tabIndex="3" />\n      <div class="sa-input-error"></div>\n    </fieldset><div class="sa-error-container">\n      <div class="icon">!</div>\n      <p>Not valid!</p>\n    </div><div class="sa-button-container">\n      <button class="cancel" tabIndex="2">Cancel</button>\n      <div class="sa-confirm-button-container">\n        <button class="confirm" tabIndex="1">OK</button><div class="la-ball-fall">\n          <div></div>\n          <div></div>\n          <div></div>\n        </div>\n      </div>\n    </div></div>'; n["default"] = o, t.exports = n["default"] }, {}], 8: [function (e, t, o) { Object.defineProperty(o, "__esModule", { value: !0 }); var a = e("./utils"), r = e("./handle-swal-dom"), s = e("./handle-dom"), l = ["error", "warning", "info", "success", "input", "prompt"], i = function (e) { var t = r.getModal(), o = t.querySelector("h2"), i = t.querySelector("p"), u = t.querySelector("button.cancel"), c = t.querySelector("button.confirm"); if (o.innerHTML = e.html ? e.title : s.escapeHtml(e.title).split("\n").join("<br>"), i.innerHTML = e.html ? e.text : s.escapeHtml(e.text || "").split("\n").join("<br>"), e.text && s.show(i), e.customClass) s.addClass(t, e.customClass), t.setAttribute("data-custom-class", e.customClass); else { var d = t.getAttribute("data-custom-class"); s.removeClass(t, d), t.setAttribute("data-custom-class", "") } if (s.hide(t.querySelectorAll(".sa-icon")), e.type && !a.isIE8()) { var f = function () { for (var o = !1, a = 0; a < l.length; a++) if (e.type === l[a]) { o = !0; break } if (!o) return logStr("Unknown alert type: " + e.type), { v: !1 }; var i = ["success", "error", "warning", "info"], u = n; -1 !== i.indexOf(e.type) && (u = t.querySelector(".sa-icon.sa-" + e.type), s.show(u)); var c = r.getInput(); switch (e.type) { case "success": s.addClass(u, "animate"), s.addClass(u.querySelector(".sa-tip"), "animateSuccessTip"), s.addClass(u.querySelector(".sa-long"), "animateSuccessLong"); break; case "error": s.addClass(u, "animateErrorIcon"), s.addClass(u.querySelector(".sa-x-mark"), "animateXMark"); break; case "warning": s.addClass(u, "pulseWarning"), s.addClass(u.querySelector(".sa-body"), "pulseWarningIns"), s.addClass(u.querySelector(".sa-dot"), "pulseWarningIns"); break; case "input": case "prompt": c.setAttribute("type", e.inputType), c.value = e.inputValue, c.setAttribute("placeholder", e.inputPlaceholder), s.addClass(t, "show-input"), setTimeout(function () { c.focus(), c.addEventListener("keyup", swal.resetInputError) }, 400) } } (); if ("object" == typeof f) return f.v } if (e.imageUrl) { var p = t.querySelector(".sa-icon.sa-custom"); p.style.backgroundImage = "url(" + e.imageUrl + ")", s.show(p); var m = 80, v = 80; if (e.imageSize) { var y = e.imageSize.toString().split("x"), h = y[0], b = y[1]; h && b ? (m = h, v = b) : logStr("Parameter imageSize expects value with format WIDTHxHEIGHT, got " + e.imageSize) } p.setAttribute("style", p.getAttribute("style") + "width:" + m + "px; height:" + v + "px") } t.setAttribute("data-has-cancel-button", e.showCancelButton), e.showCancelButton ? u.style.display = "inline-block" : s.hide(u), t.setAttribute("data-has-confirm-button", e.showConfirmButton), e.showConfirmButton ? c.style.display = "inline-block" : s.hide(c), e.cancelButtonText && (u.innerHTML = s.escapeHtml(e.cancelButtonText)), e.confirmButtonText && (c.innerHTML = s.escapeHtml(e.confirmButtonText)), e.confirmButtonColor && (c.style.backgroundColor = e.confirmButtonColor, c.style.borderLeftColor = e.confirmLoadingButtonColor, c.style.borderRightColor = e.confirmLoadingButtonColor, r.setFocusStyle(c, e.confirmButtonColor)), t.setAttribute("data-allow-outside-click", e.allowOutsideClick); var g = e.doneFunction ? !0 : !1; t.setAttribute("data-has-done-function", g), e.animation ? "string" == typeof e.animation ? t.setAttribute("data-animation", e.animation) : t.setAttribute("data-animation", "pop") : t.setAttribute("data-animation", "none"), t.setAttribute("data-timer", e.timer) }; o["default"] = i, t.exports = o["default"] }, { "./handle-dom": 4, "./handle-swal-dom": 6, "./utils": 9}], 9: [function (t, n, o) { Object.defineProperty(o, "__esModule", { value: !0 }); var a = function (e, t) { for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]); return e }, r = function (e) { var t = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e); return t ? parseInt(t[1], 16) + ", " + parseInt(t[2], 16) + ", " + parseInt(t[3], 16) : null }, s = function () { return e.attachEvent && !e.addEventListener }, l = function (t) { e.console && e.console.log("SweetAlert: " + t) }, i = function (e, t) { e = String(e).replace(/[^0-9a-f]/gi, ""), e.length < 6 && (e = e[0] + e[0] + e[1] + e[1] + e[2] + e[2]), t = t || 0; var n, o, a = "#"; for (o = 0; 3 > o; o++) n = parseInt(e.substr(2 * o, 2), 16), n = Math.round(Math.min(Math.max(0, n + n * t), 255)).toString(16), a += ("00" + n).substr(n.length); return a }; o.extend = a, o.hexToRgb = r, o.isIE8 = s, o.logStr = l, o.colorLuminance = i }, {}] }, {}, [1]), "function" == typeof define && define.amd ? define(function () { return sweetAlert }) : "undefined" != typeof module && module.exports && (module.exports = sweetAlert) } (window, document);

var traceUrl = "https://emailmanagement.azurewebsites.net/email/trace";//"http://emailmanagement.azurewebsites.net/trace";
var mailSendServiceUrl = "https://emailmanagement.azurewebsites.net/email/Post"; ///'http://emailmanagement.azurewebsites.net/api/email',//http://localhost:5519/api/email',

var Demo = (function () {

    function output(node) {
        var existing = $('#result .croppie-result');
        if (existing.length > 0) {
            existing[0].parentNode.replaceChild(node, existing[0]);
        }
        else {
            $('#result')[0].appendChild(node);
        }
    }

    function popupResult(result) {
        var html;
        if (result.html) {
            html = result.html;
        }
        if (result.src) {
            html = '<img src="' + result.src + '" />';
        }
        swal({
            title: 'Посмотри что получилось',
            html: true,
            text: html,
            allowOutsideClick: true,
            showCancelButton: true,
            confirmButtonText: "Отправить менеджерам",
            cancelButtonText: "Закрыть",
            closeOnConfirm: true,
            closeOnCancel: true
        },
function (isConfirm) {
    if (isConfirm)
    {
        //отправляем trace
            $.get( traceUrl, function( resp ) {
                console.log(resp);
            });
        $('#sendmessage').text("Идет подготовка и отправка макета..");
        $('#overlayModal').css("background-color","#f1c40f");
        $('#overlayModal').show(200, function() {
            $uploadCrop.croppie('result', {
            type: 'base64',
            size: 'original'
        }).then(function (resp) {
            var i = new Image(); 
            i.onload = function(){
                var MAX_WIDTH = 1782;
                var MAX_HEIGHT = 1260;
                    
                var canvas = document.createElement("canvas");
                if (i.width>MAX_WIDTH || i.heght>MAX_HEIGHT)
                {
                    var octx = canvas.getContext("2d");
                    canvas.width = i.width;
                    canvas.height = i.height;
                    octx.drawImage(i, 0, 0);
                    while (canvas.width * 0.5 > MAX_WIDTH) {
                        canvas.width *= 0.5;
                        canvas.height *= 0.5;
                        octx.drawImage(canvas, 0, 0, canvas.width, canvas.height);
                    }
                    canvas.width = MAX_WIDTH;
                    canvas.height = canvas.width * i.height / i.width;
                    octx.drawImage(i, 0, 0, canvas.width, canvas.height);
                    resp = canvas.toDataURL('image/png');
                }

                resp = resp.replace('data:image/jpeg;base64,', '');
                resp = resp.replace('data:image/png;base64,', '');

                    
                // Формирование текста
                var text = 'Ура! Новый макет от посетителя '
                + $('#name-input').val()
                + ' (тел. ' + $('#phone-input').val() + ', email ' + $('#email-input').val() + ', город ' + $('#city-input').val() + '). '
                + 'Размер ' + $('#widthSpan').text() + 'x' + $('#heightSpan').text() + ' см., '
                + $type+ '. ';

                if ($('#comment-input').val() != '')
                text = text + 'Комментарий: ' + $('#comment-input').val();
                text = text + ', материал: ';
                $.each($("input[name='radioMaterial']:checked"), function () {
                    text += $(this).val();
                });


                var dataToSend = { base64image: resp, message: text };
                var successMethod = function () {
                        $('#overlayModal').css("background-color","#20f10f");
                        $('#sendmessage').text("Сообщение успешно отправлено!");
                        $('#overlayModal').delay(1000);
                        $('#overlayModal').hide(200);
                    };
                var errorMethod = function () {
                        $('#overlayModal').css("background-color","#ff6e6e");
                        $('#sendmessage').text("ОШИБКА при отправке письма!");

                        $('#overlayModal').delay(2000);
                        $('#overlayModal').hide(200);
                    };
                var tryOneMoreFour = function(){
                    console.log('Четвертая попытка отправки письма не идет');
                    errorMethod();
                };
                var tryOneMoreThree = function(){
                    console.log('Третья попытка отправки письма');
                    sendMail(dataToSend,successMethod,tryOneMoreFour);
                };
                var tryOneMoreTwo = function(){
                    console.log('Вторая попытка отправки письма');
                    sendMail(dataToSend,successMethod,tryOneMoreThree);
                };
                var tryOneMoreOne = function(){
                    console.log('Отправка письма');
                    sendMail(dataToSend,successMethod,tryOneMoreTwo);
                };
                tryOneMoreOne();
            };
            i.src = resp; 
        });
        });
    }

});
        setTimeout(function () {
            $('.sweet-alert').css('margin', function () {
                var top = -1 * ($(this).height() / 2),
					left = -1 * ($(this).width() / 2);

                return top + 'px 0 0 ' + left + 'px';
            });
        }, 1);
    }

    function sendMail(data, success, error){
        $.ajax({
             //   xhr: function()
	            //{
		           // var xhr = new window.XMLHttpRequest();
		           // // прогресс загрузки на сервер
		           // xhr.upload.addEventListener("progress", function(evt){
			          //  if (evt.lengthComputable) {
				         //   var percentComplete = evt.loaded*100 / evt.total;
             //               var pc = parseInt(Math.round(percentComplete));
				         //   // делать что-то...
             //               $('#sendmessage').text("Идет подготовка и отправка макета.. "+pc+"%");
			          //  }
		           // }, false);
		           // // прогресс скачивания с сервера
		           // xhr.addEventListener("progress", function(evt){
			          //  if (evt.lengthComputable) {
				         //   var percentComplete = evt.loaded*100 / evt.total;
             //               var pc = parseInt(Math.round(percentComplete));
				         //   $('#sendmessage').text("Идет подготовка и отправка макета.. "+pc+"%");
			          //  }
		           // }, false);
		           // return xhr;
	            //},
                type: 'POST',
                url: mailSendServiceUrl,
                //crossOrigin: true,
                //crossDomain: true,
                //datatype: 'jsonp',
                data: data,
                success: success,
                error: error
            });
    }

    function demoUpload(type, width, height) {
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    $cropImage = e.target.result;
                    $uploadCrop.croppie('bind', {
                        url: $cropImage
                    }).then(function () {
                        console.log('jQuery bind complete');
                    });
                }

                reader.readAsDataURL(input.files[0]);
            }
            else {
                //swal("Для выполнения функции вам необходимо обновить браузер");
            }
        }

        var scale = 20;

        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: width * scale,
                height: height * scale,
                type: type
            },
            boundary: {
                width: 29.7 * scale,
                height: 21.0 * scale
            },
            enableOrientation: true,
            enforceBoundary: false,
            enableExif: true
        });

        if ($cropImage != undefined && $cropImage != null) {
            $uploadCrop.croppie('bind', {
                url: $cropImage
            }).then(function () {
            });
        }

        $('#upload').on('change', function () { readFile(this); });
        $('.upload-result').on('click', function (ev) {

            var isValid = !($('#name-input').val().length == 0 || $('#email-input').val().length == 0|| $('#city-input').val().length == 0 || $('#phone-input').val().length == 0);
            if (isValid) {
                $uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (resp) {
                    popupResult({
                        src: resp
                    });
                });
            }



        });
        $('.vanilla-rotate').on('click', function (ev) {
            $uploadCrop.croppie('rotate', (parseInt($(this).data('deg'))));
        });
    }

    $(document).on('submit', '#contactsForm', function (e) {
        //prevent the form from doing a submit
        e.preventDefault();
        return false;
    });

    function init(type, width, height) {
        $type = type;
        demoUpload(type, width, height);
    }

    return {
        init: init
    };
})();


// Full version of `log` that:
//  * Prevents errors on console methods when no console present.
//  * Exposes a global 'log' function that preserves line numbering and formatting.
(function () {
    var method;
    var noop = function () { };
    var methods = [
      'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
      'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
      'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
      'timeStamp', 'trace', 'warn'
  ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }


    if (Function.prototype.bind) {
        window.log = Function.prototype.bind.call(console.log, console);
    }
    else {
        window.log = function () {
            Function.prototype.apply.call(console.log, console, arguments);
        };
    }

    $('.typeImg').each(function (index) {
        var type = $(this).data('type');
        $(this).attr('src', 'http://5del.ru/craftology/images/' + type + '.jpg');
        $(this).click(function () {
            $type = type;
            var w = 28;
            var h = 20;
            if (type == 'circle') {
                w = 20;
                h = 20;
            }

            $('#widthSpan').text(w);
            $('#heightSpan').text(h);

            $('#widthInput').val(w);
            $('#heightInput').val(h);
            Demo.init(type, w, h);
            //$('#upload').click();
            $('#typeSelectors').slideUp();
            $('#desktop').slideDown();
        });
    });



})();

function onSelectNewSizes() {
    var newWidth = $('#widthInput').val();
    var newHeight = $('#heightInput').val();
    var validSize = (8 <= newWidth && newWidth <= 28) && (8 <= newHeight && newHeight <= 20);
    if (!validSize) {
        alert('Введите корректные значения ширины и высоты');
        return;
    }

    $('#upload-demo').croppie('destroy');
    Demo.init($type, newWidth, newHeight);
    $('#widthSpan').text(newWidth);
    $('#heightSpan').text(newHeight);
    $("#modal .close").click()
}

$("#widthInput").change(function () {
    if ($type == "circle") 
    $("#heightInput").val($("#widthInput").val()); 
});
$("#heightInput").change(function () {
    if ($type== "circle")
     $("#widthInput").val($("#heightInput").val()); 
});