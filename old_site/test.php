<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");


function SetBuyerValues($ID)
{
    if (CModule::IncludeModule("sale")) {

        global $USER;

        $arUser=CUser::GetByID($USER->GetID())->Fetch();

        $db_vals = CSaleOrderPropsValue::GetList(
                    array("SORT" => "ASC"),
                    array(
                            "ORDER_ID" => $ID,
                        )
                );
        $out=array();
        while ($arVals = $db_vals->Fetch())
        {
            if ($arVals["CODE"]=="CITY") {
                $arLoc=CSaleLocation::GetByID($arVals["VALUE"]);
                if ($arLoc) {
                    if (!$arUser["PERSONAL_CITY"])
                        $out["PERSONAL_CITY"]=$arLoc["CITY_NAME"];
                    if (!$arUser["PERSONAL_COUNTRY"])
                        $out["PERSONAL_COUNTRY"]=$arLoc["COUNTRY_ID"];
                }
            } else
                if ($arVals["CODE"]=="ZIP" && !$arUser["PERSONAL_ZIP"])
                    $out["PERSONAL_ZIP"]=$arVals["VALUE"];
        }
        if (count($out)>0)
        {
            $user = new CUser;
            $user->Update($USER->GetID(), $out);
        }
    }
}

SetBuyerValues(12);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>