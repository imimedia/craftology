<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оптовое сотрудничество");
?><b>Мы открыты к сотрудничеству, поэтому:</b><br>
 <br>
<ul>
	<li>Предлагаем заключить договор с юр.лицами и ИП.<br>
 </li>
	<li>При постоянных заказах дадим скидки за объем.</li>
	<li>Готовы на постоянной основе снабжать Вас необходимым сырьем&nbsp;на специальных условиях.</li>
</ul>
 Наши контакты:<br>
 тел. 8-800-500-30-47<br>
 <a href="mailto:info@craftology.ru">info@craftology.ru</a><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>