<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
?><h1>Если у Вас возникают проблемы с оформлением заказа - позвоните 8 800 500 30 47</h1>
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.order.ajax", 
	"main", 
	array(
		"ADDITIONAL_PICT_PROP_3" => "-",
		"ADDITIONAL_PICT_PROP_4" => "-",
		"ALLOW_AUTO_REGISTER" => "Y",
		"ALLOW_USER_PROFILES" => "N",
		"BASKET_IMAGES_SCALING" => "standard",
		"BASKET_POSITION" => "after",
		"COMPATIBLE_MODE" => "Y",
		"DELIVERIES_PER_PAGE" => "8",
		"DELIVERY_FADE_EXTRA_SERVICES" => "N",
		"DELIVERY_NO_AJAX" => "Y",
		"DELIVERY_NO_SESSION" => "N",
		"DELIVERY_TO_PAYSYSTEM" => "d2p",
		"DISABLE_BASKET_REDIRECT" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"PATH_TO_AUTH" => "",
		"PATH_TO_BASKET" => "/cart/",
		"PATH_TO_PAYMENT" => "payment.php",
		"PATH_TO_PERSONAL" => "/profile/",
		"PAY_FROM_ACCOUNT" => "N",
		"PAY_SYSTEMS_PER_PAGE" => "8",
		"PICKUPS_PER_PAGE" => "5",
		"PRODUCT_COLUMNS_HIDDEN" => "",
		"PRODUCT_COLUMNS_VISIBLE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "PROPS",
		),
		"SEND_NEW_USER_NOTIFY" => "Y",
		"SERVICES_IMAGES_SCALING" => "standard",
		"SET_TITLE" => "Y",
		"SHOW_BASKET_HEADERS" => "N",
		"SHOW_COUPONS_BASKET" => "N",
		"SHOW_COUPONS_DELIVERY" => "N",
		"SHOW_COUPONS_PAY_SYSTEM" => "N",
		"SHOW_DELIVERY_INFO_NAME" => "Y",
		"SHOW_DELIVERY_LIST_NAMES" => "Y",
		"SHOW_DELIVERY_PARENT_NAMES" => "Y",
		"SHOW_MAP_IN_PROPS" => "N",
		"SHOW_NEAREST_PICKUP" => "N",
		"SHOW_ORDER_BUTTON" => "final_step",
		"SHOW_PAY_SYSTEM_INFO_NAME" => "Y",
		"SHOW_PAY_SYSTEM_LIST_NAMES" => "Y",
		"SHOW_STORES_IMAGES" => "Y",
		"SHOW_TOTAL_ORDER_BUTTON" => "N",
		"SKIP_USELESS_BLOCK" => "Y",
		"TEMPLATE_LOCATION" => "popup",
		"TEMPLATE_THEME" => "site",
		"USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
		"USE_CUSTOM_ERROR_MESSAGES" => "N",
		"USE_CUSTOM_MAIN_MESSAGES" => "N",
		"USE_PREPAYMENT" => "N",
		"USE_YM_GOALS" => "N",
		"COMPONENT_TEMPLATE" => "main",
		"PROPS_FADE_LIST_1" => "",
		"ALLOW_NEW_PROFILE" => "N",
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"ALLOW_APPEND_ORDER" => "Y",
		"SHOW_NOT_CALCULATED_DELIVERIES" => "L",
		"SHOW_VAT_PRICE" => "Y",
		"USE_PRELOAD" => "Y",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"ACTION_VARIABLE" => "action",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"SPOT_LOCATION_BY_GEOIP" => "Y"
	),
	false
);?>

<?
	global $USER;
	if ($USER->IsAuthorized()){
		
		$rsUser = CUser::GetByID($USER->GetID()); //$USER->GetID() - получаем ID авторизованного пользователя  и сразу же - его поля
		$arUser = $rsUser->Fetch();
	
//		$city=$arUser['PERSONAL_CITY'];
        $zip=$arUser['PERSONAL_ZIP'];
        $arCity = CSaleLocation::GetByZIP($zip);
        $city = $arCity["CITY_ID"];
        if($city>0)
            $def_value=$city;
        else
            $def_value = 0;
//      echo '<pre>', print_r($def_value), '</pre>' ;

//		if (isset($city) && !empty($city))
//	    {
//		    $zip=UserEx::getInstance()->getField('PERSONAL_ZIP');
//			$rsLocation = CSaleLocation::GetList(
//		        array(
//		                "SORT" => "ASC",
//		                "COUNTRY_NAME_LANG" => "ASC",
//		                "CITY_NAME_LANG" => "ASC"
//		            ),
//		        array("CITY_NAME" => $city),
//		        false,
//		        false,
//		        array()
//		    );
//
//
//		    if ($arLocation=$rsLocation->Fetch()){
//			  $def_value = $arLocation["ID"];
//			}
//		}
?>
    <?if($def_value > 0){?>
            <script>
                $(document).ready(

                    function(){
                        $('input[name = ORDER_PROP_1]').val(<?=$def_value?>);
                        submitForm();
                    }
                );
            </script>
        <?}?>
	        
<?}?>

<script>
    $('#ORDER_PROP_5').bind("keyup", function() {
        if (this.value.match(/[А-Яа-я]|\s/g)) {
            this.value = this.value.replace(/[А-Яа-я]|\s/g, '');
        }
    });
//	$('#ORDER_PROP_5').on('keyup', function() {
//      var that = this;
//        alert(that);
//      setTimeout(function() {
//
//        var res = /[^a-zA-Z@0-9\._-]/g.exec(that.value);
//        that.value = that.value.replace(res, '');
//      }, 0);
//      submitForm();
//    });
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>