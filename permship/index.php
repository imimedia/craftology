<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "все для кондитера, товары для кондитера, магазин для кондитера, магазин кондитерских принадлежностей, кондитерский интернет-магазин");
$APPLICATION->SetPageProperty("description", "Магазин кондитерских ингредиентов и инвентаря Крафтология, доставка по России");
$APPLICATION->SetTitle("Доставка по городу Пермь");
?><h3>Если Вы живете в Перми или будете проездом, можно:</h3>
<div class="tabs">
	<div class="tabs-tabs">
		<div class="tabs-tab tabs-tab">
			 Забрать заказ самостоятельно
		</div>
		<div class="tabs-tab tabs-tab">
			 Получить с доставкой на дом<br>
		</div>
	</div>
	<div class="tabs-panes">
		<div class="tabs-pane">
			 Самовывоз в Перми: &nbsp;<b> ул. Екатерининская 28 (1 этаж) Магазин "Суперкондитер"</b>
			<p>
			</p>
			<ul>
				<li>Мы отправляем Ваш заказ в магазин и собираем его<br>
 </li>
				<li>После сборки&nbsp;звоним Вам по указанному телефону</li>
				<li>Если в течение дня не дозвонимся - пришлем СМС</li>
			</ul>
			<p>
			</p>
			<p>
 <b>Режим работы:</b>
			</p>
			<p>
			</p>
			<ul>
				<li><i>ПН - ПТ с 10-00 до 19-00</i><br>
 </li>
				<li><i>СБ с 11-00 до 17-00</i><br>
 </li>
				<li><i>ВС - выходной</i><br>
 </li>
			</ul>
			 <script src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script>&nbsp;<script type="text/javascript">// <![CDATA[
new DGWidgetLoader({"width":320,"height":300,"borderColor":"#a3a3a3","pos":{"lat":58.010979000000006,"lon":56.254393000000015,"zoom":16},"opt":{"city":"perm"},"org":[{"id":"2252328095459938"}]});
// ]]></script>
		</div>
		<div class="tabs-pane">
 <b>Доставка по Перми</b><br>
 <br>
			<ul>
				<li>Заявки на доставку сегодняшним днем принимаются до 12.00.</li>
				<li>Если заявка оформлена после 12.00 - доставляется на следующий день.&nbsp;</li>
				<li>Если заявка оформлена после 12.00 но нужна доставка срочно, а также если Вам нужно получить заказ с требованием коридора доставки (к примеру: в 12.00), то такие доставки считаются&nbsp;срочными.<br>
 </li>
				<li>Срочная доставка оплачивается сверх&nbsp;обычного тарифа 50-100 руб, в зависимости от района.<br>
 </li>
				<li>Обратите внимание: <i><b>Время доставки можно обговорить отдельно по телефону.</b></i>&nbsp;</li>
			</ul>
 <br>
			 <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A874c069e79fe5aa788cbda417e01b52f184c9f4c4d6c2812b832c87997ff9d30&amp;width=800&amp;height=600&amp;lang=ru_RU&amp;scroll=true"></script>
		</div>
	</div>
</div>
 <br>

 <script>
var show;
 function hidetxt(type){ param=document.getElementById(type); 
if(param.style.display == "none") { if(show) show.style.display = "none";
 param.style.display = "block"; 
show = param; 
}else param.style.display = "none" 
}
</script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>