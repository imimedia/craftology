<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");

$filterFavorite = array();
if ($USER->IsAuthorized()) {
	$user = CUser::GetList($by, $order, array('ID' => $USER->GetID()), array('SELECT' => array('UF_FAVORITE_PRODUCTS')))->Fetch();
	$filterFavorite['ID'] = $user['UF_FAVORITE_PRODUCTS'];
} else {
	$filterFavorite['ID'] = $APPLICATION->get_cookie('FAVORITE_PRODUCTS') ? array_filter(array_map(function ($v) {
		return intval($v) > 0 ? intval($v) : null;
	}, explode(',', $APPLICATION->get_cookie('FAVORITE_PRODUCTS')))) : array();
}
?>

<?$APPLICATION->IncludeComponent("bitrix:menu", "personal", Array(
	"ROOT_MENU_TYPE" => "personal",	// Тип меню для первого уровня
	"MENU_CACHE_TYPE" => "A",	// Тип кеширования
	"MENU_CACHE_TIME" => "86400",	// Время кеширования (сек.)
	"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
	"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
	"MAX_LEVEL" => "1",	// Уровень вложенности меню
	"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
	"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	"DELAY" => "N",	// Откладывать выполнение шаблона меню
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно,
),
	false
);?>
	<div class="page-content-favorite">
		<div class="catalog__meta">
			<div class="count">
				<span class="count__label">Показать:</span>
				<?
				$countItems = array(
					'60' => array('title' => '60', 'value' => 60),
					'120' => array('title' => '120', 'value' => 120),
					'all' => array('title' => 'все', 'value' => 999)
				);
				$countItem = $countItems['60'];
				if (isset($_REQUEST['count']) && in_array($_REQUEST['count'], array_keys($countItems))) {
					$countItem = $countItems[$_REQUEST['count']];
					$USER->SetParam('CATALOG_COUNT_ITEMS', $_REQUEST['count']);
				}
				else if ($USER->GetParam('CATALOG_COUNT_ITEMS') && in_array($USER->GetParam('CATALOG_COUNT_ITEMS'), array_keys($countItems))) {
					$countItem = $countItems[$USER->GetParam('CATALOG_COUNT_ITEMS')];
				}

				$n = 0;
				foreach ($countItems as $countKey => $_cntItem) {
					echo ($_cntItem['value'] == $countItem['value'])
						? '<span>'.$_cntItem['title'].'</span>'
						: '<a href="'.$APPLICATION->GetCurPageParam('count='.$countKey, array('count')).'">'.$_cntItem['title'].'</a>';
					echo (count($countItems) > ++$n) ? ', ' : '';
				} ?>
			</div>
			<div class="sorter">
				<div class="sorter__label">Сортировка: </div>
				<div class="dropdown sorter__dropdown">
					<?
					$sortItems = array(
						'name' => array('title' => 'по названию', 'FIELD' => 'NAME', 'ORDER' => 'ASC'),
						'scu' => array('title' => 'по артикулу', 'FIELD' => 'PROPERTY_CML2_ARTICLE', 'ORDER' => 'ASC'),
						'priceup' => array('title' => 'по возрастанию цены', 'FIELD' => 'CATALOG_PRICE_'.CATALOG_PRICE, 'ORDER' => 'ASC,nulls'),
						'pricedown' => array('title' => 'по убыванию цены', 'FIELD' => 'CATALOG_PRICE_'.CATALOG_PRICE, 'ORDER' => 'DESC,nulls')
					);
					$sortName = 'name';
					$sortItem = $sortItems[$sortName];
					if (isset($_REQUEST['sort']) && in_array($_REQUEST['sort'], array_keys($sortItems))) {
						$sortName = $_REQUEST['sort'];
						$sortItem = $sortItems[$sortName];
						$USER->SetParam('CATALOG_SORT_ITEMS', $_REQUEST['sort']);
					}
					else if ($USER->GetParam('CATALOG_SORT_ITEMS') && in_array($USER->GetParam('CATALOG_SORT_ITEMS'), array_keys($sortItems))) {
						$sortName = $USER->GetParam('CATALOG_SORT_ITEMS');
						$sortItem = $sortItems[$sortName];
					}
					?>
					<span class="dropdown__trigger"><?= $sortItem['title'] ?></span>
					<ul class="dropdown__content">
						<? foreach ($sortItems as $sortKey => $_sortItem) {
							echo '<li><a href="'.$APPLICATION->GetCurPageParam('sort='.$sortKey, array('sort')).'">'.$_sortItem['title'].'</a></li>';
						} ?>
					</ul>
				</div>
			</div>
		</div>
		<? if ($filterFavorite['ID']) { ?>
			<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"main", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "86400",
		"CACHE_TYPE" => "A",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => $sortItem["FIELD"],
		"ELEMENT_SORT_ORDER" => $sortItem["ORDER"],
		"ELEMENT_SORT_FIELD2" => $sortItem2["FIELD"],
		"ELEMENT_SORT_ORDER2" => $sortItem2["ORDER"],
		"FILTER_NAME" => "filterFavorite",
		"HIDE_NOT_AVAILABLE" => "L",
		"IBLOCK_ID" => IBLOCK_CATALOG,
		"IBLOCK_TYPE" => "1c_catalog",
		"INCLUDE_SUBSECTIONS" => "A",
		"LINE_ELEMENT_COUNT" => "",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => $countItem["value"],
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "Цена продажи",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "NEW",
			2 => "HIT",
			3 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "Y",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "blue",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "main",
		"SEF_RULE" => "",
		"SECTION_CODE_PATH" => "",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"COMPATIBLE_MODE" => "Y"
	),
	false
);?>
		<? } ?>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>