<div class="lents">
		<?$APPLICATION->IncludeComponent(
			"bitrix:sale.bestsellers",
			"slider_small",
			array(
				"COMPONENT_TEMPLATE" => "slider_small",
				"HIDE_NOT_AVAILABLE" => "N",
				"SHOW_DISCOUNT_PERCENT" => "N",
				"PRODUCT_SUBSCRIPTION" => "N",
				"SHOW_NAME" => "N",
				"SHOW_IMAGE" => "N",
				"MESS_BTN_BUY" => "Купить",
				"MESS_BTN_DETAIL" => "Подробнее",
				"MESS_NOT_AVAILABLE" => "Нет в наличии",
				"MESS_BTN_SUBSCRIBE" => "Подписаться",
				"PAGE_ELEMENT_COUNT" => "7",
				"LINE_ELEMENT_COUNT" => "3",
				"TEMPLATE_THEME" => "blue",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "86400",
				"BY" => "AMOUNT",
				"PERIOD" => "0",
				"FILTER" => array(
					0 => "F",
				),
				"DISPLAY_COMPARE" => "N",
				"SHOW_OLD_PRICE" => "N",
				"PRICE_CODE" => array(
					0 => "Розничная",
				),
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" => "Y",
				"CONVERT_CURRENCY" => "N",
				"BASKET_URL" => "/personal/basket.php",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"ADD_PROPERTIES_TO_BASKET" => "Y",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"PARTIAL_PRODUCT_PROPERTIES" => "N",
				"USE_PRODUCT_QUANTITY" => "N",
				"SHOW_PRODUCTS_3" => "Y",
				"PROPERTY_CODE_3" => array(
					0 => "",
					1 => "",
				),
				"CART_PROPERTIES_3" => array(
					0 => "",
					1 => "",
				),
				"ADDITIONAL_PICT_PROP_3" => "MORE_PHOTO",
				"LABEL_PROP_3" => "-",
				"PROPERTY_CODE_4" => array(
					0 => "",
					1 => "",
				),
				"CART_PROPERTIES_4" => array(
					0 => "",
					1 => "",
				),
				"ADDITIONAL_PICT_PROP_4" => "MORE_PHOTO",
				"OFFER_TREE_PROPS_4" => array(
				),
				"TITLE" => "Популярные товары"
			),
			false
		);?>
</div>
