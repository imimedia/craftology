<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Редактор");
?>Выберите интересующую Вас форму картинки: Прямоугольник(квадрат) или Круг(овал)&nbsp;

<br>
<!-- СКРИПТЫ ДЛЯ РЕДАКТОРА --> <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-
Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> <script>
$('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'redactor.css?v=2017-12-10-1') );
</script> <!-- // --> <section style="padding: 0 20px 20px 20px">
<h1 style="text-align: center;"></h1>
 <!-- РЕДАКТОР -->
<div class="row" id="typeSelectors">
	<div class="col-sm-6" style="text-align: center !important">
 <img alt="Напечатать квадратное изображение" src="#" class="typeImg make-it-slow box" data-type="square">
	</div>
	<div class="col-sm-6" style="text-align: center !important">
 <img alt="Напечатать круглое изображение" src="#" class="typeImg make-it-slow box" data-type="circle">
	</div>
</div>
<div id="desktop" style="display: none" class="row">
	<div id="editorWrap" class="col-sm-8" style="text-align: center; margin-top: 15px; width: 740px; height: 580px;">
		<div id="instruments" class="row" style="width: 720px; margin: 0 auto;">
			<div class="col-sm-6">
				<div class="btn btn-link" onclick="location.reload();">
                                      &#8592; назад
				</div>
				<div class="fileUpload btn btn-link">
					 другая картинка <input id="upload" accept="image/*" type="file" class="upload">
				</div>
				<div class="btn-group">
					 <a class="btn btn-link vanilla-rotate" data-deg="-90" >
                                    <img alt="-90" src="/redactorImages/rotateLeft.svg" style="width: 16px;" data-bx-orig-src="/redactorImages/rotateLeft.svg">
                                </a> <a class="btn btn-link vanilla-rotate" data-deg="90" >
                                    <img alt="+90" src="/redactorImages/rotateRight.svg" style="width: 16px" data-bx-orig-src="/redactorImages/rotateRight.svg">
                                </a>
				</div>
			</div>
			<div class="col-sm-6">
 <span style="top: 2px; position: relative; right: 5px">
Область печати <span id="widthSpan"></span>x<span id="heightSpan"></span> см
</span> 
<button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-example-modal-sm">изменить</button>
			</div>
			<div class="col-sm-4">
			</div>
		</div>
		<div class="demo-wrap upload-demo">
			<div class="upload-msg">
				 <a onclick="$('#upload').click()" style="color: #337ab7; text-decoration: underline; cursor:pointer">Загрузите изображение и выберите область печати</a>
			</div>
			<div class="upload-demo-wrap">
				<div id="a4wrapper">
					<div id="upload-demo">
					</div>
				</div>
			</div>
		</div>
	</div>
 <br>
	<div id="buttons" class="col-sm-4" style="padding-top: 20px; width: 100%; margin: 0 auto">
		<div class="row">
			<div class="col-sm-6">
				<div id="materials">
 <label class="control-label" style="font-weight: normal">Материал для печати</label> <br>
					<div style="clear: both;">
 <input type="radio" name="radioMaterial" value="Вафельная бумага" id="radioWafer" class="radio" checked=""> <label class="radioLabel" for="radioWafer" style="height: 34px; width:300px;">Вафельная бумага, <span style="font-size:9pt">180 рублей</span></label>
					</div>
					<div style="clear: both;">
 <input type="radio" name="radioMaterial" value="Сахарная бумага" id="radioChocolate" class="radio"> <label class="radioLabel" for="radioChocolate" style="height: 34px; width:300px;">Сахарная бумага, <span style="font-size:9pt">320 рублей</span></label>
					</div>
					<div style="clear: both;">
 <input type="radio" name="radioMaterial" value="Шокотрансфер" id="radioShokotransfer" class="radio"> <label class="radioLabel" for="radioShokotransfer" style="height: 34px; width:300px;">Шокотрансфер, <span style="font-size:9pt">320 рублей</span></label>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
 <label class="control-label" style="font-weight: normal">Ваши данные</label>
				<form data-toggle="validator" id="contactsForm" role="form">
					<div class="form-group row" style="margin-top: 20px">
 <label for="example-text-input" class="col-sm-4" style="font-weight: normal">Ваше имя</label>
						<div class="col-sm-8">
 <input class="form-control" required="" type="text" value="" id="name-input">
						</div>
					</div>
					<div class="form-group row">
 <label for="example-text-input" class="col-sm-4" style="font-weight: normal">E-mail</label>
						<div class="col-sm-8">
 <input class="form-control" required="" type="email" id="email-input">
						</div>
					</div>
					<div class="form-group row">
 <label for="example-text-input" class="col-sm-4" style="font-weight: normal">Телефон</label>
						<div class="col-sm-8">
 <input class="form-control" required="" type="text" id="phone-input">
						</div>
					</div>
					<div class="form-group row">
 <label for="example-text-input" class="col-sm-4" style="font-weight: normal">Город</label>
						<div class="col-sm-8">
 <input class="form-control" required="" type="text" id="city-input">
						</div>
					</div>
					<div class="form-group row">
                                    <label for="example-text-input" class="col-sm-4" style="font-weight: normal">Комментарий</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" id="comment-input"></textarea>
                                    </div>
                                </div>
					<div style="clear: both; margin-top: 30px;">
 <button class="upload-result btn btn-success">Посмотреть результат</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
 </section>
<div id="modal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
 <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
				<h3>Выбор размера печати</h3>
			</div>
			<div class="modal-body">
				<div style="padding: 20px;">
					<div style="padding: 5px; text-align: center">
 <input type="number" id="widthInput" class="form-control" min="8" max="28" value="28" style="width: 60px; display: inline"> X <input type="number" id="heightInput" class="form-control" min="8" max="20" value="20" style="width: 60px; display: inline"> см
					</div>
					<div style="text-align: center; margin-top: 7px; color: gray">
						 * от 8x8 см до 28x20 см
					</div>
				</div>
			</div>
			<div class="modal-footer">
 <button type="button" class="btn btn-primary" onclick="onSelectNewSizes()">Выбрать размер</button>
			</div>
		</div>
	</div>
</div>
        <script src="redactor.js?v=2017-12-10-5"></script>
<div id="overlayModal">
            <span id="sendmessage">Подождите, идет отправка макета...</span>
            <br />
            <br />
            <img src="redactorImages/spinner.svg?v=2017-07-07-5" alt="идет отправка..." />
        </div>
<!-- // --><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>