<? 
	$_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www/';
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	
	define('NO_KEEP_STATISTIC', true);
	define('NOT_CHECK_PERMISSIONS', true);
	
	require($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/main/include/prolog_before.php');
	set_time_limit(0);


/* для создания агента, скопируйте функцию getMsStatus() в init.php и создайте агента на стороне Битрикс http://joxi.ru/Vrwq8BouOzv6a2

function getMsStatus()
{

    CModule::IncludeModule('gwi.msstatusimport');
    $objStatus = new GWIMSStatusImport();

    $arLogs = array();
    $arLogs['date'] = date("Y-m-d H:i:s");

    if ($arrOrders = $objStatus->getOrderMCChange()) {

        $arLogs['arrOrders'] = $arrOrders;

        if ($arIDOrderForUpdate = $objStatus->getBXOrderByID(array_keys($arrOrders))) {
            $arrayOrderUpdate = array();
            foreach ($arIDOrderForUpdate as $keyOrder => $aItem) {
                if ($arrOrders[$aItem['ID']]) {
                    if ($sIDHelper = $objStatus->getIdStatus($arrOrders[$aItem["ID"]]["STATUS_NAME"], '[', ']', true)) {
                        if ($aItem['STATUS_ID'] != $sIDHelper) {
                            $arrayOrderUpdate[] = array("ORDER_ID" => $aItem['ID'], "STATUS_ID" => $sIDHelper);
                        } else {
                            $arLogs['status MC == status BX'][] = 'order id ' . $aItem["ID"] . ' status BX ' . $aItem['STATUS_ID'] . ' == status MC ' . $sIDHelper;
                        }
                    } else {
                        $arLogs['status MC incorrect, must be [P]name_status'][] = $aItem["ID"];
                    }
                }
            }

            $arLogs['arrayOrderUpdate'] = $arrayOrderUpdate;
            if (!empty($arrayOrderUpdate)) {
                $objStatus->saveStatusImport2($arrayOrderUpdate);
            } else {
                $arLogs['saveStatusImport2'] = 'not order update';
            }
        } else {
            $arLogs['getBXOrderByID'] = 'not found order BX';
        }
    } else {
        $arLogs['Logs'] = 'orders not found MC, change time interval on setting modules';
    }

    $arLogs['Logs'] = $objStatus->errorsLogs;
    $arLogs['writeErrorsLog'] = $objStatus->writeErrorsLog;
    $arLogs['arResult'] = $objStatus->arResult;

    //echo '<pre>'.print_r($arLogs, 1).'</pre>';

    if ($objStatus->writeErrorsLog == 'Y') {
        $objStatus->writelogs($arLogs, 'arLogsAll');
    }

    return "getMsStatus();";
}
*/

CModule::IncludeModule('gwi.msstatusimport');
$objStatus = new GWIMSStatusImport();

$arLogs = array();
$arLogs['date'] = date("Y-m-d H:i:s");

if ($arrOrders = $objStatus->getOrderMCChange()) {

    $arLogs['arrOrders'] = $arrOrders;

    if ($arIDOrderForUpdate = $objStatus->getBXOrderByID(array_keys($arrOrders))) {
        $arrayOrderUpdate = array();
        foreach ($arIDOrderForUpdate as $keyOrder => $aItem) {
            if ($arrOrders[$aItem['ID']]) {
                if ($sIDHelper = $objStatus->getIdStatus($arrOrders[$aItem["ID"]]["STATUS_NAME"], '[', ']', true)) {
                    if ($aItem['STATUS_ID'] != $sIDHelper) {
                        $arrayOrderUpdate[] = array("ORDER_ID" => $aItem['ID'], "STATUS_ID" => $sIDHelper);
                    } else {
                        $arLogs['status MC == status BX'][] = 'order id ' . $aItem["ID"] . ' status BX ' . $aItem['STATUS_ID'] . ' == status MC ' . $sIDHelper;
                    }
                } else {
                    $arLogs['status MC incorrect, must be [P]name_status'][] = $aItem["ID"];
                }
            }
        }

        $arLogs['arrayOrderUpdate'] = $arrayOrderUpdate;
        if (!empty($arrayOrderUpdate)) {
            $objStatus->saveStatusImport2($arrayOrderUpdate);
        } else {
            $arLogs['saveStatusImport2'] = 'not order update';
        }
    } else {
        $arLogs['getBXOrderByID'] = 'not found order BX';
    }
} else {
    $arLogs['Logs'] = 'orders not found MC, change time interval on setting modules';
}

$arLogs['Logs'] = $objStatus->errorsLogs;
$arLogs['writeErrorsLog'] = $objStatus->writeErrorsLog;
$arLogs['arResult'] = $objStatus->arResult;

//echo '<pre>'.print_r($arLogs, 1).'</pre>';

if ($objStatus->writeErrorsLog == 'Y') {
    $objStatus->writelogs($arLogs, 'arLogsAll');
}
?>