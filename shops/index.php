<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "все для кондитера, товары для кондитера, магазин для кондитера, магазин кондитерских принадлежностей, кондитерский интернет-магазин");
$APPLICATION->SetPageProperty("description", "Магазин кондитерских ингредиентов и инвентаря Крафтология, доставка по России");
$APPLICATION->SetTitle("Наши магазины");
?><p style="color: #4d002e;">
 <span style="font-weight: 700;">НАШИ РОЗНИЧНЫЕ МАГАЗИНЫ:</span>
</p>
<p style="color: #4d002e;">
 <span style="font-weight: 700;"><br>
 </span>
</p>
<p style="color: #4d002e;">
 <span style="font-weight: 700;">1. Москва</span><span style="font-weight: 700;">, Щелковское шоссе, 3 ст.1&nbsp; (</span><b>ТЦ "Город Хобби", 2 этаж)&nbsp;</b>
</p>
<p style="color: #4d002e;">
	 тел. 8 (800) 500-30-47
</p>
<p style="color: #4d002e;">
	 тел. 8 (499) 110-61-40
</p>
<p style="color: #4d002e;">
	 График работы с 11-00 до 20-00 без выходных
</p>
<p style="color: #4d002e;">
 <br>
</p>
<p style="color: #4d002e;">
 <span style="font-weight: 700;">2. Пермь</span><span style="font-weight: 700;">,</span><span style="font-weight: 700;">&nbsp;ул. Екатерининская 28 (1 этаж)</span>
</p>
<p style="color: #4d002e;">
	 тел. 8-800-500-30-47
</p>
<p style="color: #4d002e;">
	 График работы:<br>
</p>
<p style="color: #4d002e;">
	 Понедельник - пятница 10-00 - 19-00
</p>
<p style="color: #4d002e;">
	 Суббота 11-00 - 17-00
</p>
<p style="color: #4d002e;">
	 Воскресенье - выходной
</p>
<p style="color: #4d002e;">
 <br>
</p>
<p style="color: #4d002e;">
 <span style="font-weight: 700;">В ДРУГИЕ ГОРОДА РОССИИ ВЫСЫЛАЕМ ЗАКАЗЫ ПОЧТОЙ И ТК.</span>
</p>
<p style="color: #4d002e;">
 <span style="font-weight: 700;">Заказы принимаем на нашем сайте.</span>
</p>
 <br>
 <br>
 <img width="864" alt="Магазин Крафтология фото" src="/upload/medialibrary/925/925e4021f87355ec7b72349cf97cd007.jpg" height="471" title="Магазин Крафтология фото" align="middle"><br>
 <img width="864" alt="Фотография магазин Крафтология" src="/upload/medialibrary/ef1/ef1d1cbed2681118ef0e2575b4c55612.jpg" height="517" title="Фотография магазин Крафтология" align="middle"><br>
 <img width="864" alt="Магазин Крафтология Пермь" src="/upload/medialibrary/1a6/1a6e1ebbdc932787de3fb3ef437651b4.jpg" height="452" title="Магазин Крафтология Пермь"><br>
 <img width="864" alt="Все для кондитера в Крафтологии" src="/upload/medialibrary/d99/d99a77135243fe6763f5a0838ea4ea2b.jpg" height="441" title="Все для кондитера в Крафтологии"><br>
 <img width="864" alt="Витрины магазина Крафтология" src="/upload/medialibrary/bc9/bc9853fb0174950dd0206037b86f614c.jpg" height="647" title="Витрины магазина Крафтология"><br>
 <img width="864" alt="Магазин для кондитера Пермь" src="/upload/medialibrary/7e3/7e3b48ff09166091c3745378cf48dd1b.jpg" height="681" title="Магазин для кондитера Пермь"><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>