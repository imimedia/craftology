<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Скидки от 1500 рублей");
?>Теперь, чтобы получить скидку на&nbsp;нашем сайте&nbsp;необязательно получать дисконтную карту. <br>
<br>
Достаточно оформить заказ от 1500 рублей без учета доставки.
<p>
	 Размер скидки сможете посмотреть в корзине. После того, как положите товаров в корзину на 1500 рублей.
</p>
<h2>Скидки в зависимости от суммы заказа</h2>
<h3>От 1500 до 2499 рублей — 1%</h3>
 <figure>
<p style="text-align: center;">
 <img src="https://pp.userapi.com/c840322/v840322343/5f397/nPSf4hi4t-A.jpg" style="width: 800px;">
</p>
 <br>
 <br>
 <br>
 </figure>
<h3>От 2500 рублей до 3499 рублей — 2%</h3>
 <figure>
<p style="text-align: center;">
 <img src="https://pp.userapi.com/c840322/v840322343/5f3a1/k83mDJkXCoc.jpg" style="width: 800px;">
</p>
 <br>
 <br>
 <br>
 </figure>
<h3>От 3500 — 3%</h3>
 <figure>
<p style="text-align: center;">
 <img src="https://pp.userapi.com/c840322/v840322343/5f3b4/-_VI-Mlu-P8.jpg" style="width: 800px;">
</p>
 <br>
 <br>
 <br>
 </figure>
<h2>Важно!</h2>
<h3>1. Скидки и акции не суммируются;</h3>
<h3>2. Если у Вас есть дисконтная карта на 5% или 10% — действует только скидка по дисконтной карте;</h3>
<h3>3. На мелкооптовые позиции посыпок, шоколада, упаковки скидки не действуют.</h3><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>