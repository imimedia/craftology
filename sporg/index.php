<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "все для кондитера, товары для кондитера, магазин для кондитера, магазин кондитерских принадлежностей, кондитерский интернет-магазин");
$APPLICATION->SetPageProperty("description", "Магазин кондитерских ингредиентов и инвентаря Крафтология, доставка по России");
$APPLICATION->SetTitle("Организаторам совместных закупок");
?><h2>
Специальное предложение для организаторов совместных покупок. </h2>
<p>
 <i>Внимание: условия действуют только для организаторов покупок, предоставивших ссылку на место сбора участников закупки. Это может быть ссылка на форум, группу "Вконтакте", любой другой сайт, страничку или сообщество.</i>
</p>
<h3> <b>Скидки:&nbsp;</b> </h3>
<p>
</p>
<ul>
	<li>от 10000 рублей - 5% (см. исключения)*</li>
	<li>от 25000 рублей - 7% (см. исключения)*</li>
	<li>от 50000 рублей - 10% (см. исключения)*</li>
</ul>
 <b>Для постоянных закупщиков</b>&nbsp;мы практикуем индивидуальный подход к скидкам, а также дополнительные бонусы.&nbsp;
<h3>Сайт добавлен в каталог Sliza<img width="150" alt="sliza_logo.png" src="/upload/medialibrary/985/9853958a0167f9693b2fb12fa992fab8.png" height="63" title="sliza_logo.png"></h3>
<div>
 <a href="https://sliza.ru/instruction.php">Как пользоваться Sliza - читайте в инструкции</a>
</div>
<h3><br>
 *<b> Внимание!</b> </h3>
<p>
	 Скидки <b>НЕ</b> РАСПРОСТРАНЯЮТСЯ НА:
</p>
<p>
	 1. Товары, на которые уже действуют скидки и акции;
</p>
<p>
	 2. Все виды товаров в оптовых упаковках (подложки и коробки по 100 шт., кондитерские мешки по 100 шт., посыпки по 1 кг и т.п.)
</p>
<p>
 <br>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>